//
//  PublicFuncs.h
//  Mag Client iOS
//
//  Created by ivy on 13/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PublicFuncs : NSObject

+ (long)getCurrentTimestampNanoSecond;
+ (NSArray *)getRotationMatrixGravity:(NSArray *)gravity mag:(NSArray *)geomagnetic;
+ (NSArray *)getOrientation:(NSArray *)R;
+ (float)normalize:(float)degree;

@end
