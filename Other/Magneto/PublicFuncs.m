//
//  PublicFuncs.m
//  Mag Client iOS
//
//  Created by ivy on 13/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "PublicFuncs.h"

@implementation PublicFuncs

+ (long)getCurrentTimestampNanoSecond {
    long ret = [[NSDate date] timeIntervalSince1970] * 1000000000;
    return ret;
}

+ (NSArray *)getRotationMatrixGravity:(NSArray *)gravity mag:(NSArray *)geomagnetic {
    float Ax = [gravity[0] doubleValue];
    float Ay = [gravity[1] doubleValue];
    float Az = [gravity[2] doubleValue];
    
    float normsqA = (Ax*Ax + Ay*Ay + Az*Az);
    float g = 9.81f;
    float freeFallGravitySquared = 0.01f * g * g;
    if (normsqA < freeFallGravitySquared) {
        // gravity less than 10% of normal values
        return nil;
    }
    
    float Ex = [geomagnetic[0] doubleValue];
    float Ey = [geomagnetic[1] doubleValue];
    float Ez = [geomagnetic[2] doubleValue];
    float Hx = Ey*Az - Ez*Ay;
    float Hy = Ez*Ax - Ex*Az;
    float Hz = Ex*Ay - Ey*Ax;
    float normH = (float)sqrt(Hx*Hx + Hy*Hy + Hz*Hz);
    
    if (normH < 0.1f) {
        // device is close to free fall or close to magnetic north pole
        return nil;
    }
    
    float invH = 1.0f / normH;
    Hx *= invH;
    Hy *= invH;
    Hz *= invH;
    float invA = 1.0f / (float)sqrt(Ax*Ax + Ay*Ay + Az*Az);
    Ax *= invA;
    Ay *= invA;
    Az *= invA;
    
    float Mx = Ay*Hz - Az*Hy;
    float My = Az*Hx - Ax*Hz;
    float Mz = Ax*Hy - Ay*Hx;
    
    return @[@(Hx), @(Hy), @(Hz),
             @(Mx), @(My), @(Mz),
             @(Ax), @(Ay), @(Az)];
}

+ (NSArray *)getOrientation:(NSArray *)R {
    return @[@((float)atan2([R[1] doubleValue], [R[4] doubleValue])), @((float)asin(-[R[7] doubleValue])), @((float)atan2(-[R[6] doubleValue], [R[8] doubleValue]))];
}

+ (float)normalize:(float)degree {
    while (degree >= 180) {
        degree -= 360;
    }
    while (degree < -180) {
        degree += 360;
    }
    return degree;
}

@end
