//
//  MagnetoHandler.h
//  Mag Client iOS
//
//  Created by ivy on 11/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

@interface MagnetoHandler : NSObject

@property MagDataSource sourcetype;

@end
