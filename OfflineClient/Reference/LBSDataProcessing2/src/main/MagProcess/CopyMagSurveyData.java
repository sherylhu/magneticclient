package main.MagProcess;

import Utils.Settings;
import Utils.DebugTool;
import Utils.FileUtils;
import Utils.InfoReader;

public class CopyMagSurveyData {
    private final static String tag = "CopyMagSurveyData";

    /**
     * Copy the survey folders (WifiData) into database.
     * If files/folders already exist in the destination, then it will replace the origin files/folders.
     */
    public static void run() {
        DebugTool.printLog(tag, "************************Run CopyWiFiSurveyData***********************");
        for (String floor : Settings.Site.allFloors) {
            String surveyFolder = InfoReader.floorsMapMagSurveyFolder.get(floor);
            String magRawFolderInFloor = Settings.Site.Mag.getMagRawFolder(floor);
            FileUtils.copyEntireFolder(surveyFolder, magRawFolderInFloor, true);
            DebugTool.printLog(tag, "Copy files from " + surveyFolder + " to " + magRawFolderInFloor);
        }

    }
}
