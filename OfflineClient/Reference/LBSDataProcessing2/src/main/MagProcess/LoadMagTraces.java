package main.MagProcess;

import Format.Position;
import Utils.Settings;
import Utils.TxtTool;

import java.io.File;
import java.util.*;

public class LoadMagTraces {
    private final static String tag = "LoadMagTraces";
    private static final String fpTraceSuffix = "_trace.txt";
    private static final String fpCoorSuffix = "_coor.txt";

    public static Map<String, List<List<MagData>>> run() {
        Map<String, List<List<MagData>>>answer = new HashMap<>();
        for (String floor : Settings.Site.allFloors) {
            try {
                Set<String> allIdentities = new HashSet<String>();
                for (String path : (new TxtTool(Settings.Site.magRaws.get(floor),
                        TxtTool.TxtMode.read)).getAllTxtPathsInFolder(true)) {
                    File file = new File(path);
                    String name = file.getName();

                    if (name.endsWith(fpTraceSuffix)) {
                        allIdentities.add(name.split(fpTraceSuffix)[0]);
                    } else if (name.endsWith(fpCoorSuffix)) {
                        allIdentities.add(name.split(fpCoorSuffix)[0]);
                    }
                }

                List<List<MagData>> magFpTraces = new ArrayList<>();
                for (String name : allIdentities) {
                    String tracePath = Settings.Site.magRaws.get(floor) + name + fpTraceSuffix;
                    String coorPath = Settings.Site.magRaws.get(floor) + name + fpCoorSuffix;

                    List<Position> allRecordPositions = new ArrayList<Position>();

                    System.out.println(name);


                    for (String line : (new TxtTool(coorPath, TxtTool.TxtMode.read)).getAllLinesInFile()) {
                        String[] cols = line.split(" ");
                        double x = Double.parseDouble(cols[0]);
                        double y = Double.parseDouble(cols[1]);
                        Position position = new Position(x, y);
                        allRecordPositions.add(position);

                        System.out.println(x + " " + y);
                    }

                    List<MagData> magFpTrace = genMagFpTraceByFileName(tracePath, allRecordPositions);
                    magFpTraces.add(magFpTrace);
                }
                answer.put(floor, magFpTraces);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return answer;
    }

    private static List<MagData> genMagFpTraceByFileName(String fileName, List<Position> allRecordPositions) throws Exception {
        List<MagData> allMagData = new ArrayList<>();

        for (String line : (new TxtTool(fileName, TxtTool.TxtMode.read)).getAllLinesInFile()) {
            MagData magData = new MagData();
            String[]cols = line.split(" ");
            magData.i = Double.parseDouble(cols[0]);
            magData.v = Double.parseDouble(cols[1]);
            magData.h = Double.parseDouble(cols[2]);
            magData.o = Double.parseDouble(cols[3]);
            magData.ts = Long.parseLong(cols[4]);

            allMagData.add(magData);
        }

        System.out.println(fileName);
        System.out.println(allRecordPositions.size());

        List<Double> allDifference = new ArrayList<Double>();
        List<Position> allPos = allRecordPositions;
        double totalDifference = 0;
        Position lastPosition = null;
        for (int i = 0; i < allPos.size() - 1; i++) {
            double curDifference = allPos.get(i).dist(allPos.get(i + 1));
            allDifference.add(curDifference);
            totalDifference += curDifference;
        }

        List<Double> allRates = new ArrayList<Double>();
        for (double diff : allDifference) {
            allRates.add(diff / totalDifference);
        }

        //check time difference
//        for (int i = 1; i < allMagData.size(); i++) {
//            long timeDiff = allMagData.get(i).ts - allMagData.get(i - 1).ts;
//            if (timeDiff <= 0 || timeDiff > 25000000L) { // 20ms
//                System.out.println("time difference is not normal: " + timeDiff
//                        + ", between line " + i + " and " + (i + 1));
//            }
//        }

        double totalTimeDiff = allMagData.get(allMagData.size() - 1).ts - allMagData.get(0).ts;

        for (MagData magData : allMagData) {
            double curTimeRate = (magData.ts - allMagData.get(0).ts) * 1.0 / totalTimeDiff;

            int index = 0;
            while (curTimeRate > allRates.get(index)) {
                curTimeRate -= allRates.get(index);
                index++;
            }
            Position beginPos = allPos.get(index);
            Position endPos = allPos.get(index + 1);
            double xDiff = endPos.getX() - beginPos.getX();
            double yDiff = endPos.getY() - beginPos.getY();
            double rate = curTimeRate * totalDifference / allDifference.get(index);
            Position curPos = new Position(beginPos.getX() + xDiff * rate, beginPos.getY() + yDiff * rate);
            magData.position = curPos;
        }

        return allMagData;
    }

}
