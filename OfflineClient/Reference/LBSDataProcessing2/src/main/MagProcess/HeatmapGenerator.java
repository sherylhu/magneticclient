package main.MagProcess;

import Format.Position;
import Utils.Settings;
import Utils.Constants;
import Utils.MathTool;

import java.util.*;

public class HeatmapGenerator {
    public static final double notParallalDegreeDifference = 5; //degree
    public static final double metersDistanceForInterpolation = 2;
    public static final int minInterpolationTimes = 5;

    public static List<MagData> run(List<Position>referencePoints, String floor, List<List<MagData>>allMagTraces) {
        List<Double> degreesNotParallal2FpTraces = getDegreesNotParallal2MagTraces(allMagTraces);

        List<MagData> answer = new ArrayList<>();
        for (Position candidatePos : referencePoints) {
            MagData magData = getMagDataOfCandidatePos(floor, candidatePos, degreesNotParallal2FpTraces, allMagTraces);
            if (magData!= null) {
                answer.add(magData);
            }
        }

        return answer;
    }

    private static List<Double> getDegreesNotParallal2MagTraces(List<List<MagData>>allMagTraces) {
        double beginDegree = -90;
        Random random = new Random();
        List<Double> answer = new ArrayList<Double>();
        while (true) {
            beginDegree += random.nextDouble() + Constants.eps;
            boolean fail = false;
            for (double degree = beginDegree; degree < beginDegree + 180; degree += notParallalDegreeDifference) {
                answer.add(degree);
                if (Math.abs(degree - 90) < Constants.eps) {
                    fail = true;
                    answer.clear();
                    break;
                }
                double slope = Math.tan(degree / 180 * Math.PI);
                for (List<MagData> magTrace : allMagTraces) {
                    Position beginPos = magTrace.get(0).position;
                    Position endPos = magTrace.get(magTrace.size() - 1).position;
                    double slope2 = MathTool.getSlope(beginPos, endPos);
                    if (!Double.isNaN(slope)) {
                        if (Math.abs(slope - slope2) < Constants.eps) {
                            fail = true;
                            answer.clear();
                            break;
                        }
                    }
                }
                if (fail) {
                    answer.clear();
                    break;
                }
            }
            if (!fail) {
                return answer;
            }
        }
    }

    private static MagData getMagDataOfCandidatePos(String floor,
            Position candidatePos, List<Double>degreesNotParallal2FpTraces, List<List<MagData>>allMagTraces) {
        double pixelsDistanceForInterpolation = metersDistanceForInterpolation * Settings.Site.floorsMapScales.get(floor);

        MagData magDataOfCandidateOnFpTrace = getMagDataOfPosOnFpTraces(candidatePos, allMagTraces);
        if (magDataOfCandidateOnFpTrace != null) {
            return magDataOfCandidateOnFpTrace;
        }

        List<MagData> resultDataInAllDirections = new ArrayList<MagData>();
        for (double degree : degreesNotParallal2FpTraces) {
            MagData leftClosestMagData = null, rightClosestMagData = null;
            double leftClosestDistance = Double.MAX_VALUE, rightClosestDistance = Double.MAX_VALUE;
            // slope cannot be NaN
            double slope = Math.tan(degree / 180 * Math.PI);

            for (List<MagData> magFpTrace : allMagTraces) {
                List<Position> allPos = new ArrayList<Position>();
                for (MagData magData : magFpTrace) {
                    allPos.add(magData.position);
                }
                List<Integer> allTurningPoints = MathTool.getTurningPoint(allPos);
                allTurningPoints.add(0);
                allTurningPoints.add(allPos.size() - 1);
                for (int turningPointIndex = 0; turningPointIndex < allTurningPoints.size() - 1; turningPointIndex++) {
                    int beginPosIndex = allTurningPoints.get(turningPointIndex);
                    int endPosIndex = allTurningPoints.get(turningPointIndex + 1);
                    Position beginPos = magFpTrace.get(beginPosIndex).position;
                    Position endPos = magFpTrace.get(endPosIndex).position;
                    boolean beginPosAboveLine = MathTool.posAccording2Line(beginPos, candidatePos, slope) > 0;
                    boolean endPosAboveLine = MathTool.posAccording2Line(endPos, candidatePos, slope) > 0;
                    if ((beginPosAboveLine && !endPosAboveLine) || (endPosAboveLine && !beginPosAboveLine)) {
                        // get intersection point
                        //slope2 can be NaN
                        double slope2 = MathTool.getSlope(beginPos, endPos);
                        double interX, interY;
                        if (!Double.isNaN(slope2)) {
                            interX = (-beginPos.getX() * slope2 + beginPos.getY() + candidatePos.getX() * slope
                                    - candidatePos.getY()) / (slope - slope2);
                        } else {
                            interX = beginPos.getX();
                        }
                        interY = slope * (interX - candidatePos.getX()) + candidatePos.getY();
                        Position intersectionPos = new Position(interX, interY);
                        double dist2CandiatePoint = candidatePos.dist(intersectionPos);
                        if (dist2CandiatePoint > pixelsDistanceForInterpolation) {
                            continue;
                        }
                        if (candidatePos.getX() < interX) {
                            // intersection point is on the right of candidate point
                            if (dist2CandiatePoint < rightClosestDistance) {
                                rightClosestDistance = dist2CandiatePoint;
                                rightClosestMagData = getMagDataOfPosOnFpTrace(intersectionPos, magFpTrace,
                                        beginPosIndex, endPosIndex);
                            }
                        } else if (candidatePos.getX() > interX) {
                            // intersection point is on the left of candidate point
                            if (dist2CandiatePoint < leftClosestDistance) {
                                leftClosestDistance = dist2CandiatePoint;
                                leftClosestMagData = getMagDataOfPosOnFpTrace(intersectionPos, magFpTrace,
                                        beginPosIndex, endPosIndex);
                            }
                        }
                    }
                }
            }
            if (leftClosestMagData != null && rightClosestMagData != null) {
                resultDataInAllDirections
                        .add(getMagDataByInterpolation(candidatePos, leftClosestMagData, rightClosestMagData));
            }
        }

        if (resultDataInAllDirections.size() < minInterpolationTimes) {
            return null;
        } else {
            return getMeanMagData(resultDataInAllDirections);
        }
    }

    private static MagData getMagDataOfPosOnFpTraces(Position candidatePos, List<List<MagData>>allMagTraces) {
        for (List<MagData> magTrace : allMagTraces) {
            List<Position> allPos = new ArrayList<Position>();
            for (MagData magData : magTrace) {
                allPos.add(magData.position);
            }
            List<Integer> allTurningPoints = MathTool.getTurningPoint(allPos);
            allTurningPoints.add(0);
            allTurningPoints.add(allPos.size() - 1);
            for (int turningPointIndex = 0; turningPointIndex < allTurningPoints.size() - 1; turningPointIndex++) {
                int beginPosIndex = allTurningPoints.get(turningPointIndex);
                int endPosIndex = allTurningPoints.get(turningPointIndex + 1);
                Position beginPos = magTrace.get(beginPosIndex).position;
                Position endPos = magTrace.get(endPosIndex).position;
                if (MathTool.posOnSegment2(candidatePos, beginPos, endPos)) {
                    return getMagDataOfPosOnFpTrace(candidatePos, magTrace, beginPosIndex, endPosIndex);
                }
            }
        }
        return null;
    }

    private static MagData getMagDataOfPosOnFpTrace(Position pos, List<MagData> magFpTrace, int beginPosIndex, int endPosIndex) {
        MagData lastData = null;
        for (int i = beginPosIndex; i <= endPosIndex; i++) {
            MagData magData = magFpTrace.get(i);
            if (magData.position.dist(pos) < Constants.eps) {
                return magData;
            } else {
                if (lastData != null) {
                    if (MathTool.posOnSegment2(pos, lastData.position, magData.position)) {
                        return getMagDataByInterpolation(pos, lastData, magData);
                    }
                }
                lastData = magData;
            }
        }
        return null;
    }

    private static MagData getMagDataByInterpolation(Position pos, MagData beginData, MagData endData) {
        double rate = pos.dist(beginData.position)
                / endData.position.dist(beginData.position);

        double intensity = beginData.i + rate * (endData.i - beginData.i);
        double vertical = beginData.v + rate * (endData.v - beginData.v);
        double horizontal = beginData.h + rate * (endData.h - beginData.h);
        long timestamp = (long) (beginData.ts + rate * (endData.ts - beginData.ts));

        double orientation;
        if (Math.abs(endData.o - beginData.o) < 180) {
            orientation = beginData.o + rate * (endData.o - beginData.o);
        } else {
            double begin = beginData.o;
            double end = endData.o;
            if (begin < end) {
                begin += 360;
            } else {
                end += 360;
            }
            orientation = begin + rate * (end - begin);
            while (orientation > 180) {
                orientation -= 360;
            }
            while (orientation < -180) {
                orientation += 360;
            }
        }

        MagData magData = new MagData(intensity, vertical, horizontal, orientation, timestamp);
        magData.position = pos;
        return magData;
    }

    private static MagData getMeanMagData(List<MagData>allData) {
        double allIntensity = 0;
        double allVertical = 0;
        double allHorizontal = 0;
        double allOrientationX = 0;
        double allOrientationY = 0;
        long allTimestamp = 0;
        for (MagData magData : allData) {
            allIntensity += magData.i;
            allVertical += magData.v;
            allHorizontal += magData.h;
            allOrientationX += Math.cos(magData.o / 180 * Math.PI);
            allOrientationY += Math.sin(magData.o / 180 * Math.PI);
            allTimestamp += magData.ts;
        }
        allIntensity /= allData.size();
        allVertical /= allData.size();
        allHorizontal /= allData.size();
        allOrientationX /= allData.size();
        allOrientationY /= allData.size();
        allTimestamp = (long)(Math.round(allTimestamp * 1.0 / allData.size()));
        MagData magData =  new MagData(allIntensity, allVertical, allHorizontal, Math.atan2(allOrientationY, allOrientationX) / Math.PI * 180, allTimestamp);
        magData.position = allData.get(0).position;
        return magData;
    }
}
