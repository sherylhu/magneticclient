package main.MagProcess;

import Format.Position;

public class MagData {
    public double i, v, h, o;
    public long ts;
    public Position position;

    public MagData() {
    }

    public MagData(double i, double v, double h, double o, long ts) {
        this.i = i;
        this.v = v;
        this.h = h;
        this.o = o;
        this.ts = ts;
    }
}
