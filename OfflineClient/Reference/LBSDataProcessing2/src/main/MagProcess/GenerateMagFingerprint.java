package main.MagProcess;

import Format.Position;
import Utils.*;
import main.ReferencePointGenerator;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenerateMagFingerprint {
    private final static String tag = "GenerateMagFingerprint";

    public static void run() {
        DebugTool.printLog(tag, "**********************Running GenerateMagFingerprint************************");

        Map<String, List<List<MagData>>>floorMapAllMagTraces = LoadMagTraces.run();

        Map<String, List<MagData>>floorMapHeatmap = new HashMap<>();
        for (String floor : Settings.Site.allFloors) {
            DebugTool.printLog(tag, "In " + floor + ":");
            List<List<Position>> allOutConstraints = getAllConstraints(Settings.Site.Constraint.getAllOutConstraintPaths(floor));
            if (allOutConstraints.isEmpty()) {
                DebugTool.printLog(tag, "You should add at least one out_constraint file ");
                return;
            }
            List<List<Position>> allInConstraints = getAllConstraints(Settings.Site.Constraint.getAllInConstraintPaths(floor));

            DebugTool.printLog(tag, "generate referencePoints");
            List<Position> referencePoints = ReferencePointGenerator.run(InfoReader.meterDistBetweenMagRfPts,
                    Settings.Site.floorsMapScales.get(floor), allOutConstraints, allInConstraints);
            DebugTool.printLog(tag, "generated " + referencePoints.size() + " referencePoints");

            List<MagData> heatmapInFloor = HeatmapGenerator.run(referencePoints, floor, floorMapAllMagTraces.get(floor));

            floorMapHeatmap.put(floor, heatmapInFloor);
        }

        try {
            Map<String, List<List<Integer>>> floorMapNeighbor = detectNeighbor(floorMapHeatmap);
            writeNeighborMap(floorMapNeighbor);
            drawNeighborConnection(floorMapHeatmap, floorMapNeighbor);
            writeCombinedHeatmap(floorMapHeatmap);
            drawHeatmap(floorMapHeatmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<List<Position>> getAllConstraints(List<String> allConstraintPaths) {
        List<List<Position>> answer = new ArrayList<List<Position>>();
        for (String path : allConstraintPaths) {
            try {
                List<Position> constraint = new ArrayList<Position>();
                for (String line : (new TxtTool(path, TxtTool.TxtMode.read)).getAllLinesInFile()) {
                    String[] cols = line.split(" ");
                    double x = Double.parseDouble(cols[0]);
                    double y = Double.parseDouble(cols[1]);
                    Position position = new Position(x, y);
                    constraint.add(position);
                }
                answer.add(constraint);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return answer;
    }

    private static Map<String, List<List<Integer>>> detectNeighbor(
            Map<String, List<MagData>>floorMapHeatmap) {
        Map<String, List<List<Integer>>> answer = new HashMap<>();
        for (Map.Entry<String, List<MagData>> entry : floorMapHeatmap.entrySet()) {

            String floor = entry.getKey();
            List<MagData>heatmap = entry.getValue();

            double scale = Settings.Site.floorsMapScales.get(floor);
            double pixelsDistBetweenRfPoint = InfoReader.meterDistBetweenMagRfPts * scale;
            double pixelsDistOfMagNeighboor = pixelsDistBetweenRfPoint + Constants.eps;

            List<List<Integer>>magNeighborMap = new ArrayList<>();
            for (int i = 0; i < heatmap.size(); i++) {
                MagData magData = heatmap.get(i);
                List<Integer> neighbor = new ArrayList<>();
                for (int j = 0; j < heatmap.size(); j++) {
                    if (magData.position.dist(heatmap.get(j).position) < pixelsDistOfMagNeighboor) {
                        neighbor.add(j);
                    }
                }
                magNeighborMap.add(neighbor);
            }
            answer.put(floor, magNeighborMap);
        }
        return answer;
    }

    private static void writeNeighborMap(Map<String, List<List<Integer>>> floorMapNeighbor) throws Exception {
        List<List<Integer>>totalNeighbor = new ArrayList<>();
        for (Map.Entry<String, List<List<Integer>>>entry : floorMapNeighbor.entrySet()) {
            List<List<Integer>>curNeighbor = entry.getValue();
            List<List<Integer>>newNeighbor = new ArrayList<>();
            for (List<Integer>line : curNeighbor) {
                List<Integer>temp = new ArrayList<>();
                for (int t : line) {
                    temp.add(t + totalNeighbor.size());
                }
                newNeighbor.add(temp);
            }
            totalNeighbor.addAll(newNeighbor);
        }

        List<String>lines2Write = new ArrayList<>();
        for (List<Integer>neighbors : totalNeighbor) {
            StringBuffer line = new StringBuffer();
            for (Integer neighbor : neighbors) {
                line.append(neighbor + " ");
            }
            lines2Write.add(line.toString());
        }

        (new TxtTool(Settings.Site.Mag.getMagNeighborPath(), TxtTool.TxtMode.write)).writeAllLines2File(lines2Write);
    }

    private static void drawNeighborConnection(
            Map<String, List<MagData>>floorMapHeatmap,
            Map<String, List<List<Integer>>> floorMapNeighbor) throws Exception {

        for (String floor : Settings.Site.allFloors) {
            List<MagData>heatmap = floorMapHeatmap.get(floor);
            List<List<Integer>>neighbor = floorMapNeighbor.get(floor);

            GraphTool graphTool = new GraphTool(Settings.Site.getMapPath(floor),
                    Settings.Site.Mag.getMagHeatmapConnectionGraphPath(floor));

            for (int i = 0; i < heatmap.size(); i++) {
                List<Integer> neighbors = neighbor.get(i);
                Position p1 = heatmap.get(i).position;
                graphTool.drawPoint(p1, Color.GREEN, 1);
                for (int neighborIndex : neighbors) {
                    Position p2 = heatmap.get(neighborIndex).position;
                    graphTool.drawPoint(p2, Color.GREEN, 1);
                    graphTool.drawLine(p1, p2, Color.GREEN, 1);
                }
            }
            graphTool.close();
        }
    }

    private static void writeCombinedHeatmap(Map<String, List<MagData>>floorMapHeatmap) throws Exception {
        List<String>lines2Write = new ArrayList<>();

        for (Map.Entry<String, List<MagData>> entry : floorMapHeatmap.entrySet()) {

            String floor = entry.getKey();
            List<MagData>heatmap = entry.getValue();
            for(MagData magData : heatmap) {
                lines2Write.add(magData.i + " " + magData.v + " " + magData.h + " " + magData.o + " " +
                        magData.position.getX() + " " + magData.position.getY() + " " + floor);
            }
        }
        (new TxtTool(Settings.Site.Mag.getMagHeatmapPath(), TxtTool.TxtMode.write)).writeAllLines2File(lines2Write);
    }

    private static void drawHeatmap(Map<String, List<MagData>>floorMapHeatmap) {
        for (Map.Entry<String, List<MagData>>entry : floorMapHeatmap.entrySet()) {
            String floor = entry.getKey();
            List<MagData>heatmap = entry.getValue();
            try {
                List<Double> allValues = new ArrayList<>(heatmap.size() + 1);
                double max = Double.MIN_VALUE;
                double min = Double.MAX_VALUE;
                for (MagData magData : heatmap) {
                    double value = getMagDisplayValue(magData);
                    allValues.add(value);
                    max = Math.max(value, max);
                    min = Math.min(value, min);
                }
                Color beginColor = Color.YELLOW;
                Color endColor = Color.RED;
                Map<Integer, Color> valueAndColor = new HashMap<Integer, Color>();
                List<Color> colors = GraphTool.genColorSet(beginColor, endColor, (int) max - (int) min + 1);
                for (int index = 0; index < colors.size(); index++) {
                    valueAndColor.put((int) (min + index), colors.get(index));
                }

                GraphTool graphTool = new GraphTool(Settings.Site.getMapPath(floor),
                        Settings.Site.Mag.getMagHeatmapGraphPath(floor));
                for (int i = 0; i < allValues.size(); i++) {
                    double value = allValues.get(i);
                    Position position = heatmap.get(i).position;
                    graphTool.drawPoint(position, valueAndColor.get((int) value), 2);
                }
                graphTool.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static double getMagDisplayValue(MagData magData) {
        return magData.i;
    }

}
