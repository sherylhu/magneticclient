package main;

import Utils.Settings;
import Utils.DebugTool;
import Utils.InfoReader;
import main.MagProcess.CopyMagSurveyData;
import main.MagProcess.GenerateMagFingerprint;
import main.WiFiProcess.CopyWiFiSurveyData;
//import main.WiFiProcess.GenerateWiFiFingerprint;
import main.WiFiProcess.GenerateWiFiFingerprint;
import main.WiFiProcess.GenerateWiFiReferencePoints;

public class Main {
    private final static String tag = "Main";

    public static void main(String [] args) throws Exception {
        if (args.length != 1) {
            DebugTool.printLog(tag, "We need 1 argument in main()");
        }

        String argsStr = args[0];

        InfoReader.run();

        Settings.Site.init();

        if (argsStr.equals("create")) {
            GenerateFiles.run();
        }

        // wifi process
        else if (argsStr.equals("genWiFiRfPts")) {
            GenerateWiFiReferencePoints.run();
        } else if (argsStr.equals("copyWiFiRawData")) {
            InfoReader.loadWiFiSurveyFolders();
            CopyWiFiSurveyData.run();
        } else if (argsStr.equals("genWiFiFp")) {
            GenerateWiFiFingerprint.run();
        }

        // magnetic process
        else if (argsStr.equals("copyMagRawData")) {
            InfoReader.loadMagSurveyFolders();
            CopyMagSurveyData.run();
        } else if (argsStr.equals("genMagFp")) {
            GenerateMagFingerprint.run();
        }

        System.out.println("\nAll Finished!");
    }

}
