package main.WiFiProcess;

import Format.Position;
import Utils.Settings;
import Utils.DebugTool;
import Utils.GraphTool;
import Utils.MathTool;
import Utils.TxtTool;

import java.awt.*;
import java.util.*;
import java.util.List;

public class GenerateWiFiFingerprint {
    private final static String tag = "GenerateWiFiFingerprint";
    public static void run() {
        DebugTool.printLog(tag, "\n************************Run GenerateWiFiFingerprint***********************");

        for (String floor: Settings.Site.allFloors) {
            System.out.println("In " + floor + ": ");

            try {
                //Get all reference points
                TxtTool txtTool = new TxtTool(Settings.Site.WiFi.getWiFiReferencePointsTxtPath(floor), TxtTool.TxtMode.read);
                Map<Integer, ReferencePoint> referencePointMap = new HashMap<>();
                for (String line : txtTool.getAllLinesInFile()) {
                    String cols[] = line.split(",");
                    ReferencePoint referencePoint = new ReferencePoint(
                            new Position(Double.parseDouble(cols[1]), Double.parseDouble(cols[2])), Integer.parseInt(cols[0]));
                    referencePointMap.put(referencePoint.getReferencePointId(), referencePoint);
                }

                //Get fingerprint from each file,
                //In each file, fingerprints are separated by an empty line.
                Map<Integer, List<Fingerprint>> fingerprintMap = new HashMap<>();// referencePointNum -> list of fingerprint at that point
                for (String rawDataFolder : Settings.Site.WiFi.getWiFiRawFolders(floor)) {
                    for (String filePath : (new TxtTool(rawDataFolder, TxtTool.TxtMode.read)).getAllTxtPathsInFolder(false)) {
                        Map<String, List<Double>> apAndRssis = new HashMap<>();
                        int lastRfpId = -1;
                        long newestTimestamp = 0;
                        for (String line : (new TxtTool(filePath, TxtTool.TxtMode.read)).getAllLinesInFile()) {
                            if (!line.equals("")) {
                                String[] cols = line.split("\\|");
                                String apAddress = uniformApAddress(cols[0]);
                                double rssi = Double.parseDouble(cols[2]);
                                int rfpId = Integer.parseInt(cols[cols.length - 1]);
                                long timestamp = Long.parseLong(cols[5]);
                                if (timestamp > newestTimestamp) {
                                    newestTimestamp = timestamp;
                                }
                                if (lastRfpId == -1) {
                                    lastRfpId = rfpId;
                                } else if (lastRfpId != rfpId) {
                                    throw new Exception("lastRfpId != rfpId in the same block");
                                } else {
                                    if (!apAndRssis.containsKey(apAddress)) {
                                        apAndRssis.put(apAddress, new ArrayList<>());
                                    }
                                    apAndRssis.get(apAddress).add(rssi);
                                }
                            } else {
                                Fingerprint fingerprint = new Fingerprint();
                                List<Signal> signals = new ArrayList<>();
                                for (Map.Entry<String, List<Double>> entry : apAndRssis.entrySet()) {
                                    Signal signal = new Signal();
                                    String apAddress = entry.getKey();
                                    List<Double> rssis = entry.getValue();
                                    signal.setApAddress(apAddress);
                                    signal.setAverageRssi(MathTool.getAverage(rssis));
                                    signal.setStandardDeviation(MathTool.getStandardDeviation(rssis));
                                    signal.setSampleNum(rssis.size());
                                    signals.add(signal);
                                }
                                fingerprint.setSignals(signals);
                                ReferencePoint rfpt = referencePointMap.get(lastRfpId);
                                if (rfpt == null) {
                                    throw new Exception("The number of reference points in WiFi raw data is difference from reference_points.txt");
                                }
                                fingerprint.setRfPoint(rfpt);
                                fingerprint.setTimestamp(newestTimestamp);

                                if (!fingerprintMap.containsKey(lastRfpId)) {
                                    fingerprintMap.put(lastRfpId, new ArrayList<>());
                                }
                                fingerprintMap.get(lastRfpId).add(fingerprint);
                                if (fingerprintMap.get(lastRfpId).size() > 4) {
                                    Collections.sort(fingerprintMap.get(lastRfpId), new Comparator<Fingerprint>() {
                                        @Override
                                        public int compare(Fingerprint o1, Fingerprint o2) {
                                            if (o1.getTimestamp() > o2.getTimestamp()) {
                                                return -1;
                                            } else if (o1.getTimestamp() == o2.getTimestamp()) {
                                                return 0;
                                            } else {
                                                return 1;
                                            }
                                        }
                                    });
                                    fingerprintMap.put(lastRfpId, fingerprintMap.get(lastRfpId).subList(0, 4));
                                }

                                apAndRssis.clear();
                                lastRfpId = -1;
                                newestTimestamp = 0;
                            }
                        }
                    }
                }

                List<String> outputLines = new ArrayList<>();
                List<Map.Entry<Integer, List<Fingerprint>>> entryList = new ArrayList<>(fingerprintMap.entrySet());
                Collections.sort(entryList, new Comparator<Map.Entry<Integer, List<Fingerprint>>>() {
                    @Override
                    public int compare(Map.Entry<Integer, List<Fingerprint>> o1, Map.Entry<Integer, List<Fingerprint>> o2) {
                        int rfpId1 = o1.getKey();
                        int rfpId2 = o2.getKey();
                        if (rfpId1 < rfpId2) {
                            return -1;
                        } else if (rfpId1 == rfpId2) {
                            return 0;
                        } else {
                            return 1;
                        }
                    }
                });

                // summary fingerprints
                Map<Integer, Integer> summary = new HashMap<>();
                summary.put(1, 0);
                summary.put(2, 0);
                summary.put(3, 0);
                summary.put(4, 0);
                summary.put(8, 0);
                summary.put(7, 0);
                summary.put(6, 0);
                summary.put(5, 0);
                for (Map.Entry<Integer, List<Fingerprint>> entry : entryList) {
                    int size = entry.getValue().size();
                    summary.put(size, summary.get(size) + 1);
                }
                for (Map.Entry<Integer, Integer> entry : summary.entrySet()) {
                    System.out.println(entry.getValue() + " reference points have " + entry.getKey() + " fingerprints.");
                }

                // output fingerprint.txt and fingerprint.jpg
                GraphTool graphTool = new GraphTool(Settings.Site.getMapPath(floor),
                        Settings.Site.WiFi.getWiFiFingerprintGraphPath(floor));
                int fingerprintNum = 0;
                for (Map.Entry<Integer, List<Fingerprint>> entry : entryList) {
                    int directionNum = 1;
                    for (Fingerprint fingerprint : entry.getValue()) {
                        Position position = fingerprint.getRfPoint().getPosition();
                        String line = (int) position.getX() + "," + (int) position.getY() + "," + directionNum + ","
                                + fingerprintNum;
                        for (Signal signal : fingerprint.getSignals()) {
                            line += " " + signal.toString();
                        }
                        outputLines.add(line);

                        fingerprintNum++;
                        directionNum++;

                        graphTool.drawPoint(position, Color.GREEN, 3);
                    }
                }
                txtTool = new TxtTool(Settings.Site.WiFi.getWiFiFingerprintTxtPath(floor), TxtTool.TxtMode.write);
                txtTool.writeAllLines2File(outputLines);
                graphTool.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static class ReferencePoint{
        public Position getPosition() {
            return position;
        }

        public void setPosition(Position position) {
            this.position = position;
        }

        private Position position;

        public int getReferencePointId() {
            return referencePointId;
        }

        public void setReferencePointId(int referencePointId) {
            this.referencePointId = referencePointId;
        }

        public ReferencePoint(Position position, int referencePointId) {
            this.position = position;
            this.referencePointId = referencePointId;
        }

        private int referencePointId;

        public String getPositionString() {
            return position.getX() + "," + position.getY();
        }
    }

    public static class Fingerprint {
        public ReferencePoint getRfPoint() {
            return rfPoint;
        }

        public void setRfPoint(ReferencePoint rfPoint) {
            this.rfPoint = rfPoint;
        }

        private ReferencePoint rfPoint;

        public List<Signal> getSignals() {
            return signals;
        }

        public void setSignals(List<Signal> signals) {
            this.signals = signals;
        }

        private List<Signal>signals;

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        private long timestamp;
    }

    public static class Signal {
        public String getApAddress() {
            return apAddress;
        }

        public void setApAddress(String apAddress) {
            this.apAddress = apAddress;
        }

        private String apAddress;

        public double getAverageRssi() {
            return averageRssi;
        }

        public void setAverageRssi(double averageRssi) {
            this.averageRssi = averageRssi;
        }

        private double averageRssi;

        public void setStandardDeviation(double standardDeviation) {
            this.standardDeviation = standardDeviation;
        }

        public double getStandardDeviation() {

            return standardDeviation;
        }

        private double standardDeviation;

        public void setSampleNum(int sampleNum) {
            this.sampleNum = sampleNum;
        }

        public int getSampleNum() {

            return sampleNum;
        }

        private int sampleNum;

        public String toString() {
            return apAddress + "," + averageRssi + "," + standardDeviation + "," + sampleNum;
        }
    }

    private static String uniformApAddress(String apAddress) {
        if (apAddress.charAt(0) == '\"') {
            apAddress = apAddress.substring(1, apAddress.length());
        }
        if (apAddress.charAt(apAddress.length() - 1) == '\"') {
            apAddress = apAddress.substring(0, apAddress.length() - 1);
        }
        String answer = "";
        for (String col : apAddress.split(":")) {
            answer += col.toLowerCase();
        }
        return answer;
    }
}
