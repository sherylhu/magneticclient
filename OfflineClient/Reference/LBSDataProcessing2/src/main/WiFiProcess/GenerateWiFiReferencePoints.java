package main.WiFiProcess;

import Format.Position;
import Utils.Settings;
import Utils.*;
import main.ReferencePointGenerator;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GenerateWiFiReferencePoints {
    private final static String tag = "GenerateWiFiReferencePoints";

    public static void run() {
        DebugTool.printLog(tag, "**********************Running GenerateWiFiReferencePoints************************");
        for (String floor : Settings.Site.allFloors) {
            DebugTool.printLog(tag, "In " + floor + ":");
            List<List<Position>> allOutConstraints = getAllConstraints(Settings.Site.Constraint.getAllOutConstraintPaths(floor));
            if (allOutConstraints.isEmpty()) {
                DebugTool.printLog(tag, "You should add at least one out_constraint file ");
                return;
            }
            List<List<Position>> allInConstraints = getAllConstraints(Settings.Site.Constraint.getAllInConstraintPaths(floor));

            DebugTool.printLog(tag, "generate referencePoints");
            List<Position> referencePoints = ReferencePointGenerator.run(InfoReader.meterDistBetweenWiFiRfPts,
                    Settings.Site.floorsMapScales.get(floor), allOutConstraints, allInConstraints);
            DebugTool.printLog(tag, "generated " + referencePoints.size() + " referencePoints");
            DebugTool.printLog(tag, "output referencePoints in a txt");
            outputAllPos(referencePoints, floor);
            DebugTool.printLog(tag, "draw referencePoints in the map");
            drawAllPos(referencePoints, floor);
        }
        DebugTool.printLog(tag, "make site_folder for WiFi survey app");
        makeSiteFolderForSurveyApp();
    }

    private static List<List<Position>> getAllConstraints(List<String> allConstraintPaths) {
        List<List<Position>> answer = new ArrayList<List<Position>>();
        for (String path : allConstraintPaths) {
            try {
                List<Position> constraint = new ArrayList<Position>();
                for (String line : (new TxtTool(path, TxtTool.TxtMode.read)).getAllLinesInFile()) {
                    String[] cols = line.split(" ");
                    double x = Double.parseDouble(cols[0]);
                    double y = Double.parseDouble(cols[1]);
                    Position position = new Position(x, y);
                    constraint.add(position);
                }
                answer.add(constraint);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return answer;
    }

    private static void drawAllPos(List<Position> allPos, String floor) {
        try {
            GraphTool graphTool = new GraphTool(Settings.Site.getMapPath(floor),
                    Settings.Site.WiFi.getWiFiReferencePointsGraphPath(floor));
            for (Position position : allPos) {
                graphTool.drawPoint(position, Color.GREEN, 3);
            }
            graphTool.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void outputAllPos(List<Position> allPos, String floor) {
        List<String> lines2Write = new ArrayList<>();
        for (int i = 0; i < allPos.size(); i++) {
            Position position = allPos.get(i);
            lines2Write.add("" + i + "," + (int) position.getX() + "," + (int) position.getY());
        }

        try {
            (new TxtTool(
                    Settings.Site.WiFi.getWiFiReferencePointsTxtPath(floor),
                    TxtTool.TxtMode.write)
            ).writeAllLines2File(lines2Write);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void makeSiteFolderForSurveyApp() {
        String siteFolderForSurveyApp = Settings.Site.wiFiRoot + "SiteFolderForSurveyApp";
        String testSite = siteFolderForSurveyApp + "testSite";
        FileUtils.createFolder(siteFolderForSurveyApp, true);
        FileUtils.createFolder(testSite, true);

        List<Integer> floorNumbers = new ArrayList<>();
        for (int i = 1; i <= Settings.Site.allFloors.size(); i++) {
            floorNumbers.add(1000 + i);
        }
        // create buildings_db.txt
        String buildingsDbText = "{";
        for (int i = 0; i < floorNumbers.size(); i++) {
            buildingsDbText += floorNumbers.get(i) + "=" + Settings.Site.siteName;
            if (i < floorNumbers.size() - 1) {
                buildingsDbText += ",";
            }
        }
        buildingsDbText += "}";
        List<String> allLines2Write = new ArrayList<>();
        allLines2Write.add(buildingsDbText);
        try {
            (new TxtTool(testSite + "buildings_db.txt", TxtTool.TxtMode.write)).writeAllLines2File(allLines2Write);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < Settings.Site.allFloors.size(); i++) {
            int floorNumber = floorNumbers.get(i);
            String floor = Settings.Site.allFloors.get(i);
            String floorTestSite = testSite + floorNumber + File.separator;

            FileUtils.createFolder(floorTestSite, true);
            // create floorNumber + "/conf.ini"
            allLines2Write = new ArrayList<>();
            allLines2Write.add("numSamples=5");
            allLines2Write.add("24Only=1");
            try {
                (new TxtTool(floorTestSite + "conf.ini", TxtTool.TxtMode.write))
                        .writeAllLines2File(allLines2Write);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // copy floorNumber + "/map.jpg"
            FileUtils.copyFile(Settings.Site.getMapPath(floor), floorTestSite + "map.jpg", true);
            // create floorNumber + "/meta.txt"
            allLines2Write = new ArrayList<>();
            allLines2Write.add("1505868881060");
            allLines2Write.add("1503");
            allLines2Write.add("1");
            try {
                (new TxtTool(floorTestSite + "meta.txt", TxtTool.TxtMode.write))
                        .writeAllLines2File(allLines2Write);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // copy floorNumber + "/pts.txt"
            FileUtils.copyFile(Settings.Site.WiFi.getWiFiReferencePointsTxtPath(floor), floorTestSite + "pts.txt", true);
            FileUtils.createFolder(floorTestSite + "WifiData", true);

            // create floorNumber + "_db.txt"
            try {
                allLines2Write = new ArrayList<>();
                allLines2Write.add("{\"fileIds\":\"\",\"pkgUrl\":\"\",\"ts\":1504086465,\"_id\":"
                        + floorNumber + ",\"name\":\"" + Settings.Site.allFloors.get(i) + "\",\"numPts\":"
                        + (new TxtTool(Settings.Site.WiFi.getWiFiReferencePointsTxtPath(floor), TxtTool.TxtMode.read)).getAllLinesInFile().size()
                        + ",\"numDone\":0,\"ver\":0,\"doneIdx\":\"\",\"facilities\":[]}");
                (new TxtTool(testSite + floorNumber + "_db.txt", TxtTool.TxtMode.write)).writeAllLines2File(allLines2Write);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
