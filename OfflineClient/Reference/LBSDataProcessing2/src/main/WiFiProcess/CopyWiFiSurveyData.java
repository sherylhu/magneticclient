package main.WiFiProcess;

import Utils.Settings;
import Utils.DebugTool;
import Utils.FileUtils;
import Utils.InfoReader;

import java.io.File;
import java.util.List;

public class CopyWiFiSurveyData {
    private final static String tag = "CopyWiFiSurveyData";
    /**
     * Copy the survey folders (WifiData) into database.
     * If files/folders already exist in the destination, then it will replace the origin files/folders.
     */
    public static void run() {
        DebugTool.printLog(tag, "************************Run CopyWiFiSurveyData***********************");

        for (String floor : Settings.Site.allFloors) {
            List<String>allSurveyFoldersInFloor = InfoReader.floorsMapWiFiSurveyFolders.get(floor);
            List<String>allWiFiRawFoldersInFloor = Settings.Site.WiFi.generateWiFiRawFolders(floor, allSurveyFoldersInFloor.size());
            for (int i = 0; i < allSurveyFoldersInFloor.size(); i++) {
                String surveyFolder = allSurveyFoldersInFloor.get(i);
                String destinationFolder = allWiFiRawFoldersInFloor.get(i);
                File destinationFile = new File(destinationFolder);
                if (destinationFile.exists()) {
                    try {
                        FileUtils.deleteEntireFolder(destinationFile);
                        DebugTool.printLog(tag, "Delete origin destination folder: " + destinationFolder);
                    } catch (Exception e) {
                        DebugTool.printLog(tag, "Fail to delete origin destination folder: " + destinationFolder);
                        e.printStackTrace();
                    }
                }
                FileUtils.copyEntireFolder(surveyFolder, destinationFolder, true);
                DebugTool.printLog(tag, "Copy files from " + surveyFolder + " to " + destinationFolder);
            }
        }
    }
}
