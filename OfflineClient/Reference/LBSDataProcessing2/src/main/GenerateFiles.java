package main;

import Utils.Settings;
import Utils.FileUtils;
import Utils.InfoReader;

import java.util.List;
import java.util.Map;

public class GenerateFiles {
    private final static String tag = "GenerateFiles";

    public static void run() {
        // create folders
        for (String folder : Settings.Site.getAllSiteFolders()) {
            FileUtils.createFolder(folder, true);
        }

        // copy maps
        for (String floor : InfoReader.allFloors) {
            FileUtils.copyFile(InfoReader.floorsMapMapAddr.get(floor),Settings.Site.getMapPath(floor), true);
        }

        // copy out_constraints
        for (Map.Entry<String, List<String>> entry : InfoReader.floorsMapOutConstraints.entrySet()) {
            String floor = entry.getKey();
            List<String> outConstraints = entry.getValue();
            List<String> outputOutConstraints = Settings.Site.Constraint.generateOutConstraintPaths(floor, outConstraints.size());
            for (int i = 0; i < outConstraints.size(); i++) {
                FileUtils.copyFile(outConstraints.get(i), outputOutConstraints.get(i), true);
            }
        }

        // copy in_constraints
        for (Map.Entry<String, List<String>> entry : InfoReader.floorsMapInConstraints.entrySet()) {
            String floor = entry.getKey();
            List<String> inConstraints = entry.getValue();
            List<String> outputInConstraints = Settings.Site.Constraint.generateInConstraintPaths(floor, inConstraints.size());
            for (int i = 0; i < inConstraints.size(); i++) {
                FileUtils.copyFile(inConstraints.get(i), outputInConstraints.get(i), true);
            }
        }
    }

}
