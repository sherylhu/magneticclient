package main;

import Format.Position;
import Utils.InfoReader;
import Utils.MathTool;

import java.util.ArrayList;
import java.util.List;

public class ReferencePointGenerator {
    public static List<Position>run(double metersDistBetweenRfPoint, double scale,
            List<List<Position>>allOutConstraints, List<List<Position>>allInConstraints) {

        double pixelsDistBetweenRfPoint = metersDistBetweenRfPoint * scale;
        List<Position> allCandidatePositions = new ArrayList<>();
        double Xmax = Double.MIN_VALUE, Xmin = Double.MAX_VALUE, Ymax = Double.MIN_VALUE, Ymin = Double.MAX_VALUE;
        for (List<Position> outConstraintPath : allOutConstraints) {
            for (Position outConstraintPos : outConstraintPath) {
                double x = outConstraintPos.getX();
                double y = outConstraintPos.getY();
                Xmax = (x > Xmax) ? x : Xmax;
                Xmin = (x < Xmin) ? x : Xmin;
                Ymax = (y > Ymax) ? y : Ymax;
                Ymin = (y < Ymin) ? y : Ymin;
            }
        }
        for (double curX = Xmin; curX <= Xmax; curX += pixelsDistBetweenRfPoint) {
            for (double curY = Ymin; curY <= Ymax; curY += pixelsDistBetweenRfPoint) {
                boolean pointAvailable = true;
                Position curPos = new Position(curX, curY);
                for (List<Position> outConstraintPath : allOutConstraints) {
                    if (!MathTool.insidePolygen(outConstraintPath, curPos)) {
                        pointAvailable = false;
                        break;
                    }
                }
                if (!pointAvailable) {
                    continue;
                }
                for (List<Position> inConstraintPath : allInConstraints) {
                    if (MathTool.insidePolygen(inConstraintPath, curPos)) {
                        pointAvailable = false;
                        break;
                    }
                }
                if (pointAvailable) {
                    allCandidatePositions.add(curPos);
                }
            }
        }
        return allCandidatePositions;
    }
}
