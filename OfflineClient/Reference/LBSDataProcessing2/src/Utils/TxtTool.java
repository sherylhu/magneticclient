package Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TxtTool {
    public enum TxtMode {
        read, write;
    }

    private TxtMode curMode;
    private String filePath;

    public TxtTool(String filePath, TxtMode curMode) {
        this.curMode = curMode;
        this.filePath = filePath;
    }

    public List<String> getAllTxtPathsInFolder(boolean onlyNeedFileNameStartsWithNumber) throws Exception {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new Exception(filePath + " does not exist!");
        }
        if (!file.isDirectory()) {
            throw new Exception(filePath + " is not a folder");
        }

        List<String> answer = new ArrayList<>();
        File[] array = file.listFiles();
        if (array == null) {
            throw new Exception("got error in listFiles()");
        }
        for (File anArray : array) {
            if (anArray.isFile()) {
                String curName = anArray.getName();
                String curPath = anArray.getPath();
                if (!curName.startsWith(".") && curName.endsWith(".txt")) {
                    if (onlyNeedFileNameStartsWithNumber) {
                        if ('0' <= curName.charAt(0) && curName.charAt(0) <= '9') {
                            answer.add(curPath);
                        }
                    } else {
                        answer.add(curPath);
                    }
                }
            }
        }
        return answer;
    }

    public List<String> getAllFoldersInFolder() throws Exception {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new Exception(filePath + " does not exist!");
        }
        if (!file.isDirectory()) {
            throw new Exception(filePath + " is not a folder");
        }

        List<String> answer = new ArrayList<>();
        File[] array = file.listFiles();
        if (array == null) {
            throw new Exception("got error in listFiles()");
        }
        for (File anArray : array) {
            if (anArray.isDirectory()) {
                String curName = anArray.getName();
                String curPath = anArray.getPath();
                if (!curName.startsWith(".")) {
                    answer.add(curPath);
                }
            }
        }
        return answer;
    }

    public List<String> getAllLinesInFile() throws Exception {
        checkMode(TxtMode.read, curMode);

        File file = new File(filePath);
        if (!file.isFile()) {
            throw new Exception("Not a file: " + filePath);
        }

        List<String> answer = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            while (line.endsWith("\n") || line.endsWith("\r")) {
                line = line.substring(0, line.length() - 1);
            }
            answer.add(line);
        }
        bufferedReader.close();
        return answer;
    }

    public void writeAllLines2File(List<String>lines) throws Exception {
        checkMode(TxtMode.write, curMode);
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(filePath)));
        for (String line: lines) {
            bufferedWriter.write(line + "\n");
            bufferedWriter.flush();
        }
        bufferedWriter.close();
    }

    private void checkMode(TxtMode expectMode, TxtMode actualMode) throws Exception {
        if (!expectMode.equals(actualMode)) {
            throw new Exception("You are now not in " + expectMode.toString() + " mode but " + actualMode + " mode");
        }
    }
}
