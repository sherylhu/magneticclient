package Utils;

import Format.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class GraphTool {
    private String inputPath;
    private String outputPath;
    private Graphics2D g2d;
    private BufferedImage bimg;

    private static final Color groundTruthColor = Color.green;
    private static final Color lightGroundTruthColor = new Color(153, 255, 153);
    private static final int groundTruthPointsRadius = 3;

    private static final Color estimatedColor = Color.blue;
    private static final Color lightEstimatedColor = new Color(102, 204, 255);
    private static final int estimatedPointsRadius = 3;

    private static final Color rfCandidateColor = Color.yellow;
    private static final int rfCandidatePointsRadius = 3;

    private static final Color startPointColor = Color.gray;
    private static final int startPointRadius = 3;

    private static final int heatmapPointRadius = 5;

    private static final Color txtColor = Color.BLACK;

    public GraphTool(String inputPath, String outputPath) throws Exception {
        this.inputPath = inputPath;
        this.outputPath = outputPath;
        bimg = ImageIO.read(new FileInputStream(inputPath));
        g2d = (Graphics2D) bimg.getGraphics();
    }

    public void drawPoints(List<Position>positions, Color color, boolean showPositionTxt) throws Exception {
        for (int i = 0; i < positions.size(); i++) {
            Position position = positions.get(i);
            g2d.setColor(color);
            g2d.setStroke(new BasicStroke(10));
            g2d.drawOval((int)position.getX(),
                    (int) position.getY(), 3, 3);
            if (showPositionTxt) {
                drawTxt(position.toString(), position, Color.green);
            }
        }
    }

    public void drawPoint(Position position, Color color, int radius) {
        g2d.setColor(color);
        g2d.setStroke(new BasicStroke(radius));
        g2d.fillOval((int)position.getX() - radius,
                (int) position.getY() - radius, 2 * radius, 2 * radius);
    }

    public void drawLine(Position beginPos, Position endPos, Color color, int width) {
        g2d.setColor(color);
        g2d.setStroke(new BasicStroke(width));
        g2d.drawLine((int)beginPos.getX(), (int)beginPos.getY(),
                (int)endPos.getX(), (int)endPos.getY());
    }

    public void drawTxt(String txt, Position position, Color color) {
        g2d.setColor(color);
        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 30));
        g2d.drawString(txt, (int)position.getX(), (int)position.getY());
    }

    private void drawEstimatedPositions(List<Position> positions) throws Exception {
        List<Color>colors = genColorSet(Color.lightGray, Color.BLUE, positions.size());
        for (int i = 0; i < positions.size(); i++) {
            drawPoint(positions.get(i), colors.get(i), estimatedPointsRadius);
        }
    }

    private void drawGroundTruthPath(List<Position>positions) throws Exception {
        List<Color>colors = genColorSet(lightGroundTruthColor, groundTruthColor, positions.size() - 1);
        for (int i = 0; i < positions.size() - 1; i++) {
            Position position = positions.get(i);
            Position nextPosition = positions.get(i + 1);
            g2d.setColor(colors.get(i));
            g2d.setStroke(new BasicStroke(10));
            g2d.drawLine((int)position.getX(), (int)position.getY(),
                    (int)nextPosition.getX(), (int)nextPosition.getY());
        }
    }

    private void drawEstimatedPath(List<Position>positions) throws Exception {
        List<Color>colors = genColorSet(lightEstimatedColor, estimatedColor, positions.size() - 1);
        for (int i = 0; i < positions.size() - 1; i++) {
            Position position = positions.get(i);
            Position nextPosition = positions.get(i + 1);
            g2d.setColor(colors.get(i));
            g2d.setStroke(new BasicStroke(5));
            g2d.drawLine((int)position.getX(), (int)position.getY(),
                    (int)nextPosition.getX(), (int)nextPosition.getY());
        }
    }

    public void drawEstimatedConnectedPositions(List<Position> positions) throws Exception {
        List<Color>colors = genColorSet(Color.lightGray, Color.BLUE, positions.size() - 1);
        for (int i = 0; i < positions.size() - 1; i++) {
            Position position = positions.get(i);
            Position nextPosition = positions.get(i + 1);
            g2d.setColor(colors.get(i));
            g2d.setStroke(new BasicStroke(10));
            g2d.drawLine((int)position.getX(), (int)position.getY(),
                    (int)nextPosition.getX(), (int)nextPosition.getY());
        }
    }

    public void close() throws Exception {
        ImageIO.write(bimg, "JPG", new FileOutputStream(outputPath));
    }

    public void cropImage(double startX, double startY, double endX, double endY) {
        bimg = bimg.getSubimage((int)startX, (int)startY, (int)(endX - startX), (int)(endY - startY));
    }

    /**
     * generate a list of colors, which distribute evenly.
     * @param beginColor the first color in the list
     * @param endColor the last color in the list
     * @param size the size of the list, which infers how many colors we need to generate
     */
    public static List<Color>genColorSet(Color beginColor, Color endColor, int size) throws Exception {
        if (size == 0) {
            throw new Exception("size too small to generate a list");
        } else if (size == 1){
            List<Color>result = new ArrayList<>();
            result.add(endColor);
            return result;
        }
        int beginR = beginColor.getRed(), beginG = beginColor.getGreen(), beginB = beginColor.getBlue();
        int endR = endColor.getRed(), endG = endColor.getGreen(), endB = endColor.getBlue();
        double increaseR = 1.0 * (endR - beginR) / (size - 1),
                increaseG = 1.0 * (endG - beginG) / (size - 1),
                increaseB = 1.0 * (endB - beginB) / (size - 1);
        List<Color>result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            int currentR = (int)(i * increaseR + beginR);
            int currentG = (int)(i * increaseG + beginG);
            int currentB = (int)(i * increaseB + beginB);
            result.add(new Color(currentR, currentG, currentB));
        }
        return result;
    }

    private Color getRandomColor() {
        Random random = new Random();
        int red = random.nextInt(200);
        int green = random.nextInt(200);
        int blue = random.nextInt(200);
        return new Color(red, green, blue);
    }

    private Color getRandomColor(int begin, int end) {
        Random random = new Random();
        int red = (int)(random.nextInt(end) * 1.0 / end * (end - begin) + begin);
        int green = (int)(random.nextInt(end) * 1.0 / end * (end - begin) + begin);
        int blue = (int)(random.nextInt(end) * 1.0 / end * (end - begin) + begin);
        return new Color(red, green, blue);
    }

    public void drawRfCandidatePoint(Position position) {
        drawPoint(position, rfCandidateColor, rfCandidatePointsRadius);
    }

    public void drawEstimatedPoint(Position position) {
        drawPoint(position, estimatedColor, estimatedPointsRadius);
    }

    public void drawGroundTruthPoint(Position position) {
        drawPoint(position, groundTruthColor, groundTruthPointsRadius);
    }

    public void drawGroundTruthPoints(List<Position>allGroundTruthPos) {
        for (Position position : allGroundTruthPos) {
            drawGroundTruthPoint(position);
        }
        drawPoint(allGroundTruthPos.get(0), startPointColor, startPointRadius);
    }

    public void drawGroundTruthPointsWithTxt(List<Position>allGroundTruthPos, String txt) {
        for (Position position : allGroundTruthPos) {
            drawPoint(position, groundTruthColor, groundTruthPointsRadius);
        }
        drawPoint(allGroundTruthPos.get(0), startPointColor, startPointRadius);

        drawTxt(txt, allGroundTruthPos.get(0).getMiddlePosition(allGroundTruthPos.get(allGroundTruthPos.size() - 1)), txtColor);
    }

    public void drawHeatmapPoint(Position position, Color color) {
        drawPoint(position, color, heatmapPointRadius);
    }
}
