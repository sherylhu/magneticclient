package Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InfoReader {
    private final static String tag = "InfoReader";

    public static String siteName;
    public static List<String>allFloors = new ArrayList<>();
    public static Map<String, Float> floorsMapScales = new HashMap<>();
    public static Map<String, String> floorsMapMapAddr = new HashMap<>();
    public static Map<String, List<String>> floorsMapOutConstraints = new HashMap<>();
    public static Map<String, List<String>> floorsMapInConstraints = new HashMap<>();

    public static float meterDistBetweenWiFiRfPts;
    public static float meterDistBetweenMagRfPts;

    public static Map<String, List<String>>floorsMapWiFiSurveyFolders;
    public static Map<String, String>floorsMapMagSurveyFolder;

    private static JSONObject jsonRootObject;

    public static void run() throws Exception {
        DebugTool.printLog(tag, "************information start***********");

        StringBuffer stringBuffer = new StringBuffer();
        for (String line : (new TxtTool(Settings.Paths.infoJson, TxtTool.TxtMode.read)).getAllLinesInFile()) {
            stringBuffer.append(line);
        }

        jsonRootObject = new JSONObject(stringBuffer.toString());

        // load site_name
        siteName = jsonRootObject.getString("site_name");
        DebugTool.printLog(tag, "siteName: " + siteName);

        // load allFloors
        JSONArray floorsArray = jsonRootObject.getJSONArray("floors");
        for (int i = 0; i < floorsArray.length(); i++) {
            allFloors.add(floorsArray.getString(i));
        }
        DebugTool.printLog(tag, "allFloors: " + allFloors.toString());

        // load settings
        JSONObject settingsObj = jsonRootObject.getJSONObject("settings");
        for (String floor : allFloors) {
            JSONObject floorObj = settingsObj.getJSONObject(floor);
            float scale = (float)floorObj.getDouble("scale");
            floorsMapScales.put(floor, scale);

            String mapAddr = floorObj.getString("map");
            floorsMapMapAddr.put(floor, mapAddr);

            List<String>outConstraintsStr = new ArrayList<>();
            JSONArray outConstraintArray = floorObj.getJSONArray("out_constraints");
            for (int i = 0; i < outConstraintArray.length(); i++) {
                outConstraintsStr.add(outConstraintArray.getString(i));
            }
            floorsMapOutConstraints.put(floor, outConstraintsStr);

            List<String>inConstraintsStr = new ArrayList<>();
            JSONArray inConstraintArray = floorObj.getJSONArray("in_constraints");
            for (int i = 0; i < inConstraintArray.length(); i++) {
                inConstraintsStr.add(inConstraintArray.getString(i));
            }
            floorsMapInConstraints.put(floor, inConstraintsStr);
        }

        // load meterDistBetweenWiFiRfPts
        meterDistBetweenWiFiRfPts = (float) jsonRootObject.getDouble("meter_distance_between_WiFi_reference_points");
        DebugTool.printLog(tag, "meterDistBetweenWiFiRfPts: " + meterDistBetweenWiFiRfPts);

        // load meterDistBetweenMagRfPts
        meterDistBetweenMagRfPts = (float) jsonRootObject.getDouble("meter_distance_between_Magnetic_reference_points");
        DebugTool.printLog(tag, "meterDistBetweenMagRfPts: " + meterDistBetweenMagRfPts);

        DebugTool.printLog(tag, "************information end***********");
    }

    public static void loadWiFiSurveyFolders() {
        JSONObject wiFiSurveyFolderObj = jsonRootObject.getJSONObject("WiFi_survey_folders");
        floorsMapWiFiSurveyFolders = new HashMap<>();
        for (String floor : allFloors) {
            List<String>floorsMapWiFiSurveyFolderInFloor = new ArrayList<>();
            JSONArray wiFiSurveyFloorFolderArray = wiFiSurveyFolderObj.getJSONArray(floor);
            for (int i = 0; i < wiFiSurveyFloorFolderArray.length(); i++) {
                floorsMapWiFiSurveyFolderInFloor.add(wiFiSurveyFloorFolderArray.getString(i));
            }
            floorsMapWiFiSurveyFolders.put(floor, floorsMapWiFiSurveyFolderInFloor);
        }
    }

    public static void loadMagSurveyFolders() {
        JSONObject magSurveyFolderObj = jsonRootObject.getJSONObject("Mag_survey_folders");
        floorsMapMagSurveyFolder = new HashMap<>();
        for (String floor : allFloors) {
            floorsMapMagSurveyFolder.put(floor, magSurveyFolderObj.getString(floor));
        }
    }
}
