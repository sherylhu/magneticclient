package Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Settings {

    public static class Paths {
//        public final static String infoJson = "/Users/moziliang/workspace/LBSDataProcessing2/info4SciencePark.json";
        public final static String infoJson = "C:\\Users\\LinhTa\\MTrec\\Workspace_Ken\\LBSDataProcessing2\\info4Elements.json";
        public final static String database = "C:\\Users\\LinhTa\\MTrec\\Workspace_Ken\\Database\\";
    }

    public static class Site {
        private final static String tag = "Site";

        public static String siteName;
        public static List<String> allFloors;
        public static Map<String, Float> floorsMapScales;

        public static String siteRootFolder;

        public static String constraintFolder;
        public static String inConstraintsRootFolder;
        public static String outConstraintsRootFolder;
        public static Map<String, String> inConstraints = new HashMap<>();
        public static Map<String, String> outConstraints = new HashMap();

        public static String mapRoot;
        public static Map<String, String> maps = new HashMap<>();

        public static String wiFiRoot;
        public static String wiFiFp;
        public static Map<String, String> wiFiFps = new HashMap<>();
        public static String wiFiRaw;
        public static Map<String, String> wiFiRaws = new HashMap<>();
        public static String wiFiRfp;
        public static Map<String, String> wiFiRfps = new HashMap<>();

        public static String magRoot;
        public static String magRaw;
        public static Map<String, String> magRaws = new HashMap<>();
        public static String magHeatmap;

        public static void init() {
            Site.allFloors = InfoReader.allFloors;
            Site.siteName = InfoReader.siteName;
            Site.floorsMapScales = InfoReader.floorsMapScales;

            siteRootFolder = Settings.Paths.database + siteName + File.separator;

            // constraintFolder
            constraintFolder = siteRootFolder + "Constraint" + File.separator;
            inConstraintsRootFolder = constraintFolder + "inConstraints" + File.separator;
            outConstraintsRootFolder = constraintFolder + "outConstraints" + File.separator;

            // map
            mapRoot = siteRootFolder + "map" + File.separator;

            // WifiData
            wiFiRoot = siteRootFolder + "WifiData" + File.separator;
            wiFiFp = wiFiRoot + "FingerprintData" + File.separator;
            wiFiRaw = wiFiRoot + "RawData" + File.separator;
            wiFiRfp = wiFiRoot + "ReferencePoints" + File.separator;

            // MagData
            magRoot = siteRootFolder + "MagData" + File.separator;
            magRaw = magRoot + "RawData" + File.separator;
            magHeatmap = magRoot + "Heatmap" + File.separator;

            for (String floor : allFloors) {
                inConstraints.put(floor, inConstraintsRootFolder + floor + File.separator);
                outConstraints.put(floor, outConstraintsRootFolder + floor + File.separator);
                maps.put(floor, mapRoot + floor + File.separator);
                wiFiFps.put(floor, wiFiFp + floor + File.separator);
                wiFiRaws.put(floor, wiFiRaw + floor + File.separator);
                wiFiRfps.put(floor, wiFiRfp + floor + File.separator);
                magRaws.put(floor, magRaw + floor + File.separator);
            }
        }

        public static String getMapPath(String floor) {
            return maps.get(floor) + "map.jpg";
        }

        public static class Constraint {
            public static List<String> generateOutConstraintPaths(String floor, int pathNum) {
                List<String>answer = new ArrayList<>();
                for (int i = 1; i <= pathNum; i++) {
                    answer.add(outConstraints.get(floor) + "outConstraint" + i + ".txt");
                }
                return answer;
            }

            public static List<String> generateInConstraintPaths(String floor, int pathNum) {
                List<String>answer = new ArrayList<>();
                for (int i = 1; i <= pathNum; i++) {
                    answer.add(inConstraints.get(floor) + "inConstraint" + i + ".txt");
                }
                return answer;
            }

            public static List<String> getAllOutConstraintPaths(String floor) {
                String folder = Site.outConstraints.get(floor);
                if (!FileUtils.checkFolderExists(folder)) {
                    return null;
                }
                try {
                    return (new TxtTool(folder, TxtTool.TxtMode.read))
                            .getAllTxtPathsInFolder(false);
                } catch (Exception e) {
                    DebugTool.printLog(tag, "fail in getAllOutConstraintPaths(): " + e.getMessage());
                }
                return null;
            }

            public static List<String> getAllInConstraintPaths(String floor) {
                String folder = Site.inConstraints.get(floor);
                if (!FileUtils.checkFolderExists(folder)) {
                    return null;
                }
                try {
                    return (new TxtTool(folder, TxtTool.TxtMode.read))
                            .getAllTxtPathsInFolder(false);
                } catch (Exception e) {
                    DebugTool.printLog(tag, "fail in getAllInConstraintPaths(): " + e.getMessage());
                }
                return null;
            }
        }

        public static class WiFi {
            public static String getWiFiReferencePointsGraphPath(String floor) {
                return wiFiRfps.get(floor) + "reference_points.jpg";
            }

            public static String getWiFiReferencePointsTxtPath(String floor) {
                return wiFiRfps.get(floor) + "reference_points.txt";
            }

            public static String getWiFiRawFolder(String floor) {
                return wiFiRaws.get(floor);
            }

            public static List<String>generateWiFiRawFolders(String floor, int folderNum) {
                List<String>answer = new ArrayList<>();
                for (int i = 1; i <= folderNum; i++) {
                    answer.add(wiFiRaws.get(floor) + "WiFiData" + i);
                }
                return answer;
            }

            public static List<String>getWiFiRawFolders(String floor) {
                try {
                    return (new TxtTool(Site.wiFiRaws.get(floor), TxtTool.TxtMode.read)).getAllFoldersInFolder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            public static String getWiFiFingerprintTxtPath(String floor) {
                return wiFiFps.get(floor) + "fingerprint.txt";
            }

            public static String getWiFiFingerprintGraphPath(String floor) {
                return wiFiFps.get(floor) + "fingerprint.jpg";
            }
        }

        public static class Mag {
            public static String getMagRawFolder(String floor) {
                return magRaws.get(floor);
            }

            public static String getMagNeighborPath() {
                return magHeatmap + "neighbor.txt";
            }

            public static String getMagHeatmapPath() {
                return magHeatmap + "heatmap.txt";
            }

            public static String getMagHeatmapConnectionGraphPath(String floor) {
                return magHeatmap + "connection_" + floor + ".jpg";
            }
            public static String getMagHeatmapGraphPath(String floor) {
                return magHeatmap + "heatmap_" + floor + ".jpg";
            }
        }

        public static List<String> getAllSiteFolders() {
            List<String>allSitePaths = new ArrayList<>();
            allSitePaths.add(siteRootFolder);
            allSitePaths.add(constraintFolder);
            allSitePaths.add(inConstraintsRootFolder);
            allSitePaths.add(outConstraintsRootFolder);
            allSitePaths.add(mapRoot);
            allSitePaths.add(wiFiRoot);
            allSitePaths.add(wiFiFp);
            allSitePaths.add(wiFiRaw);
            allSitePaths.add(wiFiRfp);
            allSitePaths.add(magRoot);
            allSitePaths.add(magRaw);
            allSitePaths.add(magHeatmap);
            allSitePaths.addAll(inConstraints.values());
            allSitePaths.addAll(outConstraints.values());
            allSitePaths.addAll(maps.values());
            allSitePaths.addAll(wiFiFps.values());
            allSitePaths.addAll(wiFiRaws.values());
            allSitePaths.addAll(wiFiRfps.values());
            allSitePaths.addAll(magRaws.values());
            return allSitePaths;
        }
    }
}
