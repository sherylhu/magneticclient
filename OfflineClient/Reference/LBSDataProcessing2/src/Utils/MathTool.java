package Utils;

import Format.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MathTool {
    public static double getAverage(List<Double> nums) {
        double _sum = 0;
        for (double num : nums) {
            _sum += num;
        }
        return _sum / nums.size();
    }

    public static double getStandardDeviation(List<Double> nums) {
        double average = getAverage(nums);
        double _sum = 0;
        for (double num : nums) {
            double temp = num - average;
            _sum += temp * temp;
        }
        return Math.sqrt(_sum / nums.size());
    }

    public static double getMaximum(List<Double> nums) {
        double _max = nums.get(0);
        for (double num : nums) {
            if (_max < num) {
                _max = num;
            }
        }
        return _max;
    }

    // get a random num in range [begin, end)
    public static int getRandomNum(int begin, int end) {
        Random random = new Random();
        return begin + random.nextInt(end - begin);
    }

    public static <T>List<T>reorderListRandomly(List<T>list) {
        if (list == null || list.isEmpty()) {
            return list;
        }
        for (int index = 0; index < list.size(); index++) {
            // get an random index in range [index, list.size()), then swap these two indices
            int randomInt = getRandomNum(index, list.size());
            T data = list.get(index);
            list.set(index, list.get(randomInt));
            list.set(randomInt, data);
        }
        return list;
    }

    public static List<Integer>getTurningPoint(List<Position>allPos) {
        List<Integer>answer = new ArrayList<Integer>();
        for (int i = 1; i < allPos.size() - 1; i++) {
            if (!posOnSegment2(allPos.get(i), allPos.get(i - 1), allPos.get(i + 1))) {
                answer.add(i);
            }
        }
        return answer;
    }

    public static boolean posOnSegment2(Position pos, Position segBegin, Position segEnd) {
        double px = segBegin.getX(), py = segBegin.getY();
        double qx = pos.getX(), qy = pos.getY();
        double rx = segEnd.getX(), ry = segEnd.getY();
        boolean onLine = Math.abs((qx * ry - qy * rx) - (px * ry - py * rx) + (px * qy - py * qx) ) < 1e-6;
        if (!onLine) {
            return false;
        } else {
            if (Math.abs(px - qx) < Constants.eps) {
                return (py <= qy && qy <= ry) || (ry <= qy && qy <= py);
            } else {
                return (px <= qx && qx <= rx) || (rx <= qx && qx <= px);
            }
        }
    }

    public static double getSlope(Position p1, Position p2) {
        if (Math.abs((p1.getX() - p2.getX())) < Constants.eps) {
            return Double.NaN;
        }
        return (p1.getY() - p2.getY()) * 1.0 / (p1.getX() - p2.getX());
    }

    public static int posAccording2Line(Position pos, Position pointInLine, double slope) {
        double temp = 0;
        if (Double.isNaN(slope)) {
            temp = pos.getX() - pointInLine.getX();
        } else {
            temp = pos.getY() - (slope * (pos.getX() - pointInLine.getX()) + pointInLine.getY());
        }
        if (Math.abs(temp) < Constants.eps) {
            return 0;
        } else if (temp < 0) {
            return -1;
        } else {
            return 1;
        }
    }

    public static double getDistFromPos2LineSeg(Position pos, Position segBegin, Position segEnd) {
        double cross = (segEnd.getX() - segBegin.getX()) * (pos.getX() - segBegin.getX())
                + (segEnd.getY() - segBegin.getY()) * (pos.getY() - segBegin.getY());
        if (cross <= 0) {
            return pos.dist(segBegin);
        }

        double line_len_square = Math.pow(segBegin.dist(segEnd), 2);
        if (cross >= line_len_square) {
            return pos.dist(segEnd);
        }

        double rate = cross / line_len_square;
        double px = segBegin.getX() + (segEnd.getX() - segBegin.getX()) * rate;
        double py = segBegin.getY() + (segEnd.getY() - segBegin.getY()) * rate;
        Position answer = new Position(px, py);
        return pos.dist(answer);
    }

    public static boolean insidePolygen(List<Position> polygen, Position curPos) {
        for (int i = 0; i < polygen.size(); i++) {
            Position edgeStartPos = polygen.get(i);
            Position edgeEndPos = polygen.get((i + 1) % polygen.size());
            if (MathTool.posOnSegment2(curPos, edgeStartPos, edgeEndPos)) {
                return false;
            }
            if (MathTool.getDistFromPos2LineSeg(curPos, edgeStartPos, edgeEndPos) < Constants.eps) {
                return false;
            }
        }

        List<Double> allSlope = new ArrayList<Double>();
        for (int i = 0; i < polygen.size(); i++) {
            Position edgeStartPos = polygen.get(i);
            Position edgeEndPos = polygen.get((i + 1) % polygen.size());
            double slope = MathTool.getSlope(edgeStartPos, edgeEndPos);
            if (!Double.isNaN(slope)) {
                allSlope.add(slope);
            }
        }
        double slope = 0;
        Random random = new Random();
        for (;; slope += random.nextDouble() + Constants.eps) {
            boolean correct = true;
            for (double s : allSlope) {
                if (Math.abs(s - slope) < Constants.eps) {
                    correct = false;
                    break;
                }
            }
            if (!correct) {
                continue;
            }
            for (Position position : polygen) {
                // position on line(curPos, slope)
                if (MathTool.posAccording2Line(position, curPos, slope) == 0) {
                    correct = false;
                    break;
                }
            }
            if (correct) {
                break;
            }
        }

        int leftCrossing = 0;
        int rightCrossing = 0;
        for (int i = 0; i < polygen.size(); i++) {
            Position edgeStartPos = polygen.get(i);
            Position edgeEndPos = polygen.get((i + 1) % polygen.size());
            boolean edgeStartPosAboveLine = MathTool.posAccording2Line(edgeStartPos, curPos, slope) > 0;
            boolean edgeEndPosAboveLine = MathTool.posAccording2Line(edgeEndPos, curPos, slope) > 0;
            if ((edgeStartPosAboveLine && !edgeEndPosAboveLine) || (edgeEndPosAboveLine && !edgeStartPosAboveLine)) {
                // get intersection point
                double slope2 = MathTool.getSlope(edgeStartPos, edgeEndPos);
                double interX;
                if (!Double.isNaN(slope2)) {
                    interX = (-edgeStartPos.getX() * slope2 + edgeStartPos.getY() + curPos.getX() * slope
                            - curPos.getY()) / (slope - slope2);
                } else {
                    interX = edgeStartPos.getX();
                }

                if (curPos.getX() < interX) {
                    rightCrossing++;
                } else {
                    leftCrossing++;
                }
            }
        }

        return (leftCrossing % 2 == 1) && (rightCrossing % 2 == 1);
    }

}
