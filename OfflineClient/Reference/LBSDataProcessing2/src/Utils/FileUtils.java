package Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileUtils {
    private final static String tag = "FileUtils";

    public static void createFolder(String folder, boolean replaceIfExist) {
        File f = new File(folder);

        if (f.exists()) {
            if (replaceIfExist) {
                deleteEntireFolder(f);
            } else {
                DebugTool.printLog(tag, "Fail to create (already exists): " + folder);
                return;
            }
        }

        if (f.mkdir()) {
            DebugTool.printLog(tag, "Created folder: " + f.getPath());
        } else {
            DebugTool.printLog(tag, "Fail create folder: " + f.getPath());
        }
    }

    public static void copyFile(String source, String destination, boolean replaceIfExist) {
        File sourceFile = new File(source);
        File desFile = new File(destination);

        if (desFile.exists()) {
            if (replaceIfExist) {
                deleteEntireFolder(desFile);
            } else {
                DebugTool.printLog(tag, "Fail to copy (already exists): " + destination);
                return;
            }
        }
        try {
            Files.copy(sourceFile.toPath(), desFile.toPath());
            DebugTool.printLog(tag, "Copy file from " + source + " to " + destination);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void copyEntireFolder(String source, String destination, boolean replaceIfExist) {
        try {
            File sourceFile = new File(source);
            File desFile = new File(destination);

            if (desFile.exists()) {
                if (replaceIfExist) {
                    deleteEntireFolder(desFile);
                } else {
                    DebugTool.printLog(tag, "Fail to copy to the destination folder (already exists): " + destination);
                    return;
                }
            }

            Files.copy(sourceFile.toPath(), desFile.toPath());
            if (sourceFile.isDirectory()) {
                File[] filesInSource = sourceFile.listFiles();
                if (filesInSource == null) {
                    throw new Exception("got error in listFiles()");
                }
                for (File subFile : filesInSource) {
                    copyEntireFolder(subFile.toPath().toString(), destination + "/" + subFile.getName(), true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteEntireFolder(File f) {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                deleteEntireFolder(c);
            }
        }
        try {
            Files.delete(f.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkFolderExists(String folder) {
        File file = new File(folder);
        if (!file.exists() || !file.isDirectory()) {
            DebugTool.printLog(tag, "No such folder or it's not a folder: " + folder);
            return false;
        }
        return true;
    }
}
