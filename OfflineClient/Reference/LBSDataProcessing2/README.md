# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to use the system

* Set the values in Utils.Settings.Paths, to declare where is the info.json and database folder.
* Database folder is the folder that you want to save all the processing files. While data is processing, all of the input files will first be copied into this folder, the result of processing will also be saved inside this folder.
* info.json declares all the settings in data processing.

#### How to write info.json and process data for site-survey
##### 1.Create folders for data processing
In info.json file:

* Set the site_name , like "SciencePark".
* Set the floors, like "2F", "3F".
* Set the settings: for each floor, declare the scale(how many pixels is equal to 1 meter), the path of map(should be .jpg), and all the out_constraints and in_constraints files(should be .txt). You should declare at least 1 out_constraint file. 
* The file of in_constraint or out_constraint describes a polygon. So each line in the file describes a point, or x and y, seperated by a space. A point (x, y) describes a pixel coordinate in the map picture, where the origin is the left top of the map, and x is the horizontal coordinate while y is for the vertical one.

* Run main.Main with argument "create", then it will run main.GenerateFiles.run().
It will create all of the folders in database folder. 
For example, if you set the site_name as "SciencePark", and the floors as ["2F","3F"], and the settings as

"settings" : {
    "2F": {
      "scale" : 23.7,
      "map" : "/..../map.jpg",
      "out_constraints" : [
        "/..../outConstraint.txt"
      ],
      "in_constraints" : []
    },
    "3F": {
      "scale" : 23.7,
      "map" : "/..../map.jpg",
      "out_constraints" : [
        "/..../outConstraint.txt"
      ],
      "in_constraints" : []
    }
  },
then The folder created should be like this:

database_folder
|--SciencePark
  |--Constraint
    |------inConstraints
      |--------2F
      |--------3F
    |------outConstraints
      |--------2F
        |----------outConstraint1.txt
      |--------3F
        |----------outConstraint1.txt
  |----MagData
    |------Heatmap
      |--------2F
      |--------3F
    |------RawData
      |--------2F
      |--------3F
  |----map
    |------2F
      |--------map.jpg
    |------3F
      |--------map.jpg
  |----WifiData
    |------FingerprintData
      |--------2F
      |--------3F
    |------RawData
      |--------2F
      |--------3F
    |------ReferencePoints
      |--------2F
      |--------3F
      
##### 2.Generate WiFi reference points
In info.json file:

* Set the meter_distance_between_WiFi_reference_points, which means the meter distance between two reference points. For a big open space, you can set this distance as 3 or 5, but if the site contains some corridors or small/narrow area, it is better to set this distance smaller, like 1 or 2 meter, to make at least 2 lines of reference points passing through the corridor. If the site contains both big open space and narrow corridors, you should set the distance as corridor's case. 
It is ok if you want to have different distances in difference area, like big area with big distance, small area with small distance, but you need to change the client program by yourself. You can refer to the README.md in client app.

* Run main.Main with argument "genWiFiRfPts", then it will run main.WiFiProcess.GenerateWiFiReferencePoints.run().
This function will generate the WiFi reference points based on the constraint files, and the meter_distance_between_WiFi_reference_points.
The reference point output is database_folder/site_name/WifiData/ReferencePoints/each_floor/reference_points.txt.

In reference_points.txt, for each line, it contains point_id, x, and y. And there is also reference_points.jpg in the same folder, which shows all the reference points in the map.

And it also generates a testSite folder in database_folder/site_name/WifiData/SiteFolderForSurveyApp/. This is used for wifi-site-survey app. And the file structure for testSite should be like this:

testSite
|--1001
  |--conf.ini
  |--map.jpg
  |--meta.txt
  |--pts.txt
  |--WifiData
|--1001_db.txt
|--1002
  |--conf.ini
  |--map.jpg
  |--meta.txt
  |--pts.txt
  |--WifiData
|--1002_db.txt
|--buildings_db.txt

In this case 1001 corresponds to 2F, while 1002 corresponds to 3F. This information can be seen in 1001_db.txt/1002_db.txt

##### 3. deploy WiFi survey app and grab fingerprint files from the app
* Place the testSite folder from PC to phone, inside external_storage_directory/WiFiCollectorNew/areas/.
* Turn off the internet of the phone, so that you don't need to log in while using the survey app.
* open the survey app WiFiCollectorNew.
* After you collect the WiFi data, you can see WifiData folder inside external_storage_directory/WiFiCollectorNew/areas/testSite/each_floor_id/. In the previous case, you will have folders:

external_storage_directory/WiFiCollectorNew/areas/testSite/1001/WiFiData/
external_storage_directory/WiFiCollectorNew/areas/testSite/1002/WiFiData/

* then grab this 2 folders from the phone to PC, name them as:

/..../folder1/,
/..../folder2/
(any name you like)

Then in info.json:
* set WiFi_survey_folders as:

"WiFi_survey_folders" : {
    "2F" : [
      "/..../folder1/"
    ],
    "3F" : [
      "/..../folder2/"
    ]
  },
  
If you use 2 phones to do survey, then you should add 2 folders inside each folder path inside "WiFi_survey_folders".

* Run main.Main with argument "copyWiFiRawData", then it will run main.WiFiProcess.CopyWiFiSurveyData.run().
This function will help you copy the raw data you just grab to the database_folder. Then the raw data is saved in /database_folder/site_name/WifiData/RawData/.

##### 4. generate WiFi fingerprint/database
* Run main.Main with argument "genWiFiFp", then it will run main.WiFiProcess.GenerateWiFiFingerprint.run().
This function will help you generate the WiFi fingerprint/database. 
The fingerprint file is saved in /database_folder/site_name/WifiData/FingerprintData/each_floor/fingerprint.txt. For each line in this file, the format is:

x,y,_,_ ap1,rssi1,_,_ ap2,rssi2,_,_ ......

Each line means one fingerprint(one direction in one reference point). In each line, components are seperated by space. The first component describe the position coordinate (x, y) of the fingerprint. The following each component means one ap signal, containing an ap bssid, and an rssi.

##### 5. take magnetic fingerprint and grab data from survey app
* Use the magnetic survey app to do magnetic site-survey. See the README.md in magnetic survey app to know how to deploy the magnetic survey app.
* Grab all the files from  external_storage_directory/MagBasedNavi/site_name/SurveyPath/magnetic/each_floor/fp_traces/ into the a PC folder, named folder1 (any name you like).

* Run main.Main with argument "copyMagRawData", then it will run main.MagProcess.CopyMagSurveyData.run().
This function will help you copy the raw data you just grab to the database_folder. Then the raw data is saved in /database_folder/site_name/MagData/RawData/.

##### 6. generate magnetic fingerprint/heatmap/database
* Set meter_distance_between_Magnetic_reference_points, which means the meter distance between magnetic reference points. Usually you can set it as 0.5, because magnetic field can change 0.5 meter away. And if we set it even smaller, the calculation time can be too much.

* Run main.Main with argument "genMagFp", then it will run main.MagProcess.GenerateMagFingerprint.run().
This function will help you generate the magnetic fingerprint/heatmap/database. 
The databse file is saved in 
/database_folder/site_name/MagData/Heatmap/heatmap.txt and 
/database_folder/site_name/MagData/Heatmap/neighbor.txt. 
For each line in heatmap.txt, the format is:

intensity vertical horizontal orientation x y floor_id

which means the magnetic reading in (x, y) in the floor(floor_id) is (intensity, vertical, horizontal). The orientation column is redundant, or is useless currently.

For each line in neighbor.txt, the format is:

neighbor1 neighbor2 neighbor3 ....

If this line NO. is 0, then this line means the neighbors of NO.0 magnetic fingerprint is the fingerprints with line NO. neighbor1, neighbor2, neighbor3, ..., and so on. Especially, the neighbors of each fingerprint should include itself.

In the same folder, there are a few pictures for you to check the database. 
heatmap_floor.jpg describes the fingerprints in a map. Different colors mean different magnetic readings. Red color means high readings while yellow color mean low readings.
connection_floor.jpg describes the neighbors of each fingerprint. If two reference points are connected in connection_floor.jpg, then they are neighbor to each other.

##### 7. move the database to phone folder
* For WiFi, for each floor, move /PC/database_folder/site_name/WifiData/FingerprintData/floor_id/fingerprint.txt to phone/MtrecLocOffline/site_name/WifiData/FingerprintData/floor_id/. No need to rename.
* For magnetic, move the heatmap.txt and neighbor.txt from PC/database_folder/site_name/MagData/Heatmap/ to phone/external_storage_directory/MtrecLocOffline/site_name/SurveyPath/magnetic/heatmap. No need to rename.
* Then run the client app, then we can do localization.
