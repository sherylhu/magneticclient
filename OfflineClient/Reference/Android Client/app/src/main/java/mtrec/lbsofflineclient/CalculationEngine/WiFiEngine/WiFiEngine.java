package mtrec.lbsofflineclient.CalculationEngine.WiFiEngine;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.CalculationEngine.AbstractEngine;
import mtrec.lbsofflineclient.model.ApSignal;
import mtrec.lbsofflineclient.model.WiFiVector;
import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.Position;

public class WiFiEngine extends AbstractEngine {
    private static final String tag = "MyWiFiEngine";

    // For PCCW Only
//    private static final Map<String, Integer> allSpecialAps = new HashMap<>();
//
//    static {
//        allSpecialAps.put("e6956e41a17a", -55);
//        allSpecialAps.put("e6956e41a18f", -55);
//        allSpecialAps.put("e6956e41a6e0", -50);
//        allSpecialAps.put("e6956e41a255", -47);
//    }

    private static final int getWifiPeriod = 100; //milliseconds
    private GetWifiVectorThread mGetWifiVectorThread = null;

    // the calculation time limitation for each period
    private static final long calculationPeriod = 100; //milliseconds

    private Context context;
    private WifiManager mWiFiManager;
    private WiFiBroadcastReceiver mWiFiBroadcastReceiver;

    private List<WiFiVector> allUserVectorCache = new ArrayList<>();
    private final Object allUserVectorCacheSynObj = new Object();
    private WiFiAlgo mWiFiAlgo;

    private WiFiSmooth wiFiSmooth = new WiFiSmooth();

    public WiFiEngine(Context context) {
        super(calculationPeriod);

        this.context = context;

        mWiFiAlgo = new WiFiAlgo();

        // start getting wifi data.
        mWiFiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mWiFiBroadcastReceiver = new WiFiBroadcastReceiver();
        context.registerReceiver(mWiFiBroadcastReceiver,
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        mGetWifiVectorThread = new GetWifiVectorThread();
        mGetWifiVectorThread.start();
    }

    private void init() {
        synchronized (allUserVectorCacheSynObj) {
            allUserVectorCache.clear();
        }
    }

    public void start() {
        init();
        super.start();
    }

    public void stop() {
        super.stop();
    }

    public void destroy() {
        context.unregisterReceiver(mWiFiBroadcastReceiver);
        super.destroy();
        mGetWifiVectorThread.interrupt();
        mGetWifiVectorThread = null;
    }

    private List<WiFiDetectionListener> mWiFiDetectionListeners = new ArrayList<>(); // the result listeners

    public void addDetectionListener(WiFiDetectionListener wiFiDetectionListener) {
        mWiFiDetectionListeners.add(wiFiDetectionListener);
    }

    private class GetWifiVectorThread extends Thread {
        /**
         * Scan the WiFi readings every {@link #getWifiPeriod}.
         * Actually the WiFi receiving period (the period that
         * {@link WiFiBroadcastReceiver#onReceive(Context, Intent)} is invoked) is always larger
         * than {@link #getWifiPeriod}.
         * But different devices may have different receiving periods, and we cannot estimate this
         * period for each device. So we scan frequently to push the device to receive WiFi as fast
         * as possible.
         * Another method is to scan immediately after receiving each WiFi signal.
         */
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    mWiFiManager.startScan();

                    Thread.sleep(getWifiPeriod);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

            }

        }

    }

    private class WiFiBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context c, Intent intent) {
            if (!WiFiEngine.this.isRunning()) {
                return;
            }

            WiFiVector wiFiVector = new WiFiVector();
//            String debugString = "getWifiVector: ";
            for (ScanResult result : mWiFiManager.getScanResults()) {
                String wifiApAdd = WiFiUtils.uniformApAddress(result.BSSID);
//                debugString += "（" + wifiApAdd + "&" + result.level + "&" + result.timestamp + "） ";
                wiFiVector.addApSignal(new ApSignal(wifiApAdd, result.level, result.timestamp));
            }
//            DebugTool.printLog(tag, debugString);

            // buffer the readings in the cache, then the engine thread would get the content in the cache
            synchronized (allUserVectorCacheSynObj) {
                allUserVectorCache.add(wiFiVector);
            }

        }

    }

    /**
     * This function is invoked by the inner thread of {@link AbstractEngine}
     * roughly every {@link #calculationPeriod}.
     * If the actual calculation time of this function is larger than {@link #calculationPeriod},
     * then this function would be invoked again immediately after last time it is invoked.
     * This period is set in the constructor {@link #WiFiEngine(Context)}.
     */
    @Override
    protected void process() {
        List<WiFiVector> dataCopy;

        synchronized (allUserVectorCacheSynObj) {
            dataCopy = allUserVectorCache;
            allUserVectorCache = new ArrayList<>();
        }

        if (dataCopy == null || dataCopy.isEmpty()) {
            return;
        }

        for (WiFiVector vector : dataCopy) {
            wiFiSmooth.addWiFiVector(vector);
        }

        WiFiVector smoothVector = wiFiSmooth.getCurWiFiVector();

        WiFiVector filterSmoothVector = new WiFiVector();

        // filter special aps, and find strong special ap
        String foundStrongSpecialAp = null;

        for (ApSignal signal : smoothVector.apSignalList) {
            // FOR PCCW demo area only. Not generally applicable
//            boolean found = false;
//
//            // loop all the special aps
//            for (Map.Entry<String, Integer> entry : allSpecialAps.entrySet()) {
//                String specialApAdd = entry.getKey();
//                int specialApRssi = entry.getValue();
//
//                if (signal.getApAddress().equals(specialApAdd)) {
//                    found = true;
//                    DebugTool.printLog(tag, "found special ap: " + specialApAdd + "&" + signal.getRssi());
//                    if (signal.getRssi() >= specialApRssi) {
////                            DebugTool.printLog(tag, "found strong special ap: " + specialApAdd + "&" + specialApRssi);
//                        foundStrongSpecialAp = specialApAdd;
//                    }
////                        break;
//                }
//            }
//
//            if (!found) {
//                filterSmoothVector.addApSignal(signal);
//            }

            filterSmoothVector.addApSignal(signal);
        }

        // FOR PCCW DEMO ONLY. Not generally applicable
//        for (WiFiDetectionListener wiFiDetectionListener : mWiFiDetectionListeners) {
//            wiFiDetectionListener.onApproachingSpecialAp(foundStrongSpecialAp);
//        }

        Map<String, List<Position>> areaIdMapTopPositions= mWiFiAlgo.detect(filterSmoothVector);
        for (WiFiDetectionListener wiFiDetectionListener : mWiFiDetectionListeners) {
            wiFiDetectionListener.onTopPositionsDetected(areaIdMapTopPositions);
        }

    }

}
