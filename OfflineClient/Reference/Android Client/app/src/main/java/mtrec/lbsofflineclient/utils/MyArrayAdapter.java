package mtrec.lbsofflineclient.utils;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.R;

public class MyArrayAdapter<T> extends ArrayAdapter<T> {
    private int defaultSetectedItemPosition;
    private boolean firstTime = true;
    public MyArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                          @NonNull List<T> objects, int defaultSetectedItemPosition) {
        super(context, resource, objects);
        this.defaultSetectedItemPosition = defaultSetectedItemPosition;
    }

    Map<Integer, View>itemIndexMapView = new HashMap<>();
    private int currentSelectedIndex = 0;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        DebugTool.printLog("hello", "position: " + position);
        itemIndexMapView.put(position, view);
        if (firstTime) {
            currentSelectedIndex = defaultSetectedItemPosition;
            firstTime = false;
        }
        view.setBackgroundColor(getContext().getResources().getColor(R.color.red));
//        select(currentSelectedIndex);
        return view;
    }

    public void select(int index) {
//        DebugTool.printLog("hello", "currentSelectedIndex: " + currentSelectedIndex);
//        currentSelectedIndex = index;
        for (Map.Entry<Integer, View>entry : itemIndexMapView.entrySet()) {
            int position = entry.getKey();
            View view = entry.getValue();
            if (position == index) {
                view.setBackgroundColor(getContext().getResources().getColor(R.color.blue));
            } else {
                view.setBackgroundColor(getContext().getResources().getColor(R.color.transparent));
            }
        }
    }
}
