package mtrec.lbsofflineclient.CalculationEngine.WiFiEngine;

import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.utils.Position;

public interface WiFiDetectionListener {
    void onApproachingSpecialAp(String ap);
    void onTopPositionsDetected(Map<String, List<Position>> topPositions);
}
