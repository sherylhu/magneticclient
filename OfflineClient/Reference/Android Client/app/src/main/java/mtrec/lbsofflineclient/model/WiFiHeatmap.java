package mtrec.lbsofflineclient.model;

import java.util.ArrayList;
import java.util.List;

public class WiFiHeatmap {
    public List<WiFiVector> getWiFiVectorList() {
        return wiFiVectorList;
    }

    List<WiFiVector>wiFiVectorList = new ArrayList<>();

    public void addWiFiVector(WiFiVector wiFiVector) {
        wiFiVectorList.add(wiFiVector);
    }
}
