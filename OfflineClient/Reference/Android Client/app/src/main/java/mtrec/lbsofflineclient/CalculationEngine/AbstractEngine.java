package mtrec.lbsofflineclient.CalculationEngine;

import android.util.Log;

import mtrec.lbsofflineclient.utils.DebugTool;

public abstract class AbstractEngine {
    private static final String tag = "MyAbstractEngine";

    // the control state of engine
    private enum State {
        started, stopped
    }
    private State curState = State.stopped;
    private final Object stateSynObj = new Object();

    public boolean isRunning() {
        return curState.equals(State.started);
    }

    private EngineThread mEngineThread;
    private long calculationPeriod;
    public AbstractEngine(long calculationPeriod) {
        this.calculationPeriod = calculationPeriod;
        mEngineThread = new EngineThread();
        mEngineThread.start();
    }

    public void start() {
        DebugTool.printLog(tag, "start() triggered");
        curState = State.started;
        synchronized (stateSynObj) {
            stateSynObj.notify();
        }
    }

    public void stop() {
        DebugTool.printLog(tag, "stop() triggered");
        curState = State.stopped;
    }

    public void destroy() {
        DebugTool.printLog(tag, "destroy() triggered");
        curState = State.stopped;
        mEngineThread.interrupt();
        mEngineThread = null;
    }

    protected long lastCalculationTime = 0;
    private class EngineThread extends Thread {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                // Wait until {@link #start()} is called.
                if (!curState.equals(AbstractEngine.State.started)) {
                    synchronized (stateSynObj) {
                        try {
                            stateSynObj.wait();
                            lastCalculationTime = System.currentTimeMillis();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                }

                /**
                 * We hope each calculation can process enough data, so we wait for a time period:
                 * {@link #calculationPeriod}.
                 * If {@link # calculationPeriod} is too short, data may be too few and then decrease
                 * CPU utility.
                 * And the calculation time should not large than
                 * {@link # calculationPeriod}, otherwise the calculation time would be longer and longer.
                 */
                long curTime = System.currentTimeMillis();
                long timeDiff = curTime - lastCalculationTime;
//                System.out.println("Calculation Time: " + timeDiff);
                if (timeDiff < calculationPeriod) {
                    try {
                        Thread.sleep(calculationPeriod - timeDiff);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
                }
                lastCalculationTime = System.currentTimeMillis();

                process();
            }
        }
    }

    protected abstract void process();
}
