package mtrec.lbsofflineclient.mapview;

import android.content.Context;
import android.graphics.BitmapRegionDecoder;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import mtrec.lbsofflineclient.R;
import mtrec.lbsofflineclient.utils.Position;

public class MapController {
    private static final String TAG = "MapController";



    public MapView getMapView() {
        return mapView;
    }

    private MapView mapView = null;
    private int width;
    private int height;

    private static final UUID locationResultUUID = UUID.randomUUID();

    private Drawable locationMarkDrawable;

    private Drawable iconDrawable;



    public MapController(Context context, String mapFilePath) {
        mapView = new MapView(context);

        try {
            File file = new File(mapFilePath);
            FileInputStream is = new FileInputStream(file);

            BitmapRegionDecoder bitmapRegionDecoder = BitmapRegionDecoder.newInstance(is, false);

            mapView.initNewMap(bitmapRegionDecoder);

            width = bitmapRegionDecoder.getWidth();
            height = bitmapRegionDecoder.getHeight();

            locationMarkDrawable = ContextCompat.getDrawable(context, R.drawable.location_mark);

            iconDrawable = ContextCompat.getDrawable(context, R.drawable.booth);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public int getWidth() {
        return width;
    }


    public int getHeight() {
        return height;
    }


    public void setDetectedLocation(Position position, double mapPixelAccuracy, int color) {
        mapView.setDetectedLocationPoint(
            new PointF((float)position.getX(), (float)position.getY()),
            mapPixelAccuracy,
            color
        );

    }


    public void clearDetectedLocation() {
        mapView.clearDetectedLocationPoint();
    }



    public void focus(Position location) {
        mapView.bringToCenter(new PointF((float) location.getX(), (float) location.getY()));
    }


    public void addFixedPoints(List<Position> positions, int paintColor) {
        List<PointF> allPointFs = new ArrayList<PointF>();
        for (Position position : positions) {
            allPointFs.add(new PointF((float) position.getX(), (float) position.getY()));
        }

        mapView.addFixedPoints(allPointFs, paintColor);
    }


    public void addFixedPoint(Position position, int paintColor) {
        mapView.addFixedPoint(new PointF((float) position.getX(), (float) position.getY()), paintColor);
    }


    public void clearFixedPoint() {
        mapView.clearFixedPoint();
    }


    public void addFixedText(Position position, int paintColor, String text) {
        mapView.addFixedText(new PointF((float) position.getX(), (float) position.getY()), paintColor, text);
    }


    public void addLine(Position position1, Position position2, int paintColor) {
        mapView.addLine(
            new PointF((float) position1.getX(), (float) position1.getY()),
            new PointF((float) position2.getX(), (float) position2.getY()),
            paintColor
        );

    }


    public void clearLines() {
        mapView.clearLines();
    }


    public void addStaticLine(Position position1, Position position2, int paintColor) {
        mapView.addStaticLine(
                new PointF((float) position1.getX(), (float) position1.getY()),
                new PointF((float) position2.getX(), (float) position2.getY()),
                paintColor
        );

    }


    public void clearStaticLines() { mapView.clearStaticLines(); }


    public void setMarkDrawable(Position pos) {
        mapView.setDynamicMarkDrawable(
            locationMarkDrawable,
            new PointF((float) pos.getX(), (float) pos.getY()),
            locationMarkDrawable.getIntrinsicWidth() / 10f,
            locationMarkDrawable.getIntrinsicHeight() / 10f
        );

    }


    public void clearMarkDrawable() {
        mapView.clearDynamicMarkDrawable();
    }


    public void addFixedDrawable(Drawable drawable, Position pos) {
        mapView.addFixedDrawable(
            drawable,
            new PointF((float) pos.getX(), (float) pos.getY()),
            iconDrawable.getIntrinsicWidth() / 5f,
            iconDrawable.getIntrinsicHeight() / 5f
        );

    }


    public void display() {
        mapView.display();
    }


    public void setOnMapClickListener(OnMapClickListener onMapClickListener) {
        mapView.setOnMapClickListener(onMapClickListener);
    }

}
