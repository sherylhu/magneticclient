package mtrec.lbsofflineclient.transaction;

import mtrec.lbsofflineclient.utils.Position;

public class Shop {
    public String icon;
    public String name;
    public String description;
    public Position location;

    public float gravityRadius = -1;

    public Shop(String icon, String name, String description, Position location) {
        this.icon = icon;
        this.name = name;
        this.description = description;
        this.location = location;
    }
}
