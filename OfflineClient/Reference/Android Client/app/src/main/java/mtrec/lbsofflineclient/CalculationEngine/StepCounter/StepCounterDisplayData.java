package mtrec.lbsofflineclient.CalculationEngine.StepCounter;

import mtrec.lbsofflineclient.model.AccelerometerData;

public class StepCounterDisplayData {
    public AccelerometerData rawData; // raw acceleration data
    public double rawNormData; // raw acceleration magnitude data
    public double smoothData; // smoothed acceleration data
    public double walkingData; // the raw the data which is declared as walking data.
    public double peakData; // the smoothed data which is declared as peak
    public double timestamp; //The timestamp(second) of the data.

    public StepCounterDisplayData(AccelerometerData rawData, double rawNormData, double smoothData,
                                  double walkingData, double peakData, double timestamp) {
        this.rawData = rawData;
        this.rawNormData = rawNormData;
        this.smoothData = smoothData;
        this.walkingData = walkingData;
        this.peakData = peakData;
        this.timestamp = timestamp;
    }
}
