package mtrec.lbsofflineclient.CalculationEngine.WiFiEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.CalculationEngine.StepCounter.StepCounterEngine;
import mtrec.lbsofflineclient.model.ApSignal;
import mtrec.lbsofflineclient.model.WiFiVector;
import mtrec.lbsofflineclient.utils.DebugTool;

public class WiFiSmooth {

    private static final String tag = "MyWiFiSmooth";
    private static final long signalLivePeriodWalking = 3000000L; // microsecond
    private static final long signalLivePeriodIdle = 15000000L; // microsecond

    private Map<String, List<ApSignal>> apSignalCache = new HashMap<>();
    private long newestTime = 0; // microsecond

    public WiFiSmooth() {
    }

    public void addWiFiVector(WiFiVector vector) {
        for (ApSignal signal : vector.apSignalList) {
            List<ApSignal> apSignalList = apSignalCache.get(signal.getApAddress());
            if (apSignalList == null) {
                apSignalList = new ArrayList<>();
                apSignalCache.put(signal.getApAddress(), apSignalList);
            }
            if (apSignalList.isEmpty()) {
                apSignalList.add(signal);
            } else {
                if (apSignalList.get(apSignalList.size() - 1).getMicroTimestamp() < signal.getMicroTimestamp()) {
                    apSignalList.add(signal);
                }
            }

            long receiveTime = signal.getMicroTimestamp();
            newestTime = (newestTime < receiveTime) ? receiveTime : newestTime;
        }
    }

    public WiFiVector getCurWiFiVector() {
//        DebugTool.printLog(tag, "************** start smooth WiFi *******************");
        if (newestTime == 0) {
            return null;
        }
        long signalLivePeriod;
        if (StepCounterEngine.walking) {
            signalLivePeriod = signalLivePeriodWalking;
        } else {
            signalLivePeriod = signalLivePeriodIdle;
        }

        WiFiVector answer = new WiFiVector();
        for (Map.Entry<String, List<ApSignal>> entry : apSignalCache.entrySet()) {
            String apAdd = entry.getKey();
            List<ApSignal> apSignalList = entry.getValue();
            if (apSignalList == null || apSignalList.isEmpty()) {
                continue;
            }
            int removeNum = 0;
            double totalRssi = 0;
            for (ApSignal signal : apSignalList) {
                if (newestTime - signal.getMicroTimestamp() > signalLivePeriod) {
                    removeNum++;
                } else {
                    totalRssi += signal.getRssi();
                }
            }
            if (apSignalList.size() - removeNum > 0) {
                double averageSignal = totalRssi / (apSignalList.size() - removeNum);
                answer.addApSignal(new ApSignal(apAdd, averageSignal));
//                DebugTool.printLog(tag, apAdd + ": " + (apSignalList.size() - removeNum) + " signals, average: " + averageSignal);
            }
//            DebugTool.printLog(tag, "before: apSignalList.size() = " + apSignalList.size());
            apSignalList.subList(0, removeNum).clear();
//            DebugTool.printLog(tag, "after: apSignalList.size() = " + apSignalList.size());
        }
//        DebugTool.printLog(tag, "Got " + answer.apSignalList.size() + " aps");
//        DebugTool.printLog(tag, "************** finish smooth WiFi *******************");
        return answer;
    }
}
