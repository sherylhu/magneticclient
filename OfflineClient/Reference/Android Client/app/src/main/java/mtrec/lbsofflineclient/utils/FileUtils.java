package mtrec.lbsofflineclient.utils;

import java.io.File;
import java.io.IOException;

public class FileUtils {
    private final static String tag = "FileUtils";

    public static boolean checkFolderExists(String folder) {
        File file = new File(folder);
        if (!file.exists() || !file.isDirectory()) {
            DebugTool.printLog(tag, "No such folder or it's not a folder: " + folder);
            return false;
        }
        return true;
    }
}
