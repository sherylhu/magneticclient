package mtrec.lbsofflineclient.utils;

import java.util.ArrayList;
import java.util.List;

public class ResultSmooth {
    private List<Position> allPositions = new ArrayList<>();
    static private final int wantedLength = 5;

    private long displayTime = 0;
    private Position displayPosition = null;

    public void addResult(Position result) {
        allPositions.add(result);

        if (allPositions.size() > wantedLength) {
            allPositions = allPositions.subList(allPositions.size() - wantedLength, allPositions.size());
        }
    }

    public void clear() {
        allPositions.clear();
        displayTime = 0;
        displayPosition = null;
    }

    public Position getNextRfPosition(double scale) {
        if (allPositions.size() > 0) {
            if (displayPosition != null) {
                Position newPosition = allPositions.get(allPositions.size() - 1).clone();

                double distance = newPosition.dist(displayPosition);
                double threshold = 2 * scale * (System.currentTimeMillis() - displayTime) / 1000; // Move 2m in 1 sec at most

                if (distance > threshold) {
                    newPosition.setX(displayPosition.getX() + (newPosition.getX() - displayPosition.getX()) * threshold / distance);
                    newPosition.setY(displayPosition.getY() + (newPosition.getY() - displayPosition.getY()) * threshold / distance);

                    displayTime = System.currentTimeMillis();
                    displayPosition = newPosition;

                    return displayPosition;
                } else {
                    displayTime = System.currentTimeMillis();
                    return displayPosition;
                }
            } else {
                displayTime = System.currentTimeMillis();
                displayPosition = allPositions.get(allPositions.size() - 1).clone();
                return displayPosition;
            }

        }

        return null;
    }
}
