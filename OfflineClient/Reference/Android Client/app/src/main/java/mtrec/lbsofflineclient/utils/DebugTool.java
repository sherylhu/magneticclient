package mtrec.lbsofflineclient.utils;

import android.util.Log;

public class DebugTool {
    public static void printLog(String tag, String content) {
        Log.d(tag, content);
    }
}
