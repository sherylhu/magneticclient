package mtrec.lbsofflineclient.model;

public class GravityData {
    public long nanoTimestamp;
    public float[] gravValues;
}
