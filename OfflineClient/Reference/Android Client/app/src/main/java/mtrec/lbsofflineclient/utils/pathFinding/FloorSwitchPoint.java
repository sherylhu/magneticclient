package mtrec.lbsofflineclient.utils.pathFinding;

import mtrec.lbsofflineclient.utils.Position;

public class FloorSwitchPoint {
    private static final String[] SWITCH_TYPES = new String[]{"lift", "escalator"};


    public Position location1;
    public Position location2;
    public double switchWeightInMeters;
    public String switchType;


    public FloorSwitchPoint(
        Position location1, Position location2,
        double switchWeightInMeters,
        String switchType
    ) {
        this.location1 = location1;
        this.location2 = location2;
        this.switchWeightInMeters = switchWeightInMeters;
        this.switchType = switchType;
    }
}
