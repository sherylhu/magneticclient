package mtrec.lbsofflineclient.utils;

import android.os.Parcel;
import android.os.Parcelable;

public class Position implements Parcelable {
    private double x = Double.NaN;
    private double y = Double.NaN;
    private String areaId = "";
    private String floorSwitchType = "";


    public Position(double x, double y) {
        this.x = x;
        this.y = y;
    }


    public Position (double x, double y, String areaId) {
        this.x = x;
        this.y = y;
        this.areaId = areaId;
    }


    public Position(double x, double y, String areaId, String floorSwitchType) {
        this.x = x;
        this.y = y;
        this.areaId = areaId;
        this.floorSwitchType = floorSwitchType;
    }


    public Position(String areaId) {
        this.areaId = areaId;
    }


    public Position() {}


    public boolean isNoneXY() {
        return Double.isNaN(x) || Double.isNaN(y);
    }


    public boolean isNoneArea() {
        return this.areaId.equals("");
    }


    public boolean isNone() {
        return isNoneXY() || isNoneArea();
    }


    public double getY() {
        return y;
    }


    public void setY(double y) {
        this.y = y;
    }


    public double getX() {
        return x;
    }


    public void setX(double x) {
        this.x = x;
    }


    public String getAreaId() {
        return areaId;
    }


    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }


    public String getFloorSwitchType() {
        return floorSwitchType;
    }


    @Override
    public String toString(){
        return String.format("(%.5f,%.5f,%s,%s)", x, y, areaId, floorSwitchType);
    }


    public boolean equals(Position other) {
        if (other == null) {
            return false;
        }

        return (Math.abs(x - other.x) < ConstantValues.EPS && Math.abs(y - other.y) < ConstantValues.EPS);
    }


    public Position add(Position other) {
        return new Position(x + other.x, y + other.y);
    }


    public Position multiple(double multipler) {
        return new Position(x * multipler, y * multipler);
    }


    public double dist(Position other) {
        double xDiff = x - other.x;
        double yDiff = y - other.y;
        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }


    public double getNorm() {
        return Math.sqrt(x * x + y * y);
    }


    public Position getMiddlePosition(Position other) {
        double middleX = (x + other.x) / 2.0;
        double middleY = (y + other.y) / 2.0;
        return new Position(middleX, middleY);
    }


    public double getSlope(Position other) throws Exception {
        if (Math.abs(x - other.x) < ConstantValues.EPS) {
            throw new Exception("unavailable slope between " + this + " and " + other);
        }
        return (y - other.y) / (x - other.x);
    }


    public double dist2Line(Position begin, Position end) {
        double cross = (end.x - begin.x) * (x - begin.x) + (end.y - begin.y) * (y - begin.y);
        if (cross <= 0) {
            return dist(begin);
        }

        double lineLenSquare = Math.pow(begin.dist(end), 2);
        if (cross >= lineLenSquare) {
            return dist(end);
        }

        double rate = cross / lineLenSquare;
        double px = begin.x + (end.x - begin.x) * rate;
        double py = begin.y + (end.y - begin.y) * rate;
        return dist(new Position(px, py));
    }


    public Position clone(){
        return new Position(x, y, areaId, floorSwitchType);
    }


    // Implements Parcelable
    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(x);
        out.writeDouble(y);
        out.writeString(areaId);
        out.writeString(floorSwitchType);
    }


    public static final Parcelable.Creator<Position> CREATOR = new Parcelable.Creator<Position>() {
        public Position createFromParcel(Parcel in) {
            return new Position(in);
        }


        public Position[] newArray(int size) {
            return new Position[size];
        }

    };


    private Position(Parcel in) {
        x = in.readDouble();
        y = in.readDouble();
        areaId = in.readString();
        floorSwitchType = in.readString();
    }

}
