package mtrec.lbsofflineclient.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.transaction.Shop;
import mtrec.lbsofflineclient.utils.pathFinding.FloorSwitchPoint;

public class InfoReader {
    private static final String tag = "InfoReader";


    public static Map<String, List<String>>floorsMapWiFiSurveyFolders;
    public static Map<String, String>floorsMapMagSurveyFolder;

    public static Map<String, List<String>> combineWiFiSiteNameMapFloors = new HashMap<>();
    public static Map<String, List<String>> combineMagSiteNameMapFloors = new HashMap<>();



    public static void init() {
        try {
            // load settings.json
            BufferedReader reader = new BufferedReader(new FileReader(new File(Settings.Paths.settingJson)));
            String line;
            StringBuffer stringBuffer = new StringBuffer();
            while ((line = reader.readLine()) != null) {
                stringBuffer.append(line);
            }
            reader.close();
            JSONObject jsonRootObject = new JSONObject(stringBuffer.toString());
            Settings.Site.siteName = jsonRootObject.getString("current_site");
            Settings.debugModeTurnOn = jsonRootObject.getJSONObject("debug_mode").getBoolean("turn_on");

            // load info.json for each site
            reader = new BufferedReader(new FileReader(new File(Settings.Site.getInfoJsonPath())));
            stringBuffer = new StringBuffer();
            while ((line = reader.readLine()) != null) {
                stringBuffer.append(line);
            }
            reader.close();
            jsonRootObject = new JSONObject(stringBuffer.toString());

            // load site_name
//            siteName = jsonRootObject.getString("site_name");
//            DebugTool.printLog(tag, "siteName: " + siteName);

            // load allFloors
            JSONArray floorsArray = jsonRootObject.getJSONArray("floors");
            for (int i = 0; i < floorsArray.length(); i++) {
                Settings.Site.allFloors.add(floorsArray.getString(i));
            }
            DebugTool.printLog(tag, "Settings.Site.allFloors: " + Settings.Site.allFloors.toString());

            // load settings
            JSONObject settingsObj = jsonRootObject.getJSONObject("settings");
            for (String floor : Settings.Site.allFloors) {
                JSONObject floorObj = settingsObj.getJSONObject(floor);
                float scale = (float) floorObj.getDouble("scale");
                Settings.Site.floorsMapScales.put(floor, scale);
            }

            // load WiFi
            JSONObject wiFiObj = jsonRootObject.getJSONObject("WiFi");
            Settings.Site.WiFi.meterDistBetweenWiFiRfPts = (float) wiFiObj.getDouble("meter_distance_between_WiFi_reference_points");
            DebugTool.printLog(tag, "WiFi.meterDistBetweenWiFiRfPts: " + Settings.Site.WiFi.meterDistBetweenWiFiRfPts);
            Settings.Site.WiFi.numberOfSelectedPositionWithTopPotential = wiFiObj.getInt("number_of_selected_position_with_top_potential");
            DebugTool.printLog(tag, "WiFi.numberOfSelectedPositionWithTopPotential: " + Settings.Site.WiFi.numberOfSelectedPositionWithTopPotential);
            Settings.Site.WiFi.percentage2DefineBigEnoughCluster = (float) wiFiObj.getDouble("percentage_to_define_a_big_enough_cluster");
            DebugTool.printLog(tag, "WiFi.percentage2DefineBigEnoughCluster: " + Settings.Site.WiFi.percentage2DefineBigEnoughCluster);

            // load meterDistBetweenMagRfPts
            JSONObject magObj = jsonRootObject.getJSONObject("magnetic");
            Settings.Site.Mag.meterDistBetweenMagRfPts = (float) magObj.getInt("meter_distance_between_Magnetic_reference_points");
            DebugTool.printLog(tag, "meterDistBetweenMagRfPts: " + Settings.Site.Mag.meterDistBetweenMagRfPts);
            Settings.Site.Mag.numberOfSelectedPositionWithTopPotential = magObj.getInt("number_of_selected_position_with_top_potential");
            DebugTool.printLog(tag, "Mag.numberOfSelectedPositionWithTopPotential: " + Settings.Site.Mag.numberOfSelectedPositionWithTopPotential);

            // load percentage_to_define_a_big_enough_cluster
            Settings.Site.Fusion.percentage2DefineBigEnoughCluster = (float) jsonRootObject.getJSONObject("fusion").getDouble("percentage_to_define_a_big_enough_cluster");
            DebugTool.printLog(tag, "percentage2DefineBigEnoughCluster: " + Settings.Site.Fusion.percentage2DefineBigEnoughCluster);

            // Load shops
            JSONArray shopArray = jsonRootObject.getJSONArray("shops");

            for (int shopIndex = 0; shopIndex < shopArray.length(); shopIndex++) {
                JSONObject shopObj = (JSONObject) shopArray.get(shopIndex);

                String shopIcon = shopObj.getString("icon");
                String shopName = shopObj.getString("name");
                String shopDes = shopObj.getString("description");

                JSONArray locArray = shopObj.getJSONArray("location");

                Position location = new Position(
                    locArray.getInt(0),
                    locArray.getInt(1),
                    locArray.getString(2)
                );

                Shop shop = new Shop(shopIcon, shopName, shopDes, location);

                // HACK: This value means that:
                //  If the detected location is [gravity_radius] away from the shop position,
                //  make it "very close to" the shop
                if (shopObj.has("gravity_radius")) {
                    shop.gravityRadius = (float)shopObj.getDouble("gravity_radius");
                }

                Settings.Site.shopList.add(shop);
            }

            // Sort shop alphabetically
            Collections.sort(Settings.Site.shopList, new Comparator<Shop>() {
                @Override
                public int compare(Shop s1, Shop s2) {
                    return s1.name.compareTo(s2.name);
                }

            });

            // load Connection points
            JSONObject connectionPointsObject = jsonRootObject.getJSONObject("path_finding").getJSONObject("connection_points");
            for (String floor : Settings.Site.allFloors) {
                List<Position>connectionPoints = new ArrayList<>();
                JSONArray connectionPointsArray = connectionPointsObject.getJSONArray(floor);
                for (int i = 0; i < connectionPointsArray.length(); i++) {
                    JSONArray connectionPoint = connectionPointsArray.getJSONArray(i);
                    double x = connectionPoint.getDouble(0);
                    double y = connectionPoint.getDouble(1);
                    Position connectionPos = new Position(x, y);
                    connectionPos.setAreaId(floor);
                    connectionPoints.add(connectionPos);
                }
                Settings.Site.floorMapConnectionPoints.put(floor, connectionPoints);
            }

            // Read switch points
            JSONArray switchPoints = connectionPointsObject.getJSONArray("floor_switch_points");

            for (int i = 0; i < switchPoints.length(); i++) {
                JSONObject switchPoint = switchPoints.getJSONObject(i);

                String switchType = switchPoint.getString("type");

                JSONArray location1Array = switchPoint.getJSONArray("location1");

                // Include switch type into position values
                Position location1 = new Position(
                    location1Array.getDouble(0),
                    location1Array.getDouble(1),
                    location1Array.getString(2),
                    switchType
                );

                JSONArray location2Array = switchPoint.getJSONArray("location2");

                Position location2 = new Position(
                    location2Array.getDouble(0),
                    location2Array.getDouble(1),
                    location2Array.getString(2),
                    switchType
                );

                double switchWeightInMeters = switchPoint.getDouble("switch_weight_in_meters");

                Settings.Site.floorSwitchPoints.add(
                    new FloorSwitchPoint(location1, location2, switchWeightInMeters, switchType)
                );

            }

            // Read area which allow floor-switching
            JSONArray floorSwitchArrays = jsonRootObject.getJSONArray("floor_switch_check");

            for (int i = 0; i < floorSwitchArrays.length(); ++i) {
                JSONObject switchGroup = floorSwitchArrays.getJSONObject(i);

                Iterator<String> switchGroupKeyIter = switchGroup.keys();

                while (switchGroupKeyIter.hasNext()) {
                    String floor = switchGroupKeyIter.next();

                    List<List<Position>> mapFloorSwitchableAreas;

                    if (!Settings.Site.floorSwitchAreas.containsKey(floor)) {
                        mapFloorSwitchableAreas = new ArrayList<>();
                        Settings.Site.floorSwitchAreas.put(floor, mapFloorSwitchableAreas);
                    } else {
                        mapFloorSwitchableAreas = Settings.Site.floorSwitchAreas.get(floor);
                    }

                    JSONArray areaArrays = switchGroup.getJSONArray(floor);

                    List<Position> area = new ArrayList<>();

                    for (int j = 0; j < areaArrays.length(); ++j) {
                        JSONArray coors = areaArrays.getJSONArray(j);

                        double x = coors.getDouble(0);
                        double y = coors.getDouble(1);

                        area.add(new Position(x, y, floor));

//                        Log.d(tag, floor + " " + String.valueOf(x) + " " + String.valueOf(y));
                    }

                    mapFloorSwitchableAreas.add(area);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean checkIfFormatCorrect() {
        if (Settings.Site.siteName == null || Settings.Site.siteName.equals("")) {
            DebugTool.printLog(tag, "no site_name or site_name is empty");
            return false;
        }
        if (Settings.Site.allFloors.isEmpty()) {
            DebugTool.printLog(tag, "no floors, pls input in info.json");
            return false;
        }
        if (Settings.Site.floorsMapScales.isEmpty()) {
            DebugTool.printLog(tag, "no scales, pls input in info.json");
            return false;
        }
        for (String floor : Settings.Site.allFloors) {
            if (!Settings.Site.floorsMapScales.containsKey(floor)) {
                DebugTool.printLog(tag, "You include floor " + floor + ", but no scale for this floor in info.json");
                return false;
            }
        }
        return true;
    }
}
