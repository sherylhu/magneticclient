package mtrec.lbsofflineclient.model;

public class ApSignal {
    private double rssi;
    private String apAddress;
    private long microTimestamp;

    public ApSignal(String apAddress, double rssi) {
        this.rssi = rssi;
        this.apAddress = apAddress;
        this.microTimestamp = -1;
    }

    public ApSignal(String apAddress, double rssi, long microTimestamp) {
        this.apAddress = apAddress;
        this.rssi = rssi;
        this.microTimestamp = microTimestamp;
    }

    public String getApAddress() {
        return apAddress;
    }

    public double getRssi() {
        return rssi;
    }

    public long getMicroTimestamp() {
        return microTimestamp;
    }
}
