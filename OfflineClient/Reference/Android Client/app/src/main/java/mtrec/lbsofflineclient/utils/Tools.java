package mtrec.lbsofflineclient.utils;

import android.content.Context;
import android.util.DisplayMetrics;

public class Tools {
    private static int screenW = -1, screenH = -1;

    public static int getScreenW(Context context) {
        if (screenW < 0) {
            initScreenDisplayParams(context);
        }
        return screenW;
    }

    public static int getScreenH(Context context) {
        if (screenH < 0) {
            initScreenDisplayParams(context);
        }
        return screenH;
    }

    private static void initScreenDisplayParams(Context context) {
    	DisplayMetrics dm = context.getApplicationContext().getResources().getDisplayMetrics();
        screenW = dm.widthPixels;
        screenH = dm.heightPixels;
    }
}
