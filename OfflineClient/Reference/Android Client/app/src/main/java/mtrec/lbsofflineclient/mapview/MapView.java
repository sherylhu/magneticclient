package mtrec.lbsofflineclient.mapview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import mtrec.lbsofflineclient.utils.ConstantValues;
import mtrec.lbsofflineclient.utils.Tools;

public class MapView extends GestureView {
	private static final String TAG = "MapView";


	private final float INITIAL_MATRIX[] = new float[] {
		4.129425f		, 0.0077566043f		, -1769.6367f,
		-0.0077566043f	, 4.129425f			, 15.729126f,
		0.0f			, 0.0f				, 1.0f
	};



	private Context mContext;

	private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();

	private Bitmap mapBitmap = null;

	private Matrix transformMatrix = new Matrix();
	private float accumScale = 1;
	private float accumAngleRotation = 0;

	private float heightScaleFromOrigin2Compressed = 1;
	private float widthScaleFromOrigin2Compressed = 1;

	private final Paint pointPaint = new Paint();

	private final Object detectedLocationPointSynObj = new Object();
	private DetectedLocationPoint detectedLocationPoint = null;

	private final Object detectedLocToDrawSyncObj = new Object();
	private DetectedLocationPoint detectedLocToDraw = null;

	private final Object allFixedPointsSynObj = new Object();
	private List<MapPoint2Draw> allFixedPoints = new ArrayList<MapPoint2Draw>();

	private final Object allFixedTextsSynObj = new Object();
	private List<MapText2Draw> allFixedTexts = new ArrayList<MapText2Draw>();

	private final Object allLinesSynObj = new Object();
	private List<Line2Draw> allLines = new ArrayList<Line2Draw>();

	private final Object allStaticLinesSynObj = new Object();
	private List<Line2Draw> allStaticLines = new ArrayList<>();

	private AnimationThread animateThread = new AnimationThread();

	private final Object dynamicMarkSynObj = new Object();
	private Mark2Draw dynamicMark = null;

	private final Object fixedDrawableSynObj = new Object();
	private List<Mark2Draw> fixedDrawables = new ArrayList<>();

	private boolean viewInitialized = false;
	private boolean showMarkLocation = false;

	private OnMapClickListener mOnMapClickListener = null;
	private OnFollowBlueDotAutoDisabled onFollowBlueDotAutoDisabled = null;

	private boolean reloadingFinished = false;
	private boolean followingBluedot = false;



	public MapView(Context context) {
		super(context);
		mContext = context;
		init();
	}


	public MapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		init();
	}


	public MapView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
		init();
	}


	private void init() {
		pointPaint.setStyle(Paint.Style.FILL);
		pointPaint.setAntiAlias(true);

		rwLock.writeLock().lock();

		transformMatrix.setValues(INITIAL_MATRIX);

		PointF viewOrigin = convertMapPointToViewPoint(new PointF(0, 0), transformMatrix);
		PointF viewUnitX = convertMapPointToViewPoint(new PointF(1, 0), transformMatrix);

		accumScale = (float)Math.sqrt(
			Math.pow(viewUnitX.x - viewOrigin.x, 2)
			+ Math.pow(viewUnitX.y - viewOrigin.y, 2)
		);

		rwLock.writeLock().unlock();
	}


	public void initNewMap(BitmapRegionDecoder bitmapRegionDecoder) {
		int height = bitmapRegionDecoder.getHeight();
		int width = bitmapRegionDecoder.getWidth();

		Rect rect = new Rect(0, 0, width, height); // show the whole range
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// down-sample the original picture to around 1024 pixels
		options.inSampleSize = Math.round(Math.max(width, height) / 1024f);

		mapBitmap = bitmapRegionDecoder.decodeRegion(rect, options);
		heightScaleFromOrigin2Compressed *= 1f * mapBitmap.getHeight() / height;
		widthScaleFromOrigin2Compressed *= 1f * mapBitmap.getWidth() / width;

		viewInitialized = true;

		invalidate();
	}


	public void reload() {
		reloadingFinished = false;
	}


	public void enableFollowingBlueDot(boolean enabled) {
		followingBluedot = enabled;
	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (!viewInitialized) {
			return;
		}

		rwLock.readLock().lock();

		canvas.drawBitmap(mapBitmap, transformMatrix, null);

		synchronized (allLinesSynObj) {
			for (Line2Draw line : allLines) {
				drawLine(
					canvas,
					convertMapPointToViewPoint(line.beginPoint, transformMatrix),
					convertMapPointToViewPoint(line.endPoint, transformMatrix),
					line.paintColor
				);

			}

		}

		synchronized (allStaticLinesSynObj) {
			for (Line2Draw line : allStaticLines) {
				drawLine(
					canvas,
					convertMapPointToViewPoint(line.beginPoint, transformMatrix),
					convertMapPointToViewPoint(line.endPoint, transformMatrix),
					line.paintColor
				);

			}

		}

		synchronized (allFixedPointsSynObj) {
			float fixedPointRadius = Tools.getScreenW(mContext) / ConstantValues.SUPPLEMENTING_POINT_RADIUS_TO_SCREEN;

			for (MapPoint2Draw point2Draw : allFixedPoints) {
				PointF point = convertMapPointToViewPoint(point2Draw.point, transformMatrix);
				drawPoint(canvas, new MapPoint2Draw(point, point2Draw.paintColor), fixedPointRadius);
			}

		}

        synchronized (fixedDrawableSynObj) {
            for (Mark2Draw mark2Draw : fixedDrawables) {
                drawMarkOnCenter(
					canvas,
					new Mark2Draw(
						mark2Draw.drawable,
                        convertMapPointToViewPoint(mark2Draw.location, transformMatrix),
                        mark2Draw.width,
						mark2Draw.height
					),
					transformMatrix
				);

            }

        }

		synchronized (allFixedTextsSynObj) {
			for (MapText2Draw text2Draw : allFixedTexts) {
				PointF point = convertMapPointToViewPoint(text2Draw.point, transformMatrix);
				drawText(canvas, new MapText2Draw(point, text2Draw.paintColor, text2Draw.text));
			}

		}

		synchronized (dynamicMarkSynObj) {
			if (dynamicMark != null) {
				drawMark(
					canvas,
					new Mark2Draw(
						dynamicMark.drawable,
						convertMapPointToViewPoint(dynamicMark.location, transformMatrix),
						dynamicMark.width,
						dynamicMark.height
					),
					transformMatrix
				);

			}

		}

		synchronized (detectedLocToDrawSyncObj) {
			if (detectedLocToDraw != null) {
				PointF detectedLocViewPoint = convertMapPointToViewPoint(
					detectedLocToDraw.point,
					transformMatrix
				);

				// Draw error circle first
				pointPaint.setColor(detectedLocToDraw.errorRangeColor);

				canvas.drawCircle(
						detectedLocViewPoint.x, detectedLocViewPoint.y,
						(float)detectedLocToDraw.mapPixelAccuracy * accumScale,
						pointPaint
				);

				//Log.d(TAG, String.valueOf((float)detectedLocToDraw.mapPixelAccuracy * accumScale) + " " + String.valueOf(accumScale));

				// Draw main point above it
				float detectedLocPointRadius =
					Tools.getScreenW(mContext) / ConstantValues.LOCATION_POINT_RADIUS_TO_SCRREEN;

				drawPoint(
					canvas,
					new MapPoint2Draw(detectedLocViewPoint, Color.WHITE),
					detectedLocPointRadius + 5
				);

				drawPoint(
					canvas,
					new MapPoint2Draw(detectedLocViewPoint, detectedLocToDraw.paintColor),
					detectedLocPointRadius
				);

			}

		}

		rwLock.readLock().unlock();
	}


	@Override
	protected void onClick(float x, float y) {
		rwLock.readLock().lock();

		PointF mapPoint = convertViewPointToMapPoint(new PointF(x, y), transformMatrix);

		rwLock.readLock().unlock();

		mapPoint = convertMapPointToOriginalMapPoint(mapPoint);

		if (mOnMapClickListener != null) {
			mOnMapClickListener.onClick(mapPoint);
		}
	}


	@Override
	protected void onGestureDetected(float[] translate, float[] scale, float[] rotate) {
		rwLock.writeLock().lock();

		transformMatrix.postTranslate(translate[0], translate[1]);
		transformMatrix.postScale(scale[0], scale[1], scale[2], scale[3]);
		transformMatrix.postRotate(rotate[0], rotate[1], rotate[2]);

		accumScale *= scale[0];
		accumAngleRotation += rotate[0];

		rwLock.writeLock().unlock();

		invalidate();

		// Automatically disable blue-dot following
		followingBluedot = false;

		if (onFollowBlueDotAutoDisabled != null) {
			onFollowBlueDotAutoDisabled.onFollowingDisabled();
		}

		animateThread.cancelExceptBlueDotMoving();
	}


	public void bringToCenter(PointF originMapPointF) {
		final PointF convertedPoint = convertOriginalMapPointToMapPoint(originMapPointF);

		if (reloadingFinished) {
			bringMapPointToCenter(convertedPoint);
		} else {
			// The layout is not ready
			postDelayed(new Runnable() {
				@Override
				public void run() {
					bringMapPointToCenter(convertedPoint);
				}

			}, 50);

		}

	}


	private void bringMapPointToCenter(PointF mapPointF) {
		Thread.State state = animateThread.getState();

		if (state.equals(Thread.State.TERMINATED)) {
			animateThread = new AnimationThread();
			state = animateThread.getState();
		}

		animateThread.setMapPointToCenter(mapPointF);

		if (state.equals(Thread.State.NEW)) {
			animateThread.start();
		}

	}


	public void display() {
		postInvalidate();
	}


	private PointF convertMapPointToViewPoint(PointF point, Matrix matrix) {
		float[] mapPoint = new float[] { point.x, point.y };
		matrix.mapPoints(mapPoint);

		return new PointF(mapPoint[0], mapPoint[1]);
	}


	private PointF convertViewPointToMapPoint(PointF point, Matrix matrix) {
		Matrix invertMatrix = new Matrix();
		matrix.invert(invertMatrix);
		float[] result = new float[] { point.x, point.y };
		invertMatrix.mapPoints(result);

		return new PointF(result[0], result[1]);
	}


	private PointF convertOriginalMapPointToMapPoint(PointF point) {
		return new PointF(
				point.x * widthScaleFromOrigin2Compressed,
				point.y * heightScaleFromOrigin2Compressed
		);

	}


	private PointF convertMapPointToOriginalMapPoint(PointF point) {
		return new PointF(
				point.x / widthScaleFromOrigin2Compressed,
				point.y / heightScaleFromOrigin2Compressed
		);

	}


	public void setOnMapClickListener (OnMapClickListener onMapClickListener) {
		mOnMapClickListener = onMapClickListener;
	}


	public void setOnFollowBlueDotAutoDisabled(OnFollowBlueDotAutoDisabled e) {
		this.onFollowBlueDotAutoDisabled = e;
	}


	private void drawLine(Canvas canvas, PointF beginPoint, PointF endPoint, int color) {
		pointPaint.setColor(color);
        pointPaint.setStrokeWidth(10);

		canvas.drawLine(beginPoint.x, beginPoint.y, endPoint.x, endPoint.y, pointPaint);
	}


	private void drawPoint(Canvas canvas, MapPoint2Draw point2Draw, float radius) {
		pointPaint.setColor(point2Draw.paintColor);

		canvas.drawCircle(
			point2Draw.point.x, point2Draw.point.y,
			radius,
			pointPaint
		);

	}


	private void drawText(Canvas canvas, MapText2Draw text2Draw) {
		pointPaint.setColor(text2Draw.paintColor);
		pointPaint.setTextSize(50);
        pointPaint.setTextAlign(Paint.Align.CENTER);

        // Make bold text
        pointPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

		canvas.drawText(text2Draw.text, text2Draw.point.x, text2Draw.point.y, pointPaint);
	}


	private void drawMark(Canvas canvas, Mark2Draw mk, Matrix matrix) {
		PointF position = mk.location;

		mk.drawable.setBounds(
			(int) (position.x - mk.width / 2f), (int) (position.y - mk.height),
			(int) (position.x + mk.width / 2f), (int) (position.y)
		);

		mk.drawable.draw(canvas);
		
		if (showMarkLocation) {
			PointF point = convertViewPointToMapPoint(mk.location, matrix);
			point = convertMapPointToOriginalMapPoint(point);

			pointPaint.setTextSize(50);

			canvas.drawText("" + point.x + "," + point.y, 0, 50, pointPaint);
		}

	}


    private void drawMarkOnCenter(Canvas canvas, Mark2Draw mk, Matrix matrix) {
        PointF position = mk.location;

        mk.drawable.setBounds(
			(int) (position.x - mk.width / 2f), (int) (position.y - mk.height / 2f),
			(int) (position.x + mk.width / 2f), (int) (position.y + mk.height / 2f)
		);

        mk.drawable.draw(canvas);

        if (showMarkLocation) {
            PointF point = convertViewPointToMapPoint(mk.location, matrix);
            point = convertMapPointToOriginalMapPoint(point);

            pointPaint.setTextSize(50);

            canvas.drawText("" + point.x + "," + point.y, 0, 50, pointPaint);
        }

    }


	public void addFixedPoints(List<PointF> allFixedPoints, int paintColor) {
		synchronized (allFixedPointsSynObj) {
			for (PointF point : allFixedPoints) {
				MapPoint2Draw mapPoint2Draw = new MapPoint2Draw(
					convertOriginalMapPointToMapPoint(point),
					paintColor
				);

				this.allFixedPoints.add(mapPoint2Draw);
			}

		}

	}


	public void addFixedPoint(PointF fixedPoint, int paintColor) {
		synchronized (allFixedPointsSynObj) {
			MapPoint2Draw mapPoint2Draw = new MapPoint2Draw(
				convertOriginalMapPointToMapPoint(fixedPoint),
					paintColor
			);

			this.allFixedPoints.add(mapPoint2Draw);
		}

	}


	public void clearFixedPoint() {
		synchronized (allFixedPointsSynObj) {
			allFixedPoints.clear();
		}

	}


	public void addFixedText(PointF fixedPoint, int paintColor, String text) {
		synchronized (allFixedTextsSynObj) {
			MapText2Draw mapText2Draw = new MapText2Draw(
				convertOriginalMapPointToMapPoint(fixedPoint),
				paintColor,
				text
			);

			this.allFixedTexts.add(mapText2Draw);
		}

	}


	public void addLine(PointF point1, PointF point2, int paintColor) {
		synchronized (allLinesSynObj) {
			allLines.add(
				new Line2Draw(
					convertOriginalMapPointToMapPoint(point1),
					convertOriginalMapPointToMapPoint(point2),
					paintColor
				)

			);

		}

	}


	public void clearLines() {
		synchronized (allLinesSynObj) {
			allLines.clear();
		}

	}


	public void addStaticLine(PointF point1, PointF point2, int paintColor) {
		synchronized (allStaticLinesSynObj) {
			allStaticLines.add(
					new Line2Draw(
							convertOriginalMapPointToMapPoint(point1),
							convertOriginalMapPointToMapPoint(point2),
							paintColor
					)

			);

		}

	}


	public void clearStaticLines() {
		synchronized (allStaticLinesSynObj) {
			allStaticLines.clear();
		}

	}


	public void setDynamicMarkDrawable(Drawable drawable, PointF originMapLocation, float width, float height) {
		PointF mapPoint = convertOriginalMapPointToMapPoint(originMapLocation);
		
		synchronized (dynamicMarkSynObj) {
			dynamicMark = new Mark2Draw(drawable, mapPoint, width, height);
		}

		postInvalidate();
	}


	public void clearDynamicMarkDrawable() {
        synchronized (dynamicMarkSynObj) {
            dynamicMark = null;
        }

		postInvalidate();
	}


	public void setDetectedLocationPoint(PointF originMapLocation, double originMapPixelAccuracy, int color) {
		PointF mapPoint = convertOriginalMapPointToMapPoint(originMapLocation);
		double mapPixelAccuracy = originMapPixelAccuracy * widthScaleFromOrigin2Compressed;

		DetectedLocationPoint prev;

		synchronized (detectedLocationPointSynObj) {
			prev = detectedLocationPoint;
			detectedLocationPoint = new DetectedLocationPoint(mapPoint, mapPixelAccuracy, color);
		}

		if (followingBluedot) {
			bringMapPointToCenter(mapPoint);
		}

		Thread.State state = animateThread.getState();

		if (state.equals(Thread.State.TERMINATED)) {
			animateThread = new AnimationThread();
			state = animateThread.getState();
		}

		animateThread.setBlueDotMapVector(prev, detectedLocationPoint);

		if (state.equals(Thread.State.NEW)) {
			animateThread.start();
		}

	}


	public void clearDetectedLocationPoint() {
		synchronized (detectedLocationPointSynObj) {
			detectedLocationPoint = null;
		}

		postInvalidate();
	}


	public void addFixedDrawable(Drawable drawable, PointF originMapLocation, float width, float height) {
		PointF mapPoint = convertOriginalMapPointToMapPoint(originMapLocation);

		synchronized (fixedDrawableSynObj) {
            fixedDrawables.add(new Mark2Draw(drawable, mapPoint, width, height));
		}

	}


	public void showMarkLocation(boolean showMarkLocation) {
		this.showMarkLocation = showMarkLocation;
	}


	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		reloadingFinished = true;
	}


	private class Mark2Draw {
		public Mark2Draw(Drawable drawable, PointF location, float width, float height) {
			this.drawable = drawable;
			this.location = location;
			this.width = width;
			this.height = height;
		}

		Drawable drawable;
		PointF location;
		float width;
		float height;
	}


	private class Line2Draw {
		PointF beginPoint, endPoint;
		int paintColor;

		public Line2Draw(PointF point1, PointF point2, int paintColor) {
			beginPoint = point1;
			endPoint = point2;
			this.paintColor = paintColor;
		}

	}


	private class MapPoint2Draw {
		PointF point;
		int paintColor;

		public MapPoint2Draw(PointF point, int color) {
			this.point = point;
			this.paintColor = color;
		}

	}


	private class MapText2Draw {
		PointF point;
		int paintColor;
		String text;

		public MapText2Draw(PointF point, int color, String text) {
			this.point = point;
			this.paintColor = color;
			this.text = text;
		}

	}


	private class DetectedLocationPoint {
		PointF point;
		double mapPixelAccuracy = 0;
		int paintColor, errorRangeColor;


		public DetectedLocationPoint(PointF point, double mapPixelAccuracy, int paintColor) {
			this.point = point;

			this.mapPixelAccuracy = mapPixelAccuracy;

			this.paintColor = paintColor;

			// Create color for error range based on given paintColor
			float[] hsv = new float[3];
			Color.colorToHSV(this.paintColor, hsv);
			hsv[1] *= 0.7;

			this.errorRangeColor = Color.HSVToColor(60, hsv);
		}

	}


	private class AnimationThread extends Thread {
		private boolean cancelExceptMovingBlueDot = false;

		private final Object centerSynObj = new Object();
		private PointF mapPointToCenter = null;

		private final Object blueDotMoveSynObj = new Object();
		private DetectedLocationPoint fromBlueDotMapPoint = null;
		private DetectedLocationPoint toBlueDotMapPoint = null;

		private final Object angleSynObj = new Object();
		private float angleMapToTop = Float.NaN;		//(+) = Clockwise from (+) x-axis



		public void setMapPointToCenter(PointF mapPointToCenter) {
			synchronized (centerSynObj) {
				this.mapPointToCenter = mapPointToCenter;
			}

		}


		public void setBlueDotMapVector(
			DetectedLocationPoint fromMapPoint,
			DetectedLocationPoint toMapPoint
		) {
			synchronized (blueDotMoveSynObj) {
				if (fromBlueDotMapPoint == null) {
					fromBlueDotMapPoint = fromMapPoint;
				}

				toBlueDotMapPoint = toMapPoint;
			}

		}


		public void setAngleMapToTop(float angleMap) {
			synchronized (angleSynObj) {
				angleMapToTop = angleMap;
			}

		}


		public void cancelExceptBlueDotMoving() {
			cancelExceptMovingBlueDot = true;
		}


		@Override
		public void run() {
			// Wait until everything is initialized
			while (!viewInitialized) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

			PointF centerViewPoint = new PointF(getWidth() / 2f, getHeight() / 2f);

			while (true) {
				rwLock.writeLock().lock();

				boolean animationFinished = true;

				// Animate centering and rotating effect
				if (!cancelExceptMovingBlueDot) {
					synchronized (centerSynObj) {
						if (mapPointToCenter != null) {
							PointF viewPointToCenter = convertMapPointToViewPoint(
									mapPointToCenter, transformMatrix
							);

							float stepX = (centerViewPoint.x - viewPointToCenter.x) / 2f;
							float stepY = (centerViewPoint.y - viewPointToCenter.y) / 2f;

							if (Math.abs(stepX) > 1 || Math.abs(stepY) > 1) {
								transformMatrix.postTranslate(stepX, stepY);

								animationFinished = false;
							}

						}

					}

					synchronized (angleSynObj) {
						if (!Float.isNaN(angleMapToTop)) {
							float angleViewToTop = accumAngleRotation + angleMapToTop;

							// Standardize angle to (-180; 180]
							while (angleViewToTop <= -180) {
								angleViewToTop += 360;
							}

							while (angleViewToTop > 180) {
								angleViewToTop -= 360;
							}

							// The vector described by angleViewToTop to pointing upward
							float step = (-90 - angleViewToTop) / 2f;

							if (Math.abs(step) > 1e-2) {
								transformMatrix.postRotate(
									step, centerViewPoint.x, centerViewPoint.y
								);

								accumAngleRotation += step;

								animationFinished = false;
							}

						}

					}

				}

				rwLock.writeLock().unlock();

				rwLock.readLock().lock();

				// Animate blue-dot moving to new position
				synchronized (blueDotMoveSynObj) {
					if (toBlueDotMapPoint != null) {
						DetectedLocationPoint pointToDraw;

						if (fromBlueDotMapPoint != null) {
							float stepX =
								(toBlueDotMapPoint.point.x - fromBlueDotMapPoint.point.x) / 2f;

							float stepY =
								(toBlueDotMapPoint.point.y - fromBlueDotMapPoint.point.y) / 2f;

							if (Math.abs(stepX) > 1 || Math.abs(stepY) > 1) {
								fromBlueDotMapPoint.point.x += stepX;
								fromBlueDotMapPoint.point.y += stepY;

								animationFinished = false;
							}

							double stepRangeAccuracy = toBlueDotMapPoint.mapPixelAccuracy
									- fromBlueDotMapPoint.mapPixelAccuracy;

							if (stepRangeAccuracy > 1e-1) {
								fromBlueDotMapPoint.mapPixelAccuracy += stepRangeAccuracy;

								animationFinished = false;
							}

							pointToDraw = fromBlueDotMapPoint;
						} else {
							pointToDraw = toBlueDotMapPoint;
						}

						synchronized (detectedLocToDrawSyncObj) {
							detectedLocToDraw = new DetectedLocationPoint(
									new PointF(pointToDraw.point.x, pointToDraw.point.y),
									pointToDraw.mapPixelAccuracy,
									pointToDraw.paintColor
							);

						}

					}

				}

				rwLock.readLock().unlock();

				postInvalidate();

				if (animationFinished) {
					break;
				}

				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

		}

	}

}
