package mtrec.lbsofflineclient.CalculationEngine.WiFiEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.model.ApSignal;
import mtrec.lbsofflineclient.utils.ConstantValues;

public class CosineSimilarity {
    public static double run(List<ApSignal>userSignals, List<ApSignal>fpSignals) throws Exception {
        Map<String, Double> userApAddRssiMap = buildApAddressAndRssiMap(userSignals); // user's key-value pair: ap_address -> rssi
        Map<String, Double> fpApAddRssiMap = buildApAddressAndRssiMap(fpSignals); // fingerprint's key-value pair: ap_address -> rssi
        List<CosineDotProductTuple>cosineDotProductTuples = new ArrayList<>();
        int intersectionNum = 0;
        for (String userApAddress : userApAddRssiMap.keySet()) {
            if (fpApAddRssiMap.containsKey(userApAddress)) {
                cosineDotProductTuples.add(
                        new CosineDotProductTuple(
                                WiFiUtils.signalDBM2Watt(userApAddRssiMap.get(userApAddress)),
                                WiFiUtils.signalDBM2Watt(fpApAddRssiMap.get(userApAddress))
                        )
                );
                intersectionNum++;
            } else {
                cosineDotProductTuples.add(
                        new CosineDotProductTuple(
                                WiFiUtils.signalDBM2Watt(userApAddRssiMap.get(userApAddress)),
                                0
                        )
                );
            }
        }

        if (intersectionNum == 0) {
            return 0;
        }
        double cosineSimilarity = getCosineSimilarityFromTuples(cosineDotProductTuples);
        if ((Math.abs(cosineSimilarity - 0) < ConstantValues.EPS)) {
            cosineSimilarity = 0;
        } else if (cosineSimilarity < 0) {
            throw new Exception("cosine similarity becomes < 0");
        }
        if ((Math.abs(cosineSimilarity - 1) < ConstantValues.EPS)) {
            cosineSimilarity = 1;
        } else if (cosineSimilarity > 1) {
            throw new Exception("cosine similarity becomes > 1");
        }
        return cosineSimilarity;
    }

    public static Map<String, Double> buildApAddressAndRssiMap(List<ApSignal>signals) {
        Map<String, Double>answer = new HashMap<>();
        for (ApSignal rfSignal : signals) {
            answer.put(rfSignal.getApAddress(), rfSignal.getRssi());
        }
        return answer;
    }

    private static double getCosineSimilarityFromTuples(List<CosineDotProductTuple>cosineDotProductTuples) {
        double tupleSum = 0;
        double tupleSum1 = 0;
        double tupleSum2 = 0;
        for (CosineDotProductTuple tuple :cosineDotProductTuples) {
            tupleSum += tuple.getUserRssi() * tuple.getFpRssi();
            tupleSum1 += tuple.getUserRssi() * tuple.getUserRssi();
            tupleSum2 += tuple.getFpRssi() * tuple.getFpRssi();
        }
        return tupleSum / Math.sqrt(tupleSum1 * tupleSum2);
    }

    private static class CosineDotProductTuple {
        public CosineDotProductTuple(double userRssi, double fpRssi) {
            this.userRssi = userRssi;
            this.fpRssi = fpRssi;
        }

        private double userRssi;

        private double fpRssi;

        public double getUserRssi() {
            return userRssi;
        }

        public void setUserRssi(double userRssi) {
            this.userRssi = userRssi;
        }

        public double getFpRssi() {
            return fpRssi;
        }

        public void setFpRssi(double fpRssi) {
            this.fpRssi = fpRssi;
        }
    }
}
