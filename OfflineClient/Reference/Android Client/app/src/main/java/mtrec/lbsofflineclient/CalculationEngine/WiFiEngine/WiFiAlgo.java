package mtrec.lbsofflineclient.CalculationEngine.WiFiEngine;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import mtrec.lbsofflineclient.model.ApSignal;
import mtrec.lbsofflineclient.model.WiFiHeatmap;
import mtrec.lbsofflineclient.model.WiFiVector;
import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.Position;
import mtrec.lbsofflineclient.utils.Settings;

public class WiFiAlgo {
    private final static String tag = "MyWiFiAlgo";

    public static int requiredTopSimilaritiesNum = 40;

    private List<String> allAreaIds;
    private WiFiHeatmap allWiFiHeatmap = new WiFiHeatmap();
    private Set<String> allWiFiApAdds = new HashSet<>();

    public WiFiAlgo() {
        try {
            allAreaIds = Settings.Site.allFloors;

            for (Map.Entry<String, WiFiHeatmap>entry : HeatmapLoader.load().entrySet()) {
                for (WiFiVector wiFiVector : entry.getValue().getWiFiVectorList()) {
                    allWiFiHeatmap.addWiFiVector(wiFiVector);
                    for (ApSignal signal : wiFiVector.apSignalList) {
                        allWiFiApAdds.add(signal.getApAddress());
                    }
                }
            }
        } catch (Exception e) {
            DebugTool.printLog(tag, "Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Detect the positions whose corresponding fingerprints have the largest similarities to the
     * user input.
     * These positions would be separated into different floors before return.
     *
     * @param userVector the user input reading
     * @return the positions whose corresponding fingerprints have the largest similarities to the
     * user input. The format is {@link Map}, which maps floorId --> the positions in that floor.
     */
    public Map<String, List<Position>> detect(WiFiVector userVector) {
        filterUselessAps(userVector);

        Map<String, List<Position>> areaIdMapTopPositions = new HashMap<>();
        for (String areaId : allAreaIds) {
            areaIdMapTopPositions.put(areaId, new ArrayList<Position>());
        }
        try {
            List<WiFiVector> allFpVectorList = allWiFiHeatmap.getWiFiVectorList();
            for (FpVectorIndexAndSimilarity fpVectorIndexAndSimilarity : getTopCandidates(userVector)) {
                WiFiVector wiFiVector = allFpVectorList.get(fpVectorIndexAndSimilarity.fpVectorIndex);
                areaIdMapTopPositions.get(wiFiVector.floorId).add(wiFiVector.pos);
            }
        } catch (Exception e) {
//            DebugTool.printLog(tag, "Exception: " + e.getMessage());
            e.printStackTrace();
        }
        return areaIdMapTopPositions;
    }

    private void filterUselessAps(WiFiVector userVector) {
        List<ApSignal>newApSignalList = new ArrayList<>();
        for (ApSignal signal : userVector.apSignalList) {
            if (allWiFiApAdds.contains(signal.getApAddress())) {
                newApSignalList.add(signal);
            }
        }
        userVector.apSignalList = newApSignalList;
    }

    private Queue<FpVectorIndexAndSimilarity> getTopCandidates(WiFiVector userVector) throws Exception {
        List<WiFiVector> curFpVectorList = allWiFiHeatmap.getWiFiVectorList();
        List<FpVectorIndexAndSimilarity> fpVectorIndexAndSimilarities = new ArrayList<>();
        for (int i = 0; i < curFpVectorList.size(); i++) {
            WiFiVector fpVector = curFpVectorList.get(i);
            fpVectorIndexAndSimilarities.add(new FpVectorIndexAndSimilarity(
                    i, CosineSimilarity.run(userVector.apSignalList, fpVector.apSignalList)
            ));
        }

        int localTopSimilaritiesNum = (requiredTopSimilaritiesNum < fpVectorIndexAndSimilarities.size()) ?
                requiredTopSimilaritiesNum : fpVectorIndexAndSimilarities.size();

        PriorityQueue<FpVectorIndexAndSimilarity> topSimilaritiesMinHeap = new PriorityQueue<FpVectorIndexAndSimilarity>(
                localTopSimilaritiesNum + 1, new Comparator<FpVectorIndexAndSimilarity>() {
            @Override
            public int compare(FpVectorIndexAndSimilarity o1, FpVectorIndexAndSimilarity o2) {
                return Double.compare(o1.similarity, o2.similarity);
            }
        });
        for (FpVectorIndexAndSimilarity fpVectorIndexAndSimilarity : fpVectorIndexAndSimilarities) {
            topSimilaritiesMinHeap.add(fpVectorIndexAndSimilarity);
            if (topSimilaritiesMinHeap.size() == localTopSimilaritiesNum + 1) {
                topSimilaritiesMinHeap.poll();
            }
        }
        return topSimilaritiesMinHeap;
    }

    private class FpVectorIndexAndSimilarity {
        public int fpVectorIndex;
        public double similarity;

        public FpVectorIndexAndSimilarity(int fpVectorIndex, double similarity) {
            this.fpVectorIndex = fpVectorIndex;
            this.similarity = similarity;
        }
    }


}
