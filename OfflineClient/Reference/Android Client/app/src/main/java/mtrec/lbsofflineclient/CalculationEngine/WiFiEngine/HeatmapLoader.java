package mtrec.lbsofflineclient.CalculationEngine.WiFiEngine;

import java.util.HashMap;
import java.util.Map;

import mtrec.lbsofflineclient.model.ApSignal;
import mtrec.lbsofflineclient.model.WiFiHeatmap;
import mtrec.lbsofflineclient.model.WiFiVector;
import mtrec.lbsofflineclient.utils.ConstantValues;
import mtrec.lbsofflineclient.utils.Position;
import mtrec.lbsofflineclient.utils.Settings;
import mtrec.lbsofflineclient.utils.TxtTool;

public class HeatmapLoader {
    public static Map<String, WiFiHeatmap> load() throws Exception {

        Map<String, WiFiHeatmap> heatmaps = new HashMap<>();
        for (String floorId : Settings.Site.allFloors) {
            WiFiHeatmap wiFiHeatmap = new WiFiHeatmap();
            for (String line : new TxtTool(Settings.Site.WiFi.getWiFiFingerprintTxtPath(floorId), TxtTool.TxtMode.read)
                    .getAllLinesInFile()) {
                String[] cols = line.split(" ");

                String[] positionInfo = cols[0].split(",");
                Position rfFpPosition = new Position(
                        Double.parseDouble(positionInfo[0]), Double.parseDouble(positionInfo[1]));

                rfFpPosition.setAreaId(floorId);

                WiFiVector wiFiVector = new WiFiVector();
                wiFiVector.pos = rfFpPosition;
                wiFiVector.floorId = floorId;
                for (int i = 1; i < cols.length; i++) {
                    String[] signalFeatures = cols[i].split(",");
                    String apAddress = WiFiUtils.uniformApAddress(signalFeatures[0]);

                    if (WiFiUtils.isVirtualMac(apAddress)) {
                        continue;
                    }

                    double rssi = Double.parseDouble(signalFeatures[1]);
//                    double standardDeviation = Double.parseDouble(signalFeatures[2]);

                    if (Math.abs(rssi - 0) < ConstantValues.EPS) {
                        continue;
                    }

                    wiFiVector.addApSignal(new ApSignal(apAddress, rssi));
                }
                if (!wiFiVector.isEmpty()) {
                    wiFiHeatmap.addWiFiVector(wiFiVector);
                }
            }

            heatmaps.put(floorId, wiFiHeatmap);
        }
        return heatmaps;
    }
}
