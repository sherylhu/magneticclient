package mtrec.lbsofflineclient.CalculationEngine.MagneticEngine;

import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.utils.Position;

public interface MagneticDetectionListener {
    void onTopPositionsDetected(Map<String, List<Position>> topPositions);
}
