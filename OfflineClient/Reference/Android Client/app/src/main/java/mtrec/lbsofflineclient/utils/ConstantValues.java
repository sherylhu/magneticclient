package mtrec.lbsofflineclient.utils;

public class ConstantValues {
    public static final double EPS = 1e-6;

    public static final float SUPPLEMENTING_POINT_RADIUS_TO_SCREEN = 64;
    public static final float LOCATION_POINT_RADIUS_TO_SCRREEN = 40;
}
