package mtrec.lbsofflineclient;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mtrec.lbsofflineclient.utils.Position;
import mtrec.lbsofflineclient.utils.Settings;



public class NavigationInstructionActivity extends AppCompatActivity {
    public static final String NAV_POINT_KEY = "navpoints";

    private static final String TAG = "NavInstruction";
    private static final float WALKING_SPEED_METER_PER_SEC = 0.5f;



    private ListView listInstruction;
    private InstructionAdapter listInstructionAdapter;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_instruction);

        NavigationInstruction[] navInstructions = getInstructions();

        float totalWalkingDistance = 0;

        for (NavigationInstruction ins : navInstructions) {
            if (ins.type == NavigationInstruction.TYPE_INFLOOR) {
                totalWalkingDistance += ins.walkingDistance;
            }

        }

        String timeUnit = "min";
        float estimatedWalkingTime = totalWalkingDistance / (WALKING_SPEED_METER_PER_SEC * 60);

        if (estimatedWalkingTime < 1) {
            estimatedWalkingTime *= 60;
            timeUnit = "sec";
        }

        estimatedWalkingTime = Math.round(estimatedWalkingTime);

        ((TextView)findViewById(R.id.txtNavSummary)).setText(
            String.format("%.1fm - %.0f%s(s)", totalWalkingDistance, estimatedWalkingTime, timeUnit)
        );

        listInstruction = (ListView)findViewById(R.id.listInstruction);
        listInstructionAdapter = new InstructionAdapter(this, navInstructions);

        listInstruction.setAdapter(listInstructionAdapter);

        ((ImageView)findViewById(R.id.imgCloseInstruction)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationInstructionActivity.this.finish();
            }

        });

    }


    // Return values can't be null because of ArrayAdapter
    private NavigationInstruction[] getInstructions() {
        ArrayList<Position> navigationPoints = getIntent().getParcelableArrayListExtra(NAV_POINT_KEY);

        if (navigationPoints == null) {
            Log.d(TAG, "No navpoints data provided");
            return new NavigationInstruction[]{};
        }

        LinkedList<NavigationInstruction> navInstructions = new LinkedList<>();

        Position prevPoint = navigationPoints.get(0);
        int startIdx = 0;

        NavigationInstruction curInfloorInstruction = new NavigationInstruction();
        curInfloorInstruction.floorName = prevPoint.getAreaId();
        curInfloorInstruction.walkingDistance = 0;

        float curMapScale = Settings.Site.floorsMapScales.get(curInfloorInstruction.floorName);

        for (int i = 1; i < navigationPoints.size(); ++i) {
            Position curPoint = navigationPoints.get(i);

            if (curPoint.getAreaId().equals(curInfloorInstruction.floorName)) {
                curInfloorInstruction.walkingDistance += (prevPoint.dist(curPoint) / curMapScale);

            } else {
                // Finish instruction in the current floor
                curInfloorInstruction.imgInstruction = createInstructionImage(
                    navigationPoints.subList(startIdx, i)
                );

                navInstructions.add(curInfloorInstruction);

                // Create floor switch instruction
                NavigationInstruction switchFloorInstruction = new NavigationInstruction();
                switchFloorInstruction.type = NavigationInstruction.TYPE_SWITCHFLOOR;
                switchFloorInstruction.floorSwitchType = curPoint.getFloorSwitchType();

                navInstructions.add(switchFloorInstruction);

                // Prepare instruction for next floor
                curInfloorInstruction = new NavigationInstruction();

                curInfloorInstruction.floorName = curPoint.getAreaId();
                curInfloorInstruction.walkingDistance = 0;

                curMapScale = Settings.Site.floorsMapScales.get(curInfloorInstruction.floorName);
                startIdx = i;
            }

            prevPoint = curPoint;
        }

        // Add the last infloor instruction
        curInfloorInstruction.imgInstruction = createInstructionImage(
            navigationPoints.subList(startIdx, navigationPoints.size())
        );

        navInstructions.add(curInfloorInstruction);

        return navInstructions.toArray(new NavigationInstruction[]{});
    }


    private Bitmap createInstructionImage(List<Position> points) {
        return null;
    }



    private static class NavigationInstruction {
        public static final int TYPE_INFLOOR = 0;
        public static final int TYPE_SWITCHFLOOR = 1;

        public int type;

        public String floorName;
        public float walkingDistance;
        public Bitmap imgInstruction;

        public String floorSwitchType;
    }



    private static class InstructionAdapter extends ArrayAdapter<NavigationInstruction> {
        public InstructionAdapter(Context context, NavigationInstruction[] instructions) {
            super(context, R.layout.navigation_switchfloor_item, instructions);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NavigationInstruction ins = getItem(position);

            switch (ins.type) {
                case NavigationInstruction.TYPE_INFLOOR:
                    if (convertView == null) {
                        convertView = LayoutInflater.from(getContext()).inflate(
                                R.layout.navigation_infloor_item,
                                parent,
                                false
                        );

                    }

                    ((TextView) convertView.findViewById(R.id.txtFloorName))
                            .setText(ins.floorName);

                    ((TextView) convertView.findViewById(R.id.txtWalkingDistance))
                            .setText(String.format("%.1fm", ins.walkingDistance));

                    break;

                case NavigationInstruction.TYPE_SWITCHFLOOR:
                    if (convertView == null) {
                        convertView = LayoutInflater.from(getContext()).inflate(
                                R.layout.navigation_switchfloor_item,
                                parent,
                                false
                        );

                    }

                    ((TextView) convertView.findViewById(R.id.txtSwitchFloorInstruction))
                            .setText(
                                    "Take the "
                                            + ins.floorSwitchType.substring(0, 1).toUpperCase()
                                            + ins.floorSwitchType.substring(1).toLowerCase()
                            );

                    ImageView imgSwitchFloor = (ImageView) convertView.findViewById(R.id.imgSwitchFloor);

                    Context context = imgSwitchFloor.getContext();
                    int resId = context.getResources().getIdentifier(
                            ins.floorSwitchType, "drawable", context.getPackageName()
                    );

                    imgSwitchFloor.setImageResource(resId);

                    break;
            }

            return convertView;
        }

    }

}
