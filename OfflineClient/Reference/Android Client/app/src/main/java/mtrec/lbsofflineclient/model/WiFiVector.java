package mtrec.lbsofflineclient.model;

import java.util.ArrayList;
import java.util.List;

import mtrec.lbsofflineclient.utils.Position;

public class WiFiVector {
    public String floorId;
    public Position pos;
    public List<ApSignal>apSignalList = new ArrayList<>();

    public void addApSignal(ApSignal apSignal) {
        apSignalList.add(apSignal);
    }

    public boolean isEmpty() {
        return apSignalList.isEmpty();
    }

    public String toString() {
        String answer = "";
        for (ApSignal apSignal: apSignalList) {
            answer +="(" + apSignal.getApAddress() + "&" + apSignal.getRssi() + "&" + apSignal.getMicroTimestamp() + ") ";
        }
        return answer;
    }
}
