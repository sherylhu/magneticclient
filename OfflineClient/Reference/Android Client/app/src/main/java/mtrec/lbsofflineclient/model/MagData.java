package mtrec.lbsofflineclient.model;

import mtrec.lbsofflineclient.utils.Position;

public class MagData {

    private float intensity = Float.NaN;
    private float vertical = Float.NaN;
    private float horizontal = Float.NaN;
    private long timestamp = -1;
    private Position magPosition = null;

    public MagData(float intensity, float vertical, float horizontal, float orientation, long timestamp, int step) {
        this.intensity = intensity;
        this.vertical = vertical;
        this.horizontal = horizontal;
        // transfer timestamp into millisecond
        if (timestamp > 2000000000000L) {
            // timestamp is in nanosecond
            timestamp = timestamp / 1000000L;
        }
        this.timestamp = timestamp;
    }

    public float getIntensity() {
        return intensity;
    }

    public float getVertical() {
        return vertical;
    }

    public float getHorizontal() {
        return horizontal;
    }

    public Position getMagPosition() {
        return magPosition;
    }

    public void setMagPosition(Position magPosition) {
        this.magPosition = magPosition;
    }
}
