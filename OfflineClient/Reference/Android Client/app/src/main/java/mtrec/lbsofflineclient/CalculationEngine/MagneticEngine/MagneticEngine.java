package mtrec.lbsofflineclient.CalculationEngine.MagneticEngine;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.CalculationEngine.AbstractEngine;
import mtrec.lbsofflineclient.CalculationEngine.StepCounter.StepCounterEngine;
import mtrec.lbsofflineclient.model.GravityData;
import mtrec.lbsofflineclient.model.MagData;
import mtrec.lbsofflineclient.model.MagneticData;
import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.Position;

public class MagneticEngine extends AbstractEngine{
    private static final String tag = "MyMagneticEngine";

    // the calculation time limitation for each period
    private static final long calculationPeriod = 500; //milliseconds

    private SensorManager mSensorManager;
    private MagSensorEventListener mMagSensorEventListener;
    private GravSensorEventListener mGravSensorEventListener;
    public static final int sensorFrequency = 50; //The frequency of getting acceleration data.

    private final Object magDataSynObj = new Object();
    private List<MagneticData> magRawBuffer = new ArrayList<>();
    private List<MagneticData> allMagRawData = new ArrayList<>();

    private final Object gravDataSynObj = new Object();
    private List<GravityData> gravRawBuffer = new ArrayList<>();
    private List<GravityData> allGravRawData = new ArrayList<>();

    private List<MagneticData> allGeoMagneticData = new ArrayList<>();
    private static int PAALength = 15; // important for performance

    private CrfAlgo mCrfAlgo;

    private boolean needCleanHistory = false;

    private MagneticData idleMagneticDataSum;
    private int idleMagneticDataNum = 0;

    public MagneticEngine(Context context) {
        super(calculationPeriod);
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        Sensor mMagSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mMagSensorEventListener = new MagSensorEventListener();
        mSensorManager.registerListener(mMagSensorEventListener, mMagSensor, 1000000 / sensorFrequency);

        Sensor mGravSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        mGravSensorEventListener = new GravSensorEventListener();
        mSensorManager.registerListener(mGravSensorEventListener, mGravSensor, 1000000 / sensorFrequency);

        mCrfAlgo = new CrfAlgo();

        idleMagneticDataSum = new MagneticData();
        idleMagneticDataSum.magValues = new float[3];
    }

    private void init() {
        synchronized (magDataSynObj) {
            magRawBuffer.clear();
        }
        allMagRawData.clear();

        synchronized (gravDataSynObj) {
            gravRawBuffer.clear();
        }
        allGravRawData.clear();
    }

    public void start() {
        init();
        super.start();
    }

    public void stop() {
        clearHistory();
        super.stop();
    }

    public void destroy() {
        mSensorManager.unregisterListener(mMagSensorEventListener);
        mSensorManager.unregisterListener(mGravSensorEventListener);
        super.destroy();
    }

    private List<MagneticDetectionListener> allMagneticDetectionListeners = new ArrayList<>();

    public void addMagneticDetectionListener(MagneticDetectionListener magneticDetectionListener) {
        allMagneticDetectionListeners.add(magneticDetectionListener);
    }

    private class MagSensorEventListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (!MagneticEngine.this.isRunning()) {
                return;
            }

            float[] values = new float[]{event.values[0], event.values[1], event.values[2]};
            long timestamp = event.timestamp;

            MagneticData magneticData = new MagneticData();
            magneticData.magValues = values;
            magneticData.nanoTimestamp = timestamp;

            synchronized (magDataSynObj) {
                magRawBuffer.add(magneticData);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }

    private class GravSensorEventListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (!MagneticEngine.this.isRunning()) {
                return;
            }

            float[] values = new float[]{event.values[0], event.values[1], event.values[2]};
            long timestamp = event.timestamp;

            GravityData gravityData = new GravityData();
            gravityData.gravValues = values;
            gravityData.nanoTimestamp = timestamp;

            synchronized (gravDataSynObj) {
                gravRawBuffer.add(gravityData);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }

    private void idleDataInit() {
        idleMagneticDataNum = 0;
        idleMagneticDataSum.magValues[0] = idleMagneticDataSum.magValues[1] = idleMagneticDataSum.magValues[2] = 0;
    }

    @Override
    protected void process() {
        List<MagneticData> magDataCopy;
        synchronized (magDataSynObj) {
            magDataCopy = magRawBuffer;
            magRawBuffer = new ArrayList<>();
        }
        allMagRawData.addAll(magDataCopy);

        List<GravityData> gravDataCopy;
        synchronized (gravDataSynObj) {
            gravDataCopy = gravRawBuffer;
            gravRawBuffer = new ArrayList<>();
        }
        allGravRawData.addAll(gravDataCopy);

        List<MagneticData> geoMagneticDataList = getGeoMagneticDataFromRawData();
        allGeoMagneticData.addAll(geoMagneticDataList);

        if (StepCounterEngine.walking) {
            PAALength = allGeoMagneticData.size();
            int smoothSize = allGeoMagneticData.size() / PAALength * PAALength;
            List<MagneticData> smoothedMagneticDataList = smooth(allGeoMagneticData.subList(0, smoothSize));

            calculateMagneticData(smoothedMagneticDataList);

            allGeoMagneticData.subList(0, smoothSize).clear();

            idleDataInit();
        } else {
            for (MagneticData magneticData : allGeoMagneticData) {
                for (int i = 0; i < idleMagneticDataSum.magValues.length; i++) {
                    idleMagneticDataSum.magValues[i] += magneticData.magValues[i];
                }
                idleMagneticDataNum++;
            }
            MagneticData smoothData = new MagneticData();
            smoothData.magValues = new float[]{
                    idleMagneticDataSum.magValues[0] / idleMagneticDataNum,
                    idleMagneticDataSum.magValues[1] / idleMagneticDataNum,
                    idleMagneticDataSum.magValues[2] / idleMagneticDataNum
            };
            List<MagneticData> smoothedMagneticDataList = new ArrayList<>();
            smoothedMagneticDataList.add(smoothData);
            calculateMagneticData(smoothedMagneticDataList);
            allGeoMagneticData.clear();
        }
    }

    private List<MagneticData> getGeoMagneticDataFromRawData() {
        List<MagneticData> geoMagneticDataList = new ArrayList<>();
        if (allMagRawData.isEmpty() || allGravRawData.isEmpty()) {
            return geoMagneticDataList;
        }

        int gravIndex = 0, magIndex = 0;
        for (; magIndex < allMagRawData.size(); magIndex++) {
            MagneticData curMagneticData = allMagRawData.get(magIndex);
            long curMagTimestamp = curMagneticData.nanoTimestamp;
            long curGravTimestamp = allGravRawData.get(gravIndex).nanoTimestamp;
            if (curMagTimestamp < curGravTimestamp) {
                continue;
            }

            // find two continuous gravity data whose timestamp period cover {@code curMagTimestamp}
            boolean found = false;
            while (gravIndex + 1 < allGravRawData.size()) {
                if (allGravRawData.get(gravIndex).nanoTimestamp <= curMagTimestamp &&
                        curMagTimestamp < allGravRawData.get(gravIndex + 1).nanoTimestamp) {
                    found = true;
                    break;
                } else {
                    gravIndex++;
                }
            }
            if (!found) {
                // no more gravity data is available
                break;
            } else {
                // use interpolation to get the approximate gravity data at {@code curMagTimestamp}
                GravityData prevGravityData = allGravRawData.get(gravIndex);
                GravityData nextGravityData = allGravRawData.get(gravIndex + 1);
                float[] interpolatedGravValues = new float[3];
                float rate = (curMagTimestamp - prevGravityData.nanoTimestamp) * 1f / (nextGravityData.nanoTimestamp - prevGravityData.nanoTimestamp);
                for (int dataIndex = 0; dataIndex < prevGravityData.gravValues.length; dataIndex++) {
                    interpolatedGravValues[dataIndex] = rate * (nextGravityData.gravValues[dataIndex] - prevGravityData.gravValues[dataIndex]) + prevGravityData.gravValues[dataIndex];
                }
                GravityData interpolatedGravityData = new GravityData();
                interpolatedGravityData.gravValues = interpolatedGravValues;
                interpolatedGravityData.nanoTimestamp = curMagTimestamp;

                MagneticData transferredData = transferGeoMagneticData(curMagneticData, interpolatedGravityData);
                if (transferredData != null) {
                    geoMagneticDataList.add(transferredData);
                }
            }
        }
        allGravRawData.subList(0, gravIndex).clear();
        allMagRawData.subList(0, magIndex).clear();

        return geoMagneticDataList;
    }

    private List<MagneticData> smooth(List<MagneticData> magneticDataList) {
        List<MagneticData> answer = new ArrayList<>();
        float intensitySum = 0;
        float verticalSum = 0;
        float horizontalSum = 0;
        long timestampSum = 0;

        for (int i = 0; i < magneticDataList.size(); i++) {
            MagneticData data = magneticDataList.get(i);
            intensitySum += data.magValues[0];
            verticalSum += data.magValues[1];
            horizontalSum += data.magValues[2];
            timestampSum += data.nanoTimestamp;

            if ((i + 1) % PAALength == 0) {
                MagneticData magneticData = new MagneticData();
                magneticData.magValues = new float[]{
                        (intensitySum / PAALength),
                        (verticalSum / PAALength),
                        (horizontalSum / PAALength)};
                magneticData.nanoTimestamp = timestampSum / PAALength;

                answer.add(magneticData);

                intensitySum = 0;
                verticalSum = 0;
                horizontalSum = 0;
                timestampSum = 0;
            }
        }
        return answer;
    }

    /**
     * Use the magnetic raw data and the gravity raw data, which correspond to the same timestamp,
     * to calculate the geo-magnetic data at that timestamp.
     *
     * @param magneticData
     * @param gravityData
     * @return the geo-magnetic data at the same timestamp as the input data.
     */
    private MagneticData transferGeoMagneticData(MagneticData magneticData, GravityData gravityData) {
        MagneticData transferredMagneticData = new MagneticData();
        float[] R = new float[9], I = new float[9];
        float[] magnet = magneticData.magValues;
        float[] gravity = gravityData.gravValues;
        if (SensorManager.getRotationMatrix(R, I, gravity, magnet)) {
            float intensity = (float) Math.sqrt(magnet[0] * magnet[0] + magnet[1] * magnet[1] + magnet[2] * magnet[2]);
            float vertical = magnet[0] * R[6] + magnet[1] * R[7] + magnet[2] * R[8];
            float horizontal = magnet[0] * R[3] + magnet[1] * R[4] + magnet[2] * R[5];
            transferredMagneticData.magValues = new float[]{intensity, vertical, horizontal};
            transferredMagneticData.nanoTimestamp = magneticData.nanoTimestamp;
            return transferredMagneticData;
        } else {
            return null;
        }
    }

    private void calculateMagneticData(List<MagneticData> geoMagneticDataList) {
        if (geoMagneticDataList.isEmpty()) {
            return;
        }
        DebugTool.printLog(tag, "***********calculateMagneticData************");
        DebugTool.printLog(tag, "geoMagneticDataList.size(): " + geoMagneticDataList.size());
        if (needCleanHistory) {
            needCleanHistory = false;
            mCrfAlgo.clearHistory();
        }

        for (int i = 0; i < geoMagneticDataList.size(); i++) {
            if (System.currentTimeMillis() - lastCalculationTime >= calculationPeriod - 100) { // suppose calculating genResultLocation() needs 100ms.
                DebugTool.printLog(tag, "Warning: Calculation time may be not enough for {@code genResultLocation()}, we need to break here. " +
                        "You can also give more time for calculation, or change the PAA value.");
                break;
            }

            MagneticData magneticData = geoMagneticDataList.get(i);
            MagData magData = new MagData(magneticData.magValues[0], magneticData.magValues[1], magneticData.magValues[2], 0, 0, 0);
            long time1 = System.currentTimeMillis();
            mCrfAlgo.inputMagData(magData);
            long time2 = System.currentTimeMillis();
            DebugTool.printLog(tag, "inputMagData" + i + " used time: " + (time2 - time1));
        }
        long beginTime = System.currentTimeMillis();
        Map<String, List<Position>> areaIdMapTopPositions = mCrfAlgo.genResultLocation();
        long endTime = System.currentTimeMillis();
        DebugTool.printLog(tag, "genResultLocation() uses time: " + (endTime - beginTime));

        for (MagneticDetectionListener magneticDetectionListener : allMagneticDetectionListeners) {
            magneticDetectionListener.onTopPositionsDetected(areaIdMapTopPositions);
        }
    }

    public void clearHistory() {
        needCleanHistory = true;
    }
}
