package mtrec.lbsofflineclient.utils;

import android.os.Environment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.transaction.Shop;
import mtrec.lbsofflineclient.utils.pathFinding.FloorSwitchPoint;

public class Settings {
    public static boolean debugModeTurnOn;

    public static class Paths {
        public static String settingJson = Environment.getExternalStorageDirectory() + "/LBSOfflineClient/settings.json";
        public static String database = Environment.getExternalStorageDirectory() + "/LBSOfflineClient/";
    }

    public static class Site {
        private final static String tag = "Site";

        public static String siteName;
        public static List<String> allFloors = new ArrayList<>();
        public static Map<String, Float> floorsMapScales = new HashMap<>();

        public static Map< String, List<List<Position>> > floorSwitchAreas = new HashMap<>();

        public static List<Shop> shopList = new ArrayList<>();
        public static Map<String, List<Position>> floorMapConnectionPoints = new HashMap<>();
        public static List<FloorSwitchPoint>floorSwitchPoints = new ArrayList<>();

        public static String siteRootFolder;

        public static String constraintFolder;
        public static String inConstraintsRootFolder;
        public static String outConstraintsRootFolder;
        public static Map<String, String> inConstraints = new HashMap<>();
        public static Map<String, String> outConstraints = new HashMap();

        public static String mapRoot;
        public static Map<String, String> maps = new HashMap<>();

        public static String wiFiRoot;
        public static String wiFiFp;
        public static Map<String, String> wiFiFps = new HashMap<>();
        public static String wiFiRaw;
        public static Map<String, String> wiFiRaws = new HashMap<>();
        public static String wiFiRfp;
        public static Map<String, String> wiFiRfps = new HashMap<>();

        public static String magRoot;
        public static String magRaw;
        public static Map<String, String> magRaws = new HashMap<>();
        public static String magHeatmap;

        public static void init() {
            siteRootFolder = Paths.database + siteName + "/";

            // constraintFolder
            constraintFolder = siteRootFolder + "Constraint/";
            inConstraintsRootFolder = constraintFolder + "inConstraints/";
            outConstraintsRootFolder = constraintFolder + "outConstraints/";

            // map
            mapRoot = siteRootFolder + "map/";

            // WifiData
            wiFiRoot = siteRootFolder + "WifiData/";
            wiFiFp = wiFiRoot + "FingerprintData/";
            wiFiRaw = wiFiRoot + "RawData/";
            wiFiRfp = wiFiRoot + "ReferencePoints/";

            // MagData
            magRoot = siteRootFolder + "MagData/";
            magRaw = magRoot + "RawData/";
            magHeatmap = magRoot + "Heatmap/";

            for (String floor : allFloors) {
                inConstraints.put(floor, inConstraintsRootFolder + floor + "/");
                outConstraints.put(floor, outConstraintsRootFolder + floor + "/");
                maps.put(floor, mapRoot + floor + "/");
                wiFiFps.put(floor, wiFiFp + floor + "/");
                wiFiRaws.put(floor, wiFiRaw + floor + "/");
                wiFiRfps.put(floor, wiFiRfp + floor + "/");
                magRaws.put(floor, magRaw + floor + "/");
            }
        }

        public static String getInfoJsonPath() {
            return Paths.database + siteName + "/info.json";
        }

        public static String getMapPath(String floor) {
            return maps.get(floor) + "map.jpg";
        }

        public static class Constraint {
            public static List<String> getAllOutConstraintPaths(String floor) {
                String folder = Site.outConstraints.get(floor);
                if (!FileUtils.checkFolderExists(folder)) {
                    return null;
                }
                try {
                    return (new TxtTool(folder, TxtTool.TxtMode.read))
                            .getAllTxtPathsInFolder(false);
                } catch (Exception e) {
                    DebugTool.printLog(tag, "fail in getAllOutConstraintPaths(): " + e.getMessage());
                }
                return null;
            }

            public static List<String> getAllInConstraintPaths(String floor) {
                String folder = Site.inConstraints.get(floor);
                if (!FileUtils.checkFolderExists(folder)) {
                    return null;
                }
                try {
                    return (new TxtTool(folder, TxtTool.TxtMode.read))
                            .getAllTxtPathsInFolder(false);
                } catch (Exception e) {
                    DebugTool.printLog(tag, "fail in getAllInConstraintPaths(): " + e.getMessage());
                }
                return null;
            }
        }

        public static class WiFi {
            public static int numberOfSelectedPositionWithTopPotential;
            public static float meterDistBetweenWiFiRfPts;
            public static float percentage2DefineBigEnoughCluster;

            public static String getWiFiReferencePointsGraphPath(String floor) {
                return wiFiRfps.get(floor) + "reference_points.jpg";
            }

            public static String getWiFiReferencePointsTxtPath(String floor) {
                return wiFiRfps.get(floor) + "reference_points.txt";
            }

            public static String getWiFiRawFolder(String floor) {
                return wiFiRaws.get(floor);
            }

            public static List<String>generateWiFiRawFolders(String floor, int folderNum) {
                List<String>answer = new ArrayList<>();
                for (int i = 1; i <= folderNum; i++) {
                    answer.add(wiFiRaws.get(floor) + "WiFiData" + i);
                }
                return answer;
            }

            public static List<String>getWiFiRawFolders(String floor) {
                try {
                    return (new TxtTool(Site.wiFiRaws.get(floor), TxtTool.TxtMode.read)).getAllFoldersInFolder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            public static String getWiFiFingerprintTxtPath(String floor) {
                return wiFiFps.get(floor) + "fingerprint.txt";
            }

            public static String getWiFiFingerprintGraphPath(String floor) {
                return wiFiFps.get(floor) + "fingerprint.jpg";
            }
        }

        public static class Mag {
            public static int numberOfSelectedPositionWithTopPotential;
            public static float meterDistBetweenMagRfPts;

            public static String getMagRawFolder(String floor) {
                return magRaws.get(floor);
            }

            public static String getMagNeighborPath() {
                return magHeatmap + "neighbor.txt";
            }

            public static String getMagHeatmapPath() {
                return magHeatmap + "heatmap.txt";
            }

            public static String getMagHeatmapConnectionGraphPath(String floor) {
                return magHeatmap + "connection_" + floor + ".jpg";
            }
            public static String getMagHeatmapGraphPath(String floor) {
                return magHeatmap + "heatmap_" + floor + ".jpg";
            }
        }

        public static class Fusion{
            public static float percentage2DefineBigEnoughCluster;
        }

        public static List<String> getAllSiteFolders() {
            List<String>allSitePaths = new ArrayList<>();
            allSitePaths.add(siteRootFolder);
            allSitePaths.add(constraintFolder);
            allSitePaths.add(inConstraintsRootFolder);
            allSitePaths.add(outConstraintsRootFolder);
            allSitePaths.add(mapRoot);
            allSitePaths.add(wiFiRoot);
            allSitePaths.add(wiFiFp);
            allSitePaths.add(wiFiRaw);
            allSitePaths.add(wiFiRfp);
            allSitePaths.add(magRoot);
            allSitePaths.add(magRaw);
            allSitePaths.add(magHeatmap);
            allSitePaths.addAll(inConstraints.values());
            allSitePaths.addAll(outConstraints.values());
            allSitePaths.addAll(maps.values());
            allSitePaths.addAll(wiFiFps.values());
            allSitePaths.addAll(wiFiRaws.values());
            allSitePaths.addAll(wiFiRfps.values());
            allSitePaths.addAll(magRaws.values());
            return allSitePaths;
        }
    }

}
