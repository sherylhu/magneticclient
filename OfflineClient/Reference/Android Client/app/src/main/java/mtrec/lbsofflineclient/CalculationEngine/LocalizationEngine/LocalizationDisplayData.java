package mtrec.lbsofflineclient.CalculationEngine.LocalizationEngine;

import java.util.List;

import mtrec.lbsofflineclient.utils.Position;

public class LocalizationDisplayData {
    public List<Position> magPosList;
    public List<Position> wifiPosList;
    public Position resultPos;
    public String detectedAreaId;

    public LocalizationDisplayData(List<Position> magPosList, List<Position> wifiPosList,
                                   Position resultPos, String detectedAreaId) {
        this.magPosList = magPosList;
        this.wifiPosList = wifiPosList;
        this.resultPos = resultPos;
        this.detectedAreaId = detectedAreaId;
    }
}
