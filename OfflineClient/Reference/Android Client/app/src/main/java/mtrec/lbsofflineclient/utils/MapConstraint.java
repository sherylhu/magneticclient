package mtrec.lbsofflineclient.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapConstraint {
    public static Map<String, List<List<Position>>> floorMapOutConstraints = new HashMap<>();
    public static Map<String, List<List<Position>>> floorMapInConstraints = new HashMap<>();



    public static void init() {
        try {
            // load outConstraints
            for (String floor : Settings.Site.allFloors) {
                List<List<Position>>outConstraints = new ArrayList<>();

                for (String outConstraintPath : Settings.Site.Constraint.getAllOutConstraintPaths(floor)) {
                    List<Position>outConstraint = new ArrayList<>();

                    for (String line : (new TxtTool(outConstraintPath, TxtTool.TxtMode.read)).getAllLinesInFile()) {
                        String[]cols = line.split(" ");
                        outConstraint.add(new Position(Double.parseDouble(cols[0]), Double.parseDouble(cols[1])));
                    }

                    outConstraints.add(outConstraint);
                }

                floorMapOutConstraints.put(floor, outConstraints);
            }

            // load inConstraints
            for (String floor : Settings.Site.allFloors) {
                List<List<Position>>inConstraints = new ArrayList<>();

                for (String inConstraintPath : Settings.Site.Constraint.getAllInConstraintPaths(floor)) {
                    List<Position>inConstraint = new ArrayList<>();

                    for (String line : (new TxtTool(inConstraintPath, TxtTool.TxtMode.read)).getAllLinesInFile()) {
                        String[]cols = line.split(" ");
                        inConstraint.add(new Position(Double.parseDouble(cols[0]), Double.parseDouble(cols[1])));
                    }

                    inConstraints.add(inConstraint);
                }

                floorMapInConstraints.put(floor, inConstraints);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static Position run(Position position) {
        String floor = position.getAreaId();

        List<List<Position>>outConstraints = floorMapOutConstraints.get(floor);

        if (outConstraints != null) {
            for (List<Position> outConstraint : outConstraints) {
                if (!MathUtil.insidePolygen(outConstraint, position)) {
                    Position nearestProjPoint = null;
                    double nearestDist = Double.MAX_VALUE;

                    for (int i = 0; i < outConstraint.size(); i++) {
                        int next = (i + 1) % outConstraint.size();

                        Position projPoint = MathUtil.getProjectPointFromPos2LineSeg(
                            position,
                            outConstraint.get(i),
                            outConstraint.get(next)
                        );

                        double projDist = projPoint.dist(position);

                        if (projDist < nearestDist) {
                            nearestDist = projDist;
                            nearestProjPoint = projPoint;
                        }

                    }

                    return nearestProjPoint;
                }

            }

        }

        List<List<Position>>inConstraints = floorMapInConstraints.get(floor);

        if (inConstraints != null) {
            for (List<Position> inConstraint : inConstraints) {
                if (MathUtil.insidePolygen(inConstraint, position)) {
                    Position nearestProjPoint = null;
                    double nearestDist = Double.MAX_VALUE;

                    for (int i = 0; i < inConstraint.size(); i++) {
                        int next = (i + 1) % inConstraint.size();

                        Position projPoint = MathUtil.getProjectPointFromPos2LineSeg(
                            position,
                            inConstraint.get(i),
                            inConstraint.get(next)
                        );

                        double projDist = projPoint.dist(position);

                        if (projDist < nearestDist) {
                            nearestDist = projDist;
                            nearestProjPoint = projPoint;
                        }

                    }

                    return nearestProjPoint;

                }

            }

        }

        return position;
    }


    public static boolean checkValidPointForInOutConstraint(Position position, String floor) {
        List<List<Position>>outConstraints = floorMapOutConstraints.get(floor);
        List<List<Position>>inConstraints = floorMapInConstraints.get(floor);

        for (List<Position>outConstraint : outConstraints) {
            if (!MathUtil.insidePolygen(outConstraint, position)) {
                return false;
            }

        }

        for (List<Position>inConstraint : inConstraints) {
            if (MathUtil.insidePolygen(inConstraint, position)) {
                return false;
            }

        }

        return true;
    }


    public static boolean checkValidLineForInOutConstraint(Position start, Position end, String floor) {
        List<List<Position>>outConstraints = floorMapOutConstraints.get(floor);
        List<List<Position>>inConstraints = floorMapInConstraints.get(floor);

        for (List<Position>outConstraint : outConstraints) {
            for (int k = 0; k < outConstraint.size(); k++) {
                int next = (k + 1 == outConstraint.size()) ? 0 : k + 1;
                if (MathUtil.checkLinesIntersection(start, end, outConstraint.get(k), outConstraint.get(next))) {
                    return false;
                }

            }

        }

        for (List<Position>inConstraint : inConstraints) {
            for (int k = 0; k < inConstraint.size(); k++) {
                int next = (k + 1 == inConstraint.size()) ? 0 : k + 1;
                if (MathUtil.checkLinesIntersection(start, end, inConstraint.get(k), inConstraint.get(next))) {
                    return false;
                }

            }

        }

        return true;
    }


    public static List<List<Position>>getAllConstraint(String floor) {
        List<List<Position>>answer = new ArrayList<>();

        answer.addAll(floorMapOutConstraints.get(floor));
        answer.addAll(floorMapInConstraints.get(floor));

        return answer;
    }

}
