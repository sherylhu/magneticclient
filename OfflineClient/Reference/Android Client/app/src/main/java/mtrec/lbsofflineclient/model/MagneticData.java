package mtrec.lbsofflineclient.model;

public class MagneticData {
    public long nanoTimestamp;
    public float[] magValues;

    public MagneticData() {}

    public MagneticData(MagneticData other) {
        nanoTimestamp = other.nanoTimestamp;
        magValues = new float[other.magValues.length];
        for (int i = 0; i < other.magValues.length; i++) {
            magValues[i] = other.magValues[i];
        }
    }

    @Override
    public String toString() {
        String str = "(";
        for (int i = 0; i < magValues.length - 1;i++) {
            str += magValues[i] + ",";
        }
        str += magValues[magValues.length - 1] + ")";
        return str;
    }
}
