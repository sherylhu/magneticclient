package mtrec.lbsofflineclient;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import mtrec.lbsofflineclient.transaction.Shop;
import mtrec.lbsofflineclient.utils.Settings;

public class SearchActivity extends AppCompatActivity {
    public static final String NAME_OF_EXTRA_DATA = "shopIndex";



    private EditText searchTxt;
    private LinearLayout linear;

    private RelativeLayout[] shopLayouts;
    private String[] standardizedShopName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setResult(RESULT_CANCELED);

        searchTxt = (EditText) findViewById(R.id.search_edit);
        searchTxt.addTextChangedListener(new SearchTextWatcher());

        linear = (LinearLayout) findViewById(R.id.linear);

        prepareShopList();
    }


    private void initShopView(RelativeLayout contactItemLayout, Shop shop, final int shopIndex) {
        ImageView imgView = (ImageView) contactItemLayout.findViewById(R.id.shop_item_img);
        Context context = imgView.getContext();
        int id = context.getResources().getIdentifier(shop.icon, "drawable", context.getPackageName());
        imgView.setImageResource(id);

        TextView nameText = (TextView) contactItemLayout.findViewById(R.id.shop_item_name);
        nameText.setText(shop.name);

        TextView decText = (TextView) contactItemLayout.findViewById(R.id.shop_item_desc);
        decText.setText(String.format("(%s) %s", shop.location.getAreaId(), shop.description));

        Button selectBtn = (Button) contactItemLayout.findViewById(R.id.shop_select_btn);

        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(NAME_OF_EXTRA_DATA, shopIndex);
                setResult(RESULT_OK, intent);
                SearchActivity.this.finish();
            }
        });

    }


    private void prepareShopList() {
        linear.removeAllViews();
        LayoutInflater layoutInflater = LayoutInflater.from(SearchActivity.this);

        shopLayouts = new RelativeLayout[Settings.Site.shopList.size()];
        standardizedShopName = new String[Settings.Site.shopList.size()];

        for (int shopId = 0; shopId < Settings.Site.shopList.size(); shopId++) {
            Shop shop = Settings.Site.shopList.get(shopId);

            // Standardize shop name
            standardizedShopName[shopId] = shop.name.toUpperCase().replace(" ", "");

            // Prepare an list item for each shop
            shopLayouts[shopId] = (RelativeLayout)layoutInflater.inflate(R.layout.shop_item, null);
            linear.addView(shopLayouts[shopId]);
            initShopView(shopLayouts[shopId], shop, shopId);
        }

    }


    private class SearchTextWatcher implements TextWatcher {
        public void afterTextChanged(Editable s) {
            String standardizedSearchInput = s.toString().toUpperCase().replace(" ", "");

//            Log.d("Search input", standardizedSearchInput);

            boolean displayAll = false;

            if (standardizedSearchInput.compareTo("") == 0) {
                displayAll = true;
            }

            for (int shopId = 0; shopId < Settings.Site.shopList.size(); ++shopId) {
                if (displayAll) {
                    shopLayouts[shopId].setVisibility(View.VISIBLE);
                    continue;
                }

                if (standardizedShopName[shopId].contains(standardizedSearchInput)) {
                    shopLayouts[shopId].setVisibility(View.VISIBLE);
                } else {
                    shopLayouts[shopId].setVisibility(View.GONE);
                }

            }

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

    }

}
