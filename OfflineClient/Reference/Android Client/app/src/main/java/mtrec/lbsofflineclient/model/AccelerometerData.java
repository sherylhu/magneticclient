package mtrec.lbsofflineclient.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccelerometerData {
    public float[] accValues;
    public long nanoTimestamp;

    public double getMagnitude() {
        return Math.sqrt(accValues[0] * accValues[0] + accValues[1] * accValues[1] + accValues[2] * accValues[2]);
    }

    public AccelerometerData() {
    }

    private static final String tag = "StepCounterEngine2";

    public AccelerometerData(String gsonLine) {
        Pattern pattern = Pattern.compile("\"ax\":(.+),\"az\":(.+),\"ay\":(.+)\\},\"elapsedTimeUs\":(.+),\"sysTimeMs\"");
        Matcher matcher = pattern.matcher(gsonLine);
        if (matcher.find()) {
            accValues = new float[]{
                    Float.parseFloat(matcher.group(1)),
                    Float.parseFloat(matcher.group(2)),
                    Float.parseFloat(matcher.group(3)
                    )};
            nanoTimestamp = Long.parseLong(matcher.group(4)) * 1000L;
        }
    }
}
