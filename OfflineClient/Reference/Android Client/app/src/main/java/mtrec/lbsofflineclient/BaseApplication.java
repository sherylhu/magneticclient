package mtrec.lbsofflineclient;

import android.app.Application;
import android.content.Context;

import mtrec.lbsofflineclient.utils.DebugTool;

public class BaseApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getContext() {
        return mContext;
    }
}
