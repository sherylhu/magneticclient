package mtrec.lbsofflineclient;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.CalculationEngine.LocalizationEngine.LocalizationDetectionListener;
import mtrec.lbsofflineclient.CalculationEngine.LocalizationEngine.LocalizationEngine;
import mtrec.lbsofflineclient.mapview.MapController;
import mtrec.lbsofflineclient.mapview.MapLayout;
import mtrec.lbsofflineclient.mapview.OnFollowBlueDotAutoDisabled;
import mtrec.lbsofflineclient.mapview.OnMapClickListener;
import mtrec.lbsofflineclient.transaction.Shop;
import mtrec.lbsofflineclient.utils.InfoReader;
import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.MapConstraint;
import mtrec.lbsofflineclient.utils.pathFinding.PathFinder;
import mtrec.lbsofflineclient.utils.Position;
import mtrec.lbsofflineclient.utils.Settings;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final int SEARCH_RESULT = 0xFFFF;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 0xEFFF;

    private static final int NAVIGATION_PATH_COLOR = Color.rgb(255, 194, 0);
    private static final double REACHING_NAVIGATION_TARGET_METER_THRESHOLD = 10;

    private static final int SELECTED_FLOOR_COLOR = Color.rgb(146, 147, 151);
    private static final int DETECTED_LOCATION_COLOR = Color.rgb(35, 167, 241);

    // HACK
    private static final double THRESHOLD_METER_LOCATION_ATTACHMENT = 0.7;



    private RelativeLayout relLayoutMap;
    private MapLayout mMapLayout;

    private ImageButton searchBtn;

    private ImageButton btnStartLocalization;

    private RelativeLayout relLayoutShopBar;
    private ImageButton btnStartNavigation;
    private ImageButton btnCancelNavigation;
    private ImageButton btnShowNavigationInstruction;
    private ImageButton btnFollowBlueDot;

    private TextView floorText;
    private LinearLayout linLayoutFloorList;

    private LocalizationEngine locEngine = null;

    private Handler handler;

    private boolean localizationJustActivated = false;
    private boolean doingNavigation = false;
    private boolean isFollowingBlueDot = false;

    private Shop curSelectedShop = null;
    private Shop curNavigatedShop = null;
    ArrayList<Position> navPathPos = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initBeforeGettingAllPermissions();
        requestMyPermissions();
    }


    private void initBeforeGettingAllPermissions() {
        searchBtn = (ImageButton) findViewById(R.id.search_btn);
        searchBtn.setOnClickListener(new BtnSearchOnClickListener());

        relLayoutShopBar = (RelativeLayout) findViewById(R.id.navigation_bar);

        btnStartNavigation = (ImageButton) findViewById(R.id.navi_btn);
        btnStartNavigation.setOnClickListener(new BtnStartNavOnClickListener());

        btnCancelNavigation = (ImageButton) findViewById(R.id.cancel_btn);
        btnCancelNavigation.setOnClickListener(new BtnCancelNavOnClickListener());

        btnShowNavigationInstruction = (ImageButton)findViewById(R.id.navi_instruction_btn);
        btnShowNavigationInstruction.setOnClickListener(new BtnShowNavInstructOnClickListener());

        btnStartLocalization = (ImageButton) findViewById(R.id.localization_btn);
        btnStartLocalization.setOnClickListener(new BtnStartLocalizeOnClickListener());

        btnFollowBlueDot = (ImageButton) findViewById(R.id.btnFollowBlueDot);
        btnFollowBlueDot.setOnClickListener(new BtnFollowBlueDotOnClickListener());

        relLayoutMap = (RelativeLayout) findViewById(R.id.map_layout);

        handler = new Handler(this.getMainLooper());
    }


    private void initAfterGettingAllPermissions() {
        // only after getting the permissions to read files, can we read local files
        // and we should load all the project information(like site information) before we set
        // anything related to the site (like maps, shop icons).
        loadProjectInformation();
        MapConstraint.init();

        initMap();

        // floorText settings should be after initMap(), because we need the lastAreaId from it.
        floorText = (TextView) findViewById(R.id.floor_text);
        floorText.setText(mMapLayout.getLastAreaId());

        linLayoutFloorList = (LinearLayout) findViewById(R.id.floor_list);

        for (int i = 0; i < Settings.Site.allFloors.size(); i++) {
            String floor = Settings.Site.allFloors.get(i);
            linLayoutFloorList.addView(new MyFloorButton(this, i, floor));
        }

        selectFloor(Settings.Site.allFloors.indexOf(mMapLayout.getLastAreaId()));

        TextView siteNameText = (TextView) findViewById(R.id.site_name);
        siteNameText.setText(Settings.Site.siteName);

        locEngine = new LocalizationEngine(this);
        locEngine.addLocalizationDetectionListener(new MyLocalizationDetectionListener());

        PathFinder.init();
    }


    private void loadProjectInformation() {
        DebugTool.printLog(TAG, "loading project information");

        InfoReader.init();
        Settings.Site.init();

        if (!InfoReader.checkIfFormatCorrect()) {
            Toast.makeText(this, "The database is wrong, pls check logcat", Toast.LENGTH_SHORT).show();
            finish();
        }

    }


    private void initMap() {
        mMapLayout = new MapLayout(this);

        mMapLayout.setLayoutParams(new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        relLayoutMap.addView(mMapLayout);

        mMapLayout.setOnFollowBlueDotAutoDisabled(new MainActivityOnFollowingBlueDotDisabled());

        for (Shop shop : Settings.Site.shopList) {
            int id = getResources().getIdentifier(shop.icon, "drawable", getPackageName());

            mMapLayout.addDrawable(
                shop.location.getAreaId(),
                shop.location,
                getResources().getDrawable(id)
            );

            mMapLayout.addText(
                shop.location.getAreaId(),
                new Position(shop.location.getX(), shop.location.getY() - 50),
                shop.name
            );

        }

        if (Settings.debugModeTurnOn) {
            // Draw inconstraints, outconstraints
            for (Map.Entry<String, MapController> entry : mMapLayout.allMapControllers.entrySet()) {
                String floor = entry.getKey();
                MapController controller = entry.getValue();

                for (List<Position> outConstraint : MapConstraint.floorMapOutConstraints.get(floor)) {
                    for (int k = 0; k < outConstraint.size(); k++) {
                        int next = (k + 1 == outConstraint.size()) ? 0 : k + 1;

                        controller.addStaticLine(
                            outConstraint.get(k),
                            outConstraint.get(next),
                            Color.RED
                        );

                    }

                }

                for (List<Position> inConstraint : MapConstraint.floorMapInConstraints.get(floor)) {
                    for (int k = 0; k < inConstraint.size(); k++) {
                        int next = (k + 1 == inConstraint.size()) ? 0 : k + 1;

                        controller.addStaticLine(
                            inConstraint.get(k),
                            inConstraint.get(next),
                            Color.BLUE
                        );

                    }

                }

                for (List<Position> floorSwitchArea : Settings.Site.floorSwitchAreas.get(floor)) {
                    for (int k = 0; k < floorSwitchArea.size(); ++k) {
                        int next = (k + 1 == floorSwitchArea.size() ? 0 : k + 1);

                        controller.addStaticLine(
                                floorSwitchArea.get(k),
                                floorSwitchArea.get(next),
                                Color.GREEN
                        );
                    }

                }

            }

        }

        mMapLayout.setOnMapClickListener(new MyOnMapClickListener());
    }


    private void showShopInfoBarAndMark(Shop shop) {
        relLayoutShopBar.setVisibility(View.VISIBLE);

        // Adjust btnStartLocalization position
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) btnStartLocalization.getLayoutParams();
        params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        btnStartLocalization.setLayoutParams(params);

        // Adjust linLayoutFloorList position
        params = (RelativeLayout.LayoutParams) linLayoutFloorList.getLayoutParams();
        params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        linLayoutFloorList.setLayoutParams(params);

        // NOTE: This is compare by reference. This is OK by now, as we only get reference from
        // allShops list
        if (shop != curNavigatedShop) {
            // Show navigation arrow, not navigation instruction icon

            btnStartNavigation.setVisibility(View.VISIBLE);
            btnShowNavigationInstruction.setVisibility(View.GONE);
        } else {
            // This shop has been targeted for navigation
            // show navigation instruction, not navigation arrow icon

            btnStartNavigation.setVisibility(View.GONE);
            btnShowNavigationInstruction.setVisibility(View.VISIBLE);
        }

        ImageView picture = (ImageView) relLayoutShopBar.findViewById(R.id.navi_shop_img);

        Context context = picture.getContext();
        int id = context.getResources().getIdentifier(shop.icon, "drawable", context.getPackageName());
        picture.setImageResource(id);

        TextView nameText = (TextView) relLayoutShopBar.findViewById(R.id.navi_shop_name);
        nameText.setText(shop.name);

        TextView desText = (TextView) relLayoutShopBar.findViewById(R.id.navi_shop_desc);
        desText.setText(shop.description);

        String shopFloor = shop.location.getAreaId();

        // Put a mark at the shop
        for (Map.Entry<String, MapController>entry : mMapLayout.allMapControllers.entrySet()) {
            String floor = entry.getKey();
            MapController controller = entry.getValue();

            if (floor.equals(shopFloor)) {
                controller.setMarkDrawable(shop.location);
            } else {
                controller.clearMarkDrawable();
            }

        }

        // Change linLayoutFloorList selection
        if (!shop.location.getAreaId().equals(mMapLayout.getLastAreaId())) {
            int indexOfSelectedFloor = Settings.Site.allFloors.indexOf(shop.location.getAreaId());
            selectFloor(indexOfSelectedFloor);
        }

        mMapLayout.bringToCenter(shop.location);
    }


    private void hideShopInfoBarAndMark() {
        relLayoutShopBar.setVisibility(View.GONE);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) btnStartLocalization.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        btnStartLocalization.setLayoutParams(params);

        // Adjust linLayoutFloorList position
        params = (RelativeLayout.LayoutParams) linLayoutFloorList.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        linLayoutFloorList.setLayoutParams(params);

        for (MapController entry : mMapLayout.allMapControllers.values()) {
            entry.clearMarkDrawable();
        }

    }


    private void selectFloor(int floorIndex) {
        for (int i = 0; i < Settings.Site.allFloors.size(); i++) {
            MyFloorButton btn = (MyFloorButton) linLayoutFloorList.getChildAt(i);

            if (i == floorIndex) {
                btn.setBackgroundColor(SELECTED_FLOOR_COLOR);
                floorText.setText(Settings.Site.allFloors.get(i));
            } else {
                btn.setBackgroundColor(Color.TRANSPARENT);
            }

        }

    }


    private void enableFollowBlueDot(boolean enabled, boolean isAutoDisabled) {
        if (isFollowingBlueDot == enabled) {
            return;
        }

        if (!isAutoDisabled) {
            mMapLayout.enableFollowingBluedot(enabled);
        }

        if (enabled) {
            btnFollowBlueDot.setImageResource(R.drawable.heading_enabled);
        } else {
            btnFollowBlueDot.setImageResource(R.drawable.heading_disabled);
        }

        isFollowingBlueDot = enabled;
    }


    private void requestMyPermissions() {
        String[] allPermissions = null;
        try {
            allPermissions = getPackageManager()
                    .getPackageInfo(this.getPackageName(), PackageManager.GET_PERMISSIONS)
                    .requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (allPermissions == null) {
            //Actually this cannot happen;
            DebugTool.printLog(TAG, "allPermissions == null in MainActivity.requestMyPermissions()");
            finish();
        } else {
            boolean allPermissionsAlreadyGranted = true;

            if (Build.VERSION.SDK_INT >= 23) {
                List<String> permissions2request = new ArrayList<>();
                for (String permission : allPermissions) {
                    if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        permissions2request.add(permission);
                    }
                }

                if (!permissions2request.isEmpty()) {
                    allPermissionsAlreadyGranted = false;
                    ActivityCompat.requestPermissions(this,
                            permissions2request.toArray(new String[0]),
                            MY_PERMISSIONS_REQUEST_CODE);
                }
            }

            if (allPermissionsAlreadyGranted) {
                initAfterGettingAllPermissions();
            }

        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        locEngine.stop();
        locEngine.destroy();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                boolean allPermissionsGranted = true;
                if (grantResults.length == 0) {
                    // If request is cancelled, the result arrays are empty.
                    allPermissionsGranted = false;
                } else {
                    PackageManager pm = getPackageManager();
                    for (int i = 0; i < permissions.length; i++) {
                        int grantResult = grantResults[i];
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            allPermissionsGranted = false;
                            try {
                                CharSequence description = pm.getPermissionInfo(permissions[i],
                                        PackageManager.GET_META_DATA).loadDescription(pm);
                                Toast.makeText(MainActivity.this,
                                        "The app needs permission: " + description,
                                        Toast.LENGTH_SHORT).show();
                            } catch (PackageManager.NameNotFoundException e) {
                                // this cannot happen
                                e.printStackTrace();
                            }
                        }
                    }
                }

                if (allPermissionsGranted) {
                    // permission was granted
                    initAfterGettingAllPermissions();
                } else {
                    finish();
                }

                break;
            }
        }
    }


    /**
     * getting the result after selecting a shop from SearchActivity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SEARCH_RESULT) {
            if (resultCode == RESULT_OK) {
                int shopIndex = data.getIntExtra(SearchActivity.NAME_OF_EXTRA_DATA, -1);

                if (shopIndex == -1) {
                    DebugTool.printLog(TAG, "This cannot happen: the returning shopIndex from " +
                            "SearchActivity is: " + shopIndex);
                    return;
                }

                curSelectedShop = Settings.Site.shopList.get(shopIndex);

                showShopInfoBarAndMark(curSelectedShop);
            }

        }

    }


    private class MyLocalizationDetectionListener implements LocalizationDetectionListener {
        @Override
        public void onGotResult(
                final Map<String, List<Position>> topMagneticPositions,
                final Map<String, List<Position>> topWiFiPositions,
                final Position detectedLocation,
                final double meterAccuracy
        ) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (Settings.debugModeTurnOn) {
                        // Remove all debug drawn in last step
                        for (MapController controller : mMapLayout.allMapControllers.values()) {
                            controller.clearFixedPoint();
                        }

                        if (topMagneticPositions != null) {
                            for (Map.Entry<String, MapController> entry : mMapLayout.allMapControllers.entrySet()) {
                                String floor = entry.getKey();
                                MapController controller = entry.getValue();
                                controller.addFixedPoints(topMagneticPositions.get(floor), Color.YELLOW);
                            }
                        }

                        if (topWiFiPositions != null) {
                            for (Map.Entry<String, MapController> entry : mMapLayout.allMapControllers.entrySet()) {
                                String floor = entry.getKey();
                                MapController controller = entry.getValue();
                                controller.addFixedPoints(topWiFiPositions.get(floor), Color.GREEN);
                            }

                        }

                    }

                    if (!detectedLocation.isNone()) {
//                        // HACk: Not always needed
//                        doPositionAttachment(detectedLocation);

                        String detectedArea = detectedLocation.getAreaId();
                        int detectedAreaIndex = Settings.Site.allFloors.indexOf(detectedArea);

                        mMapLayout.updateDetectedLocation(
                                detectedLocation,
                                meterAccuracy,
                                DETECTED_LOCATION_COLOR
                        );

                        // First time to get location, so bring it to center
                        if (localizationJustActivated) {
                             mMapLayout.bringToCenter(detectedLocation);
                             selectFloor(detectedAreaIndex);

                            localizationJustActivated = false;
                        }

                        if (
                            isFollowingBlueDot
                            && !detectedArea.equals(mMapLayout.getLastAreaId())
                        ) {
                            selectFloor(detectedAreaIndex);
                            mMapLayout.reloadMap(detectedArea);
                        }

                        PathFinder.setStartPos(
                                new Position(detectedLocation.getX(), detectedLocation.getY()),
                                detectedLocation.getAreaId()
                        );

                        Position destination = PathFinder.getEndPos();

                        if (destination != null) {
                            if (
                                    doingNavigation && destination.dist(detectedLocation)
                                            < REACHING_NAVIGATION_TARGET_METER_THRESHOLD
                                            * Settings.Site.floorsMapScales.get(detectedArea)
                                    ) {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle(curSelectedShop.name)
                                        .setMessage("You arrived")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }

                                        })
                                        .show();

                                doingNavigation = false;

                                for (MapController controller : mMapLayout.allMapControllers.values()) {
                                    controller.clearLines();
                                }

                            }

                        }

                    }

                    mMapLayout.getCurrentMapController().display();
                }

            });

        }


        private void doPositionAttachment(Position detectedLocation) {
            String detectedArea = detectedLocation.getAreaId();
            float detectedFloorMapScale = Settings.Site.floorsMapScales.get(detectedArea);

            // HACKS: Make the detected location closer to nearby shop
            Shop nearestShop = null;
            double minDist = -1;

            for (Shop shop : Settings.Site.shopList) {
                // Ignore shop not in the current floor
                if (!shop.location.getAreaId().equals(detectedLocation.getAreaId())) {
                    continue;
                }

                // This shop is not a "target" for location "attachment"
                if (shop.gravityRadius < 0) {
                    continue;
                }

                double d = shop.location.dist(detectedLocation) / detectedFloorMapScale;

                if (d <= shop.gravityRadius) {
                    if (nearestShop == null || (nearestShop != null && d < minDist)) {
                        minDist = d;
                        nearestShop = shop;
                    }

                }

            }

            // HACKS: Found a shop for target location "attachment". Perform it
            if (nearestShop != null && minDist > THRESHOLD_METER_LOCATION_ATTACHMENT) {
                double movingDistRatio = (minDist - THRESHOLD_METER_LOCATION_ATTACHMENT) / minDist;

                double curDetectedX = detectedLocation.getX();
                double curDetectedY = detectedLocation.getY();

                detectedLocation.setX(
                        curDetectedX + (nearestShop.location.getX() - curDetectedX) * movingDistRatio
                );

                detectedLocation.setY(
                        curDetectedY + (nearestShop.location.getY() - curDetectedY) * movingDistRatio
                );

            }

        }
    }


    private class MyOnMapClickListener implements OnMapClickListener {
        @Override
        public void onClick(PointF clickPoint) {
            Position clickedPos = new Position(clickPoint.x, clickPoint.y);

            String lastAreaId = mMapLayout.getLastAreaId();
            float scale = Settings.Site.floorsMapScales.get(lastAreaId);
            Shop foundShop = null;

            for (Shop shop : Settings.Site.shopList) {
                if (!shop.location.getAreaId().equals(lastAreaId)) {
                    continue;
                }

                if (shop.location.dist(clickedPos) / scale <= 1) {
                    foundShop = shop;
                    break;
                }

            }

            if (foundShop == null) {
                return;
            }

            curSelectedShop = foundShop;

            showShopInfoBarAndMark(curSelectedShop);
        }

    }


    private class MainActivityOnFollowingBlueDotDisabled implements OnFollowBlueDotAutoDisabled {
        public void onFollowingDisabled() {
            enableFollowBlueDot(false, true);
        }

    }


    private class BtnSearchOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, SearchActivity.class);
            startActivityForResult(intent, SEARCH_RESULT);
        }

    }


    private class BtnShowNavInstructOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();

            intent.setClass(MainActivity.this, NavigationInstructionActivity.class);
            intent.putParcelableArrayListExtra(
                NavigationInstructionActivity.NAV_POINT_KEY,
                navPathPos
            );

            startActivity(intent);
        }

    }


    private class BtnStartNavOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (curSelectedShop == null) {
                Toast.makeText(
                    MainActivity.this,
                    "Please select a shop first",
                    Toast.LENGTH_SHORT
                ).show();

                return;
            }

            PathFinder.setEndPos(curSelectedShop.location, curSelectedShop.location.getAreaId());

            navPathPos = PathFinder.calculate();

            // No path found
            if (navPathPos == null) {
                Toast.makeText(MainActivity.this, "Cannot find a navigation path", Toast.LENGTH_SHORT).show();
                return;
            }

            // Show new path
            curNavigatedShop = curSelectedShop;
            showShopInfoBarAndMark(curNavigatedShop);

            for (Map.Entry<String, MapController> entry : mMapLayout.allMapControllers.entrySet()) {
                entry.getValue().clearLines();
            }

            for (int i = 0; i < navPathPos.size() - 1; i++) {
                Position from = navPathPos.get(i);
                Position to = navPathPos.get(i + 1);

                if (from.getAreaId().equals(to.getAreaId())) {
                    mMapLayout.allMapControllers.get(
                            from.getAreaId()).addLine(
                            from, to,
                            NAVIGATION_PATH_COLOR
                    );

                }

            }

            doingNavigation = true;

            // mMapLayout.getCurrentMapController().display();
        }

    }


    private class BtnCancelNavOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (curSelectedShop == null) {
                return;
            }

            if (curSelectedShop == curNavigatedShop) {
                // This is navigation cancelling

                // Clear all lines
                for (MapController entry : mMapLayout.allMapControllers.values()) {
                    entry.clearLines();
                }

                hideShopInfoBarAndMark();

                doingNavigation = false;
                curNavigatedShop = null;
                navPathPos = null;

            } else if (curNavigatedShop != null) {
                // This is just cancelling the display of currently selected shop

                curSelectedShop = curNavigatedShop;
                showShopInfoBarAndMark(curSelectedShop);

            } else {
                // Just remove shop bar. curNavigatedShop not yet set
                hideShopInfoBarAndMark();
            }

            curSelectedShop = null;
        }

    }


    private class BtnStartLocalizeOnClickListener implements View.OnClickListener {
        private boolean doingLocalization = false;


        @Override
        public void onClick(View v) {
            if (locEngine == null) {
                Log.d(TAG, "locEngine == null, maybe some permission not granted");
                return;
            }

            if (!doingLocalization) {
                // Turn on
                btnStartLocalization.setImageResource(R.drawable.target_orange_with_bg);

                // Show Follow Blue Dot button
                btnFollowBlueDot.setVisibility(View.VISIBLE);
                btnFollowBlueDot.setImageResource(R.drawable.heading_disabled);

                doingLocalization = true;

                isFollowingBlueDot = false;
                localizationJustActivated = true;

                locEngine.start();
            } else {
                // Turn off
                btnStartLocalization.setImageResource(R.drawable.target_with_bg);
                doingLocalization = false;

                mMapLayout.clearDetectedLocation();

                // Hide Follow Blue Dot button
                btnFollowBlueDot.setVisibility(View.GONE);

                locEngine.stop();
            }

        }

    }


    private class BtnFollowBlueDotOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            enableFollowBlueDot(!isFollowingBlueDot, false);
        }

    }


    private class MyFloorButton extends AppCompatButton {
        public int floorIndex;

        public MyFloorButton(Context context, int floorIndex, String floor) {
            super(context);
            this.floorIndex = floorIndex;
            setText(floor);
            setOnClickListener(new FloorButtonOnClickListener());
        }

    }


    private class FloorButtonOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MyFloorButton clickedBtn = (MyFloorButton)v;
            String selectedFloor = Settings.Site.allFloors.get(clickedBtn.floorIndex);

//            DebugTool.printLog(TAG, "select floor: " + selectedFloor + " to display");

            if (mMapLayout.getLastAreaId().equals(selectedFloor)) {
                return;
            }

            selectFloor(clickedBtn.floorIndex);
            mMapLayout.reloadMap(selectedFloor);

            // Disable following bluedot
            enableFollowBlueDot(false, false);
        }

    }




}
