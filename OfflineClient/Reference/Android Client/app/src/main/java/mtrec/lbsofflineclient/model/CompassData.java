package mtrec.lbsofflineclient.model;

public class CompassData {
    public float value;
    public long nanoTimestamp;

    public CompassData(float value, long nanoTimestamp) {
        this.value = value;
        this.nanoTimestamp = nanoTimestamp;
    }
}
