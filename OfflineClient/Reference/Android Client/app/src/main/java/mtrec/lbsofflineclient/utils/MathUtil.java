package mtrec.lbsofflineclient.utils;

import android.graphics.PointF;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MathUtil {
	public static double getAverage(List<Double> nums, int begin, int end) {
        double _sum = 0;
        for (int i = begin; i < end; i++) {
            _sum += nums.get(i);
        }
        return _sum / (end - begin);
    }

    public static double getStandardDeviation(List<Double> nums, int begin, int end) {
        double average = getAverage(nums, begin, end);
        double _sum = 0;
        for (int i = begin; i < end; i++) {
            double temp = nums.get(i) - average;
            _sum += temp * temp;
        }
        return Math.sqrt(_sum / (end - begin));
    }

    public static double getMaximum(List<Double> nums) {
        double _max = nums.get(0);
        for (double num : nums) {
            if (_max < num) {
                _max = num;
            }
        }
        return _max;
    }
    
	public static double mean(List<Integer> list) {
		double sum = 0;
		for (int i : list) {
			sum += i;
		}
		return sum / list.size();
	}

	public static double sum(double[] d) {
		double sum = 0;
		for (double i : d) {
			sum += i;
		}
		return sum;
	}

	static public float max(float[] a) {
		if (a.length == 1) {
			return a[0];
		}
		float maxNum = a[0];
		for (int i = 1; i < a.length; i++) {
			if (a[i] > maxNum) {
				maxNum = a[i];
			}
		}
		return maxNum;
	}

	static public float[] averageSensorValue(ArrayList<float[]> a) {
		float result[] = a.get(0).clone();
		for (int i = 1; i < a.size(); i++) {
			for (int j = 0; j < result.length; j++) {
				result[j] += a.get(i)[j];
			}
		}
		for (int j = 0; j < result.length; j++) {
			result[j] /= a.size();
		}
		return result;
	}

	static public float normalizeDegree(float degree) {
		while (degree >= 180)
			degree -= 360;
		while (degree < -180)
			degree += 360;
		return degree;
	}

	static public float stdInRange(List<Float> data, int start, int end) {
		if (start == end)
			return 0;
		float mean = meanInRange(data, start, end);
		float std = 0;
		for (int i = start; i < end; i++) {
			float temp = data.get(i) - mean;
			std += temp * temp;
		}
		return (float) Math.sqrt(std / (end - start));
	}

	static public float meanInRange(List<Float> data, int start, int end) {
		if (start == end)
			return 0;
		float sum = 0;
		for (int i = start; i < end; i++) {
			sum += data.get(i);
		}
		return sum / (end - start);
	}

	/**
	 * check in the specific scope of the list, whether there exists a number
	 * above a certain value
	 */
	static public boolean hasDataAboveValue(List<Float> data, int start, int end, float value) {
		for (int i = start; i < end; i++) {
			if (data.get(i) > value) {
				return true;
			}
		}
		return false;
	}

	public static double squareDistance(PointF p1, PointF p2) {
		return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
	}

	public static PointF midPoint(PointF p1, PointF p2) {
		return new PointF((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
	}

	public static double distance(PointF p1, PointF p2) {
		return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
	}

    public static double angle(PointF origin, PointF p) {
        return Math.atan2(p.y - origin.y, p.x - origin.x);
    }

	public static double getSlope(Position p1, Position p2) {
		if (Math.abs((p1.getX() - p2.getX())) < ConstantValues.EPS) {
			return Double.NaN;
		}
		return (p1.getY() - p2.getY()) * 1.0 / (p1.getX() - p2.getX());
	}

	public static boolean posOnSegment2(Position pos, Position segBegin, Position segEnd) {
		double px = segBegin.getX(), py = segBegin.getY();
		double qx = pos.getX(), qy = pos.getY();
		double rx = segEnd.getX(), ry = segEnd.getY();
		boolean onLine = Math.abs((qx * ry - qy * rx) - (px * ry - py * rx) + (px * qy - py * qx) ) < 1e-6;
		if (!onLine) {
			return false;
		} else {
			if (Math.abs(px - qx) < ConstantValues.EPS) {
				return (py <= qy && qy <= ry) || (ry <= qy && qy <= py);
			} else {
				return (px <= qx && qx <= rx) || (rx <= qx && qx <= px);
			}
		}
	}

	/**
	 * check if pos is above/on/below the line(pointInLine, slope)
	 * if the slope is Nan, it means the slope is 90 degree,
	 * and then the function will check if the pos is on the left of/on/on the right of the line(pointInLine, slope)
	 *
	 * @param pos the position to be checked
	 * @param pointInLine a point in the line
	 * @param slope the slope of the line
	 * @return
	 */
	public static int posAccording2Line(Position pos, Position pointInLine, double slope) {
		double temp = 0;
		if (Double.isNaN(slope)) {
			temp = pos.getX() - pointInLine.getX();
		} else {
			temp = pos.getY() - (slope * (pos.getX() - pointInLine.getX()) + pointInLine.getY());
		}
		if (Math.abs(temp) < ConstantValues.EPS) {
			return 0;
		} else if (temp < 0) {
			return -1;
		} else {
			return 1;
		}
	}


	public static boolean insidePolygen(List<Position> polygen, Position curPos) {
		for (int i = 0; i < polygen.size(); i++) {
			Position edgeStartPos = polygen.get(i);
			Position edgeEndPos = polygen.get((i + 1) % polygen.size());
			if (MathUtil.posOnSegment2(curPos, edgeStartPos, edgeEndPos)) {
				return false;
			}
		}

		List<Double> allSlope = new ArrayList<Double>();
		for (int i = 0; i < polygen.size(); i++) {
			Position edgeStartPos = polygen.get(i);
			Position edgeEndPos = polygen.get((i + 1) % polygen.size());
			double slope = MathUtil.getSlope(edgeStartPos, edgeEndPos);
			if (!Double.isNaN(slope)) {
				allSlope.add(slope);
			}
		}
		double slope = 0;
		Random random = new Random();
		for (;; slope += random.nextDouble() + ConstantValues.EPS) {
			boolean correct = true;
			for (double s : allSlope) {
				if (Math.abs(s - slope) < ConstantValues.EPS) {
					correct = false;
					break;
				}
			}
			if (!correct) {
				continue;
			}
			for (Position position : polygen) {
				// position on line(curPos, slope)
				if (MathUtil.posAccording2Line(position, curPos, slope) == 0) {
					correct = false;
					break;
				}
			}
			if (correct) {
				break;
			}
		}

		int leftCrossing = 0;
		int rightCrossing = 0;
		for (int i = 0; i < polygen.size(); i++) {
			Position edgeStartPos = polygen.get(i);
			Position edgeEndPos = polygen.get((i + 1) % polygen.size());
			boolean edgeStartPosAboveLine = MathUtil.posAccording2Line(edgeStartPos, curPos, slope) > 0;
			boolean edgeEndPosAboveLine = MathUtil.posAccording2Line(edgeEndPos, curPos, slope) > 0;
			if ((edgeStartPosAboveLine && !edgeEndPosAboveLine) || (edgeEndPosAboveLine && !edgeStartPosAboveLine)) {
				// get intersection point
				double slope2 = MathUtil.getSlope(edgeStartPos, edgeEndPos);
				double interX;
				if (!Double.isNaN(slope2)) {
					interX = (-edgeStartPos.getX() * slope2 + edgeStartPos.getY() + curPos.getX() * slope
							- curPos.getY()) / (slope - slope2);
				} else {
					interX = edgeStartPos.getX();
				}

				if (curPos.getX() < interX) {
					rightCrossing++;
				} else {
					leftCrossing++;
				}
			}
		}

		return (leftCrossing % 2 == 1) && (rightCrossing % 2 == 1);
	}

	public static Position getProjectPointFromPos2LineSeg(Position pos, Position segBegin, Position segEnd) {
		double cross = (segEnd.getX() - segBegin.getX()) * (pos.getX() - segBegin.getX())
				+ (segEnd.getY() - segBegin.getY()) * (pos.getY() - segBegin.getY());
		if (cross <= 0) {
			return segBegin;
		}

		double line_len_square = Math.pow(segBegin.dist(segEnd), 2);
		if (cross >= line_len_square) {
			return segEnd;
		}

		double rate = cross / line_len_square;
		double px = segBegin.getX() + (segEnd.getX() - segBegin.getX()) * rate;
		double py = segBegin.getY() + (segEnd.getY() - segBegin.getY()) * rate;
		return new Position(px, py);
	}

	public static boolean checkLinesIntersection(Position startPos1, Position endPos1, Position startPos2, Position endPos2) {
		startPos1 = new Position(startPos1.getX() + ConstantValues.EPS, startPos1.getY());
		startPos2 = new Position(startPos2.getX() + ConstantValues.EPS, startPos2.getY());

		double k1 = getSlope(startPos1, endPos1);
		double k2 = getSlope(startPos2, endPos2);
		// equation1 : y - endPos1.getY() = k1 * (x - endPos1.getX())
		// equation2 : y - endPos2.getY() = k2 * (x - endPos2.getX())
		// => k1 * (x - endPos1.getX()) + endPos1.getY() == k2 * (x - endPos2.getX()) + endPos2.getY()
		// => k1 * x - k1 * endPos1.getX() + endPos1.getY() == k2 * x - k2 * endPos2.getX() + endPos2.getY()
		// => (k1 - k2) * x == - k2 * endPos2.getX() + endPos2.getY() + k1 * endPos1.getX() - endPos1.getY()
		double x = (- k2 * endPos2.getX() + endPos2.getY() + k1 * endPos1.getX() - endPos1.getY()) / (k1 - k2);
		double y = k1 * (x - endPos1.getX()) + endPos1.getY();
		Position intersection = new Position(x, y);

		return posOnSegment2(intersection, startPos1, endPos1) && posOnSegment2(intersection, startPos2, endPos2);
	}
}
