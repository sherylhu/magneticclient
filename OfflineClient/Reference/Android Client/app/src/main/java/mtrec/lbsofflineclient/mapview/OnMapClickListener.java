package mtrec.lbsofflineclient.mapview;

import android.graphics.PointF;

public interface OnMapClickListener {
	public void onClick(PointF clickPoint);
}
