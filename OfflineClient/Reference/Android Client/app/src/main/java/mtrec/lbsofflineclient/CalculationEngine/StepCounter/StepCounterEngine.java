package mtrec.lbsofflineclient.CalculationEngine.StepCounter;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.List;

import mtrec.lbsofflineclient.CalculationEngine.AbstractEngine;
import mtrec.lbsofflineclient.model.AccelerometerData;
import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.MathUtil;

public class StepCounterEngine extends AbstractEngine {
    private static final String tag = "MyStepCounterEngine";

    // sensor setting
    private SensorManager mSensorManager;
    private AccSensorListener mAccSensorListener;
    public static final int sensorFrequency = 50; //The frequency of getting acceleration data.

    // the calculation time limitation for each period
    private static final long calculationPeriod = 100; //milliseconds

    // parameters for walk-detection
    private static final double walkingDetectionStdThreshold = 0.6;//0.7616689225153694;
    private static final double walkingDetectionStdWindow = 0.8; // second
    private int lastWalkDetectionIndex;

    // parameters for smoothing
    private static final double smoothWindow = 0.31; // seconds
    private int lastSmoothIndex;

    // all stored data and processed results
    private final Object accSyn = new Object();
    private List<AccelerometerData> accBuffer = new ArrayList<AccelerometerData>();
    private List<Double> allRawNormData = new ArrayList<Double>(); // store raw data
    private List<Double> allSmoothData = new ArrayList<Double>(); // store smoothed data
    private List<Long> allNanoTimestamp = new ArrayList<Long>(); // store all the nano-timestamp of each data
    private List<Integer> allFoundPeaks = new ArrayList<Integer>(); // store all the indices of found peaks

    private long firstNanoTimestamp; // all timestamps should be calculated by the difference from {@code firstNanoTimestamp}

    private List<StepCounterDetectionListener> mStepCounterDetectionListeners = new ArrayList<>(); // the result listeners

    public static boolean walking = false;

    public StepCounterEngine(Context context) {
        super(calculationPeriod);

        // init sensor and sensor listener
        mSensorManager = (SensorManager) context.getSystemService(Activity.SENSOR_SERVICE);
        Sensor stepCounterSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mAccSensorListener = new AccSensorListener();
        mSensorManager.registerListener(mAccSensorListener, stepCounterSensor, 1000000 / sensorFrequency);

    }

    public void addDetectionListener(StepCounterDetectionListener stepCounterDetectionListener) {
        mStepCounterDetectionListeners.add(stepCounterDetectionListener);
    }

    private void init() {
        firstNanoTimestamp = Long.MIN_VALUE; // Long.MIN_VALUE is just a symbol of non-initialization.
        lastWalkDetectionIndex = 0;
        lastSmoothIndex = 0;

        allRawNormData.clear();
        allSmoothData.clear();
        allNanoTimestamp.clear();
        allFoundPeaks.clear();
        synchronized (accSyn) {
            accBuffer.clear();
        }
    }

    public void start() {
        init();
        super.start();
    }

    public void stop() {
        super.stop();
        synchronized (accSyn) {
            accBuffer.clear();
        }
    }

    public void destroy() {
        mSensorManager.unregisterListener(mAccSensorListener);
        super.destroy();
    }


    private class AccSensorListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent event) {
            AccelerometerData accData = new AccelerometerData();
            // Here we must copy the event.values one by one instead of using
            // {@code accData.accValues = event.values}, which is just copying a reference
            accData.accValues = new float[]{event.values[0], event.values[1], event.values[2]};
            accData.nanoTimestamp = event.timestamp;

            synchronized (accSyn) {
                accBuffer.add(accData);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }

    @Override
    protected void process() {
        List<AccelerometerData> dataCopy;
        synchronized (accSyn) {
            dataCopy = accBuffer;
            accBuffer = new ArrayList<AccelerometerData>();
        }
        if (dataCopy.isEmpty()) {
            return;
        }
        List<StepCounterDisplayData> allStepCounterDisplayData = new ArrayList<>();
        List<StepCounterDisplayData> rawStepCounterDisplayData = setRawData(dataCopy);
        List<StepCounterDisplayData> smoothStepCounterDisplayData = smoothData();
        List<StepCounterDisplayData> walkDetectionStepCounterDisplayData = walkDetection();

        allStepCounterDisplayData.addAll(walkDetectionStepCounterDisplayData);

//        if (!allStepCounterDisplayData.isEmpty()) {
//            for (StepCounterDetectionListener stepCounterDetectionListener : mStepCounterDetectionListeners) {
//                stepCounterDetectionListener.onDisplayData(allStepCounterDisplayData);
//            }
//        }

        if (allRawNormData.size() > sensorFrequency * 10) { // 10 seconds
            init();
        }
    }

    /**
     * Store raw acceleration data and their nano-timestamps.
     * @param dataCopy the updated data in a calculation period.
     * @return the raw data for display.
     */
    private List<StepCounterDisplayData> setRawData(List<AccelerometerData> dataCopy) {
        List<StepCounterDisplayData> allStepCounterDisplayData = new ArrayList<>();
        // Note that all nano-timestamps are the time period, beginning from {@code firstNanoTimestamp}
        for (AccelerometerData acc : dataCopy) {
            double magnitude = acc.getMagnitude();
            long nanoTimestamp = acc.nanoTimestamp;
            allRawNormData.add(magnitude);
            allNanoTimestamp.add(nanoTimestamp);

            // get beginning of time period.
            if (firstNanoTimestamp == Long.MIN_VALUE) {
                firstNanoTimestamp = nanoTimestamp;
            }
            // display raw data
            double curTimestamp = (double) (nanoTimestamp - firstNanoTimestamp) / 1000000000L;
            allStepCounterDisplayData.add(new StepCounterDisplayData(
                    acc, magnitude, Double.NaN,
                    Double.NaN, Double.NaN, curTimestamp
            ));
        }
        return allStepCounterDisplayData;
    }

    /**
     * Smooth acceleration data and store.
     * @return the smoothed data for display.
     */
    private List<StepCounterDisplayData> smoothData() {
        List<StepCounterDisplayData> allStepCounterDisplayData = new ArrayList<>();
        // Every time we smooth date from {@code lastSmoothIndex} to
        // {@code allRawNormData.size() - halfSmoothWindowSide}.
        // The smoothing window is {@code 2 * halfSmoothWindowSide}.
        // For the data within [allRawNormData.size() - halfSmoothWindowSide, allRawNormData.size()),
        // the system would wait for more data to smooth.
        int beginIndex = lastSmoothIndex;
        int halfSmoothWindowSide = (int) (smoothWindow / 2.0 * sensorFrequency);
        for (int index = beginIndex; index < allRawNormData.size(); index++) {
            int windowLeftSide = Math.max(index - halfSmoothWindowSide, 0);
            int windowRightSide = index + halfSmoothWindowSide;
            if (windowRightSide > allRawNormData.size()) {
                // Not enough window size for smooth.
                // Smooth will begin from {@code index} next time.
                lastSmoothIndex = index;
                break;
            }
            // Here the code can be refined to reduce some duplicated calculation.
            double smoothData = MathUtil.getAverage(allRawNormData, windowLeftSide, windowRightSide);
            allSmoothData.add(smoothData);

            // Display raw data. Note that {@code curTimestamp} is in second.
            double curTimestamp = (double) (allNanoTimestamp.get(index) - firstNanoTimestamp) / 1000000000L;
            allStepCounterDisplayData.add(new StepCounterDisplayData(
                    null, Double.NaN, smoothData,
                    Double.NaN, Double.NaN, curTimestamp
            ));
        }
        return allStepCounterDisplayData;
    }

    /**
     * Use raw data to detect if the user is idle or walking.
     * The algorithm is to detect if the std is above a threshold {@code walkingDetectionStdThreshold}
     * within a moving window {@code walkingDetectionStdWindow}.
     *
     * @return the walking data for display.
     */
    private List<StepCounterDisplayData> walkDetection() {
        List<StepCounterDisplayData> allStepCounterDisplayData = new ArrayList<>();
        if (!allRawNormData.isEmpty() && lastWalkDetectionIndex != allRawNormData.size()) {
            // calculate the std of allRawNormData within walkingDetectionStdWindow
            int halfStdWindowSide = (int) (walkingDetectionStdWindow / 2.0 * sensorFrequency);
            for (int index = lastWalkDetectionIndex; index < allRawNormData.size(); index++) {
                int windowLeftSide = Math.max(index - halfStdWindowSide, 0);
                int windowRightSide = index + halfStdWindowSide;
                if (windowRightSide > allRawNormData.size()) {
                    lastWalkDetectionIndex = index;
                    break;
                }
                // Here the code can be refined to reduce some duplicated calculation.
                double std = MathUtil.getStandardDeviation(allRawNormData, windowLeftSide, windowRightSide);
                // If std > walkingDetectionStdThreshold, then we declare the user is walking
                if (std > walkingDetectionStdThreshold) {
                    double curTimestamp = (double) (allNanoTimestamp.get(index) - firstNanoTimestamp) / 1000000000L;
                    allStepCounterDisplayData.add(new StepCounterDisplayData(
                            null, Double.NaN, Double.NaN, allRawNormData.get(index),
                            Double.NaN, curTimestamp
                    ));

                    walking = true;
                    DebugTool.printLog(tag, "walking");

                    for (StepCounterDetectionListener stepCounterDetectionListener : mStepCounterDetectionListeners) {
                        stepCounterDetectionListener.onWalkingDetected();
                    }
                } else {

                    walking = false;
                    DebugTool.printLog(tag, "not walking");

                    for (StepCounterDetectionListener stepCounterDetectionListener : mStepCounterDetectionListeners) {
                        stepCounterDetectionListener.onIdleDetected();
                    }
                }
            }
        }
        return allStepCounterDisplayData;
    }
}
