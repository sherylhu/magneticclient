package mtrec.lbsofflineclient.utils.pathFinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.MapConstraint;
import mtrec.lbsofflineclient.utils.MathUtil;
import mtrec.lbsofflineclient.utils.Position;
import mtrec.lbsofflineclient.utils.Settings;

public class PathFinder {
    private static final String tag = "MyPathFinder";

    private static Position[] points;
    private static List<Integer>[] connection;
    private static List<Double>[] connectionWeights;       // weight in meter



    public static void init() {
        List<Position> allConnectionPoints = new ArrayList<>();

        for (Map.Entry<String, List<Position>> entry : Settings.Site.floorMapConnectionPoints.entrySet()) {
            allConnectionPoints.addAll(entry.getValue());
        }

        Map<FloorSwitchPair, FloorSwitchPoint> floorSwitchPairs = new HashMap<>();

        for (FloorSwitchPoint floorSwitchPoint : Settings.Site.floorSwitchPoints) {
            int indexOfLocation1 = allConnectionPoints.size();
            allConnectionPoints.add(floorSwitchPoint.location1);

            int indexOfLocation2 = allConnectionPoints.size();
            allConnectionPoints.add(floorSwitchPoint.location2);

            floorSwitchPairs.put(
                    new FloorSwitchPair(indexOfLocation1, indexOfLocation2), floorSwitchPoint
            );

        }

        points = new Position[allConnectionPoints.size() + 2];

        for (int i = 0; i < allConnectionPoints.size(); i++) {
            points[i + 1] = allConnectionPoints.get(i);
        }

        connection = new ArrayList[allConnectionPoints.size() + 2];

        for (int i = 0; i < connection.length; i++) {
            connection[i] = new ArrayList<>();
        }

        connectionWeights = new ArrayList[allConnectionPoints.size() + 2];

        for (int i = 0; i < connectionWeights.length; i++) {
            connectionWeights[i] = new ArrayList<>();
        }

        for (int i = 0; i < allConnectionPoints.size(); i++) {
            Position pos1 = allConnectionPoints.get(i);
            if (!MapConstraint.checkValidPointForInOutConstraint(pos1, pos1.getAreaId())) {
                continue;
            }

            for (int j = i + 1; j < allConnectionPoints.size(); j++) {
                // check if i connects j
                Position pos2 = allConnectionPoints.get(j);

                if (!pos2.getAreaId().equals(pos1.getAreaId())
                    || !MapConstraint.checkValidPointForInOutConstraint(pos2, pos2.getAreaId())
                ) {
                    continue;
                }

                boolean canConnect = true;

                for (List<Position> constraint : MapConstraint.getAllConstraint(pos2.getAreaId())) {
                    for (int k = 0; k < constraint.size(); k++) {
                        int next = (k + 1 == constraint.size()) ? 0 : k + 1;
                        if (
                            MathUtil.checkLinesIntersection(pos1, pos2, constraint.get(k), constraint.get(next))
                        ) {
                            canConnect = false;
                            break;
                        }
                    }

                    if (!canConnect) {
                        break;
                    }

                }

                if (canConnect) {
                    double weight = pos1.dist(pos2) / Settings.Site.floorsMapScales.get(pos1.getAreaId());

                    connection[i + 1].add(j + 1);
                    connectionWeights[i + 1].add(weight);

                    connection[j + 1].add(i + 1);
                    connectionWeights[j + 1].add(weight);
                }

            }

        }

        for (Map.Entry<FloorSwitchPair, FloorSwitchPoint> entry : floorSwitchPairs.entrySet()) {
            FloorSwitchPair floorSwitchPair = entry.getKey();
            FloorSwitchPoint floorSwitchPoint = entry.getValue();

            int indexOfLocation1 = floorSwitchPair.indexOfLocation1;
            int indexOfLocation2 = floorSwitchPair.indexOfLocation2;

            Position pos1 = allConnectionPoints.get(indexOfLocation1);
            Position pos2 = allConnectionPoints.get(indexOfLocation2);

            if (
                !MapConstraint.checkValidPointForInOutConstraint(pos1, pos1.getAreaId())
                || !MapConstraint.checkValidPointForInOutConstraint(pos2, pos2.getAreaId())
            ) {
                continue;
            }

            connection[indexOfLocation1 + 1].add(indexOfLocation2 + 1);
            connectionWeights[indexOfLocation1 + 1].add(floorSwitchPoint.switchWeightInMeters);

            connection[indexOfLocation2 + 1].add(indexOfLocation1 + 1);
            connectionWeights[indexOfLocation2 + 1].add(floorSwitchPoint.switchWeightInMeters);
        }

    }


    public static void setStartPos(Position startPos, String area) {
        points[0] = startPos;
        startPos.setAreaId(area);
    }


    public static void setEndPos(Position endPos, String area) {
        points[points.length - 1] = endPos;
        endPos.setAreaId(area);
    }


    public static ArrayList<Position> calculate() {
        DebugTool.printLog(
            tag,
            "points[0] = " + points[0] + "; points[points.length - 1] = " + points[points.length - 1]
        );

        if (points[0] == null || points[points.length - 1] == null) {
            return null;
        }

        // make connection of startPos and endPos
        initConnection();
        findStartOrEndConnection(0);
        findStartOrEndConnection(points.length - 1);

        return dijkstra(0, points.length - 1);
    }


    private static void initConnection() {
        connection[0].clear();
        connection[points.length - 1].clear();
        connectionWeights[0].clear();
        connectionWeights[points.length - 1].clear();
        for (int i = 0; i < connection.length; i++) {
            List<Integer> newConnectionList = new ArrayList<>();
            List<Double> newConnectionWeightList = new ArrayList<>();
            for (int index = 0; index < connection[i].size(); index++) {
                int ele = connection[i].get(index);
                if (ele != 0 && ele != points.length - 1) {
                    newConnectionList.add(ele);
                    newConnectionWeightList.add(connectionWeights[i].get(index));
                }
            }
            connection[i] = newConnectionList;
            connectionWeights[i] = newConnectionWeightList;
        }

    }


    public static Position getEndPos() {
        return points[points.length - 1];
    }


    private static void findStartOrEndConnection(int index) {
        Position pos1 = points[index];
        for (List<Position>outConstraint : MapConstraint.floorMapOutConstraints.get(pos1.getAreaId())) {
            if (!MathUtil.insidePolygen(outConstraint, pos1)) {
                DebugTool.printLog(tag, "point " + pos1 + " is not inside outConstraint");
                return;
            }
        }

        for (int j = 0; j < points.length; j++) {
            if (j == index) {
                continue;
            }
            // check if i connects j
            Position pos2 = points[j];
            if (!pos1.getAreaId().equals(pos2.getAreaId())) {
                continue;
            }

            boolean canConnect = true;
            for (List<Position> constraint : MapConstraint.getAllConstraint(pos2.getAreaId())) {
                for (int k = 0; k < constraint.size(); k++) {
                    int next = (k + 1 == constraint.size()) ? 0 : k + 1;
                    if (MathUtil.checkLinesIntersection(pos1, pos2, constraint.get(k), constraint.get(next))) {
                        canConnect = false;
                        break;
                    }
                }
                if (!canConnect) {
                    break;
                }
            }
            if (canConnect) {
                connection[index].add(j);
                double weight = pos1.dist(pos2) / Settings.Site.floorsMapScales.get(pos1.getAreaId());
                connectionWeights[index].add(weight);
                if (j != 0 && j != points.length - 1) {
                    connection[j].add(index);
                    connectionWeights[j].add(weight);
                }
            }
        }
    }


    private static ArrayList<Position> dijkstra(int begin, int end) {
        double[] shortestDist = new double[connection.length];

        Arrays.fill(shortestDist, -1);
        shortestDist[begin] = 0;

        int[] previous = new int[connection.length];
        previous[begin] = -1;

        boolean[] checked = new boolean[connection.length]; // default is false

        int curShortestNode = begin;

        while (true) {
            if (curShortestNode == end) {
                LinkedList<Position> answer = new LinkedList<>();
                int temp = end;
                while (temp != -1) {
                    answer.addFirst(points[temp]);
                    temp = previous[temp];
                }

                return new ArrayList<>(answer);
            }

            double curShortestDist = Double.MAX_VALUE;
            checked[curShortestNode] = true;

            int lastShortestNode = curShortestNode;
            for (int nextNodeIndex = 0; nextNodeIndex < connection[lastShortestNode].size(); nextNodeIndex++) {
                int nextNode = connection[lastShortestNode].get(nextNodeIndex);
                if (checked[nextNode]) {
                    continue;
                }

                double nextDist = connectionWeights[lastShortestNode].get(nextNodeIndex);
                if (shortestDist[nextNode] == -1 || shortestDist[nextNode] > shortestDist[lastShortestNode] + nextDist) {
                    shortestDist[nextNode] = shortestDist[lastShortestNode] + nextDist;
                    previous[nextNode] = lastShortestNode;
                }

            }
            // find new shortest node
            for (int i = 0; i < shortestDist.length; i++) {
                if (checked[i] || shortestDist[i] == -1) {
                    continue;
                }

                if (shortestDist[i] < curShortestDist) {
                    curShortestDist = shortestDist[i];
                    curShortestNode = i;
                }

            }

            if (curShortestDist == Double.MAX_VALUE) {
                // cannot find path anymore
                return null;
            }

        }

    }

    private static class FloorSwitchPair {
        int indexOfLocation1;
        int indexOfLocation2;

        public FloorSwitchPair(int indexOfLocation1, int indexOfLocation2) {
            this.indexOfLocation1 = indexOfLocation1;
            this.indexOfLocation2 = indexOfLocation2;
        }

    }

}
