package mtrec.lbsofflineclient.CalculationEngine.LocalizationEngine;

import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.utils.Position;

public interface LocalizationDetectionListener {
    void onGotResult(
        Map<String, List<Position>> topMagneticPositions,
        Map<String, List<Position>> topWiFiPositions,
        Position detectedLocation,
        double meterAccuracy
    );

}
