package mtrec.lbsofflineclient.CalculationEngine.WiFiEngine;

public class WiFiUtils {
    public static String uniformApAddress(String apAddress) {
        return apAddress.replace("\"", "").replace(":", "").toLowerCase();
    }

    public static double signalDBM2Watt(double dbm) {
        // return watt * 10000, making it not so small
        return Math.pow(2, dbm / 10.0) * 10000;
    }

    public static boolean isVirtualMac(String hexMacAddrStr) {
        String hexMacAddr = hexMacAddrStr.replace(":", "").replace("\"", "");
        if (hexMacAddr.length() % 2 != 0)
            hexMacAddr = "0" + hexMacAddr;
        byte[] macAddr = hexStringToBytes(hexMacAddr);
        return (macAddr[0] & 0x02) != 0;
    }

    private static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }
}
