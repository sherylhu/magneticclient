package mtrec.lbsofflineclient.mapview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.Position;
import mtrec.lbsofflineclient.utils.Settings;

public class MapLayout extends RelativeLayout implements OnFollowBlueDotAutoDisabled {
    private static final String TAG = "MapLayout";
    private static final int evaluatedColor = Color.BLUE;



    private String lastAreaId;
    private List<String> allAreaIds;

    public Map<String, MapController> allMapControllers = new HashMap<String, MapController>();
    private MapController currentMapController = null;

    private OnFollowBlueDotAutoDisabled onFollowBlueDotAutoDisabled;



    public MapLayout(Context context) {
        super(context);
        init(context);
    }


    public MapLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public MapLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public void onFollowingDisabled() {
        if (onFollowBlueDotAutoDisabled != null) {
            onFollowBlueDotAutoDisabled.onFollowingDisabled();
        }

    }


    public String getLastAreaId() {
        return lastAreaId;
    }


    public void setOnFollowBlueDotAutoDisabled(OnFollowBlueDotAutoDisabled e) {
        this.onFollowBlueDotAutoDisabled = e;
    }


    public void enableFollowingBluedot(boolean enabled) {
        for (MapController controller: allMapControllers.values()) {
            controller.getMapView().enableFollowingBlueDot(enabled);
        }

    }


    public void init(Context context) {
        allAreaIds = Settings.Site.allFloors;
        lastAreaId = allAreaIds.get(0);

        for (String floor : Settings.Site.allFloors) {
            String mapAddr = Settings.Site.getMapPath(floor);
            MapController controller = new MapController(context, mapAddr);

            controller.getMapView().setOnFollowBlueDotAutoDisabled(this);

            allMapControllers.put(floor, controller);


        }

        currentMapController = allMapControllers.get(lastAreaId);

        addView(currentMapController.getMapView());
    }


    public void bringToCenter(Position loc) {
        if (loc == null) {
            return;
        }

        if (loc.isNone()) {
            return;
        }

        String areaId = loc.getAreaId();

        if (!allAreaIds.contains(areaId)) {
            DebugTool.printLog(TAG, "Wrong areaId");
            return;
        }

        if (!lastAreaId.equals(areaId)) {
            reloadMap(areaId);
        }

        currentMapController.focus(loc);
    }


    public void updateDetectedLocation(Position loc, double meterAccuracy, int color) {
        if (loc == null) {
            return;
        }

        if (loc.isNone()) {
            return;
        }

        String areaId = loc.getAreaId();

        if (!allAreaIds.contains(areaId)) {
            DebugTool.printLog(TAG, "Wrong areaId");
            return;
        }

        double originMapPixelAccuracy = meterAccuracy * Settings.Site.floorsMapScales.get(areaId);

        for (Map.Entry<String, MapController> entry : allMapControllers.entrySet()) {
            String floor = entry.getKey();
            MapController controller = entry.getValue();

            if (floor.equals(areaId)) {
                controller.setDetectedLocation(loc, originMapPixelAccuracy, color);
            } else {
                controller.clearDetectedLocation();
            }

        }

    }


    public void clearDetectedLocation() {
        for (MapController controller: allMapControllers.values()) {
            controller.clearDetectedLocation();
        }

    }


    public void reloadMap(String areaId) {
        if (!allAreaIds.contains(areaId)) {
            DebugTool.printLog(TAG, "The area/floor id is wrong");
            return;
        }

        if (!lastAreaId.equals(areaId)) {
            lastAreaId = areaId;
            currentMapController = allMapControllers.get(areaId);
            removeAllViews();

            currentMapController.getMapView().reload();
            addView(currentMapController.getMapView());
        }

    }


    public MapController getCurrentMapController() {
        return currentMapController;
    }


    public void addDrawable(String areaId, Position pos, Drawable drawable) {
        MapController controller = allMapControllers.get(areaId);
        controller.addFixedDrawable(drawable, pos);
    }


    public void addText(String areaId, Position pos, String text) {
        MapController controller = allMapControllers.get(areaId);
        controller.addFixedText(pos, Color.BLACK, text);
    }


    public void setOnMapClickListener(OnMapClickListener onMapClickListener) {
        for (Map.Entry<String, MapController>entry : allMapControllers.entrySet()) {
            entry.getValue().setOnMapClickListener(onMapClickListener);
        }

    }

}
