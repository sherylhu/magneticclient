package mtrec.lbsofflineclient.CalculationEngine.LocalizationEngine;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mtrec.lbsofflineclient.CalculationEngine.AbstractEngine;
import mtrec.lbsofflineclient.CalculationEngine.MagneticEngine.MagneticDetectionListener;
import mtrec.lbsofflineclient.CalculationEngine.MagneticEngine.MagneticEngine;
import mtrec.lbsofflineclient.CalculationEngine.StepCounter.StepCounterEngine;
import mtrec.lbsofflineclient.CalculationEngine.WiFiEngine.WiFiDetectionListener;
import mtrec.lbsofflineclient.CalculationEngine.WiFiEngine.WiFiEngine;
import mtrec.lbsofflineclient.utils.ConstantValues;
import mtrec.lbsofflineclient.utils.DebugTool;
import mtrec.lbsofflineclient.utils.MapConstraint;
import mtrec.lbsofflineclient.utils.MathUtil;
import mtrec.lbsofflineclient.utils.Position;
import mtrec.lbsofflineclient.utils.ResultSmooth;
import mtrec.lbsofflineclient.utils.Settings;

public class LocalizationEngine extends AbstractEngine {
    private static final String TAG = "MyLocalizationEngine";

    private static final double DEFAULT_METER_ACCURACY = 3;
    private static final double BIG_CLUSTER_SIZE_PERCENTAGE = 0.5;

    // the calculation time limitation for each period
    private static final long CALCULATION_PERIOD = 500; //milliseconds



    private MagneticEngine mMagneticEngine;
    private WiFiEngine mWiFiEngine;
    private StepCounterEngine mStepCounterEngine;

    private List<LocalizationDetectionListener> allLocalizationDetectionListeners = new ArrayList<>();

    private final Object allPotentialPosSynObj = new Object();
    private Map<String, List<Position>> mapFloorMagCandidates = null;

    private Map<String, List<Position>> mapFloorWifiCandidates = null;
    private final Object allWiFiCandidatePosSynObj = new Object();
    private String specialAp = null;

    private String lastAreaId = "";
    private Position lastPos = new Position();

    private ResultSmooth resultSmooth = new ResultSmooth();



    public LocalizationEngine(Context context) {
        super(CALCULATION_PERIOD);
        mMagneticEngine = new MagneticEngine(context);
        mMagneticEngine.addMagneticDetectionListener(new MyMagneticDetectionListener());

        mWiFiEngine = new WiFiEngine(context);
        mWiFiEngine.addDetectionListener(new MyWiFiDetectionListener());

        mStepCounterEngine = new StepCounterEngine(context);

//        loadPolygon();
    }


    public void start() {
        super.start();
        mWiFiEngine.start();
        mMagneticEngine.start();
        mStepCounterEngine.start();
    }


    public void stop() {
        super.stop();
        mWiFiEngine.stop();
        mMagneticEngine.stop();
        mStepCounterEngine.stop();
    }


    public void destroy() {
        super.destroy();
        mWiFiEngine.destroy();
        mMagneticEngine.destroy();
        mStepCounterEngine.destroy();
    }


    public void addLocalizationDetectionListener(LocalizationDetectionListener localizationDetectionListener) {
        allLocalizationDetectionListeners.add(localizationDetectionListener);
    }


    @Override
    protected void process() {

        //Ivy Mark 0. Fusion Start
        Position detectedPosition = new Position();

        List<Position> detectedWiFiPos = null;
        List<Position> detectedMagPos = null;

        DebugTool.printLog(TAG, "****************Running fusion*******************");

        Map<String, List<Position>> mapFloorMagCandidatesCopy = null;
        Map<String, List<Position>> mapFloorWifiCandidatesCopy = null;

        if (specialAp != null) {
            // FOR PCCW DEMO ONLY. Not generally applicable

//            // proximity
//            if (specialApCopy.equals("e6956e41a17a")) {
//                detectedPosition = new Position(736, 177, "1F");
//            } else if (specialApCopy.equals("e6956e41a18f")) {
//                detectedPosition = new Position(782, 202, "1F");
//            } else if (specialApCopy.equals("e6956e41a6e0")) {
//                detectedPosition = new Position(744, 269, "1F");
//            } else if (specialApCopy.equals("e6956e41a255")) {
//                detectedPosition = new Position(715, 244, "1F");
//            }

        } else {
            synchronized (allPotentialPosSynObj) {
                mapFloorMagCandidatesCopy = mapFloorMagCandidates;
            }

            // Without magnetic data, we cannot output anything even if we have WiFi.
            if (mapFloorMagCandidatesCopy == null) {
                DebugTool.printLog(TAG, "Got NO MagneticData");
                return;
            }

            synchronized (allWiFiCandidatePosSynObj) {
                mapFloorWifiCandidatesCopy = mapFloorWifiCandidates;
            }

            if (mapFloorWifiCandidatesCopy == null) {
                DebugTool.printLog(TAG, "Got NO WiFiData");
                return;
            }

            detectedPosition = detectLocation(
                mapFloorWifiCandidatesCopy,
                mapFloorMagCandidatesCopy
            );

        }

        if (detectedPosition.isNoneArea()) {
            detectedPosition.setAreaId(lastAreaId);
        }

        boolean detectFloorSwitch = false;
        boolean canSwitchFloor = false;

        if (!lastAreaId.equals("") && !detectedPosition.isNoneArea() && !detectedPosition.getAreaId().equals(lastAreaId)) {
            detectFloorSwitch = true;

            if (!lastPos.isNoneXY()) {
                for (List<Position> switchArea: Settings.Site.floorSwitchAreas.get(lastAreaId)) {
                    if (MathUtil.insidePolygen(switchArea, lastPos)) {
                        canSwitchFloor = true;
                        break;
                    }

                }

            }

        }

        if (detectFloorSwitch && canSwitchFloor) {
            resultSmooth.clear();
        }

        if (!detectedPosition.isNone()) {
            // Smooth result
            resultSmooth.addResult(detectedPosition);

            detectedPosition = resultSmooth.getNextRfPosition(
                    Settings.Site.floorsMapScales.get(detectedPosition.getAreaId())
            );

            // map constraint
            detectedPosition = MapConstraint.run(detectedPosition);

        }

        lastAreaId = detectedPosition.getAreaId();
        lastPos = detectedPosition;

        double meterAccuracy = DEFAULT_METER_ACCURACY;

        for (LocalizationDetectionListener localizationDetectionListener : allLocalizationDetectionListeners) {
            localizationDetectionListener.onGotResult(
                mapFloorMagCandidatesCopy,
                mapFloorWifiCandidatesCopy,
                detectedPosition,
                meterAccuracy
            );

        }

    }


    private class MyMagneticDetectionListener implements MagneticDetectionListener {
        @Override
        public void onTopPositionsDetected(Map<String, List<Position>> topPositions) {
            if (topPositions != null) {
                synchronized (allPotentialPosSynObj) {
                    mapFloorMagCandidates = topPositions;
                }

            }

        }

    }


    private class MyWiFiDetectionListener implements WiFiDetectionListener {
        @Override
        public void onApproachingSpecialAp(String ap) {
            // For PCCW Demo only. Not generally applicable
//            specialAp = ap;
        }


        @Override
        public void onTopPositionsDetected(Map<String, List<Position>> topPositions) {
            if (topPositions != null) {
                synchronized (allWiFiCandidatePosSynObj) {
                    mapFloorWifiCandidates = topPositions;
                }

            }

        }

    }


    private Position detectLocation(
        Map<String, List<Position>> mapFloorWifiCandidates,
        Map<String, List<Position>> mapFloorMagCandidates
    ) {
        DebugTool.printLog(TAG, "First attempt: Fusion");

        Position fusionResult = detectLocationByFusion(
            mapFloorWifiCandidates,
            mapFloorMagCandidates
        );

        // This branch means we already got a good result from fusion algo
        if (!fusionResult.isNone()) {
            return fusionResult;
        }

        String detectedArea = fusionResult.getAreaId();

        DebugTool.printLog(TAG, "Second attempt: Wifi only");

        Position wifiResult = detectLocationByWifi(mapFloorWifiCandidates, detectedArea);

        return wifiResult;
    }


    private Position detectLocationByFusion(
        Map<String, List<Position>> mapFloorWifiCandidates,
        Map<String, List<Position>> mapFloorMagCandidates
    ) {
        Position detectedLocation = null;
        String detectedArea = null;

        Map<String, List<List<Position>>> floorMapBigEnoughClusters = new HashMap<>();

        for (String floor : Settings.Site.allFloors) {
            DebugTool.printLog(TAG, "In " + floor + ":");

            List<Position> wifiPos = mapFloorWifiCandidates.get(floor);
            List<Position> magPos = mapFloorMagCandidates.get(floor);

            DebugTool.printLog(TAG, "wifiPos.size(): " + wifiPos.size());
            DebugTool.printLog(TAG, "magPos.size(): " + magPos.size());

            if (!wifiPos.isEmpty() && !magPos.isEmpty()) {
                double scale = Settings.Site.floorsMapScales.get(floor);

                double pixelDistForIntersect =
                        (Math.sqrt(2) * Settings.Site.WiFi.meterDistBetweenWiFiRfPts)
                        * scale;

                List<Position> intersectList = findIntersection(
                    wifiPos,
                    magPos,
                    pixelDistForIntersect
                );

                double pixelDistBetweenRfPoint = Settings.Site.Mag.meterDistBetweenMagRfPts * scale;

                // A little bit more than 2 meters
                double pixelsDistOfNeighbor = pixelDistBetweenRfPoint * 2 + ConstantValues.EPS;

                // Hardcode, you can see this as constant
                int neighborForCluster = 4;

                List<List<Position>> bigEnoughClusters = findBigEnoughClusters(
                    intersectList,
                    pixelsDistOfNeighbor,
                    neighborForCluster,
                    (int)(
                        Settings.Site.Mag.numberOfSelectedPositionWithTopPotential
                        * Settings.Site.Fusion.percentage2DefineBigEnoughCluster
                    )

                );

//                DebugTool.printLog(TAG, bigEnoughClusters.size() + " bigEnoughClusters found");

                if (!bigEnoughClusters.isEmpty()) {
                    // Only the not empty clusters can be added into floorMapBigEnoughClusters.
                    // If bigEnoughClusters.isEmpty(), then it means no cluster found in this floor.
                    floorMapBigEnoughClusters.put(floor, bigEnoughClusters);
                }

            }

        }

        if (floorMapBigEnoughClusters.isEmpty()) {
            DebugTool.printLog(TAG, "Cannot find any big enough cluster in all floors");
            mMagneticEngine.clearHistory();

        } else if (floorMapBigEnoughClusters.size() > 1) {
            DebugTool.printLog(
                TAG, "Big enough clusters appear in multi floors, so cannot distinguish these floors"
            );

        // floorMapBigEnoughClusters.size() == 1
        } else {
            detectedArea = floorMapBigEnoughClusters.keySet().iterator().next();

            List<List<Position>> bigEnoughClusters = floorMapBigEnoughClusters.get(detectedArea);

            DebugTool.printLog(TAG, "Big enough clusters only appear in " + detectedArea);

            if (bigEnoughClusters.size() > 1) {
                DebugTool.printLog(
                    TAG,
                    "But more than 1 clusters appear in "
                        + detectedArea
                        + ", so we cannot distinguish which cluster is the ground truth"
                );

            // bigEnoughClusters.size() = 1 (It can't be zero)
            } else {
                List<Position>bigEnoughCluster = bigEnoughClusters.get(0);
                detectedLocation = averagePositions(bigEnoughCluster);

                DebugTool.printLog(
                    TAG,
                    "Only 1 cluster appear in "
                        + detectedArea
                        + "! Now we find the result location: " + detectedLocation.toString()
                );

                detectedLocation.setAreaId(detectedArea);

                return detectedLocation;
            }

        }

        if (detectedArea == null) {
            return new Position();
        } else {
            return new Position(detectedArea);
        }

    }


    private Position detectLocationByWifi(
        Map<String, List<Position>> mapFloorWifiCandidates,
        String detectedArea
    ) {
        List<String>allFloors;

        if (detectedArea.equals("")) {
            allFloors = Settings.Site.allFloors;
        } else {
            allFloors = new ArrayList<>();
            allFloors.add(detectedArea);
        }

        Position detectedLocation = null;

        Map<String, List<List<Position>>> mapFloorBigEnoughClusters = new HashMap<>();

        for (String floor : allFloors) {
            List<Position> detectedPositions = mapFloorWifiCandidates.get(floor);

            if (!detectedPositions.isEmpty()) {
                double scale = Settings.Site.floorsMapScales.get(floor);

                double pixelsDistBetweenRf = Settings.Site.WiFi.meterDistBetweenWiFiRfPts * scale;

                double pixelsDistOfNeighbor = pixelsDistBetweenRf * 2 + ConstantValues.EPS; // a little bit more than 2 meters

                // Hardcode, you can see this as constant
                int NEIGHBOR_FOR_WIFI_CLUSTER = 4;

                List<List<Position>> bigEnoughClusters = findBigEnoughClusters(
                    detectedPositions, pixelsDistOfNeighbor, NEIGHBOR_FOR_WIFI_CLUSTER,
                    (int) (
                        Settings.Site.WiFi.numberOfSelectedPositionWithTopPotential *
                        Settings.Site.WiFi.percentage2DefineBigEnoughCluster)
                );

                DebugTool.printLog(TAG, bigEnoughClusters.size() + " bigEnoughClusters found");

                if (!bigEnoughClusters.isEmpty()) {
                    mapFloorBigEnoughClusters.put(floor, bigEnoughClusters);
                }

            }

        }

        if (mapFloorBigEnoughClusters.isEmpty()) {
            DebugTool.printLog(
                TAG,
                "Pure Wifi: Cannot find any big enough cluster in all floors"
            );

        } else if (mapFloorBigEnoughClusters.size() > 1) {
            DebugTool.printLog(
                TAG,
                "Pure Wifi: Big enough clusters appear in multi floors, "
                    + " so cannot distinguish these floors"
            );

        // mapFloorBigEnoughClusters.size() == 1
        } else {
            detectedArea = mapFloorBigEnoughClusters.keySet().iterator().next();

            List<List<Position>> bigEnoughClusters = mapFloorBigEnoughClusters.get(detectedArea);

            DebugTool.printLog(
                TAG,
                "Pure Wifi: Big enough clusters only appear in " + detectedArea
            );

            if (bigEnoughClusters.size() > 1) {
                DebugTool.printLog(
                    TAG,
                    "Pure Wifi: But more than 1 clusters appear in "
                        + detectedArea
                        + ", so we cannot distinguish which cluster is the ground truth"
                );

            // bigEnoughClusters.size() == 1
            } else {
                List<Position> bigEnoughCluster = bigEnoughClusters.get(0);

                detectedLocation = averagePositions(bigEnoughCluster);

                DebugTool.printLog(
                    TAG,
                    "Only 1 cluster appear in "
                        + detectedArea
                        + "! Now we find the result location: " + detectedLocation.toString()
                );

                detectedLocation.setAreaId(detectedArea);
                return detectedLocation;
            }

        }

        if (detectedArea == null) {
            return new Position();

        } else {
            return new Position(detectedArea);
        }

    }


    /**
     * Find the intersection of detected WiFi positions and magnetic positions.
     * Defination of intersection: it is a subset of magnetic positions. In this subset, every
     * magnetic position should be near to some WiFi position, which means the distance between this
     * magnetic position and some WiFi position should be <= {@param pixelDistance4Intersection}.
     * @param wiFiDetectedPositions detected WiFi positions with top potential
     * @param magDetectedPositions detected magnetic positions with top potential
     * @param pixelDistance4Intersection a distance to define if a magnetic position is near to
     *                                   a WiFi position.
     * @return the intersection of detected WiFi positions and magnetic positions.
     */
    private List<Position> findIntersection(List<Position> wiFiDetectedPositions,
                                            List<Position> magDetectedPositions, double pixelDistance4Intersection) {
        List<Position> intersectionPosList = new ArrayList<>();
        for (Position magPos : magDetectedPositions) {
            boolean flag = false;
            for (Position wifiPos : wiFiDetectedPositions) {
                double distance = magPos.dist(wifiPos);
                if (distance <= pixelDistance4Intersection + ConstantValues.EPS) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                intersectionPosList.add(magPos);
            }
        }
        return intersectionPosList;
    }

    /**
     * Find the clusters in {@param positions}, whose size if bigger than {@param sizeLimitation}.
     *
     * @param positions               the positions which we use to find clusters
     * @param pixelsDistOfNeighbor    the parameter in DBScan, which means that only when the distance
     *                                between two points is <= {@param pixelsDistOfNeighbor}, these
     *                                two points can be defined as neighbor.
     * @param numOfNeighborForCluster the parameter in DBScan, which means that only when the number
     *                                of neighbor of a point is >= {@param numOfNeighborForCluster},
     *                                this point can be core point. Core point is the concept in
     *                                DBScan.
     * @param sizeLimitation          the value to define if a cluster is big enough.
     * @return the big enough clusters in {@param positions}.
     */
    private List<List<Position>> findBigEnoughClusters(List<Position> positions, double pixelsDistOfNeighbor,
                                                      int numOfNeighborForCluster, int sizeLimitation) {
        List<Set<Integer>> allFoundClusters = DBScan.getClusters(positions,
                pixelsDistOfNeighbor, numOfNeighborForCluster);
        List<List<Position>> bigEnoughClusters = new ArrayList<>();
        for (Set<Integer> cluster : allFoundClusters) {
            if (cluster.size() > sizeLimitation) {
                List<Position> bigEnoughClusterPositions = new ArrayList<>();
                for (int posIndex : cluster) {
                    bigEnoughClusterPositions.add(positions.get(posIndex));
                }
                bigEnoughClusters.add(bigEnoughClusterPositions);
            }
        }
        return bigEnoughClusters;
    }

    private Position averagePositions(List<Position> posList) {
        Position totalPos = new Position(0, 0);
        for (Position position : posList) {
            totalPos = totalPos.add(position);
        }
        return totalPos.multiple(1d / posList.size());
    }

}
