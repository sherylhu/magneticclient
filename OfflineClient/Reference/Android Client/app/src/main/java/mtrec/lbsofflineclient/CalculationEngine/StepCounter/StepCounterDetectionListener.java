package mtrec.lbsofflineclient.CalculationEngine.StepCounter;

import java.util.List;

public interface StepCounterDetectionListener {
	void onIdleDetected();
	void onWalkingDetected();
	void onUpdateStepCount(int stepCount);
	void onUpdateAccumulatedStepLength(double length);
	void onUpdateStepLengths(List<StepLength> stepLengths);
	void onDisplayData(List<StepCounterDisplayData> allStepCounterDisplayData);

	public class StepLength {
		public double beginNanoTimestamp;
		public double endNanoTimestamp;
		public double stepLength;

		public StepLength(double beginNanoTimestamp, double endNanoTimestamp, double stepLength) {
			this.beginNanoTimestamp = beginNanoTimestamp;
			this.endNanoTimestamp = endNanoTimestamp;
			this.stepLength = stepLength;
		}
	}
}
