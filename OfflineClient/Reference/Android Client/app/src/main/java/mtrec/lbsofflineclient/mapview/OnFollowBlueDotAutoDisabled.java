package mtrec.lbsofflineclient.mapview;

/**
 * Created by LinhTa on 12/19/2017.
 */

public interface OnFollowBlueDotAutoDisabled {
    public void onFollowingDisabled();
}
