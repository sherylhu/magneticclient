package mtrec.lbsofflineclient.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

public class DBScan {
    public static List<Set<Integer>> getClusters(List<Position> allPos, double availableDist, 
    		int minNeighborNum) {
        List<Integer> neighborhoodOfEachPos[] = new ArrayList[allPos.size()];

        for (int i = 0; i < allPos.size(); i++) {
            neighborhoodOfEachPos[i] = new ArrayList<Integer>();

            Position curPos = allPos.get(i);
            for (int j = 0; j < allPos.size(); j++) {
                Position otherPos = allPos.get(j);
                double curDist = curPos.dist(otherPos);
                if (curDist < availableDist || Math.abs(curDist - availableDist) < ConstantValues.EPS) {
                    neighborhoodOfEachPos[i].add(j);
                }
            }
        }

        Set<Integer>corePts = new HashSet<Integer>();
        Set<Integer>borderPts = new HashSet<Integer>();
        Set<Integer>noisePts = new HashSet<Integer>();
        for (int i = 0; i < allPos.size(); i++) {
            if (neighborhoodOfEachPos[i].size() >= minNeighborNum) {
                corePts.add(i);
            }
        }
        for (int i = 0; i < allPos.size(); i++) {
            if (corePts.contains(i)) {
                continue;
            }
            boolean foundBorderPts = false;
            for (int neighbor : neighborhoodOfEachPos[i]) {
                if (corePts.contains(neighbor)) {
                    borderPts.add(i);
                    foundBorderPts = true;
                    break;
                }
            }
            if (!foundBorderPts) {
                noisePts.add(i);
            }
        }

        List<Set<Integer>> allClusters = new ArrayList<Set<Integer>>();
        boolean checked[] = new boolean[allPos.size()];
        for (int i = 0 ; i < allPos.size(); i++) {
            if (!corePts.contains(i)) {
                continue;
            }
            if (!checked[i]) {
                Set<Integer>newCluster = new HashSet<Integer>();
                //get all density-reachable from i
                Queue<Integer> q = new LinkedBlockingQueue<Integer>();
                q.add(i);
                checked[i] = true;
                while (!q.isEmpty()){
                    int curInt = q.poll();
                    newCluster.add(curInt);
                    for (int neighbor : neighborhoodOfEachPos[curInt]) {
                        if (!checked[neighbor]) {
                            q.add(neighbor);
                            checked[neighbor] = true;
                        }
                    }
                }

                allClusters.add(newCluster);
            }
        }

        return allClusters;
    }
}
