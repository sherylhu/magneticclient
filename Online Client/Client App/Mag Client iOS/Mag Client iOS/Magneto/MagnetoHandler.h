//
//  MagnetoHandler.h
//  Mag Client iOS
//
//  Created by ivy on 11/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>

#import "MagneticData.h"

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

@protocol MagnetoDelegate <NSObject>

@optional
//- (void)magneticIntensity:(float)intensity Orientation:(float)orientation a

@end

@interface MagnetoHandler : NSObject

@end
