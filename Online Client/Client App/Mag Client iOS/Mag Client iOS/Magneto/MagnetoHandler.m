//
//  MagnetoHandler.m
//  Mag Client iOS
//
//  Created by ivy on 11/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "MagnetoHandler.h"

#import "PublicFuncs.h"

#define angular_speed_threshold 3.5f
#define low_pass_factor 0.48f
#define high_pass_factor 0.05f
#define sliding_window_size 1500
#define refresh_time 10000
#define magnet_noise_threshold 60.0f

@interface MagnetoHandler () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) CMPedometer *pedometer;

@property (strong, nonatomic) NSOperationQueue *deviceMotionQueue;
@property (strong, nonatomic) NSOperationQueue *gyroQueue;
@property (strong, nonatomic) NSOperationQueue *magnetometerQueue;

@property (assign, nonatomic) long startTime;
@property (strong, nonatomic) NSTimer *timer;

@property (strong, nonatomic) id mSyncObject;

@property (strong, nonatomic) NSMutableArray *magArray;

@property (assign, nonatomic) float magnet_field_intensity;
@property (strong, nonatomic) NSArray *magnetFieldValues;
@property (strong, nonatomic) NSMutableArray *magnetDegree;

@property (assign, nonatomic) float heading_orientation;
@property (strong, nonatomic) NSMutableArray *mOrientationData;

@property (strong, nonatomic) NSArray *gravityValues;

@property (assign, nonatomic) long period;
@property (assign, nonatomic) float max_angular_speed;
@property (strong, nonatomic) NSMutableArray *deltaAngular;
@property (strong, nonatomic) NSMutableArray *angularSpeed;
@property (strong, nonatomic) NSMutableArray *deltaQuater;
@property (strong, nonatomic) NSMutableArray *quaternionNew;
@property (strong, nonatomic) NSMutableArray *quaternionOld;
@property (strong, nonatomic) NSMutableArray *rotateDegrees;
@property (strong, nonatomic) NSMutableArray *quatDegrees;
@property (strong, nonatomic) NSArray *Rmatrix;
@property (assign, nonatomic) BOOL mDirectionStarted;
@property (assign, nonatomic) int iter;
@property (strong, nonatomic) NSMutableArray *mReferenceDegree;
@property (strong, nonatomic) NSMutableArray *mDiff;
@property (strong, nonatomic) NSMutableArray *mDiffArr;

@property (strong, nonatomic) CLHeading *heading;

@end

@implementation MagnetoHandler

#pragma mark - Updating Functions
- (void)start {
    self.mSyncObject = [NSDate date];
    [self registerSensor];
}

- (void)registerSensor
{
    self.startTime = 0;
    
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [self.locationManager requestAlwaysAuthorization];
    
    [self.motionManager startMagnetometerUpdatesToQueue:self.magnetometerQueue withHandler:^(CMMagnetometerData * _Nullable magnetometerData, NSError * _Nullable error) {
        
        @synchronized (self.mSyncObject) {
            [self updateMagneticFieldX:magnetometerData.magneticField.x y:magnetometerData.magneticField.y z:magnetometerData.magneticField.z];
        }
    }];
    
    [self.motionManager startDeviceMotionUpdatesToQueue:self.deviceMotionQueue withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
        @synchronized(self.mSyncObject) {
            
            [self updateGravityX:motion.gravity.x y:motion.gravity.y z:motion.gravity.z];
            
            [self updateQuaternionX:-motion.attitude.quaternion.x y:-motion.attitude.quaternion.y z:-motion.attitude.quaternion.z w:-motion.attitude.quaternion.w];
        }
    }];
    
    [self.motionManager startGyroUpdatesToQueue:self.gyroQueue withHandler:^(CMGyroData * _Nullable gyroData, NSError * _Nullable error) {
        
        @synchronized(self.mSyncObject) {
            [self updateGyroX:gyroData.rotationRate.x y:gyroData.rotationRate.y z:gyroData.rotationRate.z];
        }
    }];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self.locationManager selector:@selector(startUpdating) userInfo:nil repeats:YES];
}

- (void)startUpdating {
    [self.locationManager startUpdatingHeading];
}

#pragma mark - LocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    @synchronized(self.mSyncObject) {
        
        CLLocationDirection theHeading = ((newHeading.trueHeading > 0) ? newHeading.trueHeading : newHeading.magneticHeading);
        [self updateHeading:theHeading];
        
        self.heading = newHeading;
        NSArray *newValues = @[@(newHeading.x), @(newHeading.y), @(newHeading.z)];
        MagneticData *data = [[MagneticData alloc]initWithTimestamp:[PublicFuncs getCurrentTimestampNanoSecond]];
        
        NSArray *gravity = @[@([self.gravityValues[0] doubleValue] * -10.0), @([self.gravityValues[1] doubleValue] * -10.0), @([self.gravityValues[2] doubleValue] * -10.0)];
        NSArray *magnet = @[@(self.heading.x), @(self.heading.y), @(self.heading.z)];
        NSArray *R = [PublicFuncs getRotationMatrixFromGravity:gravity geoMagnetic:magnet];
        if (R != nil) {
            double orientation = [self.mOrientationData[0] doubleValue];
            double intensity = (float)sqrt([magnet[0] doubleValue] * [magnet[0] doubleValue] +
                                           [magnet[1] doubleValue] * [magnet[1] doubleValue] +
                                           [magnet[2] doubleValue] * [magnet[2] doubleValue]);
            double vertical = [magnet[0] doubleValue] * [R[6] doubleValue] + [magnet[1] doubleValue] * [R[7] doubleValue] + [magnet[2] doubleValue] * [R[8] doubleValue];
            double horizontal = [magnet[0] doubleValue] * [R[3] doubleValue] + [magnet[1] doubleValue] * [R[4] doubleValue] + [magnet[2] doubleValue]*[R[5] doubleValue];
            
            [data addIntensity:intensity andVertical:vertical andHorizontal:horizontal andOrientation:orientation];
            [self.magArray addObject:data];
        }
    }
}

#pragma mark - Data Manipulation
- (void)updateGyroX:(double)x y:(double)y z:(double)z {
    
}

- (void)updateMagneticFieldX:(double)x y:(double)y z:(double)z {
    self.magnet_field_intensity = (float)sqrt(x*x+y*y+z*z);
    self.magnetFieldValues = @[@(x), @(y), @(z)];

    [self calibrateMagnetDegree];
}

- (void)updateHeading:(double)heading {

}

- (void)updateQuaternionX:(double)x y:(double)y z:(double)z w:(double)w {
    
}

- (void)updateGravityX:(double)x y:(double)y z:(double)z {
    
}

#pragma mark - Private Functions
- (void)calibrateMagnetDegree {
    NSArray *R = [PublicFuncs getRotationMatrixGravity:self.gravityValues mag:self.magnetFieldValues];
    NSArray *values = [PublicFuncs getOrientation:R];
    self.magnetDegree[0] = @((float)RADIANS_TO_DEGREES([values[0] doubleValue]));
    self.magnetDegree[1] = @((float)RADIANS_TO_DEGREES([values[1] doubleValue]));
    self.magnetDegree[2] = @((float)RADIANS_TO_DEGREES([values[2] doubleValue]));
    
    [self calibrateMagnetDegree:self.magnetDegree isReal:NO];
}

- (void)calibrateMagnetDegree:(NSArray *)degrees isReal:(BOOL)isReal
{
    if (!isReal) {
        for (int i = 0; i < degrees.count; i++) {
            self.magnetDegree[i] = degrees[i];
        }
        if (!self.mDirectionStarted) {
            for (int i = 0; i < degrees.count; i++) {
                self.mReferenceDegree[i] = degrees[i];
            }
            self.mDirectionStarted = YES;
        } else {
            for (int i = 0; i < self.magnetDegree.count; i++) {
                float diff = ([self.magnetDegree[i] doubleValue] - [self.mReferenceDegree[i] doubleValue]);
                diff = [PublicFuncs normalize:diff]*high_pass_factor;
                if (self.iter >= sliding_window_size)
                    self.iter = 0;
                self.mDiff[i] = @([self.mDiff[i] doubleValue] - [self.mDiffArr[i][self.iter] doubleValue] / sliding_window_size);
                self.mDiff[i] = @([self.mDiff[i] doubleValue] + diff / sliding_window_size);
                self.mDiffArr[i][self.iter++] = @(diff);
            }
        }
    } else {
        if (self.mDirectionStarted) {
            if (fabs([degrees[0] doubleValue]) > 3) {
                return;
            }
            for (int i = 0; i < self.magnetDegree.count; i++) {
                self.mReferenceDegree[i] = @([PublicFuncs normalize:([self.mReferenceDegree[i] doubleValue] - [degrees[i] doubleValue])]);
                float diff = [self.magnetDegree[i] doubleValue] - [self.mReferenceDegree[i] doubleValue];
                diff = [PublicFuncs normalize:diff]*high_pass_factor;
                if (self.iter >= sliding_window_size) self.iter = 0;
                self.mDiff[i] = @([self.mDiff[i] doubleValue] - [self.mDiffArr[i][self.iter] doubleValue] / sliding_window_size);
                self.mDiff[i] = @([self.mDiff[i] doubleValue] + diff / sliding_window_size);
                self.mDiffArr[i][self.iter++] = @(diff);
            }
        }
    }
    
//    [self.delegate compass:self didUpdateCalibratedHeading:([self.mReferenceDegree[0] doubleValue] + [self.mDiff[0] doubleValue])];
}

#pragma mark - Mutators
#pragma mark Queues
- (NSOperationQueue *)deviceMotionQueue {
    if (!_deviceMotionQueue) {
        _deviceMotionQueue = [[NSOperationQueue alloc] init];
        _deviceMotionQueue.maxConcurrentOperationCount = 1;
    }
    
    return _deviceMotionQueue;
}

- (NSOperationQueue *)gyroQueue {
    if (!_gyroQueue) {
        _gyroQueue = [[NSOperationQueue alloc] init];
        _gyroQueue.maxConcurrentOperationCount = 1;
    }
    
    return _gyroQueue;
}

- (NSOperationQueue *)magnetometerQueue {
    if (!_magnetometerQueue) {
        _magnetometerQueue = [[NSOperationQueue alloc] init];
        _magnetometerQueue.maxConcurrentOperationCount = 1;
    }
    
    return _magnetometerQueue;
}

#pragma mark Arrays
- (NSMutableArray *)magnetDegree {
    if (!_magnetDegree) {
        _magnetDegree = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _magnetDegree;
}

- (NSArray *)magnetFieldValues {
    if (!_magnetFieldValues) {
        _magnetFieldValues = @[@(0), @(0), @(0)];
    }
    
    return _magnetFieldValues;
}

#pragma mark Managers
- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc]init];
        _locationManager.delegate = self;
    }
    return _locationManager;
}

- (CMMotionManager *)motionManager {
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc]init];
    }
    return _motionManager;
}

- (CMPedometer *)pedometer {
    if (!_pedometer) {
        _pedometer = [[CMPedometer alloc]init];
    }
    return _pedometer;
}

@end
