//
//  RootViewController.m
//  Mag IOS
//
//  Created by ivy on 30/4/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import "RootViewController.h"

#import "MapViewController.h"

@interface RootViewController () <UITextFieldDelegate> {
    IBOutlet UISwitch *magSwitch, *stepSwitch;
    IBOutlet UITextField *urlText;
    BOOL magOn, stepOn;
    NSString *baseUrl;
}

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    urlText.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"MapSegue"]) {
        MapViewController *mapVC = [segue destinationViewController];
        mapVC.magOn = magOn;
        mapVC.stepOn = stepOn;
        mapVC.base_url = urlText.text;
    }
}

#pragma mark - UITextField
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self resignFirstResponder];
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - IBActions
- (IBAction)finishClicked:(id)sender {
    magOn = magSwitch.on;
    stepOn = stepSwitch.on;
    if ([urlText.text length]) {
        [self performSegueWithIdentifier:@"MapSegue" sender:self];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
