//
//  TransferDataModel.m
//  Mag Client iOS
//
//  Created by ivy on 6/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "TransferDataModel.h"

@interface TransferDataModel () {
    MagneticData *mag_components;
    NSString *user_id;
    NSDictionary *wifi_vector, *ble_vector;
}

@end

@implementation TransferDataModel

- (id)initWithUserID:(NSString *)userid andMagneticComponents:(MagneticData *)magdata {
    self = [super init];
    if (self) {
        user_id = userid;
        mag_components = [[MagneticData alloc] init];
        mag_components = magdata;
    }
    return self;
}

- (NSString *)model2String {
    if (self) {
        
    }
    return @"";
}

@end
