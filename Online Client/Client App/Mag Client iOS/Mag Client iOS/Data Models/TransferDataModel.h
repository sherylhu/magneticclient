//
//  TransferDataModel.h
//  Mag Client iOS
//
//  Created by ivy on 6/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MagneticData.h"

@interface TransferDataModel : NSObject

- (id)initWithUserID:(NSString *)userid andMagneticComponents:(MagneticData *)magdata;
- (NSString *)model2String;

@end
