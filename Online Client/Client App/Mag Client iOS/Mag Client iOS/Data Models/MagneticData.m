//
//  MagneticData.m
//  Mag Client iOS
//
//  Created by ivy on 4/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "MagneticData.h"

@interface MagneticData () {
    long timestamp;
    NSArray *magvalue;
    int step;
}

@end

@implementation MagneticData

- (id)initWithTimestamp:(long)ts {
    self = [super init];
    if (self) {
        step = -1;
    }
    return self;
}

- (BOOL)addIntensity:(float)intensity andVertical:(float)vertical andHorizontal:(float)horizontal andOrientation:(float)orientation {
    
    magvalue = [NSArray arrayWithObjects:[NSNumber numberWithInt:intensity], [NSNumber numberWithInt:vertical], [NSNumber numberWithInt:horizontal], [NSNumber numberWithInt:orientation], nil];
    if ([magvalue count]) {
        NSLog(@"MagData:\nIntensity: %f,\nVertical:%f,\nHorizontal%f,\nOrientation%f", intensity, vertical, horizontal, orientation);
        return YES;
    }
    return NO;
}

@end

/*
 
 //
 //  DataTransferModel.m
 //  Mag IOS
 //
 //  Created by ivy on 3/5/2017.
 //  Copyright © 2017 Ivy Siyan HU. All rights reserved.
 //
 
 #import "DataTransferModel.h"
 
 @implementation DataTransferModel
 
 - (id)init {
 self = [super init];     if (self) {
 self.userId = @"user_id";
 self.timestamp = -1;
 self.intensity = 0.f;
 self.horizontal = 0.f;
 self.vertical = 0.f;
 self.step = 1;
 self.bleVector = [NSDictionary dictionary];
 self.wifiVector = [NSDictionary dictionary];
 
 self.gpsComponent = [NSMutableArray array];
 
 self.building_id = @"Academic Building";
 self.floor_id = @"2F";
 }
 return self;
 }
 
 - (id)proxy2Gson {
 
 NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
 self.userId, @"user_id",
 [NSNumber numberWithLong:self.timestamp], @"ts",
 [NSNumber numberWithFloat:self.intensity], @"intensity",
 [NSNumber numberWithFloat:self.horizontal], @"horizontal_comp",
 [NSNumber numberWithFloat:self.vertical], @"vertical_comp",
 [NSNumber numberWithInt:self.step], @"step",
 [NSNumber numberWithFloat:self.orientation], @"orientation",
 self.building_id, @"building_id",
 self.floor_id, @"floor_id",
 self.wifiVector, @"wifi_vector",
 self.bleVector, @"ble_vector",
 //            self.gpsComponent, @"gps_comps",
 //            self.floorDetected, @"floor",
 nil];
 return dict;
 }
 
 - (NSString *)proxy2String {
 NSString *str = [NSString stringWithFormat:@"%ld, z", self.timestamp];
 return str;
 }
 
 @end
*/

/*
 package com.magneticclientapp2.Model;
 
 public class MagneticData{
 public long nanoTimestamp;
 public float[] magValues; // intensity, vertical, horizontal, orientation
 public int step = -1;
 
 public MagneticData() {}
 
 public MagneticData(MagneticData other) {
 nanoTimestamp = other.nanoTimestamp;
 magValues = new float[other.magValues.length];
 for (int i = 0; i < other.magValues.length; i++) {
 magValues[i] = other.magValues[i];
 }
 }
 
 @Override
 public String toString() {
 String str = "(";
 for (int i = 0; i < magValues.length - 1;i++) {
 str += magValues[i] + ",";
 }
 str += magValues[magValues.length - 1] + "," + step + ")";
 return str;
 }
 }
*/
