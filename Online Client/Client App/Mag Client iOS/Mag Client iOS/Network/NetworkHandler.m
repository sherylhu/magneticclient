//
//  NetworkHandler.m
//  Wherami Union Hospital
//
//  Created by ivy on 6/1/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import "NetworkHandler.h"

#include <ifaddrs.h>
#include <arpa/inet.h>

@implementation NetworkHandler

static NetworkHandler *_instance = nil;
static AFHTTPSessionManager *_manager = nil;

+ (NetworkHandler *)instance {
    if (_instance) {
        return _instance;
    }
    @synchronized ([NetworkHandler class]) {
        if (!_instance) {
            _instance = [[self alloc]init];
            [_instance initiate];
        }
        return _instance;
    }
    return nil;
}

- (void)initiate {
    if (!_manager) {
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        [securityPolicy setValidatesDomainName:NO];
        [securityPolicy setAllowInvalidCertificates:YES];
        
        _manager = [AFHTTPSessionManager manager];
        _manager.securityPolicy = securityPolicy;
        
        [_manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
        
        [_manager.operationQueue waitUntilAllOperationsAreFinished];
    }
}

#pragma mark - Public Functions
#pragma mark HTML
- (void)getHtmlFrom:(NSString *)url
 resposneSerializer:(AFHTTPResponseSerializer *)responseSerializer
           progress:(void (^)(NSProgress *currentProgress))progress
            success:(void (^)(NSString *html_str))success
            failure:(void (^)(NSError *error))failure {
    
    _manager.responseSerializer = responseSerializer;

    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *str = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    [_manager GET:str parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        progress(downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *gotHtml = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        success(gotHtml);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Get HTML Error: %@", error.description);
        failure(error);
    }];
}

#pragma mark Download Data
- (void)downloadFrom:(NSString *)url
  resposneSerializer:(AFHTTPResponseSerializer *)responseSerializer
             success:(void (^)(id responseObject))success
            progress:(void (^)(NSProgress *currentProgress))progress
             failure:(void (^)(NSError *error))failure {
    
    _manager.responseSerializer = responseSerializer;
    
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];

    [_manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

#pragma mark - Upload Data
- (void)upload:(TransferDataModel *)transData
     toBaseUrl:(NSString *)baseurl
       success:(void (^)(id responseObject))success
       failure:(void (^)(NSError *error))failure {
    
    NSError *error = nil;
    
    _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString *uploadUrl = [NSString stringWithFormat:@"%@/api/getpos", baseurl];
    NSData *uploadData = [NSJSONSerialization dataWithJSONObject:[transData model2String] options:0 error:&error];
    NSString *test = [[NSString alloc]initWithData:uploadData encoding:NSUTF8StringEncoding];
    if (error) {
        failure(error);
        return;
    }
    NSLog(@"String: %@", test);
    [_manager POST:uploadUrl parameters:test progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

#pragma mark - IPV6 Network
- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

- (BOOL)ipv4Network {
    NSString *ip = [self getIPAddress];
    if (ip) {
        
        BOOL containdot = [ip containsString:@"."];
        if (containdot) {
            return YES;
        }
        
        BOOL containcolon = [ip containsString:@":"];
        if (containcolon) {
            return NO;
        }
        
    }
    return YES;
}

@end
