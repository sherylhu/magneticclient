//
//  MapViewController.h
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapViewController : UIViewController

@property (nonatomic) BOOL magOn, stepOn;
@property (nonatomic, strong) NSString *base_url;

@end
