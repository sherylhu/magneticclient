//
//  CRFLocationSmoother.m
//  CRF
//
//  Created by Siyan HU on 16/4/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFLocationSmoother.h"

@interface CRFPointPair : NSObject

@property (assign, nonatomic) long timestamp;
@property (assign, nonatomic) CGPoint point;

@end

@implementation CRFPointPair

- (instancetype)initWithPoint:(CGPoint)point
{
    if (self = [super init]) {
        self.point = point;
        self.timestamp = [CRFCommon getCurrentTimestampMilliSecond];
    }
    
    return self;
}

@end

@interface CRFLocationSmoother ()

@property (strong, nonatomic) NSMutableArray *points;

@end

@implementation CRFLocationSmoother

#pragma mark - Mutators

- (NSMutableArray *)points
{
    if (!_points) {
        _points = [NSMutableArray array];
    }
    
    return _points;
}

#pragma mark - Public API

- (CGPoint)addPoint:(CGPoint)point
{
    [self addHistory:[[CRFPointPair alloc] initWithPoint:point]];
    
    return [self getPoint];
}

- (CGPoint)getPoint
{
    return [self useBezierCurve2Projection];
}

#pragma mark - Smoothing

- (CGPoint)useBezierCurve2Projection
{
    long currentTime = [CRFCommon getCurrentTimestampMilliSecond];
    
    NSMutableArray *points = [[self getHistory] mutableCopy];
    for (int i = 0; i < points.count;) {
        CRFPointPair *pp = points[i];
        if (currentTime - pp.timestamp < 30000) i++;
        else [points removeObjectAtIndex:i];
    }
    
    if (points.count > 0) {
        if (points.count == 1) {
            return CGPointMake(((CRFPointPair *)points[0]).point.x, ((CRFPointPair *)points[0]).point.y);
        } else {
            NSMutableArray *ps = [NSMutableArray array];
            CGPoint centre = CGPointMake(0, 0);
            long startTime = LONG_MAX;
            long endTime = LONG_MIN;
            for (CRFPointPair *entry in points) {
                startTime = MIN(startTime, entry.timestamp);
                endTime = MAX(endTime, entry.timestamp);
                centre.x += entry.point.x;
                centre.y += entry.point.y;
                [ps addObject:[NSValue valueWithCGPoint:entry.point]];
            }
            centre.x /= points.count*1.0;
            centre.y /= points.count*1.0;
            
            CRFPointPair *nearestCentreEntry = nil;
            double nearestCentreDist = MAXFLOAT;
            for (CRFPointPair *entry in points) {
                double dist = [CRFCommon euclideanFrom:entry.point to:centre];
                if (dist < nearestCentreDist) {
                    nearestCentreEntry = entry;
                    nearestCentreDist = dist;
                }
            }
            centre = nearestCentreEntry.point;
            long averageTime = nearestCentreEntry.timestamp;
            
            if (startTime + 5000 > endTime) {
                return CGPointMake(centre.x, centre.y);
            } else {
                float x = 0;
                float y = 0;
                long refTime = averageTime;
                CGPoint refP = centre;
                int size = 0;
                for (int i = 0; i < points.count; i++) {
                    int t = (int)(((CRFPointPair *)points[i]).timestamp - refTime);
                    if (t != 0) {
                        float vx = (((CRFPointPair *)points[i]).point.x - refP.x) / t;
                        float vy = (((CRFPointPair *)points[i]).point.y - refP.y) / t;
                        x += vx;
                        y += vy;
                        size ++;
                    }
                }
                x /= size;
                y /= size;
                x = refP.x + x * (currentTime + 3000 - refTime);
                y = refP.y + y * (currentTime + 3000 - refTime);
                [ps addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
                float a1 = (float)(currentTime-startTime)/(currentTime-startTime+3000);
                while (ps.count > 1) {
                    NSMutableArray *tps = [NSMutableArray array];
                    for (int i = 0; i < ps.count - 1; i++) {
                        [tps addObject:[NSValue valueWithCGPoint:CGPointMake([ps[i] CGPointValue].x * (1 - a1) + [ps[i+1] CGPointValue].x * a1, [ps[i] CGPointValue].y * (1 - a1) + [ps[i+1] CGPointValue].y * a1)]];
                    }
                    ps = tps;
                }
                return CGPointMake([ps[0] CGPointValue].x, [ps[0] CGPointValue].y);
            }
        }
    }
    
    return CGPointMake(0, 0);
}

#pragma mark - History operations

- (void)addHistory:(CRFPointPair *)point
{
    long currentTime = [CRFCommon getCurrentTimestampMilliSecond];
    [self.points addObject:point];
    for (int i = 0; i < self.points.count;) {
        CRFPointPair *pp = self.points[i];
        if (currentTime - pp.timestamp < 30000) i++;
        else [self.points removeObjectAtIndex:i];
    }
}

- (NSArray *)getHistory
{
    return [self.points copy];
}

- (void)clearHistory
{
    [self.points removeAllObjects];
}

@end
