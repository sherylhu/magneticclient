//
//  CRFLine.m
//  CRF
//
//  Created by Siyan HU on 27/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import "CRFLine.h"

@implementation CRFLine

- (instancetype)init
{
    if (self = [super init]) {
        self.vertical = NO;
        self.a = NAN;
        self.b = NAN;
    }
    
    return self;
}

+ (instancetype)lineWithStart:(CGPoint)start end:(CGPoint)end
{
    CRFLine *line = [[CRFLine alloc] init];
    line.start = start;
    line.end = end;
    
    if (line.end.x - line.start.x != 0) {
        line.a = ((line.end.y - line.start.y) / (line.end.x - line.start.x));
        line.b = line.start.y - line.a * line.start.x;
    } else {
        line.vertical = YES;
    }
    
    return line;
}

- (BOOL)isInside:(CGPoint)point
{
    double maxX = self.start.x > self.end.x ? self.start.x : self.end.x;
    double minX = self.start.x < self.end.x ? self.start.x : self.end.x;
    double maxY = self.start.y > self.end.y ? self.start.y : self.end.y;
    double minY = self.start.y < self.end.y ? self.start.y : self.end.y;
    
    if ((point.x >= minX && point.x <= maxX) && (point.y >= minY && point.y <= maxY)) {
        return YES;
    }
    
    return NO;
}

@end
