//
//  CRFLocationSmoother.h
//  CRF
//
//  Created by Siyan HU on 16/4/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFLocationSmoother : NSObject

- (CGPoint)addPoint:(CGPoint)point;
- (CGPoint)getPoint;

@end
