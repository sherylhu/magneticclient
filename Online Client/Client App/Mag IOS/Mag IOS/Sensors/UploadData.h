//
//  UploadData.h
//  Mag IOS
//
//  Created by ivy on 28/8/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadData : NSObject

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSMutableArray *mag_components;
@property (nonatomic, strong) NSArray *wifi_components;
@property (nonatomic, strong) NSArray *ble_components;

@end
