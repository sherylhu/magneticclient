//
//  DataTransferModel.h
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataTransferModel : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic) long timestamp;

@property (nonatomic) float intensity;
@property (nonatomic) float horizontal;
@property (nonatomic) float vertical;
@property (nonatomic) float orientation;
@property (nonatomic) int step;
@property (nonatomic, strong) NSDictionary *bleVector, *wifiVector;

@property (nonatomic, strong) NSMutableArray *gpsComponent;

@property (nonatomic, strong) NSString *floor_id, *building_id;

- (id)proxy2Gson;

@end
