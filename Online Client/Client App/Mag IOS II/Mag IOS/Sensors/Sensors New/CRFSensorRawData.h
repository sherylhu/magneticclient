//
//  CRFSensorRawData.h
//  Sensor
//
//  Created by Siyan HU on 3/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFSensorRawData : NSObject

@property (strong, nonatomic) NSArray *values;
@property (assign, nonatomic) long timestamp;
@property (assign, nonatomic) int step;

- (instancetype)initWithTimestamp:(long)timestamp values:(NSArray *)values step:(int)step;

@end
