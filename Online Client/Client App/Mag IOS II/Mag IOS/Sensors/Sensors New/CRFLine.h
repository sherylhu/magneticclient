//
//  CRFLine.h
//  CRF
//
//  Created by Siyan HU on 27/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFLine : NSObject

@property (assign, nonatomic) BOOL vertical;
@property (assign, nonatomic) CGPoint start;
@property (assign, nonatomic) CGPoint end;
@property (assign, nonatomic) double a;
@property (assign, nonatomic) double b;

+ (instancetype)lineWithStart:(CGPoint)start end:(CGPoint)end;

- (BOOL)isInside:(CGPoint)point;

@end
