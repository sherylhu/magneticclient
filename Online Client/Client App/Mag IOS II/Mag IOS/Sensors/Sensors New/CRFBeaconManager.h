//
//  CRFBeaconManager.h
//  CRF
//
//  Created by Samuel Chow on 22/2/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CRFLocationManager;

@interface CRFBeaconManager : NSObject

@property (strong, nonatomic) NSArray *beaconArray;
@property (strong, nonatomic) NSDictionary *radiomap;
@property (strong, nonatomic) NSDictionary *refId2PointMap;
@property (strong, nonatomic) NSDictionary *point2RefIdMap;

- (instancetype)initWithLocationManager:(CRFLocationManager *)locationManager;

- (int)computeCurrentArea:(NSDictionary *)target_vector;
- (CGPoint)computeLocation:(NSDictionary *)target_vector;
- (CGPoint)computeLocation:(NSDictionary *)target_vector atArea:(int)areaId;
- (NSArray *)computeBeaconHeatMap:(NSDictionary *)target_vector atArea:(int)areaId;

@end
