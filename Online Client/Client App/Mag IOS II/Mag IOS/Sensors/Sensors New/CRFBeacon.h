//
//  CRFBeacon.h
//  CRF
//
//  Created by Samuel Chow on 12/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFBeacon : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSUUID *uuid;
@property (assign, nonatomic) CLBeaconMajorValue majorValue;
@property (assign, nonatomic) CLBeaconMinorValue minorValue;
@property (assign, nonatomic) CGPoint position;
@property (assign, nonatomic) double threshold_rssi;
@property (assign, nonatomic) double threshold_dist;

- (instancetype)initWithName:(NSString *)name
                        uuid:(NSUUID *)uuid
                       major:(CLBeaconMajorValue)major
                       minor:(CLBeaconMinorValue)minor
                    position:(CGPoint)position;
- (BOOL)isEqualToCLBeacon:(CLBeacon *)beacon;

+ (NSString *)getUUIDFromIdentifier:(NSString *)identifier;
+ (int)getMajorFromIdentifier:(NSString *)identifier;
+ (int)getMinorFromIdentifier:(NSString *)identifier;

@end
