//
//  TransferDataModel.h
//  Mag Client iOS
//
//  Created by ivy on 6/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransferDataModel : NSObject

@property (assign, nonatomic) int step;

- (id)initWithUserID:(NSString *)userid andMagneticComponents:(NSArray *)magdata;
- (NSString *)model2String;
- (id)proxy2Gson;

@end
