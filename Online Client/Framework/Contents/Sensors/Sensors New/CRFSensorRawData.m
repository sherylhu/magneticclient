//
//  CRFSensorRawData.m
//  Sensor
//
//  Created by Siyan HU on 3/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFSensorRawData.h"

@implementation CRFSensorRawData

- (instancetype)initWithTimestamp:(long)timestamp values:(NSArray *)values step:(int)step
{
    if (self = [super init]) {
        self.values = values;
        self.timestamp = timestamp;
        self.step = step;
    }
    
    return self;
}

@end
