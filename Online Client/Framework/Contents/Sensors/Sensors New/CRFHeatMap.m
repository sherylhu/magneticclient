//
//  CRFHeatMap.m
//  CRF
//
//  Created by Samuel Chow on 22/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import "CRFHeatMap.h"

@interface CRFHeatMap ()

@property (strong, nonatomic) NSMutableArray *mapConstraints_X;
@property (strong, nonatomic) NSMutableArray *mapConstraints_Y;

@end

@implementation CRFHeatMap

- (instancetype)initWithSetting:(CRFAreaSetting *)setting
{
    if (self = [super init]) {
        self.setting = setting;
        [self genMapConstraint];
        [self generateVisual];
    }
    
    return self;
}

- (void)genMapConstraint
{
    self.mapConstraints_X = [NSMutableArray array];
    self.mapConstraints_Y = [NSMutableArray array];
    
    NSArray *outconstraint = [CRFCommon loadConstraint:self.setting.outconstraint];
    for (NSArray *coor in outconstraint) {
        [self.mapConstraints_X addObject:coor[0]];
        [self.mapConstraints_Y addObject:coor[1]];
    }
}

- (void)generateVisual
{
    self.boundary_X[0] = self.boundary_Y[0] = @(MAXFLOAT);
    self.boundary_X[1] = self.boundary_Y[1] = @(0);
    
    for (int i = 0; i < self.mapConstraints_X.count; ++i) {
        if ([self.mapConstraints_X[i] doubleValue] < [self.boundary_X[0] doubleValue])
            self.boundary_X[0]=self.mapConstraints_X[i];
        if ([self.mapConstraints_X[i] doubleValue]>[self.boundary_X[1] doubleValue])
            self.boundary_X[1]=self.mapConstraints_X[i];
        if ([self.mapConstraints_Y[i] doubleValue]<[self.boundary_Y[0] doubleValue])
            self.boundary_Y[0]=self.mapConstraints_Y[i];
        if ([self.mapConstraints_Y[i] doubleValue]>[self.boundary_Y[1] doubleValue])
            self.boundary_Y[1]=self.mapConstraints_Y[i];
    }

    self.gridX = ([self.boundary_X[1] doubleValue] - [self.boundary_X[0] doubleValue])*3/40/(0.8/1)+1;
    self.gridY = ([self.boundary_Y[1] doubleValue] - [self.boundary_Y[0] doubleValue])*3/40/(0.8/1)+1;
    self.gridX = (int)((double)self.gridX);
    self.gridY = (int)((double)self.gridY);
    
    for (double start = [self.boundary_X[0] doubleValue]; start <= [self.boundary_X[1] doubleValue]; start += (([self.boundary_X[1] doubleValue]-[self.boundary_X[0] doubleValue])/(self.gridX-1))) {
        [self.vectorX addObject:@(start)];
    }
    for (double start = [self.boundary_Y[0] doubleValue]; start <= [self.boundary_Y[1] doubleValue]; start += (([self.boundary_Y[1] doubleValue]-[self.boundary_Y[0] doubleValue])/(self.gridY-1))) {
        [self.vectorY addObject:@(start)];
    }
    

    [self loadHeatMap];
}

- (BOOL)loadHeatMap
{
    self.visual = [NSMutableArray array];
    for (int i = 0; i < self.vectorX.count; i++) {
        NSMutableArray *subArray = [NSMutableArray array];
        for (int j = 0; j < self.vectorY.count; j++) {
            [subArray addObject:[NSMutableArray arrayWithArray:@[@(0.0),@(0.0),@(0.0)]]];
        }
        [self.visual addObject:subArray];
    }
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[self.setting.heatmap componentsSeparatedByString:@"."][0] ofType:[self.setting.heatmap componentsSeparatedByString:@"."][1]];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    if (fileData == NULL) return NO;
    NSArray *rows = [[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
    int x = [[rows[0] componentsSeparatedByString:@" "][0] intValue];
    int y = [[rows[0] componentsSeparatedByString:@" "][1] intValue];
    int currIdx = 1;
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            NSArray *temp = [rows[currIdx] componentsSeparatedByString:@" "];
            [[[self.visual objectAtIndex:i] objectAtIndex:j] setObject:@([temp[0] doubleValue]) atIndex:0];
            [[[self.visual objectAtIndex:i] objectAtIndex:j] setObject:@([temp[1] doubleValue]) atIndex:1];
            [[[self.visual objectAtIndex:i] objectAtIndex:j] setObject:@([temp[2] doubleValue]) atIndex:2];
            currIdx++;
        }
    }
    return YES;
}

#pragma mark - Computation

+ (NSArray *)findInterval:(NSArray *)trace step:(int)step
{
    NSMutableArray *ret = [NSMutableArray arrayWithArray:@[@(0), @(0)]];
    for (int i = 0; i < trace.count; ++i) {
        CRFSensorData *data = trace[i];
        if (fabs(data.step-step) < 1e-3) {
            ret[0] = @(i);
            break;
        }
    }
    for (int i = (int)trace.count-1; i >= 0; --i) {
        CRFSensorData *data = trace[i];
        if (fabs(data.step-step) < 1e-3) {
            ret[1] = @(i);
            break;
        }
    }
    return [ret copy];
}

#pragma mark - Getter

- (NSMutableArray *)boundary_X
{
    if (!_boundary_X) {
        _boundary_X = [NSMutableArray arrayWithArray:@[@(0.0), @(0.0)]];
    }
    
    return _boundary_X;
}

- (NSMutableArray *)boundary_Y
{
    if (!_boundary_Y) {
        _boundary_Y = [NSMutableArray arrayWithArray:@[@(0.0), @(0.0)]];
    }
    
    return _boundary_Y;
}

- (NSMutableArray *)vectorX
{
    if (!_vectorX) {
        _vectorX = [NSMutableArray array];
    }
    
    return _vectorX;
}

- (NSMutableArray *)vectorY
{
    if (!_vectorY) {
        _vectorY = [NSMutableArray array];
    }
    
    return _vectorY;
}

@end
