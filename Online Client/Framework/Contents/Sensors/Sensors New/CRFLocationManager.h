//
//  CRFLocationManager.h
//  CRF
//
//  Created by Samuel Chow on 22/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CRFLocationManager, CRFHeatMap, CRFBeaconManager;

@protocol CRFLocationManagerDelegate

- (void)locationManager:(CRFLocationManager *)locationManager didUpdateAreaId:(int)areaId;
- (void)locationManager:(CRFLocationManager *)locationManager didUpdateRoute:(NSArray *)route;
- (void)locationManager:(CRFLocationManager *)locationManager didUpdateBeaconEstimate:(CGPoint)point;

@end

@interface CRFLocationManager : NSObject

@property (weak, nonatomic) id<CRFLocationManagerDelegate> delegate;

@property (strong, nonatomic) CRFHeatMap *heatMap;
@property (strong, nonatomic) CRFBeaconManager *beaconManager;

- (instancetype)initWithAreaId:(int)areaId;

- (void)start;
- (void)stop;

- (void)feedSensorData:(CRFSensorData *)sensorData;

- (NSArray *)getNeigbourNode:(CGPoint)point errorRadiusInMeter:(double)error;
- (CGPoint)correctPoint:(CGPoint)point;
- (BOOL)satisfyMapConstraintX:(double)x Y:(double)y;

@end
