//
//  CRFBeaconManager.m
//  CRF
//
//  Created by Samuel Chow on 22/2/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFBeaconManager.h"
#import "CRFLocationManager.h"
//#import "CRFHeatMap.h"
#import "CRFPriorityQueue.h"

#define COSINE_K 5

@interface CRFPair : NSObject

@property (nonatomic, strong) NSString *loc;
@property (nonatomic, strong) NSNumber *areaId;
@property (nonatomic, assign) double sim;

@end

@implementation CRFPair

- (NSString *)description
{
    return [NSString stringWithFormat:@"Pair[%@]: %@ %f", self.areaId, self.loc, self.sim];
}

@end

@interface CRFBeaconManager ()

@property (weak, nonatomic) CRFLocationManager *locationManager;

@end

@implementation CRFBeaconManager
/*
- (instancetype)initWithLocationManager:(CRFLocationManager *)locationManager
{
    if (self = [super init]) {
        self.locationManager = locationManager;
        self.beaconArray = [CRFCommon loadBeaconFile:self.locationManager.heatMap.setting.beacon];
        [self initialize];
    }
    
    return self;
}

- (void)initialize
{
    NSMutableDictionary *radiomap = [NSMutableDictionary dictionary];
    NSMutableDictionary *point2RefIdMap = [NSMutableDictionary dictionary];
    NSMutableDictionary *refId2PointMap = [NSMutableDictionary dictionary];

    for (NSString *filename in self.locationManager.heatMap.setting.radiomap) {
        NSArray *fileComponent = [filename componentsSeparatedByString:@"."];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:fileComponent[0] ofType:fileComponent[1]];
        NSData *fileData = [NSData dataWithContentsOfFile:filePath];
        NSNumber *areaId = @([[filename componentsSeparatedByString:@"_"][0] intValue]);
        if (radiomap[areaId] == nil) {
            radiomap[areaId] = [NSMutableDictionary dictionary];
        }
        if (fileData) {
            NSArray *rows = [[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
            for (NSString *row in rows) {
                if (row.length == 0) continue;
                NSArray *column = [row componentsSeparatedByString:@" "];
                NSString *key = column[0];
                NSMutableDictionary *vector = [NSMutableDictionary dictionary];
                for (int i = 1; i < column.count; i++) {
                    NSArray *i_column = [column[i] componentsSeparatedByString:@"|"];
                    NSString *uuid = i_column[0];
                    NSString *major = i_column[1];
                    NSString *minor = i_column[2];
                    NSString *rssi = i_column[3];
                    vector[[NSString stringWithFormat:@"%@_%@_%@", uuid, major, minor]] = @([rssi doubleValue]);
                }
                radiomap[areaId][key] = vector;
                NSValue *pointValue = [NSValue valueWithCGPoint:[CRFCommon locationString2Point:key]];
                if (point2RefIdMap[pointValue] == nil) {
                    point2RefIdMap[pointValue] = @(point2RefIdMap.count);
                    refId2PointMap[point2RefIdMap[pointValue]] = pointValue;
                }
            }
        }

    }
        
    self.radiomap = radiomap;
    self.point2RefIdMap = point2RefIdMap;
    self.refId2PointMap = refId2PointMap;
}

- (NSArray *)get_key_from_sectj:(NSDictionary *)target_vector atArea:(int)areaId
{
    NSMutableSet *set = [NSMutableSet set];
    for (NSString *beacon in target_vector) {
        int major = [[beacon componentsSeparatedByString:@"_"][1] intValue];
        int minor = [[beacon componentsSeparatedByString:@"_"][2] intValue];
        if (major != areaId) continue;
        int lower = (int)([target_vector[beacon] doubleValue]/10)*10;
        int upper = lower+9;
        NSString *k = [NSString stringWithFormat:@"%@_%d_%d", beacon, lower, upper];
        double maxDist = [self.locationManager.heatMap.setting.sectj[k] doubleValue];
        CRFBeacon *beacon = nil;
        for (CRFBeacon *b in self.beaconArray) {
            if (b.majorValue == major && b.minorValue == minor) {
                beacon = b;
                break;
            }
        }
        for (NSString *rm in self.radiomap[@(areaId)]) {
            int x = [[rm componentsSeparatedByString:@","][0] intValue];
            int y = [[rm componentsSeparatedByString:@","][1] intValue];
            double dist = [CRFCommon euclideanFrom:CGPointMake(x, y) to:CGPointMake(beacon.position.x, beacon.position.y)];
            if (dist <= maxDist) {
                NSString *kk = [NSString stringWithFormat:@"%d_%d", x, y];
                [set addObject:kk];
            }
        }
    }
    
    return [set allObjects];
}

- (NSArray *)computeBeaconHeatMap:(NSDictionary *)target_vector atArea:(int)areaId
{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < self.refId2PointMap.count; i++) {
        [array addObject:@(0.0)];
    }
    
    NSArray *filtered_list = [self get_key_from_sectj:target_vector atArea:areaId];

    for (NSString *key in self.radiomap[@(areaId)]) {
        NSDictionary *ref_vector = self.radiomap[@(areaId)][key];
        CGPoint point = [CRFCommon locationString2Point:key];
        if (![filtered_list containsObject:[NSString stringWithFormat:@"%d_%d", (int)point.x, (int)point.y]]) continue;
        array[[self.point2RefIdMap[[NSValue valueWithCGPoint:point]] intValue]] = @([CRFCommon cosineSimilarityWithA:ref_vector B:target_vector]);
    }
    
    return array;
}

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (int)computeCurrentArea:(NSDictionary *)target_vector
{
    __block int currentAreaModel = -1;
    int currentAreaKnn = -1;
    
    // area classification using SVM
    NSMutableArray *arr = [NSMutableArray array];
    for (NSString *key in target_vector) {
        NSArray *comp = [key componentsSeparatedByString:@"_"];
        [arr addObject:[NSString stringWithFormat:@"%@_%@_%@", comp[1], comp[2], target_vector[key]]];
    }
    NSString *urlString = [NSString stringWithFormat:@"http://localhost:8081/?fp=%@", [arr componentsJoinedByString:@"__"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLResponse *response;
    NSError *error;

    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];

    if(!error) {
        currentAreaModel = [responseString intValue];
    }
    
    // knn method
    CRFPriorityQueue *queue = [[CRFPriorityQueue alloc] initWithCapacity:COSINE_K];
    queue.comparator = ^NSComparisonResult(CRFPair *obj1, CRFPair *obj2) {
        if (obj1.sim > obj2.sim) {
            return NSOrderedDescending;
        } else if (obj1.sim < obj2.sim) {
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    };
    for (NSNumber *areaId in self.radiomap) {
        for (NSString *key in self.radiomap[areaId]) {
            NSDictionary *ref_vector = self.radiomap[areaId][key];
            double similarity = [CRFCommon cosineSimilarityWithA:ref_vector B:target_vector];
            CRFPair *p = [[CRFPair alloc] init];
            p.loc = key;
            p.sim = similarity;
            p.areaId = areaId;
            [queue add:p];
            if (queue.size > COSINE_K) [queue poll];
        }
    }
    
    // majority voting
    NSNumber *cA;
    int vote = 0;
    NSMutableDictionary *voting = [NSMutableDictionary dictionary];
    for (CRFPair *p in queue.toArray) {
        if (voting[p.areaId] == nil) voting[p.areaId] = @(0);
        
        voting[p.areaId] = @([voting[p.areaId] intValue]+1);
        
        if ([voting[p.areaId] intValue] > vote) {
            vote = [voting[p.areaId] intValue];
            cA = p.areaId;
        }
    }
//    currentAreaKnn = [cA intValue];
    
    int c1100 = voting[@(1100)] != nil ? [voting[@(1100)] intValue] : 0;
    int c1101 = voting[@(1101)] != nil ? [voting[@(1101)] intValue] : 0;
    if (c1100 > c1101) {
        currentAreaKnn = 1100;
    } else if (c1101 > c1100) {
        currentAreaKnn = 1101;
    }
    
    // determine result
    int currentArea = -1;
    if (currentAreaKnn >= 0 && currentAreaModel >= 0) {
        // both method works
        if (currentAreaModel == currentAreaKnn) {
            // they output the same result, then trust them
            return currentAreaModel;
        }
    } else if (currentAreaKnn >= 0) {
        // in case web server does not response
        return currentAreaKnn;
    }

    return currentArea;
}

- (CGPoint)computeLocation:(NSDictionary *)target_vector
{
    return [self computeLocation:target_vector atArea:[self computeCurrentArea:target_vector]];
}

- (CGPoint)computeLocation:(NSDictionary *)target_vector atArea:(int)areaId
{
    CRFPriorityQueue *queue = [[CRFPriorityQueue alloc] initWithCapacity:COSINE_K];
    queue.comparator = ^NSComparisonResult(CRFPair *obj1, CRFPair *obj2) {
        if (obj1.sim > obj2.sim) {
            return NSOrderedDescending;
        } else if (obj1.sim < obj2.sim) {
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    };
    
    NSArray *filtered_list = [self get_key_from_sectj:target_vector atArea:areaId];
    for (NSString *key in self.radiomap[@(areaId)]) {
        NSDictionary *ref_vector = self.radiomap[@(areaId)][key];
        CGPoint point = [CRFCommon locationString2Point:key];
        if (![filtered_list containsObject:[NSString stringWithFormat:@"%d_%d", (int)point.x, (int)point.y]]) continue;
        double sim = [CRFCommon cosineSimilarityWithA:target_vector B:ref_vector];
        CRFPair *p = [[CRFPair alloc] init];
        p.loc = key;
        p.sim = sim;
        p.areaId = @(areaId);
        [queue add:p];
        if (queue.size > COSINE_K) [queue poll];
    }
    
    double x = 0.0;
    double y = 0.0;
    double sum = 0.0;
    for (CRFPair *p in queue.toArray) {
        if ([p.areaId intValue] != areaId) continue;
        NSArray *column = [p.loc componentsSeparatedByString:@","];
        double r_x = [column[0] doubleValue];
        double r_y = [column[1] doubleValue];
        double sim = p.sim;
        
        x += r_x*sim;
        y += r_y*sim;
        sum += sim;
    }
    
    CGPoint ret = CGPointMake(x*1.0/sum, y*1.0/sum);
    
    NSString *strongest_key = nil;
    double strongest_signal = -1000;
    for (NSString *key in target_vector) {
        double rssi = [target_vector[key] doubleValue];
        int major = [[key componentsSeparatedByString:@"_"][1] intValue];
        if (rssi > strongest_signal && major == areaId) {
            strongest_signal = rssi;
            strongest_key = key;
        }
    }
    if (strongest_key != nil) {
        double rssi = strongest_signal;
        int major = [[strongest_key componentsSeparatedByString:@"_"][1] intValue];
        int minor = [[strongest_key componentsSeparatedByString:@"_"][2] intValue];
        CRFBeacon *b = nil;
        for (CRFBeacon *b_i in self.beaconArray) {
            if (major == b_i.majorValue && minor == b_i.minorValue) {
                b = b_i;
                break;
            }
        }
        CGPoint beaconLoc = b.position;
        if (rssi > b.threshold_rssi) {
            NSLog(@"BINGO: [%d,%d] %f (%.0f,%.0f)", major, minor, rssi, beaconLoc.x, beaconLoc.y);
            double thres = b.threshold_dist * self.locationManager.heatMap.setting.scale;
            double dist = [CRFCommon euclideanFrom:beaconLoc to:ret];
            if (dist > thres) {
                ret.x = ((ret.x*thres+beaconLoc.x*(dist-thres)))/dist;
                ret.y = ((ret.y*thres+beaconLoc.y*(dist-thres)))/dist;
            }
        }
    }

    return ret;
}
*/
@end
