//
//  CRFSensorManager.h
//  Sensor
//
//  Created by Siyan HU on 3/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MagneticData.h"
#import "TransferDataModel.h"

@class CRFSensorManager, CRFBeaconManager;

typedef enum : NSUInteger {
    CRFSensorDataTypeCRF,
    CRFSensorDataTypeBeacon,
    CRFSensorDataTypeCalibratedOrientation,
    CRFSensorDataTypeRawOrientation,
} CRFSensorDataType;

@protocol CRFSensorManagerDelegate <NSObject>

@required
- (void)sensorManager:(CRFSensorManager *)sensorManager didUpdateSensorDataType:(CRFSensorDataType)type data:(id)data;
@end

@interface CRFSensorManager : NSObject

@property (weak, nonatomic) CRFBeaconManager *beaconManager;
@property (weak, nonatomic) id<CRFSensorManagerDelegate> delegate;

- (void)begin;
- (void)stop;

@end
