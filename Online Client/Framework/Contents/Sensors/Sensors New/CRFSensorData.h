//
//  CRFSensorData.h
//  CRF
//
//  Created by Siyan HU on 17/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFSensorData : NSObject

@property (assign, nonatomic) double intensity;
@property (assign, nonatomic) double vertical;
@property (assign, nonatomic) double horizontal;
@property (assign, nonatomic) double orientation;
@property (assign, nonatomic) double timestamp;
@property (assign, nonatomic) double step;
@property (assign, nonatomic) double rAltitude;
@property (assign, nonatomic) double pressure;
@property (strong, nonatomic) NSDictionary *beacon_vector;

@end
