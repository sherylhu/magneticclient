//
//  CRFCompass.h
//  Compass
//
//  Created by Samuel Chow on 16/3/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CRFCompass;

@protocol CRFCompassDelegate <NSObject>

@required
- (void)compass:(CRFCompass *)compass didUpdateCalibratedHeading:(float)calibratedHeading;

@optional
- (void)compass:(CRFCompass *)compass didUpdateMagneticHeading:(float)magneticHeading;
- (void)compass:(CRFCompass *)compass didUpdateTrueHeading:(float)trueHeading;
- (void)compass:(CRFCompass *)compass didUpdateLog:(NSString *)log;

@end

@interface CRFCompass : NSObject

@property (weak, nonatomic) id<CRFCompassDelegate> delegate;

- (void)start:(BOOL)standAlone;
- (void)stop;

- (void)updateMagneticFieldX:(double)x y:(double)y z:(double)z;
- (void)updateHeading:(double)heading;
- (void)updateQuaternionX:(double)x y:(double)y z:(double)z w:(double)w;
- (void)updateGravityX:(double)x y:(double)y z:(double)z;
- (void)updateGyroX:(double)x y:(double)y z:(double)z;

@end
