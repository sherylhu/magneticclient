//
//  TriFloatArray.h
//  Mag IOS
//
//  Created by ivy on 1/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TriFloatArray : NSObject

@property (nonatomic) float x,y,z;

- (int)count;

- (float)getValueForIndex:(int)index;
- (void)setValue:(float)value forIndex:(int)index;

@end
