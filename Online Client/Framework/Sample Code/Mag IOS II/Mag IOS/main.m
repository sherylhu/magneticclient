//
//  main.m
//  Mag IOS
//
//  Created by ivy on 30/4/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
