//
//  MapViewController.m
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import "MapViewController.h"

#import "NetworkHandler.h"
//#import "DataTransfer.h"

typedef enum MVPinType : NSUInteger {
    MVPinLoc,
    MVPinTap,
} MVPinType;

typedef enum MVPCCWLevel : NSUInteger {
    MVPCCW1F,
    MVPCCW2F,
} MVPCCWLevel;

#define MVPCCW1F_STR @"1F"
#define MVPCCW2F_STR @"2F"

@interface MapViewController () <UIGestureRecognizerDelegate, NSFileManagerDelegate, UITableViewDelegate, UITableViewDataSource, DataTransferDelegate> {
    
    UIView *backView;
    UIImageView *mapView;
    UIView *annoView;
    
    UITableView *levelTableView;
    UIView *levelShadowView;
    
    NSArray *gestures;
    CGFloat rotationRadian;
    CGFloat rotationNetRadian;
    CGFloat totalScale;
    CGFloat currentScale;
    CGFloat currentMagnitude;
    
    UIButton *locButton, *tapButton;
    BOOL concentrate;
    
    DataTransfer *datatrans;
    TransferDataModel *currentModel;
    NSTimer *uploadInterval;
    
    MVPCCWLevel currentArea;
    
    NSFileManager *fileManager;
    NSString *fName;
    
    NSArray *uploadData;
}

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentArea = MVPCCW1F;
    concentrate = NO;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    fileManager = [NSFileManager defaultManager];
    fileManager.delegate = self;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.locale = [[NSLocale  alloc]initWithLocaleIdentifier:@"en_HK"];
    format.timeZone = [NSTimeZone timeZoneForSecondsFromGMT: 3600 * 8];
    [format setDateFormat:@"MM-dd HH-mm-ss"];
    NSDate *now = [[NSDate alloc]init];
    NSString *dateString = [format stringFromDate:now];
    if ([dateString length]) {
        fName = [NSString stringWithFormat:@"%@.txt", dateString];
    } else {
        fName = @"Default.txt";
    }
    
    [self mapviewInit];
    [self startNavigationWith:YES and:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MapView Operations
- (void)mapviewInit {
    if (!backView) {
        backView = [[UIView alloc]initWithFrame:self.view.bounds];
        [backView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:backView];
        [self.view sendSubviewToBack:backView];
    }
    
    [backView.layer setMasksToBounds:YES];
    [backView setAutoresizesSubviews:YES];
    [backView setMultipleTouchEnabled:YES];
    [backView setUserInteractionEnabled:YES];
    
    UIPanGestureRecognizer  *panning = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    UIRotationGestureRecognizer  *rotation = [[UIRotationGestureRecognizer alloc]initWithTarget:self action:@selector(handleRotate:)];
    UIPinchGestureRecognizer  *zooming = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinch:)];
    gestures = [NSArray arrayWithObjects:panning, rotation, zooming, nil];
    
    panning.delegate = self;
    rotation.delegate = self;
    zooming.delegate = self;
    
    [backView addGestureRecognizer:panning];
    [backView addGestureRecognizer:rotation];
    [backView addGestureRecognizer:zooming];
    
    [self switchFloor:@"1F"];
    
    locButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [locButton setContentMode:UIViewContentModeScaleAspectFit];
    [locButton setBackgroundImage:[UIImage imageNamed:@"pin_position"] forState:UIControlStateNormal];
    [locButton setBackgroundColor:[UIColor clearColor]];
    [locButton setHidden:YES];
    
    [self showMapLevels];
}

- (void)showMapLevels {

    if (!levelTableView) {
        CGFloat viewWidth = backView.bounds.size.width;
        CGFloat viewHeight = backView.bounds.size.height;
        CGFloat tableMargin = backView.bounds.size.width * 0.07;
        CGFloat tableWidth = backView.bounds.size.width * 0.15;
        CGFloat tableHeight = (viewHeight - tableMargin * 2) < (viewHeight * 0.6) ? (viewHeight - tableMargin * 2) : (viewHeight * 0.6);
        levelTableView = [[UITableView alloc]init];
        levelTableView.frame = CGRectMake(viewWidth - (tableMargin + tableWidth),
                                          tableMargin + backView.bounds.size.height * 0.1,
                                          tableWidth,
                                          tableHeight - 70);
        levelTableView.dataSource = self;
        levelTableView.delegate = self;
        levelTableView.layer.cornerRadius = 2;
        [levelTableView.layer setBorderColor:[UIColor blackColor].CGColor];
        [levelTableView.layer setBorderWidth:0.5];
        levelTableView.cellLayoutMarginsFollowReadableWidth = NO;
        [levelTableView setSeparatorInset:UIEdgeInsetsZero];
        [levelTableView setLayoutMargins:UIEdgeInsetsZero];
        [levelTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
        
        [levelTableView setPagingEnabled:NO];
        levelTableView.bounces = NO;

        [levelTableView reloadData];
        [levelTableView setScrollsToTop:NO];
        
        [levelTableView sizeToFit];
        
        levelTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [levelTableView setBackgroundColor:[UIColor clearColor]];
        
        levelShadowView = [[UIView alloc] initWithFrame:levelTableView.frame];
        levelShadowView.backgroundColor = [UIColor whiteColor];
        levelShadowView.layer.cornerRadius = 5;
        levelShadowView.layer.shadowOpacity = 1.0;
        levelShadowView.layer.shadowColor = [[UIColor blackColor] CGColor];
        levelShadowView.layer.shadowOffset = CGSizeMake(0,0);
        levelShadowView.layer.shadowRadius = 2.0;
    } else {
        
    }
    
    [levelTableView setUserInteractionEnabled:NO];
    
    [backView addSubview:levelTableView];
    [backView bringSubviewToFront:levelTableView];
    [levelTableView reloadData];
    [backView insertSubview:levelShadowView belowSubview:levelTableView];
    
}

- (void)switchFloor:(NSString *)areaId {
    
    MVPCCWLevel tempLevel = currentArea;
    
    if ([areaId isEqualToString:MVPCCW1F_STR]) {
        currentArea = MVPCCW1F;
    } else if ([areaId isEqualToString:MVPCCW2F_STR]) {
        currentArea = MVPCCW2F;
    }
    
    if (!mapView) {
        if (currentScale == 0) {
            currentScale = 1;
            currentScale = currentScale / 2;
            currentMagnitude = 1 / currentScale;
        }
        mapView = [[UIImageView alloc]initWithFrame:backView.bounds];
        [mapView setUserInteractionEnabled:YES];

        [backView addSubview:mapView];
        
        if (!levelShadowView) {
            [backView bringSubviewToFront:mapView];
        } else {
            [backView insertSubview:mapView belowSubview:levelShadowView];
        }
        
        CGAffineTransform transform = CGAffineTransformMakeScale(currentScale, currentScale);
        mapView.transform = transform;
        UIImage *newImage = [UIImage imageNamed:[self levelToString:currentArea]];
        [mapView setFrame:CGRectMake(backView.bounds.origin.x, backView.bounds.origin.y, newImage.size.width, newImage.size.height)];
        [mapView setImage:newImage];
        
        annoView = [[UIView alloc]initWithFrame:mapView.bounds];
        [annoView setBackgroundColor:[UIColor clearColor]];
        [mapView addSubview:annoView];
        [mapView bringSubviewToFront:annoView];
        
    } else {
        if (tempLevel != currentArea) {
            [mapView removeFromSuperview];
            NSArray *subs = [mapView subviews];
            for (UIView *uisub in subs) {
                [uisub removeFromSuperview];
            }
            mapView = nil;
            
            [self removePin:MVPinLoc];
            subs = [NSArray arrayWithArray:[annoView subviews]];
            for (UIView *uisub in subs) {
                [uisub removeFromSuperview];
            }
            annoView = nil;
            
            concentrate = YES;
            
            [self switchFloor:areaId];
        }
    }
}

- (NSString *)levelToString:(MVPCCWLevel)lvl {
    NSString *str = @"PCCWMap1F";
    switch (lvl) {
        case MVPCCW1F:
            str = @"PCCWMap1F";
            break;
        case MVPCCW2F:
            str = @"PCCWMap2F";
            break;
        default:
            break;
    }
    return str;
}

- (void)movePin:(MVPinType)pinType toLoc:(CGPoint)loc {
    [self removePin:pinType];
    CGPoint locCenter = CGPointMake(loc.x, loc.y - 10);
    switch (pinType) {
        case MVPinLoc: {
            locButton.hidden = NO;
            [locButton setUserInteractionEnabled:YES];
            [locButton setCenter:locCenter];
            [annoView addSubview:locButton];
            [annoView bringSubviewToFront:locButton];
            
            CGPoint test = [mapView convertPoint:locCenter fromView:annoView];
            CGRect visibleRect = [backView convertRect:backView.bounds toView:mapView];
            
            if (!CGRectContainsPoint(visibleRect, test) && concentrate) {
                concentrate = NO;
                [self sendToCenter:test];
            }
        }
            break;
        default: {
            tapButton.hidden = NO;
            [tapButton setUserInteractionEnabled:YES];
            [backView insertSubview:tapButton aboveSubview:annoView];
            [tapButton setCenter:locCenter];
            [annoView addSubview:tapButton];
            [annoView bringSubviewToFront:tapButton];
            CGPoint test = [mapView convertPoint:locCenter fromView:annoView];
            [self sendToCenter:test];
        }
            break;
    }
}

- (void)removePin:(MVPinType)pinType {
    switch (pinType) {
        case MVPinLoc:
            [locButton setHidden:YES];
            [locButton setUserInteractionEnabled:NO];
            [locButton removeFromSuperview];
            break;
        default:
            [tapButton setHidden:YES];
            [tapButton setUserInteractionEnabled:NO];
            [tapButton removeFromSuperview];
            break;
    }
}

- (void)sendToCenter:(CGPoint)pnt {
    if (mapView) {
        [UIView animateWithDuration:0.5 animations:^{
            CGPoint newCenter = [backView convertPoint:pnt fromView:mapView];
            
            CGPoint backCenter = CGPointMake(CGRectGetMidX(backView.bounds), CGRectGetMidY(backView.bounds));
            CGPoint disp = CGPointMake(backCenter.x - newCenter.x, backCenter.y - newCenter.y);
            [self translateWithDisplacement:disp];
        }];
        
    }
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    UIGestureRecognizerState state = [recognizer state];
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [recognizer translationInView:backView];
        [self translateWithDisplacement:translation];
        
        [recognizer setTranslation:CGPointMake(0, 0) inView:backView];
    } else if (state==UIGestureRecognizerStateEnded) {
        [self adjustTranslationToAnchor:recognizer.view.center];
    }
}

- (void)translateWithDisplacement:(CGPoint)translation {
    mapView.center = CGPointMake(mapView.center.x + translation.x, mapView.center.y + translation.y);
}

- (void)adjustTranslationToAnchor:(CGPoint)anchor {
    CGFloat svgWidthThresh = mapView.frame.size.width;
    CGFloat svgHeightThresh = mapView.frame.size.height;
    
    CGFloat svgCenterX = CGRectGetMidX(mapView.frame);
    CGFloat svgCenterY = CGRectGetMidY(mapView.frame);
    CGFloat viewCenterX =anchor.x;
    CGFloat viewCenterY =anchor.y;
    
    CGPoint offset = CGPointZero;
    
    if((svgCenterX - viewCenterX) > svgWidthThresh/2) {
        offset = CGPointMake(offset.x - (svgCenterX - viewCenterX - svgWidthThresh/2), offset.y);
    }
    if((viewCenterX - svgCenterX) > svgWidthThresh/2) {
        offset = CGPointMake(offset.x + viewCenterX - svgCenterX - svgWidthThresh/2, offset.y);
    }
    if((svgCenterY - viewCenterY) > svgHeightThresh/2) {
        offset = CGPointMake(offset.x, offset.y - (svgCenterY - viewCenterY - svgHeightThresh/2));
    }
    if((viewCenterY - svgCenterY) > svgHeightThresh/2) {
        offset = CGPointMake(offset.x, offset.y + viewCenterY - svgCenterY - svgHeightThresh/2);
    }
    if(!CGPointEqualToPoint(CGPointZero, offset)) {
        [UIView animateWithDuration:0.2 animations:^{
            [self translateWithDisplacement:offset];
        }];
    }
}

- (void)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    if ([recognizer state] == UIGestureRecognizerStateBegan) {
        [self adjustAnchorPointForGestureRecognizer:recognizer];
        rotationNetRadian = 0.0f;
        rotationRadian = 0.0f;
    }
    
    if ([recognizer state] == UIGestureRecognizerStateChanged) {
        [self rotateWithRadius:recognizer.rotation];
        
        rotationNetRadian = recognizer.rotation;
        rotationRadian += recognizer.rotation;
        recognizer.rotation = 0;
    }
    
    if ([recognizer state] == UIGestureRecognizerStateEnded) {
        
    }
}

- (void)rotateWithRadius:(CGFloat)radius {
    mapView.transform = CGAffineTransformRotate(mapView.transform, radius);
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = mapView;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan) {
        [self adjustAnchorPointForGestureRecognizer:recognizer];
        totalScale = 1.0f;
    }
    
    if (state == UIGestureRecognizerStateChanged) {
        [self scaleWithScale:recognizer.scale];
        recognizer.scale = 1;
    }
    
    if(state==UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:0.2 animations:^{
        }];
    }
}

- (void)scaleWithScale:(CGFloat)scale {
    
    CGFloat preScale = totalScale;
    
    mapView.transform = CGAffineTransformScale(mapView.transform, scale, scale);
    totalScale *= scale;
    currentMagnitude = currentMagnitude / (totalScale / preScale);
}

- (void)adjustScaleToSize:(CGSize)size {
    [UIView animateWithDuration:0.2 animations:^{
        while (currentMagnitude <= 1.1 || currentMagnitude >= 70) {
            if (currentMagnitude <= 1.1) {
                NSLog(@"少於1");
                [self scaleWithScale:0.9];
            } else if (currentMagnitude >= 70) {
                NSLog(@"大於70");
                [self scaleWithScale:1.1];
            }
        }
    }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    if ([gestures containsObject:gestureRecognizer]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"LVLCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    NSString *name = MVPCCW1F_STR;
    if (indexPath.row != 0) {
        name = MVPCCW2F_STR;
    }
    cell.textLabel.text= name;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    [cell.textLabel setAdjustsFontSizeToFitWidth:YES];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setBackgroundColor:[UIColor clearColor]];
    
    [cell setSeparatorInset:UIEdgeInsetsZero];
    [cell setLayoutMargins:UIEdgeInsetsZero];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (currentArea) {
        case MVPCCW1F:
            if (indexPath.row == 0) {
                [cell setSelected:YES];
                [cell.textLabel setTextColor:[UIColor whiteColor]];
                UIView *selectedBackground = [[UIView alloc]initWithFrame:cell.bounds];
                selectedBackground.backgroundColor = [UIColor lightGrayColor];
                [cell setSelectedBackgroundView:selectedBackground];
                [cell.backgroundView removeFromSuperview];
            } else {
                [cell setSelected:NO];
                [cell.textLabel setTintColor:[UIColor darkGrayColor]];
                UIView *normalBackground = [[UIView alloc]initWithFrame:cell.bounds];
                normalBackground.backgroundColor = [UIColor whiteColor];
                [cell setBackgroundView:normalBackground];
                [cell.selectedBackgroundView removeFromSuperview];
            }
            break;
        case MVPCCW2F:
            if (indexPath.row == 0) {
                [cell setSelected:NO];
                [cell.textLabel setTintColor:[UIColor darkGrayColor]];
                UIView *normalBackground = [[UIView alloc]initWithFrame:cell.bounds];
                normalBackground.backgroundColor = [UIColor whiteColor];
                [cell setBackgroundView:normalBackground];
                [cell.selectedBackgroundView removeFromSuperview];
            } else {
                [cell setSelected:YES];
                [cell.textLabel setTextColor:[UIColor whiteColor]];
                UIView *selectedBackground = [[UIView alloc]initWithFrame:cell.bounds];
                selectedBackground.backgroundColor = [UIColor lightGrayColor];
                [cell setSelectedBackgroundView:selectedBackground];
                [cell.backgroundView removeFromSuperview];
            }
            break;
        default:
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"kelvin didSelectRowAtIndexPath, section: %li, row: %li", indexPath.section, indexPath.row);
}

#pragma mark - File Operations
- (void)write:(TransferDataModel *)model intoFile:(NSString *)fileName {
    if (fileName) {
        NSString *filePath = [self composeFileName:fileName];
        NSLog(@"File Name: %@", filePath);
        
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
        if (!fileHandler) {
            [fileManager createFileAtPath:filePath contents:nil attributes:nil];
            fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
        }
        [fileHandler seekToEndOfFile];
        
        if (model) {
            NSString *str = [model model2String];
            NSLog(@"Write to FIle: %@", str);
            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
            if (data) {
                [fileHandler writeData:data];
            }
        }
        [fileHandler closeFile];
    }
}

- (BOOL)fileExisted:(NSString *)filePath isDirectory:(BOOL)isDirectory {
    BOOL ifDirectory = isDirectory;
    return [fileManager fileExistsAtPath:filePath isDirectory:&ifDirectory];
}

- (NSString *)composeFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:name];
    NSLog(@"File Path: %@", filePath);
    return filePath;
}

#pragma mark - Sensor Operations
#pragma mark MARK: initialization 1
- (void)startNavigationWith:(BOOL)step and:(BOOL)magetic {
    datatrans = [[DataTransfer alloc]init];
    datatrans.delegate = self;
    [datatrans startSensing];
}

#pragma mark MARK: Get Result
- (void)reportData:(TransferDataModel *)data {
    if (!currentModel) {
        currentModel = [[TransferDataModel alloc]init];
    }
    currentModel = data;
    [self write:data intoFile:fName];
    
    [[NetworkHandler instance] upload:currentModel toBaseUrl:self.base_url success:^(id responseObject) {
        Position *post = [[Position alloc]initWithJson:responseObject];
        if (![[self levelToString:currentArea] isEqualToString:post.area_id]) {
            [self switchFloor:post.area_id];
        }
        if (post) {
            [self movePin:MVPinLoc toLoc:CGPointMake(post.x, post.y)];
        }
    } failure:^(NSError *error) {
        NSLog(@"Error: %@", error.description);
    }];
}

#pragma mark - IBActions
- (IBAction)cancelClicked:(id)sender {
    [uploadInterval invalidate];
    [datatrans stopSensing];
    datatrans = nil;
    [self removePin:MVPinLoc];
    [self dismissViewControllerAnimated:self completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
