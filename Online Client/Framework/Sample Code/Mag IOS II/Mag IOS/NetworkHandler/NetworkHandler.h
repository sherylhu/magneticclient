//
//  NetworkHandler.h
//  Wherami Union Hospital
//
//  Created by ivy on 6/1/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "TransferDataModel.h"

@interface NetworkHandler : NSObject

+ (NetworkHandler *)instance;

- (void)getHtmlFrom:(NSString *)url
 resposneSerializer:(AFHTTPResponseSerializer *)responseSerializer
           progress:(void (^)(NSProgress *currentProgress))progress
            success:(void (^)(NSString *html_str))success
            failure:(void (^)(NSError *error))failure;

- (void)upload:(TransferDataModel *)transData
     toBaseUrl:(NSString *)baseurl
       success:(void (^)(id responseObject))success
       failure:(void (^)(NSError *error))failure;

- (void)downloadFrom:(NSString *)url
  resposneSerializer:(AFHTTPResponseSerializer *)responseSerializer
             success:(void (^)(id responseObject))success
            progress:(void (^)(NSProgress *currentProgress))progress
             failure:(void (^)(NSError *error))failure;

- (BOOL)ipv4Network;

@end
