//
//  ViewController.swift
//  Mag Swift
//
//  Created by HU Siyan on 29/11/2017.
//  Copyright © 2017 HU Siyan. All rights reserved.
//

import UIKit
import MFDataHandler

class ViewController: UIViewController, DataTransferDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //correct position
    let dt = DataTransfer()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //His wrong position
        //let dt = DataTransfer()
        dt.delegate = self
        dt .startSensing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reportData(_ data: TransferDataModel!) {
        print("Data: %@", data)
    }

}

