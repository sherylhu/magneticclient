//
//  DataTransfer.h
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransferDataModel.h"

@protocol DataTransferDelegate <NSObject>

- (void)reportData:(TransferDataModel *)data;

@end

@interface DataTransfer : NSObject

@property (weak, nonatomic) id <DataTransferDelegate> delegate;

- (void)startSensing;
- (void)stopSensing;

@end
