//
//  MagneticData.m
//  Mag Client iOS
//
//  Created by ivy on 4/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "MagneticData.h"

@interface MagneticData () {
    long timestamp;
    NSArray *magvalue;
    int step;
}

@end

@implementation MagneticData

- (id)initWithTimestamp:(long)ts {
    self = [super init];
    if (self) {
        timestamp = ts;
        step = -1;
    }
    return self;
}

- (BOOL)addIntensity:(float)intensity andVertical:(float)vertical andHorizontal:(float)horizontal andOrientation:(float)orientation {
    
    magvalue = [NSArray arrayWithObjects:[NSNumber numberWithFloat:intensity], [NSNumber numberWithFloat:vertical], [NSNumber numberWithFloat:horizontal], [NSNumber numberWithFloat:orientation], nil];
    if ([magvalue count]) {
        return YES;
    }
    return NO;
}

- (NSString *)model2String {
    if ([magvalue count] == 4) {
        NSString *temp = [NSString stringWithFormat:@"{%@,%@,%@,%@,%ld}",[magvalue objectAtIndex:0], [magvalue objectAtIndex:1], [magvalue objectAtIndex:2], [magvalue objectAtIndex:3], timestamp];
        
        return temp;
    }
    return @"";
}

- (id)proxy2Gson {
    NSArray *array = [NSArray arrayWithObjects:
                      [magvalue objectAtIndex:0],
                      [magvalue objectAtIndex:1],
                      [magvalue objectAtIndex:2],
                      [magvalue objectAtIndex:3],
                      //                      [NSNumber numberWithLong:timestamp],
                      nil];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:array forKey:@"magValues"];
    [dict setObject:[NSNumber numberWithLong:timestamp] forKey:@"nanoTimestamp"];
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

@end
