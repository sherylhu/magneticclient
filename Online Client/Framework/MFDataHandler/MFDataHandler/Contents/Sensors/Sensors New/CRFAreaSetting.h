//
//  CRFAreaSetting.h
//  CRF
//
//  Created by Siyan HU on 16/4/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFAreaSetting : NSObject

@property (strong, nonatomic) NSString *heatmap;
@property (strong, nonatomic) NSArray *trace;
@property (strong, nonatomic) NSArray *route;
@property (strong, nonatomic) NSArray *inconstraint;
@property (strong, nonatomic) NSString *outconstraint;
@property (assign, nonatomic) double angle;
@property (strong, nonatomic) NSArray *turning;
@property (assign, nonatomic) double orientation;
@property (assign, nonatomic) double scale;
@property (strong, nonatomic) NSArray *radiomap;
@property (strong, nonatomic) NSString *beacon;
@property (strong, nonatomic) NSDictionary *sectj;

@end
