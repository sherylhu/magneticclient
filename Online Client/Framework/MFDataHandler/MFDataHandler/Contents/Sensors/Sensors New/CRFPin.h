//
//  CRFPin.h
//  CRF
//
//  Created by Siyan HU on 17/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    CRFPinColorOrange,
    CRFPinColorBlue,
    CRFPinColorRed,
    CRFPinColorYellow,
} CRFPinColor;

@interface CRFPin : UIView

- (instancetype)initWithColor:(CRFPinColor)color withCoverage:(double)coverage;

@end
