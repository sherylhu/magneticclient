//
//  CRFCompass.m
//  Compass
//
//  Created by Samuel Chow on 16/3/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFCompass.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

#define angular_speed_threshold 3.5f
#define low_pass_factor 0.48f
#define high_pass_factor 0.05f
#define sliding_window_size 1500
#define refresh_time 10000
#define magnet_noise_threshold 60.0f

@interface CRFCompass () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CMMotionManager *motionManager;

@property (strong, nonatomic) NSOperationQueue *deviceMotionQueue;
@property (strong, nonatomic) NSOperationQueue *gyroQueue;
@property (strong, nonatomic) NSOperationQueue *magnetometerQueue;

@property (assign, nonatomic) float heading_orientation;
@property (assign, nonatomic) float magnet_field_intensity;
@property (strong, nonatomic) NSArray *magnetFieldValues;
@property (strong, nonatomic) NSArray *gravityValues;
@property (assign, nonatomic) long period;
@property (assign, nonatomic) long startTime;
@property (assign, nonatomic) float max_angular_speed;
@property (strong, nonatomic) NSMutableArray *deltaAngular;
@property (strong, nonatomic) NSMutableArray *angularSpeed;
@property (strong, nonatomic) NSMutableArray *deltaQuater;
@property (strong, nonatomic) NSMutableArray *quaternionNew;
@property (strong, nonatomic) NSMutableArray *quaternionOld;
@property (strong, nonatomic) NSMutableArray *rotateDegrees;
@property (strong, nonatomic) NSMutableArray *quatDegrees;
@property (strong, nonatomic) NSArray *Rmatrix;
@property (assign, nonatomic) BOOL mDirectionStarted;
@property (assign, nonatomic) int iter;
@property (strong, nonatomic) NSMutableArray *magnetDegree;
@property (strong, nonatomic) NSMutableArray *mReferenceDegree;
@property (strong, nonatomic) NSMutableArray *mDiff;
@property (strong, nonatomic) NSMutableArray *mDiffArr;

@property (strong, nonatomic) id mCompassSyncObject;
@property (strong, nonatomic) NSTimer *timer;

@property (assign, nonatomic) BOOL isStandAlone;
@end

@implementation CRFCompass

#pragma mark - Public

- (void)start:(BOOL)standAlone
{
    [self initValues];
    self.isStandAlone = standAlone;
    if (self.isStandAlone) {
        self.mCompassSyncObject = [NSDate date];
        [self registerSensor];
    }
}

- (void)stop
{
    if (self.isStandAlone) {
        [self unregisterSensor];
    }
}

#pragma mark - Update sensor data

- (void)updateMagneticFieldX:(double)x y:(double)y z:(double)z
{
    self.magnet_field_intensity = (float)sqrt(x*x+y*y+z*z);
    self.magnetFieldValues = @[@(x), @(y), @(z)];
    
    [self calibrateMagnetDegree];
}

- (void)updateHeading:(double)heading
{
    self.heading_orientation = heading;
    
    [self calibrateMagnetDegree];
}

- (void)updateQuaternionX:(double)x y:(double)y z:(double)z w:(double)w
{
    if (self.startTime % refresh_time < 5 && self.magnet_field_intensity < magnet_noise_threshold) {
        [self initValues];
    }
    
    self.quaternionOld[0] = @(x);
    self.quaternionOld[1] = @(y);
    self.quaternionOld[2] = @(z);
    self.quaternionOld[3] = @(w);

    [self calibrateMagnetDegree];
}

- (void)updateGravityX:(double)x y:(double)y z:(double)z
{
    self.gravityValues = @[@(x), @(y), @(z)];
    [self calibrateMagnetDegree];
}

- (void)updateGyroX:(double)x y:(double)y z:(double)z
{
    long currentTime = (long)([[NSDate date] timeIntervalSince1970] * 1000);
    
    self.period += (currentTime - self.startTime);
    self.startTime = currentTime;
    
    self.max_angular_speed = MAX(x, MAX(y, z));

    if (self.max_angular_speed >= angular_speed_threshold || self.period < 16000) {
        [self initValues];
        return;
    }
    
    if (self.period > 1000000) {
        self.period = 13000;
    }
    
    // roll
    self.deltaAngular[0] = @(([self.angularSpeed[0] doubleValue] + x) * (currentTime - self.startTime) / 2000);
    self.angularSpeed[0] = @(x);
    
    // pitch
    self.deltaAngular[1] = @(([self.angularSpeed[1] doubleValue] + y) * (currentTime - self.startTime) / 2000);
    self.angularSpeed[1] = @(y);
    
    // yaw
    self.deltaAngular[2] = @(([self.angularSpeed[2] doubleValue] + z) * (currentTime - self.startTime) / 2000);
    self.angularSpeed[2] = @(z);
    
    float sin1 = (float)sin([self.deltaAngular[0] doubleValue]);
    float cos1 = (float)cos([self.deltaAngular[0] doubleValue]);
    float sin2 = (float)sin([self.deltaAngular[1] doubleValue]);
    float cos2 = (float)cos([self.deltaAngular[1] doubleValue]);
    float sin3 = (float)sin([self.deltaAngular[2] doubleValue]);
    float cos3 = (float)cos([self.deltaAngular[2] doubleValue]);
    
    self.deltaQuater[0] = @(cos1*cos2*cos3 - sin1*sin2*sin3);
    self.deltaQuater[1] = @(cos1*cos2*sin3 + sin1*sin2*cos3);
    self.deltaQuater[2] = @(sin1*cos2*cos3 + cos1*sin2*sin3);
    self.deltaQuater[3] = @(cos1*sin2*cos3 - sin1*cos2*sin3);
    
    self.quaternionNew[0] = @([self.quaternionOld[0] doubleValue]*[self.deltaQuater[0] doubleValue] -
                              [self.quaternionOld[1] doubleValue]*[self.deltaQuater[1] doubleValue] -
                              [self.quaternionOld[2] doubleValue]*[self.deltaQuater[2] doubleValue] -
                              [self.quaternionOld[3] doubleValue]*[self.deltaQuater[3] doubleValue]);
    self.quaternionNew[1] = @([self.quaternionOld[0] doubleValue]*[self.deltaQuater[1] doubleValue] +
                              [self.quaternionOld[1] doubleValue]*[self.deltaQuater[0] doubleValue] +
                              [self.quaternionOld[2] doubleValue]*[self.deltaQuater[3] doubleValue] -
                              [self.quaternionOld[3] doubleValue]*[self.deltaQuater[2] doubleValue]);
    self.quaternionNew[2] = @([self.quaternionOld[0] doubleValue]*[self.deltaQuater[2] doubleValue] -
                              [self.quaternionOld[1] doubleValue]*[self.deltaQuater[3] doubleValue] +
                              [self.quaternionOld[2] doubleValue]*[self.deltaQuater[0] doubleValue] +
                              [self.quaternionOld[3] doubleValue]*[self.deltaQuater[1] doubleValue]);
    self.quaternionNew[3] = @([self.quaternionOld[0] doubleValue]*[self.deltaQuater[3] doubleValue] +
                              [self.quaternionOld[1] doubleValue]*[self.deltaQuater[2] doubleValue] -
                              [self.quaternionOld[2] doubleValue]*[self.deltaQuater[1] doubleValue] +
                              [self.quaternionOld[3] doubleValue]*[self.deltaQuater[0] doubleValue]);
    
    // low pass filter
    for (int i = 0; i < self.quaternionOld.count; i++) {
        self.quaternionOld[i] = @(low_pass_factor*[self.quaternionOld[i] doubleValue] + (1-low_pass_factor)*[self.quaternionNew[i] doubleValue]);
    }
    
    self.Rmatrix = [CRFCompass getRotationMatrixFromVector:self.quaternionOld];

    // calculate degree changes
    // x-y plane direction rotate degree
    self.rotateDegrees[0] = @((float)RADIANS_TO_DEGREES(atan2([self.Rmatrix[3] doubleValue], [self.Rmatrix[4] doubleValue])) - [self.quatDegrees[0] doubleValue]);
    self.quatDegrees[0] = @([self.quatDegrees[0] doubleValue] + [self.rotateDegrees[0] doubleValue]);
    // x-z plane direction rotate degree
    self.rotateDegrees[1] = @((float)RADIANS_TO_DEGREES(atan2([self.Rmatrix[3] doubleValue], [self.Rmatrix[5] doubleValue])) - [self.quatDegrees[1] doubleValue]);
    self.quatDegrees[1] = @([self.quatDegrees[1] doubleValue] + [self.rotateDegrees[1] doubleValue]);
    // y-z plane direction rotate degree
    self.rotateDegrees[2] = @((float)RADIANS_TO_DEGREES(atan2([self.Rmatrix[4] doubleValue], [self.Rmatrix[5] doubleValue])) - [self.quatDegrees[2] doubleValue]);
    self.quatDegrees[2] = @([self.quatDegrees[2] doubleValue] + [self.rotateDegrees[2] doubleValue]);
    
    [self calibrateMagnetDegree:self.rotateDegrees isReal:YES];
    [self calibrateMagnetDegree];
}

#pragma mark - Sensor operations

- (void)registerSensor
{
    self.startTime = 0;
    
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [self.locationManager requestAlwaysAuthorization];
    
    [self.motionManager startDeviceMotionUpdatesToQueue:self.deviceMotionQueue withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
        @synchronized(self.mCompassSyncObject) {
            [self updateGravityX:motion.gravity.x*-10.0 y:motion.gravity.y*-10.0 z:motion.gravity.z*-10.0];
            [self updateQuaternionX:-motion.attitude.quaternion.x y:-motion.attitude.quaternion.y z:-motion.attitude.quaternion.z w:-motion.attitude.quaternion.w];
        }
    }];
    
    [self.motionManager startGyroUpdatesToQueue:self.gyroQueue withHandler:^(CMGyroData * _Nullable gyroData, NSError * _Nullable error) {
        @synchronized(self.mCompassSyncObject) {
            [self updateGyroX:gyroData.rotationRate.x y:gyroData.rotationRate.y z:gyroData.rotationRate.z];
        }
    }];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self.locationManager selector:@selector(startUpdatingHeading) userInfo:nil repeats:YES];
}

- (void)unregisterSensor
{
    [self.deviceMotionQueue cancelAllOperations];
    [self.gyroQueue cancelAllOperations];
    [self.magnetometerQueue cancelAllOperations];
    
    [self.motionManager stopDeviceMotionUpdates];
    [self.motionManager stopGyroUpdates];
    [self.motionManager stopMagnetometerUpdates];
    
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - Private

- (void)initValues
{
    self.mDirectionStarted = NO;
    self.max_angular_speed = 0;
    self.iter = 0;
    
    for (int i = 0; i < self.magnetDegree.count; i++) {
        double magnetDegree = [self.magnetDegree[i] doubleValue];
        self.mReferenceDegree[i] = @(magnetDegree);
        self.quatDegrees[i] = @(magnetDegree);

        self.mDiff[i] = @(0);
        self.rotateDegrees[i] = @(0);
        for (int j = 0; j < sliding_window_size; j++) {
            self.mDiffArr[i][j] = @(0);
        }
    }
    
    for (int i = 0; i < self.quaternionOld.count; i++) {
        self.quaternionOld[i] = @(0);
        self.quaternionNew[i] = @(0);
        self.deltaQuater[i] = @(0);
    }
}

- (void)calibrateMagnetDegree
{
    NSArray *R = [CRFCompass getRotationMatrixGravity:self.gravityValues mag:self.magnetFieldValues];
    NSArray *values = [CRFCompass getOrientation:R];
    self.magnetDegree[0] = @((float)RADIANS_TO_DEGREES([values[0] doubleValue]));
    self.magnetDegree[1] = @((float)RADIANS_TO_DEGREES([values[1] doubleValue]));
    self.magnetDegree[2] = @((float)RADIANS_TO_DEGREES([values[2] doubleValue]));

    [self calibrateMagnetDegree:self.magnetDegree isReal:NO];
}

- (void)calibrateMagnetDegree:(NSArray *)degrees isReal:(BOOL)isReal
{
    if (!isReal) {
        for (int i = 0; i < degrees.count; i++) {
            self.magnetDegree[i] = degrees[i];
        }
        if (!self.mDirectionStarted) {
            for (int i = 0; i < degrees.count; i++) {
                self.mReferenceDegree[i] = degrees[i];
            }
            self.mDirectionStarted = YES;
        } else {
            for (int i = 0; i < self.magnetDegree.count; i++) {
                float diff = ([self.magnetDegree[i] doubleValue] - [self.mReferenceDegree[i] doubleValue]);
                diff = [CRFCompass normalize:diff]*high_pass_factor;
                if (self.iter >= sliding_window_size)
                    self.iter = 0;
                self.mDiff[i] = @([self.mDiff[i] doubleValue] - [self.mDiffArr[i][self.iter] doubleValue] / sliding_window_size);
                self.mDiff[i] = @([self.mDiff[i] doubleValue] + diff / sliding_window_size);
                self.mDiffArr[i][self.iter++] = @(diff);
            }
        }
    } else {
        if (self.mDirectionStarted) {
            if (fabs([degrees[0] doubleValue]) > 3) {
                return;
            }
            for (int i = 0; i < self.magnetDegree.count; i++) {
                self.mReferenceDegree[i] = @([CRFCompass normalize:([self.mReferenceDegree[i] doubleValue] - [degrees[i] doubleValue])]);
                float diff = [self.magnetDegree[i] doubleValue] - [self.mReferenceDegree[i] doubleValue];
                diff = [CRFCompass normalize:diff]*high_pass_factor;
                if (self.iter >= sliding_window_size) self.iter = 0;
                self.mDiff[i] = @([self.mDiff[i] doubleValue] - [self.mDiffArr[i][self.iter] doubleValue] / sliding_window_size);
                self.mDiff[i] = @([self.mDiff[i] doubleValue] + diff / sliding_window_size);
                self.mDiffArr[i][self.iter++] = @(diff);
            }
        }
    }
    
    [self.delegate compass:self didUpdateCalibratedHeading:([self.mReferenceDegree[0] doubleValue] + [self.mDiff[0] doubleValue])];
}

#pragma mark - Util

+ (float)normalize:(float)degree
{
    while (degree >= 180) {
        degree -= 360;
    }
    while (degree < -180) {
        degree += 360;
    }
    return degree;
}

+ (NSArray *)getRotationMatrixFromVector:(NSArray *)rotationVector
{
    float q0;
    float q1 = [rotationVector[0] doubleValue];
    float q2 = [rotationVector[1] doubleValue];
    float q3 = [rotationVector[2] doubleValue];
    
    if (rotationVector.count == 4) {
        q0 = [rotationVector[3] doubleValue];
    } else {
        q0 = 1 - q1*q1 - q2*q2 - q3*q3;
        q0 = (q0 > 0) ? (float)sqrt(q0) : 0;
    }
    
    float sq_q1 = 2 * q1 * q1;
    float sq_q2 = 2 * q2 * q2;
    float sq_q3 = 2 * q3 * q3;
    float q1_q2 = 2 * q1 * q2;
    float q3_q0 = 2 * q3 * q0;
    float q1_q3 = 2 * q1 * q3;
    float q2_q0 = 2 * q2 * q0;
    float q2_q3 = 2 * q2 * q3;
    float q1_q0 = 2 * q1 * q0;
    
    NSMutableArray *R = [NSMutableArray array];
    [R addObject:@(1 - sq_q2 - sq_q3)];
    [R addObject:@(q1_q2 - q3_q0)];
    [R addObject:@(q1_q3 + q2_q0)];
    
    [R addObject:@(q1_q2 + q3_q0)];
    [R addObject:@(1 - sq_q1 - sq_q3)];
    [R addObject:@(q2_q3 - q1_q0)];
    
    [R addObject:@(q1_q3 - q2_q0)];
    [R addObject:@(q2_q3 + q1_q0)];
    [R addObject:@(1 - sq_q1 - sq_q2)];
    
    return R;
}

+ (NSArray *)getOrientation:(NSArray *)R
{
    return @[@((float)atan2([R[1] doubleValue], [R[4] doubleValue])), @((float)asin(-[R[7] doubleValue])), @((float)atan2(-[R[6] doubleValue], [R[8] doubleValue]))];
}

+ (NSArray *)getRotationMatrixGravity:(NSArray *)gravity mag:(NSArray *)geomagnetic
{
    float Ax = [gravity[0] doubleValue];
    float Ay = [gravity[1] doubleValue];
    float Az = [gravity[2] doubleValue];
    
    float normsqA = (Ax*Ax + Ay*Ay + Az*Az);
    float g = 9.81f;
    float freeFallGravitySquared = 0.01f * g * g;
    if (normsqA < freeFallGravitySquared) {
        // gravity less than 10% of normal values
        return nil;
    }
    
    float Ex = [geomagnetic[0] doubleValue];
    float Ey = [geomagnetic[1] doubleValue];
    float Ez = [geomagnetic[2] doubleValue];
    float Hx = Ey*Az - Ez*Ay;
    float Hy = Ez*Ax - Ex*Az;
    float Hz = Ex*Ay - Ey*Ax;
    float normH = (float)sqrt(Hx*Hx + Hy*Hy + Hz*Hz);
    
    if (normH < 0.1f) {
        // device is close to free fall or close to magnetic north pole
        return nil;
    }
    
    float invH = 1.0f / normH;
    Hx *= invH;
    Hy *= invH;
    Hz *= invH;
    float invA = 1.0f / (float)sqrt(Ax*Ax + Ay*Ay + Az*Az);
    Ax *= invA;
    Ay *= invA;
    Az *= invA;
    
    float Mx = Ay*Hz - Az*Hy;
    float My = Az*Hx - Ax*Hz;
    float Mz = Ax*Hy - Ay*Hx;
    
    return @[@(Hx), @(Hy), @(Hz),
             @(Mx), @(My), @(Mz),
             @(Ax), @(Ay), @(Az)];
}

#pragma mark <CLLocationManager>

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    @synchronized(self.mCompassSyncObject) {
        [self updateMagneticFieldX:newHeading.x y:newHeading.y z:newHeading.z];
        [self updateHeading:(newHeading.trueHeading > 0 ? newHeading.trueHeading : newHeading.magneticHeading)];
    }
    if ([self.delegate respondsToSelector:@selector(compass:didUpdateTrueHeading:)]) {
        [self.delegate compass:self didUpdateTrueHeading:newHeading.trueHeading];
    }
    if ([self.delegate respondsToSelector:@selector(compass:didUpdateMagneticHeading:)]) {
        [self.delegate compass:self didUpdateMagneticHeading:newHeading.magneticHeading];
    }
}

#pragma mark - Mutator

- (CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    
    return _locationManager;
}

- (CMMotionManager *)motionManager
{
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
    }
    
    return _motionManager;
}

- (NSOperationQueue *)deviceMotionQueue
{
    if (!_deviceMotionQueue) {
        _deviceMotionQueue = [[NSOperationQueue alloc] init];
        _deviceMotionQueue.maxConcurrentOperationCount = 1;
    }
    
    return _deviceMotionQueue;
}

- (NSOperationQueue *)gyroQueue
{
    if (!_gyroQueue) {
        _gyroQueue = [[NSOperationQueue alloc] init];
        _gyroQueue.maxConcurrentOperationCount = 1;
    }
    
    return _gyroQueue;
}

- (NSOperationQueue *)magnetometerQueue
{
    if (!_magnetometerQueue) {
        _magnetometerQueue = [[NSOperationQueue alloc] init];
        _magnetometerQueue.maxConcurrentOperationCount = 1;
    }
    
    return _magnetometerQueue;
}

- (NSArray *)magnetFieldValues
{
    if (!_magnetFieldValues) {
        _magnetFieldValues = @[@(0), @(0), @(0)];
    }
    
    return _magnetFieldValues;
}

- (NSArray *)gravityValues
{
    if (!_gravityValues) {
        _gravityValues = @[@(0), @(0), @(0)];
    }
    
    return _gravityValues;
}

- (NSMutableArray *)mReferenceDegree
{
    if (!_mReferenceDegree) {
        _mReferenceDegree = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _mReferenceDegree;
}

- (NSMutableArray *)magnetDegree
{
    if (!_magnetDegree) {
        _magnetDegree = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _magnetDegree;
}

- (NSMutableArray *)quatDegrees
{
    if (!_quatDegrees) {
        _quatDegrees = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _quatDegrees;
}

- (NSMutableArray *)rotateDegrees
{
    if (!_rotateDegrees) {
        _rotateDegrees = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _rotateDegrees;
}

- (NSMutableArray *)deltaAngular
{
    if (!_deltaAngular) {
        _deltaAngular = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _deltaAngular;
}

- (NSMutableArray *)angularSpeed
{
    if (!_angularSpeed) {
        _angularSpeed = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _angularSpeed;
}

- (NSMutableArray *)mDiff
{
    if (!_mDiff) {
        _mDiff = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return _mDiff;
}

- (NSMutableArray *)mDiffArr
{
    if (!_mDiffArr) {
        _mDiffArr = [NSMutableArray array];
        for (int i = 0; i < 3; i++) {
            NSMutableArray *arr = [NSMutableArray array];
            for (int j = 0; j < sliding_window_size; j++) {
                [arr addObject:@(0)];
            }
            [_mDiffArr addObject:arr];
        }
    }
    
    return _mDiffArr;
}

- (NSArray *)Rmatrix
{
    if (!_Rmatrix) {
        _Rmatrix = @[@(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0)];
    }
    
    return _Rmatrix;
}

- (NSMutableArray *)deltaQuater
{
    if (!_deltaQuater) {
        _deltaQuater = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), @(0), nil];
    }
    
    return _deltaQuater;
}

- (NSMutableArray *)quaternionOld
{
    if (!_quaternionOld) {
        _quaternionOld = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), @(0), nil];
    }
    
    return _quaternionOld;
}

- (NSMutableArray *)quaternionNew
{
    if (!_quaternionNew) {
        _quaternionNew = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), @(0), nil];
    }
    
    return _quaternionNew;
}

@end
