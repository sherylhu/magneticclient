package mtrec.magneticcrf.localization;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;



public class EMSignalMap {
    private static class FingerprintEntry {
        public String address = "";
        EMFingerprintValue value = new EMFingerprintValue();
    }



    public HashMap<FingerprintPosition, EMFingerprint[] > map = new HashMap<>();

    public HashSet<String> effectiveApSet = new HashSet<>();



    public static EMSignalMap loadFromFile(
            String filePath,
            double dbmRangeFilterThreshold,
            boolean filterVirtualMac
    ) throws IOException, IllegalFormatException {
        HashMap< FingerprintPosition, ArrayList<EMFingerprint> > tmpResult = new HashMap<>();

        BufferedReader signalMapFile;
        String line;

        try {
            signalMapFile = new BufferedReader(new FileReader(filePath));
        } catch (Exception e) {
            throw new IOException("Error reading the signal map file");
        }

        int lineNo = 0;

        try {
            String[] lineComponent;

            while (true) {
                line = signalMapFile.readLine();
                if (line == null) {
                    break;
                }

                ++lineNo;

                line = line.trim();
                if (line.equals("")) {
                    continue;
                }

                lineComponent = line.split(" ");

                if (lineComponent.length < 2) {
                    throw new IllegalFormatException("Error parsing the signal map file at line "
                            + String.valueOf(lineNo));
                }

                FingerprintPosition fingerprintPos = parseFingerprintPosition(lineComponent[0]);
                if (fingerprintPos == null) {
                    throw new IllegalFormatException(
                            String.format(
                                    "Error parsing the signal map file at line %d: Invalid position syntax",
                                    lineNo
                            )
                    );

                }

                EMFingerprint fingerprint = new EMFingerprint();

                for (int i = 1; i < lineComponent.length; ++i) {
                    FingerprintEntry entry = parseFingerprintEntry(lineComponent[i]);

                    if (entry == null) {
                        throw new IllegalFormatException(
                                String.format(
                                        "Error parsing the signal map file at line %d: Invalid signal value syntax",
                                        lineNo
                                )
                        );

                    }

                    fingerprint.valueByAddress.put(entry.address, entry.value);
                }

                if (!tmpResult.containsKey(fingerprintPos)) {
                    tmpResult.put(fingerprintPos, new ArrayList< EMFingerprint >());
                }

                tmpResult.get(fingerprintPos).add(fingerprint);

            }

        } catch (IOException e) {
            throw new IOException("Error reading the signal map file");

        }

        // Filter ineffective AP
        HashSet<String> effectiveAp = filterIneffectiveAP(tmpResult, dbmRangeFilterThreshold, filterVirtualMac);

        // Convert ArrayList to Array (for final result)
        EMSignalMap result = new EMSignalMap();

        for (Map.Entry<FingerprintPosition, ArrayList<EMFingerprint> > entry : tmpResult.entrySet()) {
            // Remove "empty" fingerprint (Fingerprint without any AP address due to filtering)
            ArrayList<EMFingerprint> effectiveFingerprint = new ArrayList<>();

            for (EMFingerprint fingerprintByDirection : entry.getValue()) {
                if (fingerprintByDirection.valueByAddress.size() > 0) {
                    effectiveFingerprint.add(fingerprintByDirection);
                }

            }

            // Ignore reference point with all "empty" fingerprint
            if (effectiveFingerprint.size() == 0) {
                continue;
            }

            result.map.put(
                    entry.getKey(),
                    effectiveFingerprint.toArray(new EMFingerprint[0])
            );
        }

        result.effectiveApSet = effectiveAp;

        return result;
    }


    private static FingerprintPosition parseFingerprintPosition(String s) {
        String[] component = s.split(",");

        if (component.length < 2) {
            return null;
        }

        try {
            // BLE fingerprint position is real number, so just round it to integer

            return new FingerprintPosition(
                    (int)Math.round(Double.parseDouble(component[0])),
                    (int)Math.round(Double.parseDouble(component[1]))
            );

        } catch (Exception e) {
            return null;
        }

    }


    private static FingerprintEntry parseFingerprintEntry(String s) {
        FingerprintEntry result = new FingerprintEntry();

        String[] component = s.replace(":", ",").split(",");

        if (component.length < 2) {
            return null;
        }

        result.address = component[0].toUpperCase().trim();

        try {
            result.value.dbm = Double.parseDouble(component[1]);
        } catch (Exception e) {
            return null;
        }

        result.value.watt = LocalizationUtil.dbmToWatt(result.value.dbm);

        return result;
    }


    private static HashSet<String> filterIneffectiveAP(
            HashMap<FingerprintPosition, ArrayList<EMFingerprint> > emMap,
            double dbmRangeFilterThreshold,
            boolean filterVirtualMac
    ) {
        HashMap<String, Double> maxDbmByAp = new HashMap<>();
        HashMap<String, Double> minDbmByAp = new HashMap<>();


        for (ArrayList<EMFingerprint> fingerprint : emMap.values()) {
            for (EMFingerprint fingerprintByDirection : fingerprint) {
                for (Map.Entry<String, EMFingerprintValue> entry : fingerprintByDirection.valueByAddress.entrySet()) {
                    String address = entry.getKey();
                    double dbm = entry.getValue().dbm;

                    Double maxDbmObj = maxDbmByAp.get(address);
                    Double minDbmObj = minDbmByAp.get(address);

                    if (maxDbmObj == null) {
                        maxDbmByAp.put(address, dbm);
                    } else if (dbm > maxDbmObj.doubleValue()) {
                        maxDbmByAp.put(address, dbm);
                    }

                    if (minDbmObj == null) {
                        minDbmByAp.put(address, dbm);
                    } else if (dbm < minDbmObj.doubleValue()) {
                        minDbmByAp.put(address, dbm);
                    }

                }

            }

        }

        HashSet<String> effectiveAp = new HashSet<>();

        for (String mac : maxDbmByAp.keySet()) {
            if (filterVirtualMac && LocalizationUtil.isVirtualMac(mac)) {
                continue;
            }

            if (maxDbmByAp.get(mac).doubleValue() - minDbmByAp.get(mac).doubleValue() < dbmRangeFilterThreshold) {
                continue;
            }

            effectiveAp.add(mac);
        }

        for (ArrayList<EMFingerprint> fingerprint : emMap.values()) {
            for (EMFingerprint fingerprintByDirection : fingerprint) {
                Iterator<String> iter = fingerprintByDirection.valueByAddress.keySet().iterator();

                while (iter.hasNext()) {
                    String address = iter.next();

                    if (!effectiveAp.contains(address)) {
                        iter.remove();
                    }

                }

            }

        }

        return effectiveAp;
    }


    public EMFingerprint extractEmFingerprint(HashMap<String, Double> inputSignalVector) {
        EMFingerprint result = new EMFingerprint();

        for (Map.Entry<String, Double> entry : inputSignalVector.entrySet()) {
            // Wifi MAC address usually have ":" separator. Remove them
            String formattedKey = entry.getKey().toUpperCase().replace(":", "");

            // Only consider AP in effectiveApSet list
            if (!effectiveApSet.contains(formattedKey)) {
                continue;
            }

            EMFingerprintValue value = new EMFingerprintValue();

            value.dbm = entry.getValue();
            value.watt = LocalizationUtil.dbmToWatt(value.dbm);

            result.valueByAddress.put(formattedKey, value);
        }

        return result;
    }

}
