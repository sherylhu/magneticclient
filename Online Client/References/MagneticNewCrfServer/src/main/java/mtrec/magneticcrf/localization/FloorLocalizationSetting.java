package mtrec.magneticcrf.localization;



import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;



public class FloorLocalizationSetting {
    public static final int WIFI_NEAREST_NEIGHBOR_THRESHOLD = 4;
    public static final int BLE_NEAREST_NEIGHBOR_THRESHOLD = 4;

    private static final double WIFI_DBM_THRESHOLD = 40;
    private static final double BLE_DBM_THRESHOLD = 0;



    @SerializedName("magpotential_wifi")
    public boolean enableWifiPotential = false;

    @SerializedName("magpotential_ble")
    public boolean enableBlePotential = false;

    @SerializedName("wifi_dbmrange_filtering_threshold")
    public double wifiDbmRangeThreshold = WIFI_DBM_THRESHOLD;

    @SerializedName("ble_dbmrange_filtering_threshold")
    public double bleDbmRangeThreshold = BLE_DBM_THRESHOLD;



    public static FloorLocalizationSetting loadFromFile(String filePath) throws IOException, IllegalFormatException {
        FloorLocalizationSetting result;

        Gson gson = new Gson();

        try {
            FileReader fileReader = new FileReader(filePath);
            result = gson.fromJson(fileReader, FloorLocalizationSetting.class);

        } catch (FileNotFoundException e) {
            throw new IOException("Access to localization setting file is denied or this file does not exist");

        } catch (JsonIOException e) {
            throw new IOException("Access to localization setting file is denied or this file does not exist");

        } catch (JsonSyntaxException e) {
            throw new IllegalFormatException("Syntax of localization setting file is invalid");
        }

        return result;
    }

}
