package mtrec.magneticcrf.localization;


import mtrec.magneticcrf.server.LocalizationInput;
import mtrec.magneticcrf.server.LocalizationOutput;

import java.util.logging.Level;
import java.util.logging.Logger;



public class LocalizationCoreEngine {
    private static Logger logger = Logger.getLogger(LocalizationCoreEngine.class.getName());

    private String lastBuildingId = "";
    private String lastFloorId = "";

    private MagneticLocalizationEngine lastMagLocEngine = null;



    public LocalizationOutput estimatePosition(LocalizationInput input) {
        String newBuildingId = input.buildingId;
        String newFloorId = "";

        FloorplanDataAggregator newFloorData;
        MagneticLocalizationEngine newMagLocengine;

        // Floor detection
        if (!input.floorId.equals("")) {
            newFloorId = input.floorId;
        } else {
            // SVM-based floor detection, will be implemented later
        }

        // Cannot detect floor
        if (newFloorId.equals("")) {
            logger.log(
                    Level.SEVERE,
                    String.format(
                            "%s: Cannot detect floor of building %s",
                            input.userId, newBuildingId
                    )

            );

            return new LocalizationOutput();
        }

        // Load floorplan data
        try {
            newFloorData = FloorplanManagement.getInstance().getFloorData(newBuildingId, newFloorId);
        } catch (Exception e) {
            logger.log(
                    Level.SEVERE,
                    String.format(
                            "%s: Error accessing data of building %s, floor %s",
                            input.userId, newBuildingId, newFloorId
                    )

            );

            return new LocalizationOutput();
        }

        // Trigger magnetic localization engine
        if (!lastBuildingId.equals(newBuildingId) || !lastFloorId.equals(newFloorId)) {
            newMagLocengine = new MagneticLocalizationEngine(newFloorData);
        } else {
            newMagLocengine = lastMagLocEngine;
        }

        // Run Magnetic CRF
        LocalizationOutput result = newMagLocengine.estimatePosition(input);

        // If the result of Magnetic CRF is None, the DO NOT assign the floorId and leave it as None
        // (at this step, result.floorID = "", so None means result.x == 0 && result.y == 0)
        if (!result.isNone()) {
            result.floorId = newFloorId;
        }

        // Update state-related variables
        lastBuildingId = newBuildingId;
        lastFloorId = newFloorId;

        lastMagLocEngine = newMagLocengine;

        return result;
    }

}
