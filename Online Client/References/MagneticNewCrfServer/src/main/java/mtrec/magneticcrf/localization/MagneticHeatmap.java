package mtrec.magneticcrf.localization;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class MagneticHeatmap {
    public int pixelDistanceBetweenRefPoint = 0;
    public HashMap<FingerprintPosition, MagneticFingerprint> heatmap = new HashMap<>();



    public static MagneticHeatmap loadFromFile(String filePath)
            throws IOException, IllegalFormatException {
        MagneticHeatmap result = new MagneticHeatmap();

        BufferedReader heatmapFile;
        String line;

        try {
            heatmapFile = new BufferedReader(new FileReader(filePath));
        } catch (Exception e) {
            throw new IOException("Error reading the heatmap file");
        }

        try {
            line = heatmapFile.readLine();

            if (line == null) {
                throw new IllegalFormatException("Cannot read pixel distance between reference point from magnetic " +
                        "heatmap file");
            }

            result.pixelDistanceBetweenRefPoint = Integer.parseInt(line);
        } catch (IOException e) {
            throw new IOException("Error reading the magnetic heatmap file");

        } catch (NumberFormatException e) {
            throw new IllegalFormatException("Error parsing pixel distance between reference point in " +
                    "magnetic heatmap file (this number should be an integer)");
        }

        int lineNo = 0;

        int x,y;

        try {

            while (true) {
                line = heatmapFile.readLine();
                if (line == null) {
                    break;
                }

                ++lineNo;

                line = line.trim();
                if (line.equals("")) {
                    continue;
                }

                Scanner sc = new Scanner(new StringReader(line));

                x = sc.nextInt();
                y = sc.nextInt();

                MagneticFingerprint magFingerprint = new MagneticFingerprint();

                magFingerprint.intensity = sc.nextDouble();
                magFingerprint.vertical = sc.nextDouble();
                magFingerprint.horizontal = sc.nextDouble();

                result.heatmap.put(new FingerprintPosition(x, y), magFingerprint);
            }

        } catch (IOException e) {
            throw new IOException("Error reading the magnetic heatmap file");

        } catch (InputMismatchException e) {
            throw new IllegalFormatException("Error parsing the magnetic heatmap file at line "
                    + String.valueOf(lineNo));

        } catch (NoSuchElementException e) {
            throw new IllegalFormatException("Error parsing the magnetic heatmap file at line "
                    + String.valueOf(lineNo));

        }

        return result;
    }

}
