package mtrec.magneticcrf.localization;


import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class FloorplanBasicInformation {
    @SerializedName("scale")
    public double mapScale;

    @SerializedName("deviation_from_north")
    public double deviationFromNorth;



    public static FloorplanBasicInformation loadFromFile(String filePath) throws IOException, IllegalFormatException {
        FloorplanBasicInformation result;

        Gson gson = new Gson();

        try {
            FileReader fileReader = new FileReader(filePath);
            result = gson.fromJson(fileReader, FloorplanBasicInformation.class);

        } catch (FileNotFoundException e) {
            throw new IOException("Access to floorplan information file is denied or this file does not exist");

        } catch (JsonIOException e) {
            throw new IOException("Access to floorplan information file is denied or this file does not exist");

        } catch (JsonSyntaxException e) {
            throw new IllegalFormatException("Syntax of floorplan information file is invalid");
        }

        return result;
    }

}
