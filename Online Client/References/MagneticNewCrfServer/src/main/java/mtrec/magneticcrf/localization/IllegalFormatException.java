package mtrec.magneticcrf.localization;



public class IllegalFormatException extends Exception {
    public IllegalFormatException(String message) {
        super(message);
    }

}
