package mtrec.magneticcrf.testing;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import mtrec.magneticcrf.localization.FloorplanDataAggregator;
import mtrec.magneticcrf.localization.FloorplanManagement;
import mtrec.magneticcrf.localization.LocalizationCoreEngine;
import mtrec.magneticcrf.server.LocalizationInput;
import mtrec.magneticcrf.server.LocalizationOutput;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class TestRunner {
    private static class GroundtruthData {
        public String floorId = "";

        public int[] pathX;
        public int[] pathY;
        public int pathPointCount = 0;



        public static GroundtruthData loadFromFile(File file) throws Exception {
            GroundtruthData result = new GroundtruthData();

            BufferedReader reader = null;

            int errorCode = -1;

            try {
                errorCode = 0;

                reader = new BufferedReader(new FileReader(file));

                errorCode = 1;

                String [] pathCoors = reader.readLine().trim().split(" ");

                result.pathX = new int[pathCoors.length / 2];
                result.pathY = new int[pathCoors.length / 2];

                for (int i = 0; i < pathCoors.length; i += 2) {
                    result.pathX[i / 2] = Integer.valueOf(pathCoors[i]);
                    result.pathY[i / 2] = Integer.valueOf(pathCoors[i + 1]);
                }

                result.pathPointCount = result.pathX.length;

                errorCode = 2;

                result.floorId = reader.readLine().trim();

                if (result.equals("")) {
                    throw new Exception();
                }

            } catch (Exception e) {
                String exceptionMessage = "";

                switch (errorCode) {
                    case 0:
                        exceptionMessage = "Error opening groundtruth file";
                        break;

                    case 1:
                        exceptionMessage = "Error reading coordinates of the groundtruth path (not exist, or invalid format)";
                        break;

                    case 2:
                        exceptionMessage = "Error reading groundtruth floor (not exist)";
                        break;

                    default:
                        exceptionMessage = "Error reading groundtruth file";
                        break;
                }

                throw new Exception(exceptionMessage);
            }

            return  result;
        }

    }



    private static final int POINT_RADIUS = 6;
    private static final int LINE_WIDTH = 2;



    public static void run(
            File testDataFile,
            File testGroundTruthFile,
            String overwrittenBuildingId, String overwrittenFloorId, float overwrittenOrientation,
            String outputDirectoryPath
    ) throws Exception {
        // Read ground truth testing path
        GroundtruthData groundTruth = null;

        try {
            groundTruth = GroundtruthData.loadFromFile(testGroundTruthFile);
        } catch (Exception e) {
            throw new Exception("Error loading test groundtruth file. Details: " + e.getMessage());
        }

        // Prepare localization input data
        String testFloorId = groundTruth.floorId;

        if (overwrittenFloorId == null) {
            overwrittenFloorId = testFloorId;
        } else {
            testFloorId = overwrittenFloorId;
        }

        LocalizationInput[] inputs = readTestFile(
                testDataFile,
                overwrittenBuildingId, overwrittenFloorId, overwrittenOrientation
        );

        String testBuildingId = overwrittenBuildingId;
        if (testBuildingId == null) {
            testBuildingId = inputs[0].buildingId;
        }

        // Initialize result image
        FloorplanDataAggregator floorData =
                FloorplanManagement.getInstance().getFloorData(testBuildingId, testFloorId);

        Image mapImage;

        try {
            mapImage = ImageIO.read(new File(floorData.getMapFilePath()));
        } catch (Exception e) {
            throw new Exception("Error while retrieving floorplan image");
        }

        BufferedImage outputImage = new BufferedImage(
                mapImage.getWidth(null),
                mapImage.getHeight(null),
                BufferedImage.TYPE_INT_ARGB
        );

        Graphics2D graphicOutput = outputImage.createGraphics();

        graphicOutput.drawImage(mapImage, 0, 0, null);

        // Draw the groundtruth testing path
        graphicOutput.setStroke(new BasicStroke(LINE_WIDTH));
        graphicOutput.setPaint(Color.BLUE);
        graphicOutput.drawPolyline(groundTruth.pathX, groundTruth.pathY, groundTruth.pathPointCount);

        // Draw the beginning point of the testing path
        graphicOutput.setStroke(new BasicStroke(0));
        graphicOutput.setPaint(Color.ORANGE);
        graphicOutput.fillOval(
                groundTruth.pathX[0] - POINT_RADIUS / 2,
                groundTruth.pathY[0] - POINT_RADIUS / 2,
                POINT_RADIUS, POINT_RADIUS
        );

        // Initialize result detail text file
        BufferedWriter textResultWriter = null;

        try {
            textResultWriter = new BufferedWriter(new FileWriter(
                    Paths.get(
                            outputDirectoryPath,
                            String.format("output_%s_%s_%s.csv", testBuildingId, testFloorId, testDataFile.getName())
                    ).toFile()
            ));

            textResultWriter.write("Step,X,Y,Floor ID, Processing time (ms)");
            textResultWriter.newLine();

        } catch (Exception e) {
            throw new Exception("Error initializing result CSV file");
        }

        LocalizationCoreEngine engine = new LocalizationCoreEngine();

        LocalizationOutput prevOutput = null;

        for (int i = 0; i < inputs.length; ++i) {
            long timeDiff = System.currentTimeMillis();

            LocalizationOutput curOutput = engine.estimatePosition(inputs[i]);

            timeDiff = System.currentTimeMillis() - timeDiff;

            // Write localization result to CSV file
            try {
                textResultWriter.write(
                        String.format(
                                "%d,%d,%d,%s,%d",
                                inputs[i].step,
                                curOutput.x, curOutput.y, curOutput.floorId,
                                timeDiff
                        )

                );
                textResultWriter.newLine();

            } catch (IOException e) {
                throw new Exception("Error writing result to CSV file");

            }

            if (curOutput.isNone()) {
                continue;
            }

            graphicOutput.setStroke(new BasicStroke(0));

            if (prevOutput == null) {
                graphicOutput.setPaint(Color.ORANGE);
            } else {
                graphicOutput.setPaint(Color.RED);
            }

            // Only draw a position point if this is the first un-NONE position or this position is different
            // from the previous one
            if (
                prevOutput == null
                || (prevOutput != null && (curOutput.x != prevOutput.x || curOutput.y != prevOutput.y))
            ) {
                graphicOutput.fillOval(
                        curOutput.x - POINT_RADIUS / 2,
                        curOutput.y - POINT_RADIUS / 2,
                        POINT_RADIUS,
                        POINT_RADIUS
                );

            }

            // Draw the line connecting previous localzation result with the current one
            if (prevOutput != null && (curOutput.x != prevOutput.x || curOutput.y != prevOutput.y)) {
                graphicOutput.setStroke(new BasicStroke(LINE_WIDTH));
                graphicOutput.setPaint(new Color(0, 127, 14));
                graphicOutput.drawLine(prevOutput.x, prevOutput.y, curOutput.x, curOutput.y);
            }

            prevOutput = curOutput;
        }

        try {
            String outputImageFilename = String.format(
                    "output_%s_%s_%s.png",
                    testBuildingId, testFloorId, testDataFile.getName()
            );

            ImageIO.write(outputImage, "png", Paths.get(outputDirectoryPath, outputImageFilename).toFile());
        } catch (Exception e) {
            throw new Exception("Error saving output image file at " + outputDirectoryPath);

        }

        try {
            textResultWriter.close();
        } catch (Exception e) {

        }

    }


    private static LocalizationInput[] readTestFile(
            File testFile,
            String overwrittenBuildingId, String overwrittenFloorId, float overwrittenOrientation
    ) throws Exception {

        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(testFile));
        } catch (Exception e) {
            throw new Exception("Cannot access the testing data file at " + testFile.getAbsolutePath());
        }

        ArrayList<LocalizationInput> inputs = new ArrayList<>();

        Gson gson = new Gson();
        int lineNo = -1;

        try {
            while (true) {
                ++lineNo;
                String line = reader.readLine();
                if (line == null) {
                    break;
                }

                line = line.trim();
                if (line.equals("")) {
                    continue;
                }

                LocalizationInput newInput = gson.fromJson(line, LocalizationInput.class);

                if (overwrittenBuildingId != null) {
                    newInput.buildingId = overwrittenBuildingId;
                }

                if (overwrittenFloorId != null) {
                    newInput.floorId = overwrittenFloorId;
                }

                if (!Float.isNaN(overwrittenOrientation)) {
                    newInput.orientation = overwrittenOrientation;
                }

                if (!newInput.valid()) {
                    throw new Exception(
                            String.format(
                                    "Invalid input at line %d of testing data file at %s",
                                    lineNo, testFile.getAbsolutePath()
                            )
                    );
                }

                inputs.add(newInput);
            }

            reader.close();
        } catch (IOException e) {
            throw new Exception("Error occured while reading the testing data file at " + testFile.getAbsolutePath());

        } catch (JsonSyntaxException e) {
            throw new Exception(
                    String.format(
                            "JSON syntax error at line %d of testing data file at %s",
                            lineNo, testFile.getAbsolutePath()
                    )
            );

        }

        if (inputs.size() == 0) {
            throw new Exception("No data inside testing data file at " + testFile.getAbsolutePath());
        }

        return inputs.toArray(new LocalizationInput[0]);
    }

}
