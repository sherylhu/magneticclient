package mtrec.magneticcrf.server;



import mtrec.magneticcrf.localization.LocalizationCoreEngine;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;



public class LocalizationTask implements Runnable {
    public enum TaskState {
        IDLE,           // Data unavailable
        FREE,           // Data available, not running
        RUNNING,        // Running
        SCHEDULED,      // Data available, has been scheduled
    }



    private static final Logger logger = Logger.getLogger(LocalizationTask.class.getName());

    private LinkedBlockingQueue<LocalizationInput> queueInput = new LinkedBlockingQueue<LocalizationInput>();

    private LocalizationOutput mostRecentOutput = new LocalizationOutput();
    private String mostRecentBuildingId = "";

    private TaskState state = TaskState.IDLE;
    private long lastActiveTimestampSecond = System.currentTimeMillis() / 1000;

    private LocalizationCoreEngine engine = new LocalizationCoreEngine();



    public boolean addInput(LocalizationInput input) {
        if (queueInput.offer(input)) {
            synchronized (state) {
                if (state == TaskState.IDLE) {
                    state = TaskState.FREE;
                }

                // Update last active timestamp
                lastActiveTimestampSecond = System.currentTimeMillis() / 1000;
            }

            return true;
        } else {
            return false;
        }

    }


    public LocalizationOutput getMostRecentOutput() {
        synchronized (mostRecentOutput) {
            return new LocalizationOutput(mostRecentOutput);
        }

    }


    public String getMostRecentBuildingId() {
        return mostRecentBuildingId;
    }


    public void scheduleOn() {
        synchronized (state) {
            state = TaskState.SCHEDULED;

            // Update last active timestamp
            lastActiveTimestampSecond = System.currentTimeMillis() / 1000;
        }

    }


    public TaskState getState() {
        return state;
    }


    public long getLastActiveTimestampInSecond() {
        return lastActiveTimestampSecond;
    }


    public void run() {
        try {
            synchronized (state) {
                state = TaskState.RUNNING;

                // Update last active timestamp
                lastActiveTimestampSecond = System.currentTimeMillis() / 1000;
            }

            LocalizationInput input = queueInput.poll();

            LocalizationOutput output = engine.estimatePosition(input);

            if (!output.isNone()) {
                synchronized (mostRecentOutput) {
                    mostRecentOutput = output;
                    mostRecentBuildingId = input.buildingId;
                }

            }

            synchronized (state) {
                if (queueInput.isEmpty()) {
                    state = TaskState.IDLE;
                } else {
                    state = TaskState.FREE;
                }

                // Update last active timestamp
                lastActiveTimestampSecond = System.currentTimeMillis() / 1000;
            }

        } catch (Exception e) {
            // Do this to avoid exception being swallow
            e.printStackTrace();
        }

    }

}
