package mtrec.magneticcrf.server;



import com.google.gson.Gson;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;



public class MagneticCrfServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private static final String GETPOS_API_PATH = "/api/getpos";
    private static final String GETUSERS_BY_PLACE_API_PATH = "/api/getuserbyplace";



    private static final Logger logger = Logger.getLogger(MagneticCrfServerHandler.class.getName());

    private MagneticCrfLocalizationManager localizationManager;
    private Gson gson = new Gson();

    private int i = 1;

    public MagneticCrfServerHandler(MagneticCrfLocalizationManager manager) {
        this.localizationManager = manager;
    }


    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {
        System.out.println("hello");
        QueryStringDecoder queryDecoder = new QueryStringDecoder(req.uri());

        if (req.method() == HttpMethod.POST && queryDecoder.path().equals(GETPOS_API_PATH)) {
            handleGetPositionRequest(ctx, req, null);
            return;

        }

        if (req.method() == HttpMethod.GET && queryDecoder.path().equals(GETUSERS_BY_PLACE_API_PATH)) {
            handleGetUserByPlaceRequest(ctx, req, queryDecoder.parameters());
            return;
        }

        sendError(ctx, HttpResponseStatus.BAD_REQUEST, "Invalid request");
    }


    private void handleGetPositionRequest(
            ChannelHandlerContext ctx,
            FullHttpRequest request,
            Map< String, List<String> > queryParameters
    ) {
        LocalizationInput localizeInput = null;

        String temp = URLDecoder.decode(request.content().toString(CharsetUtil.UTF_8)).substring(1);

        //System.out.println(temp1);
        //String temp =request.content().toString(CharsetUtil.UTF_8);

        System.out.println(temp);

        try {
            localizeInput = gson.fromJson(
                    temp,
                    LocalizationInput.class
            );
        } catch (Exception e) {
            sendError(ctx, HttpResponseStatus.BAD_REQUEST, "Invalid data");
            return;
        }
/*
        if (!localizeInput.valid()) {
            sendError(ctx, HttpResponseStatus.BAD_REQUEST, "Invalid data");
            return;
        }
*/
//        if (!localizationManager.addLocalizationInput(localizeInput)) {
//            sendError(ctx, HttpResponseStatus.INSUFFICIENT_STORAGE, "Server processing capacity full");
//            return;
//        }

//        LocalizationOutput localizeOutput = localizationManager.getLocalizationOutputByUserId(localizeInput.userId);
//
//        if (localizeOutput == null) {
//            sendError(ctx, HttpResponseStatus.BAD_REQUEST, "Unknown user id");
//            return;
//        }

        LocalizationOutput localizeOutput = new LocalizationOutput();
        localizeOutput.floorId = "2F";
        localizeOutput.x = 7000 + 100 * i;
        localizeOutput.y = 2000 + 100 * i;
        i++;

        // Return recent position of user
        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                HttpResponseStatus.ACCEPTED,
                Unpooled.copiedBuffer(gson.toJson(localizeOutput), CharsetUtil.UTF_8)
        );

        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json; charset=UTF-8");

        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }


    private void handleGetUserByPlaceRequest(
            ChannelHandlerContext ctx,
            FullHttpRequest request,
            Map< String, List<String> > queryParameters
    ) {
        if (!queryParameters.containsKey("building_id") || !queryParameters.containsKey("floor_id")) {
            sendError(ctx, HttpResponseStatus.BAD_REQUEST, "Unknown building ID or floor ID");
            return;
        }

        String buildingId = queryParameters.get("building_id").get(0);
        String floorId = queryParameters.get("floor_id").get(0);

        Map<String, LocalizationOutput> result = localizationManager.getLocalizationOutputByPlace(buildingId, floorId);

        // Return position of all users currently inside floorId of buildingId
        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                HttpResponseStatus.ACCEPTED,
                Unpooled.copiedBuffer(gson.toJson(result), CharsetUtil.UTF_8)
        );

        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json; charset=UTF-8");

        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }


    private void sendError(ChannelHandlerContext ctx, HttpResponseStatus status, String message) {
        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                status,
                Unpooled.copiedBuffer(message + "\r\n", CharsetUtil.UTF_8)
        );

        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain; charset=UTF-8");

        // Close the connection as soon as error message is sent
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

}
