package mtrec.magneticcrf.localization;



public class MagneticFingerprint {
    public double intensity;
    public double horizontal;
    public double vertical;


    @Override
    public String toString() {
        return String.format("(%.8f, %.8f, %.8f)", intensity, horizontal, vertical);
    }

}
