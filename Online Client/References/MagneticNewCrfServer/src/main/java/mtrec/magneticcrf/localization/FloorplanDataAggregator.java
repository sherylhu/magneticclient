package mtrec.magneticcrf.localization;



import com.sun.istack.internal.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FloorplanDataAggregator {
    private class DistanceWithPosition implements Comparable<DistanceWithPosition> {
        public FingerprintPosition position;
        public double distance;



        // This implementation support MAX-priority queue
        @Override
        public int compareTo(DistanceWithPosition o) {
            if (this.distance + 1E-6 > o.distance) {
                return -1;
            }

            if (this.distance - 1E-6 < o.distance) {
                return 1;
            }

            return 0;
        }

    }



    private static final String DATADIR_ENV_VAR = "MAGCRF_DATADIR";

    private static final Logger logger = Logger.getLogger(FloorplanDataAggregator.class.getName());



    private String buildingId = null;
    private String floorId = null;

    private String dataDirPath = null;

    private Path infoFilePath = null;
    private Path locSettingFilePath = null;

    private Path magHeatmapFilePath = null;

    private Path wifiMapFilePath = null;
    private Path bleMapFilePath = null;

    private FloorplanBasicInformation basicInfo = null;

    private FloorLocalizationSetting setting = null;

    private MagneticHeatmap magHeatmap = null;

    private EMSignalMap wifiSignalMap = null;

    // nearestWifiNeighborMap[p] = Neerest Wifi Reference Point of magnetic reference point p
    private HashMap<FingerprintPosition, FingerprintPosition[]> nearestWifiNeighborMap = null;

    private EMSignalMap bleSignalMap = null;

    // nearestBleNeighborMap[p] = Neerest BLE Reference Point of magnetic reference point p
    private HashMap<FingerprintPosition, FingerprintPosition[]> nearestBleNeighborMap = null;



    public FloorplanDataAggregator(@NotNull String buildingId, @NotNull String floorId)
            throws SecurityException, IOException, IllegalFormatException {
        this.buildingId = buildingId;
        this.floorId = floorId;

        try {
            dataDirPath = System.getenv(DATADIR_ENV_VAR);

        } catch (Exception e) {
            throw new SecurityException("Cannot access " + DATADIR_ENV_VAR + " environment variable");
        }

        if (dataDirPath == null) {
            throw new IOException("Environment variable " + DATADIR_ENV_VAR + " does not exist");
        }

        // Load info.json file (contain basic information about a floor)
        try {
            infoFilePath = Paths.get(dataDirPath, buildingId, "floorplaninfo", floorId, "info.json")
                    .toAbsolutePath();

        } catch (Exception e) {
            throw new IOException(
                    String.format("Building %s, floor %s: Error accessing info.json file (permission is denied " +
                    " or buiding ID, floor ID is invalid)", buildingId, floorId)
            );

        }

        basicInfo = FloorplanBasicInformation.loadFromFile(infoFilePath.toString());

        // Load locsetting.json file (contain some parameters needed for localization task)
        try {
            locSettingFilePath = Paths.get(dataDirPath, buildingId, "localization", floorId, "locsetting.json")
                    .toAbsolutePath();

            setting = FloorLocalizationSetting.loadFromFile(locSettingFilePath.toString());

        } catch (Exception e) {
            logger.log(
                    Level.SEVERE,
                    String.format(
                            "Building %s, floor %s: Error loading locsetting.json file, default setting will be used. "
                                + "Error details: %s",
                            buildingId, floorId,
                            e.getMessage()
                    )
            );

            setting = new FloorLocalizationSetting();
        }

        // Load magnetic heatmap file
        try {
            magHeatmapFilePath = Paths.get(dataDirPath, buildingId, "localization", floorId, "magheatmap.txt");

        } catch (Exception e) {
            throw new IOException(
                    String.format("Building %s, floor %s: Error accessing magheatmap.txt file (permission is denied " +
                            " or buiding ID, floor ID is invalid)", buildingId, floorId)
            );

        }

        magHeatmap = MagneticHeatmap.loadFromFile(magHeatmapFilePath.toString());
    }


    public double getMapScale() {
        return basicInfo.mapScale;
    }


    public double getDeviationFromNorth() {
        return basicInfo.deviationFromNorth;
    }


    public FloorLocalizationSetting getLocalizationSetting() {
        return setting;
    }


    public MagneticHeatmap getMagneticHeatmap() {
        return magHeatmap;
    }


    public EMSignalMap getWifiSignalMap()  throws Exception {
        loadWifiSignalMap();

        return wifiSignalMap;
    }


    public HashMap<FingerprintPosition, FingerprintPosition[]> getNearestWifiNeighborMap() throws Exception {
        loadWifiSignalMap();

        return nearestWifiNeighborMap;
    }


    public EMSignalMap getBleSignalMap() throws Exception {
        loadBleSignalMap();

        return bleSignalMap;
    }


    public HashMap<FingerprintPosition, FingerprintPosition[]> getNearestBleNeighborMap() throws Exception {
        loadBleSignalMap();

        return nearestBleNeighborMap;
    }


    public String getMapFilePath() throws IOException {
        File mapFile;

        try {
            mapFile = Paths.get(dataDirPath, buildingId, "floorplaninfo", floorId, "map.jpg").toFile();

        } catch (Exception e) {
            throw new IOException(String.format(
                    "Error accessing the file map.jpg of building %s, floor %s", buildingId, floorId
            ));

        }

        if (!mapFile.isFile()) {
            throw new IOException(String.format(
                    "File map.jpg of building %s, floor %s does not exist or its access is denied", buildingId, floorId
            ));

        }

        try {
            return mapFile.getAbsolutePath();

        } catch (Exception e) {
            throw new IOException(String.format(
                    "Error accessing the file map.jpg of building %s, floor %s", buildingId, floorId
            ));
        }

    }


    private HashMap<FingerprintPosition, FingerprintPosition[]> findNearestEMNeighborReferencePoint(
            EMSignalMap emMap,
            MagneticHeatmap magMap,
            int numberOfNearestNeighbor
    ) {
        HashMap<FingerprintPosition, FingerprintPosition[]> result = new HashMap<>();

        PriorityQueue<DistanceWithPosition> pq = new PriorityQueue<>();

        for (FingerprintPosition magRefPoint : magMap.heatmap.keySet()) {
            pq.clear();

            for (FingerprintPosition emRefPoint : emMap.map.keySet()) {
                DistanceWithPosition e = new DistanceWithPosition();
                e.position = emRefPoint;
                e.distance = Math.sqrt(
                        Math.pow(emRefPoint.x - magRefPoint.x, 2) + Math.pow(emRefPoint.y - magRefPoint.y, 2)
                );

                pq.add(e);

                if (pq.size() > numberOfNearestNeighbor) {
                    pq.poll();
                }

            }

            FingerprintPosition[] nearestNeighbors = new FingerprintPosition[pq.size()];

            while (pq.size() > 0) {
                nearestNeighbors[pq.size() - 1] = pq.poll().position;
            }

            result.put(magRefPoint, nearestNeighbors);
        }

        return result;
    }


    private void loadWifiSignalMap() throws Exception {
        if (wifiMapFilePath == null) {
            try {
                wifiMapFilePath = Paths.get(dataDirPath, buildingId, "localization", floorId, "wifi.fingerprint.txt");
            } catch (Exception e) {
                wifiMapFilePath = null;

                String exceptionMessage = String.format(
                        "Building %s, floor %s: Error accessing wifi.fingerprint.txt file"
                                + "(permission is denied or buiding ID, floor ID is invalid)",
                        buildingId, floorId
                );

                logger.log(Level.SEVERE, exceptionMessage);

                throw new Exception(exceptionMessage);
            }
        }

        if (wifiSignalMap == null) {
            try {
                wifiSignalMap = EMSignalMap.loadFromFile(
                        wifiMapFilePath.toString(),
                        setting.wifiDbmRangeThreshold,
                        true
                );

            } catch (Exception e) {
                wifiSignalMap = null;

                String exceptionMessage = String.format(
                        "Building %s, floor %s: Error loading wifi.fingerprint.txt file. Details: %s",
                        buildingId, floorId,
                        e.getMessage()
                );

                logger.log(Level.SEVERE, exceptionMessage);

                throw new Exception(exceptionMessage);
            }

        }

        if (nearestWifiNeighborMap == null) {
            nearestWifiNeighborMap = findNearestEMNeighborReferencePoint(
                    wifiSignalMap, magHeatmap,
                    FloorLocalizationSetting.WIFI_NEAREST_NEIGHBOR_THRESHOLD
            );

        }

    }


    private void loadBleSignalMap() throws Exception {
        if (bleMapFilePath == null) {
            try {
                bleMapFilePath = Paths.get(dataDirPath, buildingId, "localization", floorId, "ble.fingerprint.txt");
            } catch (Exception e) {
                bleMapFilePath = null;

                String exceptionMessage = String.format(
                        "Building %s, floor %s: Error accessing ble.fingerprint.txt file"
                                + "(permission is denied or buiding ID, floor ID is invalid)",
                        buildingId, floorId
                );

                logger.log(Level.SEVERE, exceptionMessage);

                throw new Exception(exceptionMessage);
            }
        }

        if (bleSignalMap == null) {
            try {
                bleSignalMap = EMSignalMap.loadFromFile(
                        bleMapFilePath.toString(),
                        setting.bleDbmRangeThreshold,
                        false
                );

            } catch (Exception e) {
                bleSignalMap = null;

                String exceptionMessage = String.format(
                        "Building %s, floor %s: Error loading ble.fingerprint.txt file. Details: %s",
                        buildingId, floorId,
                        e.getMessage()
                );

                logger.log(Level.SEVERE, exceptionMessage);

                throw new Exception(exceptionMessage);
            }

        }

        if (nearestBleNeighborMap == null) {
            nearestBleNeighborMap = findNearestEMNeighborReferencePoint(
                    bleSignalMap, magHeatmap,
                    FloorLocalizationSetting.BLE_NEAREST_NEIGHBOR_THRESHOLD
            );

        }

    }

}
