package mtrec.magneticcrf.localization;


import com.sun.istack.internal.NotNull;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FloorplanManagement {
    private class FloorKey {
        public final String buildingId, floorId;

        public final String hash;



        public FloorKey(@NotNull String buildingId, @NotNull String floorId) {
            this.buildingId = buildingId;
            this.floorId = floorId;

            this.hash = String.format("%s||%s", buildingId, floorId);
        }


        @Override
        public int hashCode() {
            return hash.hashCode();
        }


        @Override
        public boolean equals(Object o) {
            if (o instanceof FloorKey) {
                FloorKey oCast = (FloorKey)o;
                return buildingId.equals(oCast.buildingId) && floorId.equals(floorId);
            }

            return false;
        }

    }



    private static FloorplanManagement instance = new FloorplanManagement();



    private static final Logger logger = Logger.getLogger(FloorplanManagement.class.getName());

    private HashMap<FloorKey, FloorplanDataAggregator> floorData = new HashMap<FloorKey, FloorplanDataAggregator>();



    private FloorplanManagement() {

    }


    public static FloorplanManagement getInstance() {
        return instance;
    }


    public FloorplanDataAggregator getFloorData(@NotNull String buildingId, @NotNull String floorId) throws Exception {
        FloorKey key = new FloorKey(buildingId, floorId);

        synchronized (floorData) {
            if (!floorData.containsKey(key)) {
                try {
                    floorData.put(key, new FloorplanDataAggregator(buildingId, floorId));
                } catch (Exception e) {
                    logger.log(
                            Level.SEVERE,
                            String.format(
                                    "Building %s, floor %s: %s",
                                    buildingId, floorId, e.getMessage()
                            )
                    );

                    throw new Exception(
                            String.format(
                                    "Error initializing data adapter for building %s, floor %s. Details: %s",
                                    buildingId, floorId,
                                    e.getMessage()
                            )

                    );

                }

            }

        }

        return floorData.get(key);
    }

}
