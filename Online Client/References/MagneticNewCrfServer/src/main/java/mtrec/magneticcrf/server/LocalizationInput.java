package mtrec.magneticcrf.server;



import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;



public class LocalizationInput {
    @SerializedName("user_id")
    public String userId = "";

    @SerializedName("ts")
    public long timestamp = -1;

    @SerializedName("intensity")
    public float magneticIntensity = Float.NaN;

    @SerializedName("horizontal_comp")
    public float magneticHorizontal = Float.NaN;

    @SerializedName("vertical_comp")
    public float magneticVertical = Float.NaN;

    @SerializedName("orientation")
    public float orientation = Float.NaN;

    @SerializedName("step")
    public int step = -1;

    @SerializedName("wifi_vector")
    public HashMap<String, Double> wifiVector = new HashMap<>();
    @SerializedName("ble_vector")
    public HashMap<String, Double> bleVector = new HashMap<>();

    @SerializedName("building_id")
    public String buildingId = "";

    @SerializedName("floor_id")
    public String floorId = "";



    public boolean valid() {
        if (userId.equals("")) {
            return false;
        }

        if (timestamp == -1) {
            return false;
        }

        if (buildingId.equals("")) {
            return false;
        }

        if (step == -1) {
            return false;
        }

        if (Float.isNaN(magneticIntensity) || Float.isNaN(magneticHorizontal) || Float.isNaN(magneticVertical)
                || Float.isNaN(orientation)) {
            return false;
        }

        return true;
    }

}
