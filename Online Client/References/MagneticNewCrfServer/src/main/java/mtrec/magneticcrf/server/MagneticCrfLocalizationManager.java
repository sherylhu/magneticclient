package mtrec.magneticcrf.server;


import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;



public class MagneticCrfLocalizationManager extends Thread {
    private static final int THREADPOOL_SIZE = 2;

    private static final int MAX_ALLOWED_IDLE_TIME_SECOND = 30 * 60;    // 30 minutes
    private static final int CONSIDER_RESULT_WHEN_IDLE_SECOND = 5 * 60;    // 5 minutes



    private static final Logger logger = Logger.getLogger(MagneticCrfLocalizationManager.class.getName());

    private boolean wantToStop = false;

    private ConcurrentHashMap<String, LocalizationTask> userLocalizationTask =
            new ConcurrentHashMap<String, LocalizationTask>();

    private ExecutorService executor;



    public MagneticCrfLocalizationManager() {
        executor = Executors.newFixedThreadPool(THREADPOOL_SIZE);
    }


    public void run() {
        while (!wantToStop) {
            Set< Map.Entry<String, LocalizationTask> > userTaskSet = userLocalizationTask.entrySet();

            // Activate user with waiting input queue
            for (Map.Entry<String, LocalizationTask> entry : userTaskSet) {
                if (wantToStop) {
                    break;
                }

                LocalizationTask task = entry.getValue();

                if (task.getState() == LocalizationTask.TaskState.FREE) {
                    try {
                        executor.submit(task);
                        task.scheduleOn();

                    } catch (Exception e) {

                    }

                }

            }

            // Remove user which has not been active for quite a long time
            Iterator< Map.Entry<String, LocalizationTask> > iter = userTaskSet.iterator();

            while (iter.hasNext()) {
                Map.Entry<String, LocalizationTask> entry = iter.next();
                LocalizationTask task = entry.getValue();

                if (task.getState() == LocalizationTask.TaskState.IDLE &&
                        System.currentTimeMillis() / 1000 - task.getLastActiveTimestampInSecond() >= MAX_ALLOWED_IDLE_TIME_SECOND) {
                    iter.remove();

                    logger.log(Level.INFO, String.format("Removing engine of user %s", entry.getKey()));
                }

            }

        }

        executor.shutdown();
    }


    public synchronized boolean addLocalizationInput(LocalizationInput input) {
        LocalizationTask task;

        if (userLocalizationTask.containsKey(input.userId)) {
            task = userLocalizationTask.get(input.userId);
        } else {
            task = new LocalizationTask();
            userLocalizationTask.put(input.userId, task);
        }

        return task.addInput(input);
    }


    public LocalizationOutput getLocalizationOutputByUserId(String userId) {
        if (userLocalizationTask.containsKey(userId)) {
            return userLocalizationTask.get(userId).getMostRecentOutput();
        } else {
            return null;
        }

    }


    public HashMap<String, LocalizationOutput> getLocalizationOutputByPlace(String buildingId, String floorId) {
        HashMap<String, LocalizationOutput> result = new HashMap<>();

        for (Map.Entry<String, LocalizationTask> entry :  userLocalizationTask.entrySet()) {
            boolean canGetResult = false;

            LocalizationTask task = entry.getValue();

            if (task.getState() != LocalizationTask.TaskState.IDLE) {
                canGetResult = true;
            } else if (System.currentTimeMillis() / 1000 - task.getLastActiveTimestampInSecond() < CONSIDER_RESULT_WHEN_IDLE_SECOND) {
                canGetResult = true;
            }

            if (!canGetResult) {
                continue;
            }

            LocalizationOutput locOutput = task.getMostRecentOutput();

            if (!locOutput.isNone() &&
                    locOutput.floorId.equals(floorId) &&
                    task.getMostRecentBuildingId().equals(buildingId)) {
                result.put(entry.getKey(), locOutput);
            }

        }

        return result;
    }


    public void shutdownGracefully() {
        wantToStop = true;
    }

}
