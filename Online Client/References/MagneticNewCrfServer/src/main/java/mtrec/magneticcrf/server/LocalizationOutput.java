package mtrec.magneticcrf.server;



import com.google.gson.annotations.SerializedName;



public class LocalizationOutput {
    @SerializedName("x")
    public int x = 0;
    @SerializedName("y")
    public int y = 0;

    @SerializedName("floor_id")
    public String floorId = "";



    public LocalizationOutput() {

    }


    // Copy constructor
    public LocalizationOutput(LocalizationOutput output) {
        this.x = output.x;
        this.y = output.y;
        this.floorId = output.floorId;
    }


    public boolean isNone() {
        return (x == 0 && y == 0 && floorId.equals(""));
    }


    @Override
    public String toString() {
        if (isNone()) {
            return "none";
        } else {
            return String.format("(x=%d, y=%d, floor=%s)", x, y, floorId);
        }

    }

}
