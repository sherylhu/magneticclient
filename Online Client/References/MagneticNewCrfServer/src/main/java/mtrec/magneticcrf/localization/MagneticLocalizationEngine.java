package mtrec.magneticcrf.localization;


import mtrec.magneticcrf.server.LocalizationInput;
import mtrec.magneticcrf.server.LocalizationOutput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;


public class MagneticLocalizationEngine {
    private class MagneticObservation {
        public long timestamp;

        public int relativeStep;

        // Unit: meter
        public double stepLength;

        public double orientation;

        public MagneticFingerprint magFingerprint;
    }


    private class Tracking {
        public int prevRelativeStep = -1;
        public FingerprintPosition prevPosition = new FingerprintPosition();

        public double potentialWifiValue = 0;
        public double potentialBleValue = 0;

        public double potentialMagneticValue = 0;
    }



    private static final int STEP_TIME_RESET_THRESHOLD_IN_SECOND = 10;

     private static final double STEP_MODEL_PARAM_A = 0.56496839593958803000;
     private static final double STEP_MODEL_PARAM_B = 0.11408853567798248000;

    private static final double ORIENTATION_DIFFERENCE_THRESHOLD = 30;
    private static final double SIGMA_STEP_LENGTH = 0.1;
    private static final double SIGMA_ORIENTATION = 10;

    private static final int STEP_RANGE = 20;

    private static final boolean USE_DBM_FOR_COSINE_SIMILARITY = false;
    private static final double CUTOFF_COSINE_SIMILARITY_THRESHOLD = 0.2;



    private FloorplanDataAggregator floorData;

    private int startingStep = -1;

    // The previous step received from device, relative to startingStep
    // (min value = 0, which is the "first" step in series starting from startingStep)
    private int curRelativeStep;
    private long curRelativeStepTimestamp;
    private LocalizationOutput curLocalizationResult;

    private ArrayList<MagneticObservation> magObservationByStep = new ArrayList<>();

    private ArrayList< HashMap<FingerprintPosition, Double> > nodePotentialByStep = new ArrayList<>();
    private ArrayList< HashMap<FingerprintPosition, Tracking> > trackByStep = new ArrayList<>();



    public MagneticLocalizationEngine(FloorplanDataAggregator floorData) {
        this.floorData = floorData;
    }


    public LocalizationOutput estimatePosition(LocalizationInput input) {
        // If the step is repeated, then simple return the previous result
        if (startingStep != -1 && input.step - startingStep == curRelativeStep) {
            return new LocalizationOutput(curLocalizationResult);
        }

        // Reset the engine:
        //      When startingStep is not defined yet, OR
        //      When new step, relative to startingStep, is not curRelativeStep + 1, OR
        //      When the time difference between 2 consecutive step is too big
        boolean needReset = false;

        if (startingStep == -1) {
            needReset = true;
        } else if (input.step - startingStep != curRelativeStep + 1) {
            needReset = true;
        } else if (input.timestamp - curRelativeStepTimestamp >= STEP_TIME_RESET_THRESHOLD_IN_SECOND * 1000) {
            needReset = true;
        }

        if (needReset) {
            startingStep = input.step;

            magObservationByStep.clear();
            nodePotentialByStep.clear();
        }

        int newRelativeStep = input.step - startingStep;

        // Update lastRelative-related variables
        curRelativeStep = newRelativeStep;
        curRelativeStepTimestamp = input.timestamp;

        // Prepare new MagneticObservation element
        MagneticObservation newMagObservation = new MagneticObservation();

        newMagObservation.timestamp = input.timestamp;
        newMagObservation.relativeStep = newRelativeStep;

        newMagObservation.orientation = input.orientation;

        newMagObservation.magFingerprint = new MagneticFingerprint();
        newMagObservation.magFingerprint.intensity = input.magneticIntensity;
        newMagObservation.magFingerprint.horizontal = input.magneticHorizontal;
        newMagObservation.magFingerprint.vertical = input.magneticVertical;

        if (newRelativeStep < 2) {
            newMagObservation.stepLength = 0;
        } else {
            newMagObservation.stepLength = STEP_MODEL_PARAM_A +
                    1e-9 /
                    (magObservationByStep.get(newRelativeStep - 1).timestamp - magObservationByStep.get(newRelativeStep - 2).timestamp)
                    * STEP_MODEL_PARAM_B;
        }

        magObservationByStep.add(newMagObservation);

        HashMap<FingerprintPosition, Double> newNodePot = new HashMap<>();
        HashMap<FingerprintPosition, Tracking> newTrack = new HashMap<>();

        nodePotentialByStep.add(newNodePot);
        trackByStep.add(newTrack);

        if (newRelativeStep == 0) {
            // This is the first step, so initialize only
            for (FingerprintPosition p : floorData.getMagneticHeatmap().heatmap.keySet()) {
                newNodePot.put(p, 1.0);
                newTrack.put(p, new Tracking());
            }

            // This is the first step, so no position can be estimated
            curLocalizationResult = new LocalizationOutput();
            return new LocalizationOutput();
        }

        // Prepare Wifi signal vector observation, IF wifi potential function is ENABLED
        EMFingerprint wifiObservation = null;
        double wifiCutOff = Double.NaN;

        if (floorData.getLocalizationSetting().enableWifiPotential) {
            try {
                wifiObservation = floorData.getWifiSignalMap().extractEmFingerprint(input.wifiVector);

                wifiCutOff = getEMSignalCutOffCosineSimilarity(
                        floorData.getWifiSignalMap(),
                        wifiObservation
                );

            } catch (Exception e) {
                wifiObservation = null;
                wifiCutOff = Double.NaN;

            }

        }

        // Prepare BLE signal vector observation, IF BLE potential function is ENABLED
        EMFingerprint bleObservation = null;

        if (floorData.getLocalizationSetting().enableBlePotential) {
            try {
                bleObservation = floorData.getBleSignalMap().extractEmFingerprint(input.bleVector);
            } catch (Exception e) {
                bleObservation = null;
            }

        }

        // Compute potential of all nodes
        double sumPotential = 0;

        for (FingerprintPosition p : floorData.getMagneticHeatmap().heatmap.keySet()) {
            sumPotential += computePotential(
                    p, newMagObservation,
                    wifiObservation, wifiCutOff,
                    bleObservation,
                    newNodePot, newTrack
            );

        }

        LocalizationOutput newResult = new LocalizationOutput();
        double resultMaxPotential = -1;

        // Normalize all potential
        // Pick the highest one, regard it as the result
        for (Map.Entry<FingerprintPosition, Double> entry : newNodePot.entrySet()) {
            double normalizedPotential = entry.getValue() / sumPotential;

            entry.setValue(normalizedPotential);

            if (normalizedPotential > resultMaxPotential) {
                newResult.x = entry.getKey().x;
                newResult.y = entry.getKey().y;
                resultMaxPotential = normalizedPotential;
            }

        }

        curLocalizationResult = newResult;

        return new LocalizationOutput(newResult);
    }


    private double computePotential(
            FingerprintPosition newPosition,
            MagneticObservation newMagObservation,

            EMFingerprint wifiObservation,
            double wifiCutOff,

            EMFingerprint bleObservation,

            HashMap<FingerprintPosition, Double> newNodePotential,
            HashMap<FingerprintPosition, Tracking> newTrack
    ) {
        // Compute potential function of Wifi signal, if ENABLED and AVAILABLE
        double potentialWifi = 1;

        if (floorData.getLocalizationSetting().enableWifiPotential && wifiObservation != null) {
            try {
                EMSignalMap wifiSignalMap = floorData.getWifiSignalMap();

                HashMap<FingerprintPosition, FingerprintPosition[]> wifiNeighborMap
                        = floorData.getNearestWifiNeighborMap();

                if (wifiObservation.valueByAddress.size() != 0) {
                    potentialWifi = computeFeatureEM(
                            wifiSignalMap, wifiNeighborMap, newPosition, wifiObservation, wifiCutOff
                    );

                }

            } catch (Exception e) {
                potentialWifi = 1;
            }
        }

        // Compute potential function of BLE signal, if ENABLED and AVAILABLE
        double potentialBle = 1;

        if (floorData.getLocalizationSetting().enableBlePotential && bleObservation != null) {
            try {
                EMSignalMap bleSignalMap = floorData.getBleSignalMap();
                HashMap<FingerprintPosition, FingerprintPosition[]> bleNeighborMap
                        = floorData.getNearestBleNeighborMap();

                if (bleObservation.valueByAddress.size() != 0) {
                    potentialBle = computeFeatureEM(
                            bleSignalMap, bleNeighborMap, newPosition, bleObservation, Double.NaN
                    );

                }

            } catch (Exception e) {
                potentialBle = 1;
            }

        }

        // Start computing other potential functions
        int pixelDistanceBetweenRp = floorData.getMagneticHeatmap().pixelDistanceBetweenRefPoint;

        int rangePos =
                (int)Math.floor(
                        newMagObservation.stepLength * floorData.getMapScale()
                                / floorData.getMagneticHeatmap().pixelDistanceBetweenRefPoint + 1
                ) *  pixelDistanceBetweenRp;

        double localMaxPotential = 0;
        Tracking trackForMax = new Tracking();

        for (int x = newPosition.x - rangePos; x <= newPosition.x + rangePos; x += pixelDistanceBetweenRp) {
            for (int y = newPosition.y - rangePos; y <= newPosition.y + rangePos; y += pixelDistanceBetweenRp) {

                if (x == newPosition.x && y == newPosition.y) {
                    continue;
                }

                FingerprintPosition prevPosition = new FingerprintPosition(x, y);

                if (!floorData.getMagneticHeatmap().heatmap.containsKey(prevPosition)) {
                    continue;
                }

                int newRelativeStep = newMagObservation.relativeStep;
                double localPotential = 0;

                double orientationDiff = computeOrientationDifference(
                        newMagObservation.orientation,
                        computeOrientation(x, y, newPosition.x, newPosition.y)
                );

                if (orientationDiff > ORIENTATION_DIFFERENCE_THRESHOLD) {
                    continue;
                }

                for (int lookback = 1; lookback <= Math.min(2, newMagObservation.relativeStep); ++lookback) {
                    if (!nodePotentialByStep.get(newRelativeStep - lookback).containsKey(prevPosition)) {
                        continue;
                    }

                    localPotential = nodePotentialByStep.get(newRelativeStep - lookback).get(prevPosition)
                            * potentialWifi
                            * potentialBle;

                    if (localPotential < localMaxPotential) {
                        continue;
                    }

                    // Compute potential function of orientation
                    double potentialOrientation = computeFeatureOrientation(orientationDiff);
                    localPotential *= potentialOrientation;

                    if (localPotential < localMaxPotential) {
                        continue;
                    }

                    // Compute potential function of step length
                    double groundtruthStepLength =
                            Math.sqrt(
                                Math.pow(newPosition.x - x, 2) + Math.pow(newPosition.y - y, 2)
                            ) / floorData.getMapScale();

                    double observedStepLength = 0;
                    for (int i = newRelativeStep - lookback + 1; i <= newRelativeStep; ++i) {
                        observedStepLength += magObservationByStep.get(i).stepLength;
                    }

                    double potentialStepLength = computeFeatureStepLength(observedStepLength, groundtruthStepLength);
                    localPotential *= potentialStepLength;

                    if (localPotential < localMaxPotential) {
                        continue;
                    }

                    // Compute potential function of magnetic fingerprint
                    ArrayList<MagneticFingerprint> observedMagSeq = new ArrayList<>();
                    ArrayList<MagneticFingerprint> groundtruthMagSeq = new ArrayList<>();

                    observedMagSeq.add(newMagObservation.magFingerprint);
                    groundtruthMagSeq.add(floorData.getMagneticHeatmap().heatmap.get(newPosition));

                    int trackRelativeStep = newRelativeStep - lookback;
                    FingerprintPosition trackPosition = prevPosition;

                    while (true) {
                        if (trackRelativeStep == -1) {
                            break;
                        }

                        if (trackRelativeStep < newRelativeStep - lookback - STEP_RANGE) {
                            break;
                        }

                        observedMagSeq.add(magObservationByStep.get(trackRelativeStep).magFingerprint);
                        groundtruthMagSeq.add(floorData.getMagneticHeatmap().heatmap.get(trackPosition));

                        Tracking track = trackByStep.get(trackRelativeStep).get(trackPosition);

                        trackRelativeStep = track.prevRelativeStep;
                        trackPosition = track.prevPosition;
                    }

                    double potentialMagnetic = computeFeatureMagnetic(observedMagSeq, groundtruthMagSeq);

                    localPotential *= potentialMagnetic;

                    // Save the maximum potential achieved so far
                    if (localPotential > localMaxPotential) {
                        localMaxPotential = localPotential;

                        trackForMax.prevRelativeStep = newRelativeStep - lookback;
                        trackForMax.prevPosition = prevPosition;

                        trackForMax.potentialWifiValue = potentialWifi;
                        trackForMax.potentialBleValue = potentialBle;

                        trackForMax.potentialMagneticValue = potentialMagnetic;

                    }

                }

            }

        }

        newNodePotential.put(newPosition, localMaxPotential);
        newTrack.put(newPosition, trackForMax);

        return localMaxPotential;
    }


    // Normalize angle to be in range (-180, 180] degree
    private double normalizeAngle(double degree) {
        // Normalize angle to be in range (-180, 180] degree
        while (degree <= -180) {
            degree += 360;
        }

        while (degree > 180) {
            degree -= 360;
        }

        return degree;
    }


    private double computeOrientation(int fromX, int fromY, int toX, int toY) {
        double diffX = toX - fromX;
        double diffY = toY - fromY;

        double degree = Math.acos(diffX * 1 / Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2))) / Math.PI * 180;
        if (diffY > 0) {
            degree = - degree;
        }

        return normalizeAngle(degree + floorData.getDeviationFromNorth());
    }


    private double computeOrientationDifference(double a, double b) {
        double degree = Math.abs(normalizeAngle(a) - normalizeAngle(b));
        if (degree > 180) {
            degree = 360 - degree;
        }

        return degree;
    }


    private double computeFeatureOrientation(double orientationDifference) {
        return Math.exp(- Math.pow(orientationDifference, 2) / Math.pow(SIGMA_ORIENTATION, 2) / 2);
    }


    private double computeFeatureStepLength(double observation, double groundtruth) {
        return Math.exp(-Math.pow(observation - groundtruth, 2) / Math.pow(SIGMA_STEP_LENGTH, 2) / 2);

    }


    // Set cutOffvalue = Double.NaN to avoid cut-off behavior
    private double computeFeatureEM(
            EMSignalMap emSignalMap,
            HashMap<FingerprintPosition, FingerprintPosition[]> neighborMap,
            FingerprintPosition position,

            EMFingerprint observation,

            double cutOffValue
    ) {
        FingerprintPosition[] emNeighbors = neighborMap.get(position);

        double interpolatedSimilarity = 0;
        double sumWeight = 0;

        for (FingerprintPosition neighborPosition : emNeighbors) {
            double maxSimilarity = 0;

            for (EMFingerprint groundtruth : emSignalMap.map.get(neighborPosition)) {
                maxSimilarity = Math.max(
                        maxSimilarity,
                        LocalizationUtil.cosineSimilarity(groundtruth, observation, USE_DBM_FOR_COSINE_SIMILARITY)
                );

            }

            double weight = 1 / (
                    Math.pow(neighborPosition.x - position.x, 2) +
                    Math.pow(neighborPosition.y - position.y, 2)
            );

            sumWeight += weight;

            interpolatedSimilarity += weight * maxSimilarity;
        }

        interpolatedSimilarity /= sumWeight;

        if (Double.isNaN(cutOffValue)) {
            return interpolatedSimilarity;
        } else {
            return (interpolatedSimilarity >= cutOffValue ? 1 : 0);
        }

    }


    private double getEMSignalCutOffCosineSimilarity(
            EMSignalMap emSignalMap,
            EMFingerprint observation
    ) {
        int minHeapSize = (int)Math.ceil(emSignalMap.map.size() * CUTOFF_COSINE_SIMILARITY_THRESHOLD);
        PriorityQueue<Double> heapSimilarity = new PriorityQueue<>();

        for (EMFingerprint[] fingerprintAllDirection : emSignalMap.map.values()) {
            double maxSimilarity = 0;

            for (EMFingerprint fingerprint : fingerprintAllDirection) {
                maxSimilarity = Math.max(
                    maxSimilarity,
                    LocalizationUtil.cosineSimilarity(fingerprint, observation, USE_DBM_FOR_COSINE_SIMILARITY)
                );

            }

            heapSimilarity.add(maxSimilarity);

            if (heapSimilarity.size() > minHeapSize) {
                heapSimilarity.poll();
            }

        }

        return heapSimilarity.poll();
    }


    private double computeFeatureMagnetic(
            ArrayList<MagneticFingerprint> observation,
            ArrayList<MagneticFingerprint> groundtruth
    ) {
        double distIntensity = 0,
               distHorizontal = 0,
               distVertical = 0;

        for (int i = 0; i < observation.size(); ++i) {
            MagneticFingerprint obsFingerprint = observation.get(i),
                                groundFingerprint = groundtruth.get(i);

            distIntensity += Math.pow(obsFingerprint.intensity - groundFingerprint.intensity, 2);
            distHorizontal += Math.pow(obsFingerprint.horizontal - groundFingerprint.horizontal, 2);
            distVertical += Math.pow(obsFingerprint.vertical - groundFingerprint.vertical, 2);
        }

        distIntensity = Math.sqrt(distIntensity);
        distHorizontal = Math.sqrt(distHorizontal);
        distVertical = Math.sqrt(distVertical);

        return 2 - 2 / (1 + Math.exp(-(distIntensity + distHorizontal + distVertical)/ (3 * observation.size())));
    }

}
