package mtrec.magneticcrf.localization;


import java.util.HashSet;
import java.util.Set;

public class LocalizationUtil {
    private static double DBM_FOR_NONEXIST = -1000;



    public static double dbmToWatt(double dbm) {
        return Math.pow(2, dbm / 10);
    }


    public static double cosineSimilarity(EMFingerprint a, EMFingerprint b, boolean useDbm) {
        // SPECIAL CASE: Empty fingerprint
        if (a.valueByAddress.size() == 0 || b.valueByAddress.size() == 0) {
            return 0;
        }

        HashSet<String> union = new HashSet<>();

        union.addAll(a.valueByAddress.keySet());

        union.addAll(b.valueByAddress.keySet());

        double normSquareA = 0, normSquareB = 0, dot = 0;

        for (String address : union) {
            EMFingerprintValue fpA = a.valueByAddress.get(address);
            EMFingerprintValue fpB = b.valueByAddress.get(address);

            double vA = (useDbm ? DBM_FOR_NONEXIST : 0);
            double vB = (useDbm ? DBM_FOR_NONEXIST : 0);

            if (fpA != null) {
                vA = (useDbm ? fpA.dbm : fpA.watt);
            }

            if (fpB != null) {
                vB = (useDbm ? fpB.dbm : fpB.watt);
            }

            dot += vA * vB;
            normSquareA += vA * vA;
            normSquareB += vB * vB;
        }

        return dot / Math.sqrt(normSquareA * normSquareB);
    }


    public static boolean isVirtualMac(String address) {
        address = address.replace(":", "").toUpperCase();

        int firstHex = Integer.valueOf(address.substring(0, 2), 16);

        return (firstHex & (0x02)) != 0;
    }

}
