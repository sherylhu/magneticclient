package mtrec.magneticcrf.testing;



import org.apache.commons.cli.*;

import java.io.File;
import java.nio.file.Paths;



public class TestingMainProgram {
    private static final String DATADIR_ENV_VAR = "MAGCRF_DATADIR";



    private static File testDataFile = null;
    private static File testGroundTruthFile = null;

    private static String outputDirectoryPath = null;

    private static String overwrittenBuildingId = null;
    private static String overwrittenFloorId = null;
    private static float overwrittenOrientation = Float.NaN;



    public static void main(String[] args) {
        parseCommandLine(args);

        try {
            TestRunner.run(
                    testDataFile,
                    testGroundTruthFile,
                    overwrittenBuildingId, overwrittenFloorId, overwrittenOrientation,
                    outputDirectoryPath
            );

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

    }


    private static void parseCommandLine(String args[]) {
        Options options = new Options();

        Option optionTestDirectory = new Option(
                "d",
                "test-dir",
                true,
                "Path to the directory that contains the testing data"
        );
        optionTestDirectory.setRequired(false);
        options.addOption(optionTestDirectory);

        Option optionTestId = new Option(
                "t",
                "test-id",
                true,
                "ID of the test (File name without extension .txt or .groundtruth.txt)"
        );
        optionTestId.setRequired(false);
        options.addOption(optionTestId);

        Option optionOutputDirectoryPath = new Option(
                "o",
                "output-dir",
                true,
                "Path to the directory where output will be stored"
        );
        optionTestId.setRequired(false);
        options.addOption(optionOutputDirectoryPath);

        Option optionBuildingId = new Option(
                "b",
                "building-id",
                true,
                "Overwrite the building ID in the testing data file"
        );
        optionBuildingId.setRequired(false);
        options.addOption(optionBuildingId);

        Option optionFloorId = new Option(
                "f",
                "floor-id",
                true,
                "Overwrite the floor ID in the testing data file"
        );
        optionFloorId.setRequired(false);
        options.addOption(optionFloorId);

        Option optionOrientation = new Option(
                "r",
                "orientation",
                true,
                "Overwrite the orientation in the testing data file"
        );
        optionOrientation.setRequired(false);
        options.addOption(optionOrientation);

        Option help = new Option("h", "help", false, "See help");
        help.setRequired(false);
        options.addOption(help);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (Exception e) {
            System.out.println("Command line Syntax error");
            System.out.println("");

            formatter.printHelp("testing", options);
            System.exit(1);
        }

        if (cmd.hasOption("help") || args.length == 0) {
            formatter.printHelp("testing", options);
            System.exit(0);
        }

        String testId = cmd.getOptionValue("test-id");

        if (testId == null) {
            System.out.println("Test ID is not provided");
            System.exit(1);
        }

        File testDirectory = null;

        try {
            testDirectory = Paths.get(cmd.getOptionValue("test-dir")).toFile();

            if (!testDirectory.isDirectory()) {
                throw new Exception();
            }

        } catch (Exception e) {
            System.out.println("Error accessing the directory containing test data");
            System.exit(1);
        }

        try {
            testDataFile = Paths.get(testDirectory.getPath(), testId + ".txt").toFile();

            if (!testDataFile.isFile()) {
                throw new Exception();
            }

        } catch (Exception e) {
            testDataFile = null;

            System.out.println("Error accessing the test data file (file not exist or access denied)");
            System.exit(1);
        }

        try {
            testGroundTruthFile = Paths.get(testDirectory.getPath(), testId + ".groundtruth.txt").toFile();

            if (!testGroundTruthFile.isFile()) {
                throw new Exception();
            }

        } catch (Exception e) {
            testGroundTruthFile = null;

            System.out.println("Error accessing the test ground truth file (file not exist or access denied)");
            System.exit(1);
        }

        outputDirectoryPath = cmd.getOptionValue("output-dir");

        if (outputDirectoryPath == null) {
            System.out.println("Output directory is not provided");
            System.exit(1);
        }

        overwrittenBuildingId = cmd.getOptionValue("building-id");
        overwrittenFloorId = cmd.getOptionValue("floor-id");

        String strOrientation = cmd.getOptionValue("orientation");
        if (strOrientation != null) {
            try {
                overwrittenOrientation = Float.valueOf(strOrientation);

                if (!Float.isFinite(overwrittenOrientation)) {
                    throw new Exception();
                }

                // Normalize orientation
                while (overwrittenOrientation <= -180) {
                    overwrittenOrientation += 360;
                }

                while (overwrittenOrientation > 180) {
                    overwrittenOrientation -= 360;
                }

            } catch (Exception e) {
                overwrittenOrientation = Float.NaN;

                System.out.println("Invalid orientation value");
                System.exit(1);
            }

        }

    }

}
