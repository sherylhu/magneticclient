package mtrec.magneticcrf.localization;



public class FingerprintPosition {
    public static final int HASHCODE_BASE = 100000;


    public final int x,y;


    public FingerprintPosition() {
        x = -1;
        y = -1;
    }


    public FingerprintPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public boolean isUndefined() {
        return x == -1 && y == -1;
    }


    @Override
    public int hashCode() {
        if (x == -1 && y == -1) {
            return -1;
        } else {
            return x * HASHCODE_BASE + y;
        }

    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof FingerprintPosition) {
            FingerprintPosition castO = (FingerprintPosition)o;
            return this.x == castO.x && this.y == castO.y;
        }

        return false;
    }


    @Override
    public String toString() {
        if (isUndefined()) {
            return "undefined";
        } else {
            return String.format("(x=%d, y=%d)", x, y);
        }

    }

}
