package mtrec.magneticcrf.server;



import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.cors.CorsConfig;
import io.netty.handler.codec.http.cors.CorsConfigBuilder;
import io.netty.handler.codec.http.cors.CorsHandler;

import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.logging.Logger;



public class MagneticCrfServerMainProgram {
    private static final int DEFAULT_PORT = 8000;
    private static final int MAX_CONTENT_LENGTH_IN_BYTE = 2 * 1024 * 1024;



    private static final Logger logger = Logger.getLogger(MagneticCrfServerMainProgram.class.getName());



    public static void main(String[] args) throws Exception {
        int port = DEFAULT_PORT;

        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (Exception e) {
                port = DEFAULT_PORT;
            }

        }

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        final MagneticCrfLocalizationManager localizationManager = new MagneticCrfLocalizationManager();
        localizationManager.start();

        try {
            ServerBootstrap b = new ServerBootstrap();

            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        protected void initChannel(SocketChannel ch) throws Exception {
                            // Set up CORS
                            CorsConfig corsConfig = CorsConfigBuilder
                                    .forAnyOrigin()
                                    .allowNullOrigin()
                                    .allowCredentials()
                                    .build();

                            // Reference: http://netty.io/4.1/api/io/netty/channel/ChannelPipeline.html
                            ChannelPipeline p = ch.pipeline();

                            p.addLast("decoder", new HttpRequestDecoder());
                            p.addLast("aggregator", new HttpObjectAggregator(MAX_CONTENT_LENGTH_IN_BYTE));
                            p.addLast("encoder", new HttpResponseEncoder());
                            p.addLast("cors", new CorsHandler(corsConfig));
                            p.addLast("handler", new MagneticCrfServerHandler(localizationManager));
                        }

                    });

            // Start server
            ChannelFuture f = b.bind(new InetSocketAddress("0.0.0.0", port)).sync();

            logger.info(String.format("Magnetic CRF Server started on port %d", port));

            // Wait until server is closed
            f.channel().closeFuture().sync();

        } finally {
            logger.info("Magnetic CRF Server shutting down ...");

            // Shut down all event loops
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();

            // Shut down localization engines
            localizationManager.shutdownGracefully();

            logger.info("Magnetic CRF Server shutdown completed ...");
        }

    }

}
