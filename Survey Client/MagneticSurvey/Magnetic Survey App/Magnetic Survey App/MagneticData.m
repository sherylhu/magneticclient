//
//  MagneticData.m
//  Magnetic Survey App
//
//  Created by ivy on 9/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "MagneticData.h"

@implementation MagneticData

- (NSString *)saveAsString {
    if (self.intensity == self.intensity && self.horizontal == self.horizontal && self.vertical == self.vertical && self.orientation == self.orientation) {
        NSString *str = [NSString stringWithFormat:@"%f %f %f %f %ld", self.intensity, self.vertical, self.horizontal, self.orientation, self.timestamp];
        
        if ([str length] > 7) {
            return str;
        }
    }
    return @"";
}

@end
