//
//  MagneticData.h
//  Magnetic Survey App
//
//  Created by ivy on 9/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MagneticData : NSObject

@property (nonatomic) float intensity;
@property (nonatomic) float horizontal;
@property (nonatomic) float vertical;
@property (nonatomic) float orientation;

@property (nonatomic) long timestamp;

- (NSString *)saveAsString;

@end
