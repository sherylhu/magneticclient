//
//  VertexData.h
//  Magnetic Survey App
//
//  Created by ivy on 15/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VertexDataDelegate <NSObject>

- (void)getTimestamp:(NSString *)timestamp;

@end

@interface VertexData : NSObject

@property CGPoint fromPnt, toPnt;
@property (nonatomic, strong) NSString *timestamp;
@property int order;

@property (nonatomic, strong) UIButton *fromBtn, *toBtn;

@property (nonatomic, weak) id <VertexDataDelegate> delegate;

- (void)createButtonsWithIndex:(int)index;

@end
