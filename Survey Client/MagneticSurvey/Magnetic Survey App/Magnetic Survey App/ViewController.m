//
//  ViewController.m
//  Magnetic Survey App
//
//  Created by ivy on 7/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "ViewController.h"

#import "DataTransfer.h"
#import "DataTransferModel.h"
#import "MagneticData.h"

#import "VertexData.h"

#import "LMJDropdownMenu.h"

typedef enum MVPinType : NSUInteger {
    MVPinFrom,
    MVPinTo,
    MVPinLoc,
    MVPinTap,
} MVPinType;

@interface ViewController () <DataTransferDelegate, NSFileManagerDelegate, UIGestureRecognizerDelegate, LMJDropdownMenuDelegate, VertexDataDelegate> {
    
    DataTransfer *dtran;
    BOOL onReceive;
    NSTimer *savetimer;
    DataTransferModel *currentModel;
    
    CGPoint tapPnt;
    NSFileManager *fileManager;
    NSMutableArray *records;
    VertexData *vtxRecord;
    NSMutableDictionary *vtxs;
    NSMutableDictionary *fromBtns, *toBtns;
    
    IBOutlet UIButton *acqBtn;
    IBOutlet UIPickerView *itvlPicker;
    IBOutlet UIButton *deleteBtn;
    UIButton *fromButton, *toButton;
    
    IBOutlet UIView *backView;
    UIImageView *mapView;
    UIView *annoView;
    
    CAShapeLayer *routeLine;
    CGMutablePathRef routeLinePath;
    
    NSArray *gestures;
    CGFloat rotationRadian;
    CGFloat rotationNetRadian;
    CGFloat totalScale;
    CGFloat currentScale;
    CGFloat currentMagnitude;
    
    IBOutlet UIStepper *timerStepper;
    IBOutlet UILabel *timerLabel;
    LMJDropdownMenu *dropMenu;
    IBOutlet UIView *dropView;
    
    NSTimer *showTimer;
    IBOutlet UILabel *showLabel;
    NSString *testmodel;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    vtxs = [NSMutableDictionary dictionary];
    fromBtns = [NSMutableDictionary dictionary];
    toBtns = [NSMutableDictionary dictionary];
    
    [timerStepper addTarget:self action:@selector(stepperChanged) forControlEvents:UIControlEventValueChanged];
    
    UIPanGestureRecognizer  *panning = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    UIRotationGestureRecognizer  *rotation = [[UIRotationGestureRecognizer alloc]initWithTarget:self action:@selector(handleRotate:)];
    UIPinchGestureRecognizer  *zooming = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinch:)];
    UILongPressGestureRecognizer *longpressing = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPressing:)];
    
    gestures = [NSArray arrayWithObjects:panning, rotation, zooming, longpressing, nil];
    
    panning.delegate = self;
    rotation.delegate = self;
    zooming.delegate = self;
    longpressing.delegate = self;
    
    [backView addGestureRecognizer:panning];
    [backView addGestureRecognizer:rotation];
    [backView addGestureRecognizer:zooming];
    [backView addGestureRecognizer:longpressing];
    
    fromButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [fromButton setContentMode:UIViewContentModeScaleAspectFit];
    [fromButton setBackgroundImage:[UIImage imageNamed:@"Location_pin_3"] forState:UIControlStateNormal];
    [fromButton setBackgroundColor:[UIColor clearColor]];
    [fromButton setHidden:YES];
    
    toButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [toButton setContentMode:UIViewContentModeScaleAspectFit];
    [toButton setBackgroundImage:[UIImage imageNamed:@"Location_pin_1"] forState:UIControlStateNormal];
    [toButton setBackgroundColor:[UIColor clearColor]];
    [toButton setHidden:YES];
    
    UIImage *newImage = [UIImage imageNamed:@"PCCW_1"];
    if (!newImage) {
        return;
    }
    mapView = [[UIImageView alloc]initWithFrame:CGRectMake(backView.bounds.origin.x, backView.bounds.origin.y, newImage.size.width, newImage.size.height)];
    [mapView setUserInteractionEnabled:YES];
    [backView addSubview:mapView];
    [self sendToCenter:CGPointMake(2000, 1500)];
    [backView sendSubviewToBack:mapView];
    [mapView setImage:newImage];
    
    annoView = [[UIView alloc]initWithFrame:mapView.bounds];
    [mapView addSubview:annoView];
    [mapView bringSubviewToFront:annoView];
    
    dropMenu = [[LMJDropdownMenu alloc]initWithFrame:dropView.bounds];
    [dropMenu setDelegate:self];
    [dropMenu setMenuTitles:@[@"PCCW Demo L1"] rowHeight:dropMenu.frame.size.height];
    [dropView addSubview:dropMenu];
    
    routeLine = [CAShapeLayer layer];
    [routeLine setLineJoin:kCALineJoinRound];
    
    routeLine.fillColor = nil;
    routeLine.lineWidth = 10.0f;
    routeLine.strokeColor = [UIColor blueColor].CGColor;
    routeLinePath = CGPathCreateMutable();
    [annoView.layer addSublayer:routeLine];
    
    dtran = [[DataTransfer alloc]init];
    [dtran setDelegate:self];
    
    onReceive = NO;
    
    [dtran startSensing];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Acquisition
- (void)startAcquisition {
    onReceive = YES;
    [deleteBtn setUserInteractionEnabled:NO];
    [timerStepper setEnabled:NO];
    if (!dtran) {
        dtran = [[DataTransfer alloc]init];
        dtran.delegate = self;
    }
    if (!vtxRecord) {
        vtxRecord = [[VertexData alloc]init];
        vtxRecord.delegate = self;
    }
    
//    [dtran startSensing];
    
    if (showTimer) {
        [showTimer invalidate];
        showTimer = nil;
    }
    showTimer = [NSTimer timerWithTimeInterval:0.5 repeats:YES block:^(NSTimer * _Nonnull timer) {
        
        if (testmodel) {
            NSString *lastStr = testmodel;
            [showLabel setText:lastStr];
        }
    }];
    [[NSRunLoop currentRunLoop] addTimer:showTimer forMode:NSDefaultRunLoopMode];
    [[NSRunLoop currentRunLoop] run];
}

- (void)endAcquisition {
    onReceive = NO;
    [deleteBtn setUserInteractionEnabled:YES];
    [timerStepper setEnabled:YES];
    if (dtran) {
        NSString *fileName0 = [NSString stringWithFormat:@"%@_coor.txt", vtxRecord.timestamp];
        NSString *fileName1 = [NSString stringWithFormat:@"%@_trace.txt", vtxRecord.timestamp];
        
        [self writeLocation:[NSString stringWithFormat:@"%f %f\n%f %f",vtxRecord.fromPnt.x, vtxRecord.fromPnt.y, vtxRecord.toPnt.x, vtxRecord.toPnt.y] intoFile:fileName0];

        NSString *test = [records componentsJoinedByString:@"\n"];
        [self writeLocation:test intoFile:fileName1];
        
        [vtxs setObject:vtxRecord forKey:vtxRecord.timestamp];
        
        CGMutablePathRef path2 = CGPathCreateMutable();
        CGPathMoveToPoint(path2, nil, vtxRecord.fromPnt.x, vtxRecord.fromPnt.y);
        CGPathAddLineToPoint(path2, nil, vtxRecord.toPnt.x, vtxRecord.toPnt.y);
        CGPathAddPath(routeLinePath, NULL, path2);
        routeLine.path = routeLinePath;
        
        vtxRecord = nil;
        [self removePin:MVPinFrom];
        [self removePin:MVPinTo];
        
        [records removeAllObjects];
        records = nil;
    }
    
//    [dtran stopSensing];
//    dtran = nil;
}

#pragma mark - File Operations
//- (void)write:(MagneticData *)model intoFile:(NSString *)fileName {
//    if (fileName) {
//        NSString *filePath = [self composeFileName:fileName];
//        NSLog(@"File Name: %@", filePath);
//        
//        NSFileHandle *fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
//        if (!fileHandler) {
//            [fileManager createFileAtPath:filePath contents:nil attributes:nil];
//            fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
//        }
//        [fileHandler seekToEndOfFile];
//        
//        if (model) {
//            NSString *str = [model saveAsString];
//            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
//            if (data) {
//                [fileHandler writeData:data];
//            }
//        }
//        [fileHandler closeFile];
//    }
//}

- (void)writeData:(NSArray *)models intoFile:(NSString *)fileName {
    if (fileName) {
        NSString *filePath = [self composeFileName:fileName];
        NSLog(@"File Name: %@", filePath);
        
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
        if (!fileHandler) {
            [fileManager createFileAtPath:filePath contents:nil attributes:nil];
            fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
        }
        [fileHandler seekToEndOfFile];
        
        if (models) {
            [models writeToFile:filePath atomically:YES];
        }
        [fileHandler closeFile];
    }
}

- (void)writeLocation:(NSString *)str intoFile:(NSString *)fileName {
    if (fileName) {
        NSString *filePath = [self composeFileName:fileName];
        NSLog(@"File Name: %@", filePath);
        
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
        if (!fileHandler) {
            [fileManager createFileAtPath:filePath contents:nil attributes:nil];
            fileHandler = [NSFileHandle fileHandleForWritingAtPath:filePath];
        }
        [fileHandler seekToEndOfFile];
        
        if (str) {
            [str writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        }
        NSLog(@"File Path: %@", fileName);
        [fileHandler closeFile];
    }
}

- (BOOL)fileExisted:(NSString *)filePath isDirectory:(BOOL)isDirectory {
    BOOL ifDirectory = isDirectory;
    return [fileManager fileExistsAtPath:filePath isDirectory:&ifDirectory];
}

- (NSString *)composeFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:name];
    NSLog(@"File Path: %@", filePath);
    return filePath;
}

#pragma mark - Map Operations
//- (void)addButtonBetween:(NSString *)

#pragma mark - Map Getures
- (void)movePin:(MVPinType)pinType toLoc:(CGPoint)loc isCentre:(BOOL)centre {
    [self removePin:pinType];
    CGPoint locCenter = CGPointMake(loc.x, loc.y - 10);
    switch (pinType) {
        case MVPinLoc: {
        }
            break;
        case MVPinFrom: {
            fromButton.hidden = NO;
            [fromButton setUserInteractionEnabled:YES];
            [fromButton setCenter:locCenter];
            [annoView addSubview:fromButton];
            [annoView bringSubviewToFront:fromButton];
            
//            CGPoint test = [annoViewconvertPoint:locCenter fromView:backView];
//            [self sendToCenter:test];
        }
            break;
        case MVPinTo: {
            toButton.hidden = NO;
            [toButton setUserInteractionEnabled:YES];
            [toButton setCenter:locCenter];
            [annoView addSubview:toButton];
            [annoView bringSubviewToFront:toButton];
            
//            CGPoint test = [annoView convertPoint:locCenter fromView:backView];
//            [self sendToCenter:test];
        }
            break;
        default: {
        }
            break;
    }
}

- (void)removePin:(MVPinType)pinType {
    switch (pinType) {
        case MVPinLoc:
//            [locButton setHidden:YES];
//            [locButton setUserInteractionEnabled:NO];
//            [locButton removeFromSuperview];
            break;
        case MVPinFrom:
            [fromButton setHidden:YES];
            [fromButton setUserInteractionEnabled:NO];
            [fromButton removeFromSuperview];
            break;
        case MVPinTo:
            [toButton setHidden:YES];
            [toButton setUserInteractionEnabled:NO];
            [toButton removeFromSuperview];
        default:
//            [tapButton setHidden:YES];
//            [tapButton setUserInteractionEnabled:NO];
//            [tapButton removeFromSuperview];
            break;
    }
}

- (void)sendToCenter:(CGPoint)pnt {
    if (mapView) {
        [UIView animateWithDuration:0.5 animations:^{
            CGPoint newCenter = [backView convertPoint:pnt fromView:mapView];
            
            CGPoint backCenter = CGPointMake(CGRectGetMidX(backView.bounds), CGRectGetMidY(backView.bounds));
            CGPoint disp = CGPointMake(backCenter.x - newCenter.x, backCenter.y - newCenter.y);
            [self translateWithDisplacement:disp];
        }];
        
    }
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    UIGestureRecognizerState state = [recognizer state];
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [recognizer translationInView:backView];
        [self translateWithDisplacement:translation];
        
        [recognizer setTranslation:CGPointMake(0, 0) inView:backView];
    } else if (state==UIGestureRecognizerStateEnded) {
        [self adjustTranslationToAnchor:recognizer.view.center];
    }
}

- (void)translateWithDisplacement:(CGPoint)translation {
    mapView.center = CGPointMake(mapView.center.x + translation.x, mapView.center.y + translation.y);
}

- (void)adjustTranslationToAnchor:(CGPoint)anchor {
    CGFloat svgWidthThresh = mapView.frame.size.width;
    CGFloat svgHeightThresh = mapView.frame.size.height;
    
    CGFloat svgCenterX = CGRectGetMidX(mapView.frame);
    CGFloat svgCenterY = CGRectGetMidY(mapView.frame);
    CGFloat viewCenterX =anchor.x;
    CGFloat viewCenterY =anchor.y;
    
    CGPoint offset = CGPointZero;
    
    if((svgCenterX - viewCenterX) > svgWidthThresh/2) {
        offset = CGPointMake(offset.x - (svgCenterX - viewCenterX - svgWidthThresh/2), offset.y);
    }
    if((viewCenterX - svgCenterX) > svgWidthThresh/2) {
        offset = CGPointMake(offset.x + viewCenterX - svgCenterX - svgWidthThresh/2, offset.y);
    }
    if((svgCenterY - viewCenterY) > svgHeightThresh/2) {
        offset = CGPointMake(offset.x, offset.y - (svgCenterY - viewCenterY - svgHeightThresh/2));
    }
    if((viewCenterY - svgCenterY) > svgHeightThresh/2) {
        offset = CGPointMake(offset.x, offset.y + viewCenterY - svgCenterY - svgHeightThresh/2);
    }
    if(!CGPointEqualToPoint(CGPointZero, offset)) {
        [UIView animateWithDuration:0.2 animations:^{
            [self translateWithDisplacement:offset];
        }];
    }
}

- (void)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    if ([recognizer state] == UIGestureRecognizerStateBegan) {
        [self adjustAnchorPointForGestureRecognizer:recognizer];
        rotationNetRadian = 0.0f;
        rotationRadian = 0.0f;
    }
    
    if ([recognizer state] == UIGestureRecognizerStateChanged) {
        [self rotateWithRadius:recognizer.rotation];
        
        rotationNetRadian = recognizer.rotation;
        rotationRadian += recognizer.rotation;
        recognizer.rotation = 0;
    }
    
    if ([recognizer state] == UIGestureRecognizerStateEnded) {
        
    }
}

- (void)rotateWithRadius:(CGFloat)radius {
    mapView.transform = CGAffineTransformRotate(mapView.transform, radius);
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = mapView;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan) {
        [self adjustAnchorPointForGestureRecognizer:recognizer];
        totalScale = 1.0f;
    }
    
    if (state == UIGestureRecognizerStateChanged) {
        [self scaleWithScale:recognizer.scale];
        recognizer.scale = 1;
    }
    
    if(state==UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:0.2 animations:^{
//            [self adjustScaleToSize:recognizer.view.frame.size];
        }];
    }
}

- (void)scaleWithScale:(CGFloat)scale {
    
    CGFloat preScale = totalScale;
    
    mapView.transform = CGAffineTransformScale(mapView.transform, scale, scale);
    totalScale *= scale;
    currentMagnitude = currentMagnitude / (totalScale / preScale);
}

- (void)adjustScaleToSize:(CGSize)size {
    [UIView animateWithDuration:0.2 animations:^{
        while (currentMagnitude <= 1.1 || currentMagnitude >= 70) {
            if (currentMagnitude <= 1.1) {
                NSLog(@"少於1");
                [self scaleWithScale:0.9];
            } else if (currentMagnitude >= 70) {
                NSLog(@"大於70");
                [self scaleWithScale:1.1];
            }
        }
    }];
}

- (void)handleLongPressing:(UILongPressGestureRecognizer *)recognizer {
    
    if (onReceive) {
        return;
    }
    
    CGPoint menuPnt = [recognizer locationInView:backView];
    tapPnt = [recognizer locationInView:mapView];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        NSLog(@"------------->手势已经触发了");
        [self becomeFirstResponder];
        // 创建 菜单控制器
        UIMenuController *menu = [UIMenuController sharedMenuController];
        // 创建仨条 Item 每一个起一个名字
        UIMenuItem *menuItem1 = [[UIMenuItem alloc] initWithTitle:@"Set From" action:@selector(setFromResponder)];
        UIMenuItem *menuItem2 = [[UIMenuItem alloc] initWithTitle:@"Set To" action:@selector(setToResponder)];
        
        // 设置菜单显示 每条 Item
        menu.menuItems = @[menuItem1,menuItem2];
        // 弹出菜单展示的位置  箭头指向的地方为 origin
        [menu setTargetRect:CGRectMake(menuPnt.x , menuPnt.y, 50, 30) inView:backView];
        // 显示
        [menu setMenuVisible:YES animated:YES];
    }
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"长按结束");
    }
}

- (void)setFromResponder {
    
    if (!vtxRecord) {
        vtxRecord = [[VertexData alloc]init];
        vtxRecord.delegate = self;
    }
    vtxRecord.fromPnt = tapPnt;
    [self movePin:MVPinFrom toLoc:vtxRecord.fromPnt isCentre:NO];
    
//    fromPnt = tapPnt;
//    [self movePin:MVPinFrom toLoc:fromPnt isCentre:NO];
    
    tapPnt = CGPointZero;
}

- (void)setToResponder {
    
    if (!vtxRecord) {
        vtxRecord = [[VertexData alloc]init];
        vtxRecord.delegate = self;
    }
    
    vtxRecord.toPnt = tapPnt;
    [self movePin:MVPinTo toLoc:vtxRecord.toPnt isCentre:NO];
    
//    toPnt = tapPnt;
//    [self movePin:MVPinTo  toLoc:toPnt isCentre:NO];
    
    tapPnt = CGPointZero;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(setFromResponder) || action == @selector(setToResponder)) {
        return YES;
    }
    return [super canPerformAction:action withSender:sender];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    if ([gestures containsObject:gestureRecognizer]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - IBActions
- (IBAction)startClicked:(id)sender {
    
    if (onReceive) {
        [self endAcquisition];
        [acqBtn setSelected:NO];
    } else {
        
        if (!vtxRecord) {
            return;
        }
        
        if (CGPointEqualToPoint(CGPointZero, vtxRecord.fromPnt) || CGPointEqualToPoint(CGPointZero, vtxRecord.toPnt) || CGPointEqualToPoint(vtxRecord.fromPnt, vtxRecord.toPnt)) {
            return;
        }
        
        NSDate *  senddate=[NSDate date];
        NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
        //设置获取时间的格式
        [dateformatter setDateFormat:@"yyyyMMddHHmmss"];
        //    timestamp = [dateformatter stringFromDate:senddate];
        vtxRecord.timestamp = [dateformatter stringFromDate:senddate];
        
        [vtxRecord createButtonsWithIndex:(int)[[vtxs allValues] count] + 1];
        [annoView addSubview:vtxRecord.fromBtn];
        [fromBtns setObject:vtxRecord.fromBtn forKey:vtxRecord.timestamp];
        [annoView addSubview:vtxRecord.toBtn];
        [toBtns setObject:vtxRecord.toBtn forKey:vtxRecord.timestamp];
        
        [self startAcquisition];
        [acqBtn setSelected:YES];
    }
}

- (void)stepperChanged {
//    UIStepper *stepper = (UIStepper *)sender;
    NSLog(@"Stepper Value:%f", timerStepper.value);
    [timerLabel setText:[NSString stringWithFormat:@"%d", (int)timerStepper.value]];
}

- (IBAction)deleteClicked:(id)sender {
    
    if (!deleteBtn.userInteractionEnabled) {
        return;
    }
    
    if (routeLine) {
        [routeLine removeFromSuperlayer];
        routeLine = nil;
        routeLinePath = nil;
    }
    
    NSArray *subs = [annoView subviews];
    for (UIView *sub in subs) {
        [sub removeFromSuperview];
    }
    
    [fromBtns removeAllObjects];
    [toBtns removeAllObjects];
    [vtxs removeAllObjects];
    
    routeLine = [CAShapeLayer layer];
    [routeLine setLineJoin:kCALineJoinRound];
    
    routeLine.fillColor = nil;
    routeLine.lineWidth = 10.0f;
    routeLine.strokeColor = [UIColor blueColor].CGColor;
    routeLinePath = CGPathCreateMutable();
    [annoView.layer addSublayer:routeLine];
    
    vtxs = [NSMutableDictionary dictionary];
    fromBtns = [NSMutableDictionary dictionary];
    toBtns = [NSMutableDictionary dictionary];
}

#pragma mark - DataTransferDelegate
- (void)reportData:(id)data {
    DataTransferModel *model = [[DataTransferModel alloc]init];
    NSString *dataStr;
    model = data;
    if (model) {
        MagneticData *newData = [[MagneticData alloc]init];
        newData.intensity = model.intensity;
        newData.vertical = model.vertical;
        newData.horizontal = model.horizontal;
        newData.orientation = model.orientation;
        newData.timestamp = model.timestamp;
        dataStr = [newData saveAsString];
        testmodel = dataStr;
    }
    if (onReceive) {
        if (!records) {
            records = [NSMutableArray array];
        }
//        DataTransferModel *model = [[DataTransferModel alloc]init];
//        model = data;
        if (model) {
//            MagneticData *newData = [[MagneticData alloc]init];
//            newData.intensity = model.intensity;
//            newData.vertical = model.vertical;
//            newData.horizontal = model.horizontal;
//            newData.orientation = model.orientation;
//            newData.timestamp = model.timestamp;
//            NSString *dataStr = [newData saveAsString];
            [records addObject:dataStr];
        }
    }
    
//    if (!records) {
//        records = [NSMutableArray array];
//    }
//    DataTransferModel *model = [[DataTransferModel alloc]init];
//    model = data;
//    if (model) {
//        MagneticData *newData = [[MagneticData alloc]init];
//        newData.intensity = model.intensity;
//        newData.vertical = model.vertical;
//        newData.horizontal = model.horizontal;
//        newData.orientation = model.orientation;
//        newData.timestamp = model.timestamp;
//        NSString *dataStr = [newData saveAsString];
//        [records addObject:dataStr];
//    }
}

#pragma mark - VertexDataDelegate
- (void)getTimestamp:(NSString *)timestamp {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose Actions" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        BOOL isDirectory = NO;
        NSError *error = nil;
        
        NSString *fileName = [NSString stringWithFormat:@"%@-coor.txt", timestamp];
        NSString *filePath = [self composeFileName:fileName];
        if (![fileManager fileExistsAtPath:filePath isDirectory:&isDirectory]) {
            [fileManager removeItemAtPath:filePath error:&error];
            if (error) {
                NSLog(@"Create Site Folder Error!");
            }
        }
        
        fileName = [NSString stringWithFormat:@"%@-data.txt", timestamp];
        filePath = [self composeFileName:fileName];
        if (![fileManager fileExistsAtPath:filePath isDirectory:&isDirectory]) {
            [fileManager removeItemAtPath:filePath error:&error];
            if (error) {
                NSLog(@"Create Site Folder Error!");
            }
        }
        
        if (!error) {
            
            VertexData *tmpVtx = [vtxs objectForKey:timestamp];
            UIButton *tmpFrom = [fromBtns objectForKey:timestamp];
            UIButton *tmpTo = [toBtns objectForKey:timestamp];
            
            if (tmpVtx) {
                [tmpFrom removeFromSuperview];
                [tmpTo removeFromSuperview];
            }
            
            [vtxs removeObjectForKey:timestamp];
            [fromBtns removeObjectForKey:timestamp];
            [toBtns removeObjectForKey:timestamp];
            
            if (routeLinePath && routeLinePath) {

                [routeLine removeFromSuperlayer];
                routeLine = nil;
                routeLinePath = nil;
                
                routeLine = [CAShapeLayer layer];
                [routeLine setLineJoin:kCALineJoinRound];
                
                routeLine.fillColor = nil;
                routeLine.lineWidth = 10.0f;
                routeLine.strokeColor = [UIColor blueColor].CGColor;
                routeLinePath = CGPathCreateMutable();
                [annoView.layer addSublayer:routeLine];
                
                NSArray *allvtxs = [NSArray arrayWithArray:[vtxs allValues]];
                for (VertexData *vdata in allvtxs) {
                    CGPoint from = vdata.fromPnt;
                    CGPoint to  = vdata.toPnt;
                    
                    CGMutablePathRef path2 = CGPathCreateMutable();
                    CGPathMoveToPoint(path2, nil, from.x, from.y);
                    CGPathAddLineToPoint(path2, nil, to.x, to.y);
                    CGPathAddPath(routeLinePath, NULL, path2);
                    routeLine.path = routeLinePath;
                }
                
            }
        }
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - LMJDropdownMenuDelegate
- (void)dropdownMenu:(LMJDropdownMenu *)menu selectedCellNumber:(NSInteger)number {
    
}

@end
