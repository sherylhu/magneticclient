//
//  Position.h
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Position : NSObject

@property (nonatomic) float x, y, oritation_offset;
@property (nonatomic, strong) NSString *area_id;

- (id)initWithJson:(id)responseObject;

@end
