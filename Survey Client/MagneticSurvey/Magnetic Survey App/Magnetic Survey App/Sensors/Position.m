//
//  Position.m
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import "Position.h"

@implementation Position

#define x_key @"x"
#define y_key @"y"
#define orientation_key @"oritation_offset"
#define area_key @"floor_id"

- (id)initWithJson:(id)responseObject {
    self = [super init];
    if (self) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:responseObject];
        if (dict) {
            self.x = [[dict valueForKey:x_key] floatValue];
            self.y = [[dict valueForKey:y_key] floatValue];
            self.oritation_offset = [[dict valueForKey:orientation_key] floatValue];
            self.area_id = [dict valueForKey:area_key];
        }
    }
    return self;
}

@end
