//
//  DataTransfer.m
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import "DataTransfer.h"
#import "DataTransferModel.h"

#import "CRFLocationManager.h"
#import "CRFBeaconManager.h"
#import "CRFSensorManager.h"
#import "CRFLocationSmoother.h"

@interface DataTransfer () <CRFLocationManagerDelegate, CRFSensorManagerDelegate>

@property (strong, nonatomic) CRFLocationManager *locationManager;
@property (strong, nonatomic) CRFSensorManager *sensorManager;
@property (strong, nonatomic) CRFLocationSmoother *locationSmoother;

@property (assign, nonatomic) CGPoint crfEstimation;
@property (assign, nonatomic) CGPoint beaconEstimation;
@property (assign, nonatomic) CGPoint smoothEstimation;

@property (assign, nonatomic) int mCurrentStep;
@property (assign, nonatomic) int mCurrentAreaId;

@end

@implementation DataTransfer

- (id)init {
    self = [super init];
    if (self) {
        
        self.locationManager = [[CRFLocationManager alloc]init];
        self.locationManager.delegate = self;
        
        self.sensorManager = [[CRFSensorManager alloc] init];
        self.sensorManager.beaconManager = self.locationManager.beaconManager;
        self.sensorManager.delegate = self;
    }
    return self;
}

#pragma mark - Sensing data

- (void)restart
{
    [self stopSensing];
    [self startSensing];
}

- (void)startSensing
{
//    if (!RUN_SIMULATION)
    [self.sensorManager begin];
    [self.locationManager start];
    if (RUN_SIMULATION)
        return;
}

- (void)stopSensing
{
    [self.sensorManager stop];
//    [self.locationManager stop];
}

- (void)sensorManager:(CRFSensorManager *)sensorManager didUpdateSensorDataType:(CRFSensorDataType)type data:(NSArray *)data {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (type) {
            case CRFSensorDataTypeCRF:
            {
                double intensity = [data[0] doubleValue];
                double vertical = [data[1] doubleValue];
                double horizontal = [data[2] doubleValue];
                double orientation = [data[3] doubleValue];
                long timestamp = [data[4] longValue];
                int step = [data[5] intValue];
                NSDictionary *beacon_vector = data[6];
                double altitude = [data[7] doubleValue];
                double pressure = [data[8] doubleValue];

                CRFSensorData *sensorData = [[CRFSensorData alloc] init];
                sensorData.intensity = intensity;
                sensorData.vertical = vertical;
                sensorData.horizontal = horizontal;
                sensorData.orientation = orientation;
                sensorData.timestamp = timestamp;
                sensorData.step = step;
                sensorData.beacon_vector = beacon_vector;
                sensorData.rAltitude = altitude;
                sensorData.pressure = pressure;
                
                if (step - self.mCurrentStep > 1) {
                    [self restart];
                    return;
                }
                
                self.mCurrentStep = step;
                if (!DISTRIBUTION) {
                    // find 1-nn beacon for debugging
                    NSString *maxRSSIBeaconId = nil;
                    int maxRssi = -1000;
                    for (NSString *key in beacon_vector) {
                        int rssi = [beacon_vector[key] intValue];
                        if (rssi > maxRssi) {
                            maxRssi = rssi;
                            maxRSSIBeaconId = key;
                        }
                    }
                }
                
                DataTransferModel *model = [[DataTransferModel alloc]init];
                model.timestamp = timestamp;
                model.intensity = intensity;
                model.horizontal = horizontal;
                model.vertical = vertical;
                model.orientation = orientation;
                model.step = step;
                model.bleVector = beacon_vector;
                [self.delegate reportData:model];
                
                [self.locationManager feedSensorData:sensorData];
            }
                break;
            case CRFSensorDataTypeCalibratedOrientation : {
                
            }
                break;
            case CRFSensorDataTypeBeacon:
            {
            }
                break;
                
            default: {
            }
                break;
        }
    });
}

#pragma mark <CRFLocationManagerDelegate>

- (void)locationManager:(CRFLocationManager *)locationManager didUpdateAreaId:(int)areaId
{
    self.mCurrentAreaId = areaId;
}

- (void)locationManager:(CRFLocationManager *)locationManager didUpdateBeaconEstimate:(CGPoint)point
{
    self.beaconEstimation = point;
}

- (void)locationManager:(CRFLocationManager *)locationManager didUpdateRoute:(NSArray *)route
{
    dispatch_async(dispatch_get_main_queue(), ^{
//        if (!DISTRIBUTION) {
//            [self resetRoutePath];
//            
//            [self.bezierPath moveToPoint:CGPointMake([route[0][0] doubleValue], [route[0][1] doubleValue])];
//            for (int i = 1; i < route.count; i++) {
//                [self.bezierPath addLineToPoint:CGPointMake([route[i][0] doubleValue], [route[i][1] doubleValue])];
//            }
//            
//            self.lineLayer.path = self.bezierPath.CGPath;
//        }
//        
//        self.crfEstimation = CGPointMake([[route lastObject][0] doubleValue], [[route lastObject][1] doubleValue]);
//        CGPoint smoothEstimation = [self.locationSmoother addPoint:self.crfEstimation];
//        if ([CRFCommon pointIsValid:smoothEstimation]) {
//            self.smoothEstimation = smoothEstimation;
//        }
    });
}




@end
