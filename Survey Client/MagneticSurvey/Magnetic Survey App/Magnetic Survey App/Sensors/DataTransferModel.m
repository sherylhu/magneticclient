//
//  DataTransferModel.m
//  Mag IOS
//
//  Created by ivy on 3/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import "DataTransferModel.h"

@implementation DataTransferModel

- (id)init {
    self = [super init];
    if (self) {
        self.userId = @"user_id";
        self.timestamp = -1;
        self.intensity = 0.f;
        self.horizontal = 0.f;
        self.vertical = 0.f;
        self.step = 1;
        self.bleVector = [NSDictionary dictionary];
        self.wifiVector = [NSDictionary dictionary];
        
        self.gpsComponent = [NSMutableArray array];
        
        self.building_id = @"Academic Building";
        self.floor_id = @"2F";
    }
    return self;
}

- (id)proxy2Gson {
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
            self.userId, @"user_id",
            [NSNumber numberWithLong:self.timestamp], @"ts",
            [NSNumber numberWithFloat:self.intensity], @"intensity",
            [NSNumber numberWithFloat:self.horizontal], @"horizontal_comp",
            [NSNumber numberWithFloat:self.vertical], @"vertical_comp",
            [NSNumber numberWithInt:self.step], @"step",
            [NSNumber numberWithFloat:self.orientation], @"orientation",
                          self.building_id, @"building_id",
            self.floor_id, @"floor_id",
            self.wifiVector, @"wifi_vector",
            self.bleVector, @"ble_vector",
//            self.gpsComponent, @"gps_comps",
//            self.floorDetected, @"floor",
            nil];
    return dict;
}

@end
