//
//  CRFCommon.h
//  CRF
//
//  Created by Siyan HU on 17/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RUN_SIMULATION YES
#define DISTRIBUTION NO

#define SIMULATION_FILENAME @"1101-3.txt"
#define DEFAULT_AREA 1100

@interface CRFCommon : NSObject

+ (long)getCurrentTimestampNanoSecond;
+ (long)getCurrentTimestampMilliSecond;
+ (CGPoint)locationString2Point:(NSString *)locString;
+ (BOOL)pointIsValid:(CGPoint)point;

+ (double)mean:(NSArray *)array;
+ (double)euclideanFrom:(CGPoint)from to:(CGPoint)to;
+ (NSArray *)getRotationMatrixFromGravity:(NSArray *)gravity geoMagnetic:(NSArray *)geomagnetic;
+ (double)cosineSimilarityWithA:(NSDictionary *)A B:(NSDictionary *)B;

+ (NSArray *)loadTraceFile:(NSString *)fileName orientationCalibration:(double)calibration;
+ (NSArray *)loadTraceFile:(NSString *)fileName;
+ (NSArray *)loadRouteFile:(NSString *)fileName;
+ (NSArray *)loadBeaconFile:(NSString *)fileName;
+ (NSArray *)loadConstraint:(NSString *)fileName;
+ (NSDictionary *)loadSectj:(NSString *)fileName;
+ (CRFAreaSetting *)loadSiteSetting:(int)areaId;

@end
