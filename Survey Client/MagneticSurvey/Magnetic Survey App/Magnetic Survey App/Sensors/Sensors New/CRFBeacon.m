//
//  CRFBeacon.m
//  CRF
//
//  Created by Samuel Chow on 12/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFBeacon.h"

@implementation CRFBeacon

- (instancetype)initWithName:(NSString *)name uuid:(NSUUID *)uuid major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor position:(CGPoint)position
{
    if (self = [super init]) {
        self.uuid = uuid;
        self.majorValue = major;
        self.minorValue = minor;
        self.position = position;
        self.name = [NSString stringWithFormat:@"%@_%hu_%hu", uuid.UUIDString, major, minor];
    }
    
    return self;
}

- (BOOL)isEqualToCLBeacon:(CLBeacon *)beacon
{
    if ([[beacon.proximityUUID UUIDString] isEqualToString:[self.uuid UUIDString]] &&
        [beacon.major isEqual: @(self.majorValue)] &&
        [beacon.minor isEqual: @(self.minorValue)])
    {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Public API

+ (NSString *)getUUIDFromIdentifier:(NSString *)identifier
{
    return [identifier componentsSeparatedByString:@"_"][0];
}

+ (int)getMajorFromIdentifier:(NSString *)identifier
{
    return [[identifier componentsSeparatedByString:@"_"][1] intValue];
}

+ (int)getMinorFromIdentifier:(NSString *)identifier
{
    return [[identifier componentsSeparatedByString:@"_"][2] intValue];
}

@end
