//
//  CRFStepDetector.m
//  Sensor
//
//  Created by Siyan HU on 3/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFStepDetector.h"
#import "CRFSensorRawData.h"

#define EARTH_GRAVITY 9.8f

#define GYRO_SAMPLE_RATE 1

typedef enum : NSUInteger {
    CRFSensorTypeAcc,
    CRFSensorTypeGyro,
} CRFSensorType;

@interface CRFStepDetector ()

@property (assign, nonatomic) int mSamplePerSecond;
@property (assign, nonatomic) int mWindowSize;
@property (assign, nonatomic) int mStepSampleMax;
@property (assign, nonatomic) int mStepSampleMin;
@property (assign, nonatomic) int mSampleChangeInterval;
@property (assign, nonatomic) int mStepSampleScopeMax;
@property (assign, nonatomic) int mStepSampleScopeMin;
@property (assign, nonatomic) int mBufferSize;

@property (strong, nonatomic) NSMutableArray *mAccDataList;
@property (strong, nonatomic) NSMutableArray *mCombineAccDataList;
@property (strong, nonatomic) NSMutableArray *mAccDataBuffer;
@property (strong, nonatomic) NSMutableArray *mCombineAccDataBuffer;
@property (strong, nonatomic) NSMutableArray *mGyroDataBuffer;
@property (strong, nonatomic) NSMutableArray *mCombineGyroDataBuffer;

@property (assign, nonatomic) int mAccBufferCount;
@property (assign, nonatomic) int mAccDataCount;
@property (assign, nonatomic) int mCombineBufferCount;
@property (assign, nonatomic) int mCombineDataCount;
@property (assign, nonatomic) int mGyroBufferCount;
@property (assign, nonatomic) int mCombineGyroBufferCount;
@property (assign, nonatomic) int mGyroSampleCount;
@property (assign, nonatomic) int mCount;
@property (assign, nonatomic) int mFrequency;

@property (assign, nonatomic) long mPrevTimeStamp;
@property (assign, nonatomic) long startTime;

@property (strong, nonatomic) id mAccSyncObject;
@property (strong, nonatomic) id mCombineAccSyncObject;
@property (strong, nonatomic) id mGyroSyncObject;
@property (strong, nonatomic) id mCombineGyroSyncObject;

@property (assign, nonatomic) BOOL mIsThreadOn;
@property (strong, nonatomic) NSThread *thread;

@end

@implementation CRFStepDetector

#pragma mark - Initialization

- (instancetype)initWithSamplePerSecond:(int)samplePerSecond
{
    if (self = [super init]) {
        
        self.mAccSyncObject = [NSDate date];
        self.mCombineAccSyncObject = [NSDate date];
        self.mGyroSyncObject = [NSDate date];
        self.mCombineGyroSyncObject = [NSDate date];
        
        self.mSamplePerSecond = samplePerSecond;
        self.mWindowSize = 2 * self.mSamplePerSecond;
        self.mStepSampleMax = (int) (self.mSamplePerSecond);
        self.mStepSampleMin = (int) (0.3 * self.mSamplePerSecond);
        self.mSampleChangeInterval = (int) (0.2 * self.mSamplePerSecond);
        self.mStepSampleScopeMax = self.mStepSampleMax;
        self.mStepSampleScopeMin = self.mStepSampleMin;
        self.mBufferSize = 100 * self.mSamplePerSecond;
    }
    
    return self;
}

- (void)initData
{
    self.mAccDataList = [NSMutableArray arrayWithCapacity:self.mWindowSize];
    self.mCombineAccDataList = [NSMutableArray arrayWithCapacity:self.mWindowSize];
    for (int i = 0; i < self.mWindowSize; i++) {
        [self.mAccDataList addObject:[NSNull null]];
        [self.mCombineAccDataList addObject:[NSNull null]];
    }
    self.mAccDataBuffer = [NSMutableArray arrayWithCapacity:self.mBufferSize];
    self.mCombineAccDataBuffer = [NSMutableArray arrayWithCapacity:self.mBufferSize];
    self.mGyroDataBuffer = [NSMutableArray arrayWithCapacity:self.mBufferSize];
    self.mCombineGyroDataBuffer = [NSMutableArray arrayWithCapacity:self.mBufferSize];
    for (int i = 0; i < self.mBufferSize; i++) {
        [self.mAccDataBuffer addObject:[NSNull null]];
        [self.mCombineAccDataBuffer addObject:[NSNull null]];
        [self.mGyroDataBuffer addObject:[NSNull null]];
        [self.mCombineGyroDataBuffer addObject:[NSNull null]];
    }
    self.mAccBufferCount = self.mAccDataCount = self.mCombineBufferCount = self.mCombineDataCount = 0;
    self.mGyroBufferCount = self.mCombineGyroBufferCount = 0;
    self.mGyroSampleCount = 0;
    self.mCount = 0;
    self.startTime = -1;
    self.mPrevTimeStamp = 0;
    self.mFrequency = 0;
}

#pragma mark - Lifecycle

- (void)start
{
    self.mIsThreadOn = YES;
    [self initData];
    
    self.thread = [[NSThread alloc] initWithTarget:self selector:@selector(runThread) object:nil];
    [self.thread start];
}

- (void)stop
{
    self.mIsThreadOn = NO;
    [self.thread cancel];
}

- (void)runThread
{
    while (self.mIsThreadOn) {
        int spaceLeft = self.mWindowSize - self.mAccDataCount;
        if (self.mAccBufferCount > spaceLeft && spaceLeft >= 1) {
            @synchronized(self.mAccSyncObject) {
                [self fillAccData:self.mAccDataList buffer:self.mAccDataBuffer count:spaceLeft];
            }
            @synchronized(self.mCombineAccSyncObject) {
                [self fillCombineData:self.mCombineAccDataList buffer:self.mCombineAccDataBuffer count:spaceLeft];
            }
            [self processStepCount];
        }
        @try {
            [NSThread sleepForTimeInterval:50/1000];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
        }
    }
}

#pragma mark - Move detection

- (BOOL)moveDetection
{
    BOOL accWalkFlag = [self moveDetectByAcc];
    BOOL gyroWalkFlag = [self moveDetectByGyro];

    if (accWalkFlag && gyroWalkFlag) {
        return YES;
    }
    self.mPrevTimeStamp = 0;
    return NO;
}

- (BOOL)moveDetectByAcc
{
    return (([self stdWithDataList:self.mCombineAccDataList start:0 end:MIN(self.mStepSampleScopeMax, self.mWindowSize)] > 0.7 ||
            [self stdWithDataList:self.mCombineAccDataList start:0 end:MIN(self.mCombineDataCount, self.mWindowSize)] > 0.7) &&
            [self hasDataAboveValueWithDataList:self.mCombineAccDataList start:0 end:MIN(self.mStepSampleScopeMax, self.mWindowSize) value:EARTH_GRAVITY+1]);
}

- (BOOL)moveDetectByGyro
{
    @synchronized(self.mCombineGyroSyncObject) {
        int count = [self getGyroSkip:self.mStepSampleScopeMax];
        return ([self stdWithDataList:self.mCombineGyroDataBuffer start:0 end:count] < 1.5 && [self meanWithDataList:self.mCombineGyroDataBuffer start:0 end:count] < 1.5) && ![self hasDataAboveValueWithDataList:self.mCombineGyroDataBuffer start:0 end:count value:2.5f];
    }
}

#pragma mark - Step Detection

- (int)stepDetection
{
    double autoCorrelationMax = 0;
    int stepSampleFind = 0;
    for (int stepSample = self.mStepSampleScopeMin; stepSample <= self.mStepSampleScopeMax; stepSample++) {
        double autoCorrelation = [self getAutoCorrelation:stepSample];
        if (autoCorrelation > autoCorrelationMax) {
            autoCorrelationMax = autoCorrelation;
            stepSampleFind = stepSample;
        }
    }
    
    if (autoCorrelationMax > 0.4) {
        self.mCount += 1;
        if (self.mPrevTimeStamp != 0) {
            self.mFrequency = 1/((((CRFSensorRawData *)self.mAccDataList[0]).timestamp - self.mPrevTimeStamp) / 1000000000.0f);
        }
        [self.delegate stepDetector:self didDetectStepFrom:((CRFSensorRawData *)self.mAccDataList[0]).timestamp to:((CRFSensorRawData *)self.mAccDataList[self.mAccDataList.count-1]).timestamp stepCount:self.mCount frequency:self.mFrequency];
        self.mPrevTimeStamp = ((CRFSensorRawData *)self.mAccDataList[0]).timestamp;
        self.mStepSampleScopeMin = MAX(self.mStepSampleMin, stepSampleFind-self.mSampleChangeInterval);
        self.mStepSampleScopeMax = MIN(self.mStepSampleMax, stepSampleFind+self.mSampleChangeInterval);
        return stepSampleFind;
    } else {
        self.mPrevTimeStamp = 0;
    }
    
    return self.mStepSampleScopeMax;
}

- (void)noStepDetection {
    [self.delegate stepDetector:self didDetectStepFrom:((CRFSensorRawData *)self.mAccDataList[0]).timestamp to:((CRFSensorRawData *)self.mAccDataList[self.mAccDataList.count-1]).timestamp stepCount:0 frequency:self.mFrequency];
}

- (int)detectFirstShake
{
    int lastPeace, begin;
    lastPeace = begin = 0;
    for (begin = 0; begin < self.mCombineDataCount; begin++) {
        if (fabs([self.mCombineAccDataList[begin] doubleValue] - EARTH_GRAVITY) < 0.5)
            lastPeace = begin;
        if (fabs([self.mCombineAccDataList[begin] doubleValue] - EARTH_GRAVITY) > 1)
            return MAX(lastPeace, 1);
    }
    return MAX(lastPeace, 1);
}

- (void)processStepCount
{
    int accSkip = [self detectFirstShake];
    int gyroSkip = [self getGyroSkip:accSkip];
    if (accSkip > 10) {
        
        [self noStepDetection];
        [self clearDataWithAccSkip:accSkip+1 gyroSkip:gyroSkip];
        return;
    }
    if ([self moveDetection]) {
        accSkip = [self stepDetection];
        gyroSkip = [self getGyroSkip:accSkip];
    } else {
        accSkip = self.mStepSampleScopeMax;
        gyroSkip = [self getGyroSkip:accSkip];
    }
    [self clearDataWithAccSkip:accSkip+1 gyroSkip:gyroSkip];
}

#pragma mark - Data manipulation

- (int)getGyroSkip:(int)accSkip
{
    long cutTimestamp = ((CRFSensorRawData *)self.mAccDataList[accSkip]).timestamp;
    for (int i = 0; i < self.mGyroBufferCount; i++) {
        if (((CRFSensorRawData *)self.mGyroDataBuffer[i]).timestamp > cutTimestamp) return i;
    }
    return self.mGyroBufferCount;
}

- (void)clearDataWithAccSkip:(int)accSkip gyroSkip:(int)gyroSkip
{
    @synchronized(self.mAccSyncObject) {
        [self skipWithDataList:self.mAccDataList length:self.mAccDataCount skip:accSkip type:CRFSensorTypeAcc];
    }
    @synchronized(self.mCombineAccSyncObject) {
        [self skipWithCombineDataList:self.mCombineAccDataList length:self.mCombineDataCount skip:accSkip type:CRFSensorTypeAcc];
    }
    @synchronized(self.mGyroSyncObject) {
        [self skipWithDataList:self.mGyroDataBuffer length:self.mGyroBufferCount skip:gyroSkip type:CRFSensorTypeGyro];
    }
    @synchronized(self.mCombineGyroSyncObject) {
        [self skipWithCombineDataList:self.mCombineGyroDataBuffer length:self.mCombineGyroBufferCount skip:gyroSkip type:CRFSensorTypeGyro];
    }
}

- (void)skipWithDataList:(NSMutableArray *)dataList length:(int)length skip:(int)skip type:(int)type
{
    if (type == CRFSensorTypeAcc) {
        if (skip >= length) {
            [dataList removeAllObjects];
            for (int i = 0; i < self.mWindowSize; i++) {
                [dataList addObject:[NSNull null]];
            }
            self.mAccDataCount = 0;
        } else {
            for (int i = skip; i < length; i++) {
                dataList[i-skip] = dataList[i];
            }
            self.mAccDataCount -= skip;
        }
    } else if (type == CRFSensorTypeGyro) {
        if (skip >= length) {
            [dataList removeAllObjects];
            for (int i = 0; i < self.mBufferSize; i++) {
                [dataList addObject:[NSNull null]];
            }
            self.mGyroBufferCount = 0;
        } else {
            for (int i = skip; i < length; i++) {
                dataList[i-skip] = dataList[i];
            }
            self.mGyroBufferCount -= skip;
        }
    }
}

- (void)skipWithCombineDataList:(NSMutableArray *)dataList length:(int)length skip:(int)skip type:(int)type
{
    if (type == CRFSensorTypeAcc) {
        if (skip >= length) {
            [dataList removeAllObjects];
            for (int i = 0; i < self.mWindowSize; i++) {
                [dataList addObject:[NSNull null]];
            }
            self.mCombineDataCount = 0;
        } else {
            for (int i = skip; i < length; i++) {
                dataList[i-skip] = dataList[i];
            }
            self.mCombineDataCount -= skip;
        }
    } else if (type == CRFSensorTypeGyro) {
        if (skip >= length) {
            [dataList removeAllObjects];
            for (int i = 0; i < self.mBufferSize; i++) {
                [dataList addObject:[NSNull null]];
            }
            self.mCombineGyroBufferCount = 0;
        } else {
            for (int i = skip; i < length; i++) {
                dataList[i-skip] = dataList[i];
            }
            self.mCombineGyroBufferCount -= skip;
        }
    }
}

- (void)fillAccData:(NSMutableArray *)dataList buffer:(NSMutableArray *)buffer count:(int)count
{
    for (int i = 0; i < count; i++) {
        dataList[self.mAccDataCount++] = buffer[i];
    }
    for (int i = count; i < self.mAccBufferCount; i++) {
        buffer[i-count] = buffer[i];
    }
    self.mAccBufferCount -= count;
}

- (void)fillCombineData:(NSMutableArray *)dataList buffer:(NSMutableArray *)buffer count:(int)count
{
    for (int i = 0; i < count; i++) {
        dataList[self.mCombineDataCount++] = buffer[i];
    }
    for (int i = count; i < self.mCombineBufferCount; i++) {
        buffer[i-count] = buffer[i];
    }
    self.mCombineBufferCount -= count;
}

#pragma mark - Public API

- (void)addAccData:(CRFSensorRawData *)accData
{
    if (accData == nil) return;
    
    @synchronized(self.mAccSyncObject) {
        self.mAccDataBuffer[self.mAccBufferCount++] = accData;
    }
    NSArray *values = accData.values;
    if (self.startTime == -1) {
        self.startTime = accData.timestamp;
    }
    double combineData = (double)sqrt([values[0] doubleValue] * [values[0] doubleValue] + [values[1] doubleValue] * [values[1] doubleValue] + [values[2] doubleValue] * [values[2] doubleValue]);
    @synchronized(self.mCombineAccSyncObject) {
        self.mCombineAccDataBuffer[self.mCombineBufferCount++] = @(combineData);
    }
}

- (void)addGyroData:(CRFSensorRawData *)gyroData
{
    if (gyroData == nil) return;
    self.mGyroSampleCount++;
    if (self.mGyroSampleCount % GYRO_SAMPLE_RATE != 0) return;
    self.mGyroSampleCount = 0;
    @synchronized(self.mGyroSyncObject) {
        self.mGyroDataBuffer[self.mGyroBufferCount++] = gyroData;
    }
    NSArray *values = gyroData.values;
    double combineData = (double)sqrt([values[0] doubleValue] * [values[0] doubleValue] + [values[1] doubleValue] * [values[1] doubleValue] + [values[2] doubleValue] * [values[2] doubleValue]);
    @synchronized(self.mCombineGyroSyncObject) {
        self.mCombineGyroDataBuffer[self.mCombineGyroBufferCount++] = @(combineData);
    }
}

#pragma mark - MathUtils

- (double)stdWithDataList:(NSArray *)dataList start:(int)start end:(int)end
{
    double mean = [self meanWithDataList:dataList start:start end:end];
    double std = 0;
    if (start == end) return 0;
    for (int i = start; i < end; i++) {
        std += ([dataList[i] doubleValue] - mean) * ([dataList[i] doubleValue] - mean);
    }
    return (double)sqrt(std/(end-start));
}

- (double)meanWithDataList:(NSArray *)dataList start:(int)start end:(int)end
{
    if (start == end) return 0;
    double sum = 0;
    for (int i = start; i < end; i++) {
        sum += [dataList[i] doubleValue];
    }
    
    return sum / (end-start);
}

- (BOOL)hasDataAboveValueWithDataList:(NSArray *)dataList start:(int)start end:(int)end value:(double)value
{
    for (int i = start; i < end; i++) {
        if ([dataList[i] doubleValue] > value) {
            return YES;
        }
    }
    return NO;
}

- (double)getAutoCorrelation:(int)stepSample
{
    if (2 * stepSample >= self.mCombineDataCount) return 0;
    
    double mean1 = [self meanWithDataList:self.mCombineAccDataList start:0 end:stepSample];
    double mean2 = [self meanWithDataList:self.mCombineAccDataList start:stepSample end:2*stepSample];
    double std1 = [self stdWithDataList:self.mCombineAccDataList start:0 end:stepSample];
    double std2 = [self stdWithDataList:self.mCombineAccDataList start:stepSample end:2*stepSample];
    double correlation = 0;
    
    for (int i = 0; i < stepSample; i++) {
        correlation += ([self.mCombineAccDataList[i] doubleValue] - mean1) * ([self.mCombineAccDataList[i+stepSample] doubleValue] - mean2);
    }
    
    return correlation / (stepSample*std1*std2);
}

@end
