//
//  CRFPin.m
//  CRF
//
//  Created by Siyan HU on 17/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import "CRFPin.h"
#define PIN_RADIUS 20

@implementation CRFPin

- (instancetype)initWithColor:(CRFPinColor)color withCoverage:(double)coverage
{
    if (self = [super initWithFrame:CGRectMake(0, 0, coverage * 2, coverage * 2)]) {
        UIColor *themeColor;
        switch (color) {
            case CRFPinColorOrange:
                themeColor = [UIColor colorWithRed:255/255.0 green:153/255.0 blue:35/255.0 alpha:1.0f];
                break;
                
            case CRFPinColorYellow:
                themeColor = [UIColor colorWithRed:229/25.0 green:204/255.0 blue:104/255.0 alpha:1.0f];
                break;
                
            case CRFPinColorRed:
                themeColor = [UIColor colorWithRed:211/255.0 green:57/255.0 blue:159/255.0 alpha:1.0f];
                break;
                
            case CRFPinColorBlue:
                themeColor = [UIColor colorWithRed:75/255.0 green:135/255.0 blue:203/255.0 alpha:1.0f];
                break;
                
            default:
                break;
        }
        
        self.layer.cornerRadius = coverage;
        self.layer.borderColor = [themeColor colorWithAlphaComponent:0.8].CGColor;
        self.layer.borderWidth = 2;
        self.backgroundColor = [themeColor colorWithAlphaComponent:0.4f];
        
        UIView *pinView = [[UIView alloc] initWithFrame:CGRectMake(coverage-PIN_RADIUS, coverage-PIN_RADIUS, PIN_RADIUS * 2, PIN_RADIUS * 2)];
        pinView.backgroundColor = themeColor;
        pinView.layer.borderColor = [UIColor whiteColor].CGColor;
        pinView.layer.borderWidth = 6;
        pinView.layer.cornerRadius = PIN_RADIUS;
        pinView.layer.shadowColor = [[UIColor blackColor] CGColor];
        pinView.layer.shadowRadius = 2;
        pinView.layer.shadowOffset = CGSizeMake(0, 0);
        pinView.layer.shadowOpacity = 10;
        
        [self addSubview:pinView];
    }
    
    return self;
}

@end
