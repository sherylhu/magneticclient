//
//  CRFPolygon.m
//  CRF
//
//  Created by Siyan HU on 27/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import "CRFPolygon.h"
#import "CRFLine.h"

@interface CRFBoundingBox ()

@property (assign, nonatomic) double xMax;
@property (assign, nonatomic) double xMin;
@property (assign, nonatomic) double yMax;
@property (assign, nonatomic) double yMin;

@end

@implementation CRFBoundingBox

- (instancetype)init
{
    if (self = [super init]) {
        self.xMax = -MAXFLOAT;
        self.xMin = -MAXFLOAT;
        self.yMax = -MAXFLOAT;
        self.yMin = -MAXFLOAT;
    }
    
    return self;
}

@end

@interface CRFBuilder ()

@property (strong, nonatomic) NSMutableArray *vertexes;
@property (strong, nonatomic) NSMutableArray *sides;
@property (strong, nonatomic) CRFBoundingBox *boundingBox;

@property (assign, nonatomic) BOOL firstPoint;
@property (assign, nonatomic) BOOL isClosed;

@end

@implementation CRFBuilder

- (NSMutableArray *)vertexes
{
    if (!_vertexes) {
        _vertexes = [NSMutableArray array];
    }
    
    return _vertexes;
}

- (NSMutableArray *)sides
{
    if (!_sides) {
        _sides = [NSMutableArray array];
    }
    
    return _sides;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.firstPoint = YES;
        self.isClosed = NO;
    }
    
    return self;
}

- (instancetype)addVertex:(CGPoint)point
{
    if (self.isClosed) {
        self.vertexes = [NSMutableArray array];
        self.isClosed = NO;
    }
    
    [self updateBoundingBox:point];
    [self.vertexes addObject:[NSValue valueWithCGPoint:point]];
    
    if (self.vertexes.count > 1) {
        CRFLine *line = [CRFLine lineWithStart:[self.vertexes[self.vertexes.count-2] CGPointValue] end:point];
        [self.sides addObject:line];
    }
    
    return self;
}

- (instancetype)close
{
    [self validate];
    
    [self.sides addObject:[CRFLine lineWithStart:[self.vertexes[self.vertexes.count-1] CGPointValue] end:[self.vertexes[0] CGPointValue]]];
    self.isClosed = YES;
    
    return self;
}

- (CRFPolygon *)build
{
    [self validate];
    
    if (!self.isClosed) {
        [self.sides addObject:[CRFLine lineWithStart:[self.vertexes[self.vertexes.count-1] CGPointValue] end:[self.vertexes[0] CGPointValue]]];
    }
    
    CRFPolygon *polygon = [CRFPolygon polygonWithSides:self.sides boundingBox:self.boundingBox];
    return polygon;
}

- (void)updateBoundingBox:(CGPoint)point
{
    if (self.firstPoint) {
        self.boundingBox = [[CRFBoundingBox alloc] init];
        self.boundingBox.xMax = point.x;
        self.boundingBox.xMin = point.x;
        self.boundingBox.yMax = point.y;
        self.boundingBox.yMin = point.y;
        
        self.firstPoint = NO;
    } else {
        if (point.x > self.boundingBox.xMax) {
            self.boundingBox.xMax = point.x;
        } else if (point.x < self.boundingBox.xMin) {
            self.boundingBox.xMin = point.x;
        } else if (point.y > self.boundingBox.yMax) {
            self.boundingBox.yMax = point.y;
        } else if (point.y < self.boundingBox.yMin) {
            self.boundingBox.yMin = point.y;
        }
    }
}

- (void)validate
{
    if (self.vertexes.count < 3) {
        NSLog(@"[ERROR]: Polygon must have at least 3 points");
    }
}

@end

@interface CRFPolygon ()

@property (strong ,nonatomic) CRFBoundingBox *boundingBox;
@property (strong, nonatomic) NSArray *sides;

@end

@implementation CRFPolygon

+ (instancetype)polygonWithSides:(NSArray *)sides boundingBox:(CRFBoundingBox *)boundingBox
{
    CRFPolygon *polygon = [[CRFPolygon alloc] init];
    polygon.sides = [sides mutableCopy];
    polygon.boundingBox = boundingBox;
    return polygon;
}

+ (CRFBuilder *)builder
{
    return [[CRFBuilder alloc] init];
}

- (BOOL)contains:(CGPoint)point
{
    if ([self inBoundingBox:point]) {
        CRFLine *ray = [self createRay:point];
        int intersection = 0;
        
        for (CRFLine *side in self.sides) {
            if ([self intersectionRay:ray side:side]) {
                intersection++;
            }
        }
        
        if (intersection % 2 == 1) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)intersectionRay:(CRFLine *)ray side:(CRFLine *)side
{
    CGPoint intersectPoint;
    
    if (!ray.vertical && !side.vertical) {
        if (ray.a - side.a == 0) {
            return NO;
        }
        
        double x = ((side.b-ray.b) / (ray.a-side.a));
        double y = side.a*x + side.b;
        intersectPoint = CGPointMake(x, y);
    } else if (ray.vertical && !side.vertical) {
        double x = ray.start.x;
        double y = side.a*x + side.b;
        intersectPoint = CGPointMake(x, y);
    } else if (!ray.vertical && side.vertical) {
        double x = side.start.x;
        double y = ray.a*x + ray.b;
        intersectPoint = CGPointMake(x, y);
    } else {
        return NO;
    }
    
    if ([side isInside:intersectPoint] && [ray isInside:intersectPoint]) {
        return YES;
    }
    
    return NO;
}

- (CRFLine *)createRay:(CGPoint)point
{
    double epsilon = (self.boundingBox.xMax - self.boundingBox.xMin) / 100.0f;
    CGPoint outsidePoint = CGPointMake(self.boundingBox.xMin-epsilon, self.boundingBox.yMin);
    
    CRFLine *vector = [CRFLine lineWithStart:outsidePoint end:point];
    return vector;
}

- (BOOL)inBoundingBox:(CGPoint)point
{
    if (point.x < self.boundingBox.xMin || point.x > self.boundingBox.xMax || point.y < self.boundingBox.yMin || point.y > self.boundingBox.yMax) {
        return NO;
    }
    
    return YES;
}

@end
