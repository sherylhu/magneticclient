//
//  CRFHeatMap.h
//  CRF
//
//  Created by Samuel Chow on 22/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFHeatMap : NSObject

@property (strong, nonatomic) NSMutableArray *vectorX;
@property (strong, nonatomic) NSMutableArray *vectorY;

@property (strong, nonatomic) NSMutableArray *boundary_X;
@property (strong, nonatomic) NSMutableArray *boundary_Y;

@property (assign, nonatomic) double gridX;
@property (assign, nonatomic) double gridY;

@property (strong, nonatomic) NSMutableArray *visual;

@property (strong, nonatomic) CRFAreaSetting *setting;

- (instancetype)initWithSetting:(CRFAreaSetting *)setting;
+ (NSArray *)findInterval:(NSArray *)trace step:(int)step;

@end
