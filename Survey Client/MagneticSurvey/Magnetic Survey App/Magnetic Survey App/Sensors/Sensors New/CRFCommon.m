//
//  CRFCommon.m
//  CRF
//
//  Created by Siyan HU on 17/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import "CRFCommon.h"

@implementation CRFCommon

+ (long)getCurrentTimestampNanoSecond
{
    long ret = [[NSDate date] timeIntervalSince1970] * 1000000000;
    return ret;
}

+ (long)getCurrentTimestampMilliSecond
{
    long ret = [[NSDate date] timeIntervalSince1970]*1000;
    return ret;
}

+ (CGPoint)locationString2Point:(NSString *)locString
{
    NSArray *arr = [locString componentsSeparatedByString:@","];
    return CGPointMake([arr[0] intValue], [arr[1] intValue]);
}

+ (BOOL)pointIsValid:(CGPoint)point
{
    return !isnan(point.x) && !isnan(point.y) && point.x > 0 && point.y > 0;
}

#pragma mark - MathUtil

+ (double)mean:(NSArray *)array
{
    double sum = 0.0;
    for (NSNumber *num in array) {
        sum += [num doubleValue];
    }
    return sum/array.count;
}

+ (double)euclideanFrom:(CGPoint)from to:(CGPoint)to
{
    return sqrt(pow(from.x-to.x, 2) + pow(from.y-to.y, 2));
}

+ (NSArray *)getRotationMatrixFromGravity:(NSArray *)gravity geoMagnetic:(NSArray *)geomagnetic
{
    float Ax = [gravity[0] doubleValue];
    float Ay = [gravity[1] doubleValue];
    float Az = [gravity[2] doubleValue];
    
    float normsqA = (Ax*Ax + Ay*Ay + Az*Az);
    float g = 9.81f;
    float freeFallGravitySquared = 0.01f * g * g;
    if (normsqA < freeFallGravitySquared) {
        // gravity less than 10% of normal values
        return nil;
    }
    
    float Ex = [geomagnetic[0] doubleValue];
    float Ey = [geomagnetic[1] doubleValue];
    float Ez = [geomagnetic[2] doubleValue];
    float Hx = Ey*Az - Ez*Ay;
    float Hy = Ez*Ax - Ex*Az;
    float Hz = Ex*Ay - Ey*Ax;
    float normH = (float)sqrt(Hx*Hx + Hy*Hy + Hz*Hz);
    
    if (normH < 0.1f) {
        // device is close to free fall or close to magnetic north pole
        return nil;
    }
    
    float invH = 1.0f / normH;
    Hx *= invH;
    Hy *= invH;
    Hz *= invH;
    float invA = 1.0f / (float)sqrt(Ax*Ax + Ay*Ay + Az*Az);
    Ax *= invA;
    Ay *= invA;
    Az *= invA;
    
    float Mx = Ay*Hz - Az*Hy;
    float My = Az*Hx - Ax*Hz;
    float Mz = Ax*Hy - Ay*Hx;
    
    return @[@(Hx), @(Hy), @(Hz),
             @(Mx), @(My), @(Mz),
             @(Ax), @(Ay), @(Az)];
}

+ (double)rotateOrientation:(double)x init:(double)init
{
    double y;
    if (x >= -(90-init))
        y = x-(init+90);
    else
        y = x+(180+90-init);
    while (y > 180)
        y -= 180;
    while (y < -180)
        y += 180;
    return y;
}

+ (double)cosineSimilarityWithA:(NSDictionary *)A B:(NSDictionary *)B
{
    double innerProduct = 0.0;
    double normA = 0.0;
    double normB = 0.0;
    
    NSArray *keys = [[NSSet setWithArray:[[A allKeys] arrayByAddingObjectsFromArray:[B allKeys]]] allObjects];
    
    for (NSString *key in keys) {
        double Ai = A[key] != nil ? [A[key] doubleValue] : 0.0;
        double Bi = B[key] != nil ? [B[key] doubleValue] : 0.0;
        
        innerProduct += Ai*Bi;
        normA += Ai*Ai;
        normB += Bi*Bi;
    }
    
    return (normA != 0.0 && normB != 0.0) ? innerProduct*1.0/(sqrt(normA)*sqrt(normB)) : 0.0;
}

#pragma mark - FileUtil

+ (NSArray *)loadTraceFile:(NSString *)fileName orientationCalibration:(double)calibration
{
    NSArray *fileComponent = [fileName componentsSeparatedByString:@"."];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileComponent[0] ofType:fileComponent[1]];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    NSMutableArray *traceArray = [NSMutableArray array];
    if (fileData) {
        NSArray *rows = [[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
        for (NSString *row in rows) {
            if (row.length == 0) continue;
            NSArray *values = [row componentsSeparatedByString:@" "];
            double intensity = [values[0] doubleValue];
            double vertical = [values[1] doubleValue];
            double horizontal = [values[2] doubleValue];
            double orientation = fabs(calibration) < 1e-6 ? [values[3] doubleValue] : [self rotateOrientation:[values[3] doubleValue] init:calibration];
            double timestamp = [values[4] doubleValue];
            double step = [values[5] doubleValue];
            CRFSensorData *sensorData = [[CRFSensorData alloc] init];
            sensorData.intensity = intensity;
            sensorData.vertical = vertical;
            sensorData.horizontal = horizontal;
            sensorData.orientation = orientation;
            sensorData.timestamp = timestamp;
            sensorData.step = step;
            if (values.count > 6 && values.count%2 == 0) {
                NSMutableDictionary *beacon_vector = [NSMutableDictionary dictionary];
                for (int i = 6; i < values.count; i+=2) {
                    beacon_vector[values[i]] = @([values[i+1] doubleValue]);
                }
                sensorData.beacon_vector = beacon_vector;
            }
            [traceArray addObject:sensorData];
        }
    }
    return [traceArray copy];
}

+ (NSArray *)loadTraceFile:(NSString *)fileName
{
    return [self loadTraceFile:fileName orientationCalibration:0];
}

+ (NSArray *)loadRouteFile:(NSString *)fileName
{
    NSArray *fileComponent = [fileName componentsSeparatedByString:@"."];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileComponent[0] ofType:fileComponent[1]];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    NSMutableArray *routeArray = [NSMutableArray array];
    if (fileData) {
        NSArray *rows = [[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
        for (NSString *row in rows) {
            NSArray *values = [row componentsSeparatedByString:@" "];
            if (values.count != 2) continue;
            double x = [values[0] doubleValue];
            double y = [values[1] doubleValue];
            CGPoint point = CGPointMake(x, y);
            [routeArray addObject:[NSValue valueWithCGPoint:point]];
        }
    }
    return [routeArray copy];
}

+ (NSArray *)loadBeaconFile:(NSString *)fileName
{
    NSArray *fileComponent = [fileName componentsSeparatedByString:@"."];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileComponent[0] ofType:fileComponent[1]];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    NSMutableArray *beaconArray = [NSMutableArray array];
    if (fileData) {
        NSArray *rows = [[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
        for (int i = 0; i < rows.count; i++) {
            NSString *row = rows[i];
            NSArray *values = [row componentsSeparatedByString:@" "];
            NSString *uuid = values[0];
            int major = [values[1] intValue];
            int minor = [values[2] intValue];
            int x = [values[3] intValue];
            int y = [values[4] intValue];
            CRFBeacon *bb = [[CRFBeacon alloc] initWithName:[NSString stringWithFormat:@"Atrium_%d", (i+1)] uuid:[[NSUUID alloc] initWithUUIDString:uuid] major:major minor:minor position:CGPointMake(x, y)];
            if (values.count > 5) {
                double rssi = [values[5] doubleValue];
                double thres = [values[6] doubleValue];
                bb.threshold_dist = thres;
                bb.threshold_rssi = rssi;
            }
            [beaconArray addObject:bb];
        }
    }

    return [beaconArray copy];
}

+ (NSArray *)loadConstraint:(NSString *)fileName
{
    NSMutableArray *ret = [NSMutableArray array];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileName componentsSeparatedByString:@"."][0] ofType:[fileName componentsSeparatedByString:@"."][1]];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    if (fileData) {
        NSArray *rows = [[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
        for (NSString *row in rows) {
            NSArray *values = [row componentsSeparatedByString:@" "];
            if (values.count != 2) continue;
            double x = [values[0] doubleValue];
            double y = [values[1] doubleValue];
            [ret addObject:@[@(x), @(y)]];
        }
        
        if (ret.count>0 &&
            fabs([ret[0][0] doubleValue]-[ret[ret.count-1][0] doubleValue]) < 1e-3 &&
            fabs([ret[0][1] doubleValue]-[ret[ret.count-1][1] doubleValue]) < 1e-3) {
            [ret removeObjectAtIndex:ret.count-1];
        }
    }
    return [ret copy];
}

+ (NSDictionary *)loadSectj:(NSString *)fileName
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileName componentsSeparatedByString:@"."][0] ofType:[fileName componentsSeparatedByString:@"."][1]];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    if (fileData) {
        NSArray *rows = [[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
        for (NSString *row in rows) {
            if (row.length == 0) break;
            NSArray *rowa = [row componentsSeparatedByString:@"_"];
            dict[[[rowa subarrayWithRange:NSMakeRange(0, 5)] componentsJoinedByString:@"_"]] = @([rowa[5] doubleValue]);
        }        
    }
    return [dict copy];
}

+ (CRFAreaSetting *)loadSiteSetting:(int)areaId
{
    CRFAreaSetting *setting = [[CRFAreaSetting alloc] init];
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%d", areaId] ofType:@"config"];
    NSData *resourceData = [NSData dataWithContentsOfFile:resourcePath];
    if (resourceData) {
        NSArray *rows = [[[NSString alloc] initWithData:resourceData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"\n"];
        for (NSString *row in rows) {
            NSString *type = [row componentsSeparatedByString:@":"][0];
            NSArray *content = [[row componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "];
            if ([type isEqualToString:@"trace"]) {
                setting.trace = content;
            } else if ([type isEqualToString:@"route"]) {
                setting.route = content;
            } else if ([type isEqualToString:@"angle"]) {
                double angle = [content[0] doubleValue];
                setting.angle = angle;
            } else if ([type isEqualToString:@"inconstraint"]) {
                setting.inconstraint = content;
            } else if ([type isEqualToString:@"outconstraint"]) {
                setting.outconstraint = content[0];
            } else if ([type isEqualToString:@"heatmap"]) {
                setting.heatmap = content[0];
            } else if ([type isEqualToString:@"turning"]) {
                NSMutableArray *turning = [NSMutableArray array];
                for (int i = 0; i < content.count; i+=3) {
                    [turning addObject:[content subarrayWithRange:NSMakeRange(i, 3)]];
                }
                setting.turning = [turning copy];
            } else if ([type isEqualToString:@"orientation"]) {
                setting.orientation = [content[0] doubleValue];
            } else if ([type isEqualToString:@"scale"]) {
                setting.scale = [content[0] doubleValue];
            } else if ([type isEqualToString:@"radiomap"]) {
                setting.radiomap = content;
            } else if ([type isEqualToString:@"beacon"]) {
                setting.beacon = content[0];
            } else if ([type isEqualToString:@"sectj"]) {
                setting.sectj = [CRFCommon loadSectj:content[0]];
            }
        }
    }
    return setting;
}

@end
