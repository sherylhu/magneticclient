//
//  CRFSensorManager.m
//  Sensor
//
//  Created by Siyan HU on 3/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFSensorManager.h"
#import "CRFBeaconManager.h"
#import "CRFStepDetector.h"
#import "CRFSensorRawData.h"
#import "CRFCompass.h"

#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>

#define SENSORS_COUNT 5
#define ENABLE_GYRO_ORIENTATION YES

#define MAGNETIC_FIELD_WINDOW 0.02
#define BEACON_WINDOW 3

#define kFilteringFactor 0.25f

typedef enum : NSUInteger {
    CRFSensorMagneticField = 0,
    CRFSensorRotationVector = 1,
    CRFSensorAccelerometer = 2,
    CRFSensorGyroscope = 3,
    CRFSensorGravity = 4,
} CRFSensor;

@interface CRFSensorManager () <CLLocationManagerDelegate, CRFStepDetectorDelegate, CRFCompassDelegate>

@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSOperationQueue *altimeterQueue;
@property (strong, nonatomic) NSOperationQueue *motionQueue;
@property (strong, nonatomic) NSOperationQueue *accelerometerQueue;
@property (strong, nonatomic) NSOperationQueue *gyroQueue;

@property (strong, nonatomic) CMAltimeter *altimeter;
@property (strong, nonatomic) CMDeviceMotion *deviceMotion;

@property (strong, nonatomic) CMAccelerometerData *accelerometerData;
@property (strong, nonatomic) CMGyroData *gyroData;
@property (strong, nonatomic) CMAltitudeData *altitudeData;
@property (strong, nonatomic) CLHeading *heading;

@property (assign, nonatomic) int mWindowSize;
@property (assign, nonatomic) int mCurrentStep;

@property (strong, nonatomic) NSMutableArray *mDecompositionMagData;
@property (strong, nonatomic) NSMutableArray *mOrientationData;

@property (strong, nonatomic) NSMutableArray *pastValues;
@property (strong, nonatomic) NSMutableArray *pastValuesFilter;

@property (strong, nonatomic) NSMutableDictionary *beaconDictionary;

@property (strong, nonatomic) CRFStepDetector *mStepDetector;
@property (strong, nonatomic) CRFCompass *mCompass;

@property (strong, nonatomic) id mSensorSyncObject;
@property (strong, nonatomic) id mBeaconSyncObject;

@property (assign, nonatomic) BOOL isRecording;
@property (assign, nonatomic) BOOL isNewStep;

@property (strong, nonatomic) NSTimer *beaconTimer;
@property (strong, nonatomic) NSTimer *headingTimer;

@end

@implementation CRFSensorManager

#pragma mark - Initialization

- (instancetype)init
{
    if (self = [super init]) {
        // initialize variables
        self.mSensorSyncObject = [NSDate date];
        self.mBeaconSyncObject = [NSDate date];
        self.mStepDetector = [[CRFStepDetector alloc] initWithSamplePerSecond:90];
        self.mStepDetector.delegate = self;
        self.mWindowSize = 50;
        self.mDecompositionMagData = [NSMutableArray array];
        self.mOrientationData = [NSMutableArray array];
        self.pastValues = [NSMutableArray array];
        self.pastValuesFilter = [NSMutableArray array];
        self.beaconDictionary = [NSMutableDictionary dictionary];
        self.mCompass = [[CRFCompass alloc] init];
        self.mCompass.delegate = self;
        self.isRecording = NO;
        
        for (int i = 0; i < SENSORS_COUNT; ++i) {
            [self.pastValues addObject:[NSMutableArray array]];
            [self.pastValuesFilter addObject:[NSMutableArray arrayWithArray:@[@(-99999)]]];
        }
        
    }
    
    return self;
}

#pragma mark - Lifecycle

- (void)begin
{
    if (!self.isRecording) {
        self.isRecording = YES;
        self.isNewStep = YES;
        self.mCurrentStep = 0;
        [self registerSensors];
        [self.mStepDetector start];
        [self.mCompass start:NO];
    }
}

- (void)stop
{
    if (self.beaconTimer != nil) {
        [self.beaconTimer invalidate];
        self.beaconTimer = nil;
    }
    if (self.headingTimer != nil) {
        [self.headingTimer invalidate];
        self.headingTimer = nil;
    }
    
    self.isRecording = NO;
    [self unregisterSensors];
    [self.mStepDetector stop];
    [self.mCompass stop];
    [self clearData];
}

- (void)clearData
{
    self.mStepDetector = [[CRFStepDetector alloc] initWithSamplePerSecond:90];
    self.mStepDetector.delegate = self;
    self.mWindowSize = 50;
    self.mDecompositionMagData = [NSMutableArray array];
    self.mOrientationData = [NSMutableArray array];
    self.mCompass = [[CRFCompass alloc] init];
    self.mCompass.delegate = self;
    self.pastValues = [NSMutableArray array];
    self.pastValuesFilter = [NSMutableArray array];
    self.beaconDictionary = [NSMutableDictionary dictionary];
    self.isRecording = NO;
    
    for (int i = 0; i < SENSORS_COUNT; ++i) {
        [self.pastValues addObject:[NSMutableArray array]];
        [self.pastValuesFilter addObject:[NSMutableArray arrayWithArray:@[@(-99999)]]];
    }
}

- (void)unregisterSensors
{
    [self.altimeterQueue cancelAllOperations];
    [self.motionQueue cancelAllOperations];
    [self.accelerometerQueue cancelAllOperations];
    [self.gyroQueue cancelAllOperations];
    [self.motionManager stopAccelerometerUpdates];
    [self.altimeter stopRelativeAltitudeUpdates];
    [self.motionManager stopDeviceMotionUpdates];
    [self.motionManager stopGyroUpdates];
    [self.motionManager stopMagnetometerUpdates];
    [self.locationManager stopUpdatingHeading];
    
    if (self.beaconManager.beaconArray.count > 0) {
        for (CRFBeacon *beacon in self.beaconManager.beaconArray) {
            CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:beacon.uuid major:beacon.majorValue minor:beacon.minorValue identifier:beacon.name];
            [self.locationManager stopMonitoringForRegion:region];
            [self.locationManager stopRangingBeaconsInRegion:region];
        }
    }
}

- (void)registerSensors
{
    if ([CMAltimeter isRelativeAltitudeAvailable]) {
        [self.altimeter startRelativeAltitudeUpdatesToQueue:self.altimeterQueue withHandler:^(CMAltitudeData * _Nullable altitudeData, NSError * _Nullable error) {
            @synchronized(self.mSensorSyncObject) {
                if (!self.isRecording) return;
                self.altitudeData = altitudeData;
            }
        }];
    }
    
    [self.motionManager startDeviceMotionUpdatesToQueue:self.motionQueue withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
        @synchronized(self.mSensorSyncObject) {
            if (!self.isRecording) return;
            [self.mCompass updateGravityX:motion.gravity.x*-10.0 y:motion.gravity.y*-10.0 z:motion.gravity.z*-10.0];
            [self.mCompass updateQuaternionX:-motion.attitude.quaternion.x y:-motion.attitude.quaternion.y z:-motion.attitude.quaternion.z w:-motion.attitude.quaternion.w];
            self.deviceMotion = motion;
        }
    }];
    
    [self.motionManager startAccelerometerUpdatesToQueue:self.accelerometerQueue withHandler:^(CMAccelerometerData * _Nullable accelerometerData, NSError * _Nullable error) {
        @synchronized(self.mSensorSyncObject) {
            if (!self.isRecording) return;
            self.accelerometerData = accelerometerData;
            
            NSArray *values = @[@(accelerometerData.acceleration.x*-10.0), @(accelerometerData.acceleration.y*-10.0), @(accelerometerData.acceleration.z*-10.0)];
            NSArray *newValues = [self _filter_and_smoothing:CRFSensorAccelerometer values:values];
            CRFSensorRawData * sd = [[CRFSensorRawData alloc] initWithTimestamp:[CRFCommon getCurrentTimestampNanoSecond] values:newValues step:self.mCurrentStep];
            [self.mStepDetector addAccData:sd];
        }
    }];
    
    [self.motionManager startGyroUpdatesToQueue:self.gyroQueue withHandler:^(CMGyroData * _Nullable gyroData, NSError * _Nullable error) {
        @synchronized(self.mSensorSyncObject) {
            if (!self.isRecording) return;
            [self.mCompass updateGyroX:gyroData.rotationRate.x y:gyroData.rotationRate.y z:gyroData.rotationRate.z];

            self.gyroData = gyroData;
            NSArray *values = @[@(gyroData.rotationRate.x), @(gyroData.rotationRate.y), @(gyroData.rotationRate.z)];
            NSArray *newValues = [self _filter_and_smoothing:CRFSensorGyroscope values:values];
            CRFSensorRawData * sd = [[CRFSensorRawData alloc] initWithTimestamp:[CRFCommon getCurrentTimestampNanoSecond] values:newValues step:self.mCurrentStep];
            [self.mStepDetector addGyroData:sd];
        }
    }];
    
    self.headingTimer = [NSTimer scheduledTimerWithTimeInterval:MAGNETIC_FIELD_WINDOW target:self.locationManager selector:@selector(startUpdatingHeading) userInfo:nil repeats:YES];
    self.beaconTimer = [NSTimer scheduledTimerWithTimeInterval:BEACON_WINDOW target:self selector:@selector(updateBeaconVector) userInfo:nil repeats:YES];
    
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [self.locationManager requestAlwaysAuthorization];

    if (self.beaconManager.beaconArray.count > 0) {
        for (CRFBeacon *beacon in self.beaconManager.beaconArray) {
            CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:beacon.uuid major:beacon.majorValue minor:beacon.minorValue identifier:beacon.name];
            [self.locationManager startMonitoringForRegion:region];
            [self.locationManager startRangingBeaconsInRegion:region];
        }
    }
}

- (void)updateBeaconVector
{
    if (self.beaconDictionary.count > 0) {
        NSMutableDictionary *beacon_vector = [NSMutableDictionary dictionary];
        for (NSString *key in self.beaconDictionary) {
            beacon_vector[key] = @([CRFCommon mean:self.beaconDictionary[key]]);
        }
        [self.delegate sensorManager:self didUpdateSensorDataType:CRFSensorDataTypeBeacon data:beacon_vector];
    }
}

#pragma mark - MathUtil

+ (NSArray *)getNormalCoordinate:(NSArray *)rotationMatrix data:(NSArray *)data
{
    NSArray *result = nil;
    
    if (data.count == 3) {
        result = @[@([rotationMatrix[0] doubleValue]*[data[0] doubleValue] + [rotationMatrix[1] doubleValue]*[data[1] doubleValue] + [rotationMatrix[2] doubleValue]*[data[2] doubleValue]),
                   @([rotationMatrix[3] doubleValue]*[data[0] doubleValue] + [rotationMatrix[4] doubleValue]*[data[1] doubleValue] + [rotationMatrix[5] doubleValue]*[data[2] doubleValue]),
                   @([rotationMatrix[6] doubleValue]*[data[0] doubleValue] + [rotationMatrix[7] doubleValue]*[data[1] doubleValue] + [rotationMatrix[8] doubleValue]*[data[2] doubleValue])];
    } else if (data.count == 6) {
        result = @[@([rotationMatrix[0] doubleValue]*[data[0] doubleValue] + [rotationMatrix[1] doubleValue]*[data[1] doubleValue] + [rotationMatrix[2] doubleValue]*[data[2] doubleValue]),
                   @([rotationMatrix[3] doubleValue]*[data[0] doubleValue] + [rotationMatrix[4] doubleValue]*[data[1] doubleValue] + [rotationMatrix[5] doubleValue]*[data[2] doubleValue]),
                   @([rotationMatrix[6] doubleValue]*[data[0] doubleValue] + [rotationMatrix[7] doubleValue]*[data[1] doubleValue] + [rotationMatrix[8] doubleValue]*[data[2] doubleValue]),
                   @([rotationMatrix[0] doubleValue]*[data[3] doubleValue] + [rotationMatrix[1] doubleValue]*[data[4] doubleValue] + [rotationMatrix[2] doubleValue]*[data[5] doubleValue]),
                   @([rotationMatrix[3] doubleValue]*[data[3] doubleValue] + [rotationMatrix[4] doubleValue]*[data[4] doubleValue] + [rotationMatrix[5] doubleValue]*[data[5] doubleValue]),
                   @([rotationMatrix[6] doubleValue]*[data[3] doubleValue] + [rotationMatrix[7] doubleValue]*[data[4] doubleValue] + [rotationMatrix[8] doubleValue]*[data[5] doubleValue])];
    }
    
    return result;
}

+ (double)calibrateOrientation:(double)old_orientation
{
    if (old_orientation <= 180) {
        return -old_orientation;
    } else {
        return 360 - old_orientation;
    }
}

+ (NSMutableArray *)_calculate_average:(NSArray *)input
{
    NSMutableArray *ret = [NSMutableArray array];
    NSArray *inputreplace = input[0];
    int inputcount = (int)[inputreplace count];
    for (int i = 0; i < inputcount; ++i) {
        [ret addObject:@(0.0)];
    }
    for (int i = 0; i < input.count; ++i) {
        NSArray *past = input[i];
        for (int j = 0; j < ret.count; ++j) {
            ret[j] = @([ret[j] doubleValue] + [past[j] doubleValue]);
        }
    }
    for (int i = 0; i < ret.count; ++i) {
        ret[i] = @([ret[i] doubleValue]/input.count);
    }
    return ret;
}

- (NSMutableArray *)_filter_and_smoothing:(CRFSensor)index values:(NSArray *)values
{
    NSMutableArray *now = self.pastValues[index];
    [now addObject:values];
    if (now.count >= 6) [now removeObjectAtIndex:0];
    NSMutableArray *smoothing = [CRFSensorManager _calculate_average:now];
    NSMutableArray *ret = self.pastValuesFilter[index];
    if (ret.count == 1 && fabs([ret[0] doubleValue] + 99999) < 1e-5) {
        self.pastValuesFilter[index] = smoothing;
        return smoothing;
    }
    for (int i = 0; i < ret.count; ++i) {
        ret[i] = @([ret[i] doubleValue] + kFilteringFactor * ([smoothing[i] doubleValue]-[ret[i] doubleValue]));
    }
    self.pastValuesFilter[index] = ret;
    return ret;
}

#pragma mark - Mutator

- (CMAltimeter *)altimeter
{
    if (!_altimeter) {
        _altimeter = [[CMAltimeter alloc] init];
    }
    
    return _altimeter;
}

- (CMMotionManager *)motionManager
{
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
    }
    
    return _motionManager;
}

- (CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    
    return _locationManager;
}

- (NSOperationQueue *)altimeterQueue
{
    if (!_altimeterQueue) {
        _altimeterQueue = [[NSOperationQueue alloc] init];
        _altimeterQueue.maxConcurrentOperationCount = 1;
    }
    
    return _altimeterQueue;
}

- (NSOperationQueue *)motionQueue
{
    if (!_motionQueue) {
        _motionQueue = [[NSOperationQueue alloc] init];
        _motionQueue.maxConcurrentOperationCount = 1;
    }
    
    return _motionQueue;
}

- (NSOperationQueue *)accelerometerQueue
{
    if (!_accelerometerQueue) {
        _accelerometerQueue = [[NSOperationQueue alloc] init];
        _accelerometerQueue.maxConcurrentOperationCount = 1;
    }
    
    return _accelerometerQueue;
}

- (NSOperationQueue *)gyroQueue
{
    if (!_gyroQueue) {
        _gyroQueue = [[NSOperationQueue alloc] init];
        _gyroQueue.maxConcurrentOperationCount = 1;
    }
    
    return _gyroQueue;
}

#pragma mark <CRFCompassDelegate>

- (void)compass:(CRFCompass *)compass didUpdateCalibratedHeading:(float)calibratedHeading
{
    [self.delegate sensorManager:self didUpdateSensorDataType:CRFSensorDataTypeCalibratedOrientation data:@[@(-calibratedHeading)]];
    
    if (ENABLE_GYRO_ORIENTATION) {
        long ts = [CRFCommon getCurrentTimestampNanoSecond];
        [self.mOrientationData addObject:[[CRFSensorRawData alloc] initWithTimestamp:ts values:@[@(-calibratedHeading)] step:self.mCurrentStep]];
    }
}

#pragma mark <CLLocationManagerDelegate>

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    @synchronized(self.mSensorSyncObject) {
        if (!self.isRecording) return;

        if (newHeading.headingAccuracy < 0) return;
        
        [self.mCompass updateMagneticFieldX:newHeading.x y:newHeading.y z:newHeading.z];
        [self.mCompass updateHeading:(newHeading.trueHeading > 0 ? newHeading.trueHeading : newHeading.magneticHeading)];
        
        self.heading = newHeading;
        NSArray *values = @[@(newHeading.x), @(newHeading.y), @(newHeading.z)];
        NSArray *newValues = [self _filter_and_smoothing:CRFSensorMagneticField values:values];
        CRFSensorRawData *sd = [[CRFSensorRawData alloc] initWithTimestamp:[CRFCommon getCurrentTimestampNanoSecond] values:newValues step:self.mCurrentStep];
        
        long ts = [CRFCommon getCurrentTimestampNanoSecond];
        
        double heading = (newHeading.trueHeading > 0 ? newHeading.trueHeading : newHeading.magneticHeading);
        if (!ENABLE_GYRO_ORIENTATION) {
            [self.mOrientationData addObject:[[CRFSensorRawData alloc] initWithTimestamp:ts values:@[@([CRFSensorManager calibrateOrientation:heading])] step:self.mCurrentStep]];
        }
        
        NSArray *gravity = @[@(self.deviceMotion.gravity.x*-10.0), @(self.deviceMotion.gravity.y*-10.0), @(self.deviceMotion.gravity.z*-10.0)];
        NSArray *magnet = @[@(self.heading.x), @(self.heading.y), @(self.heading.z)];
        NSArray *R = [CRFCommon getRotationMatrixFromGravity:gravity geoMagnetic:magnet];
        if (R != nil) {
            double orientation = [((CRFSensorRawData *)self.mOrientationData[self.mOrientationData.count-1]).values[0] doubleValue];
            double intensity = (float)sqrt([magnet[0] doubleValue] * [magnet[0] doubleValue] +
                                           [magnet[1] doubleValue] * [magnet[1] doubleValue] +
                                           [magnet[2] doubleValue] * [magnet[2] doubleValue]);
            double vertical = [magnet[0] doubleValue] * [R[6] doubleValue] + [magnet[1] doubleValue] * [R[7] doubleValue] + [magnet[2] doubleValue] * [R[8] doubleValue];
            double horizontal = [magnet[0] doubleValue] * [R[3] doubleValue] + [magnet[1] doubleValue] * [R[4] doubleValue] + [magnet[2] doubleValue]*[R[5] doubleValue];
            
            [self.mDecompositionMagData addObject:[[CRFSensorRawData alloc] initWithTimestamp:sd.timestamp values:@[@(intensity), @(vertical), @(horizontal), @(orientation)] step:self.mCurrentStep]];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region
{
    @synchronized(self.mBeaconSyncObject) {
        if (self.isNewStep) {
            [self.beaconDictionary removeAllObjects];
            self.isNewStep = NO;
        }
        for (CLBeacon *beacon in beacons) {
            if (beacon.rssi == 0) continue;
            for (CRFBeacon *b in self.beaconManager.beaconArray) {
                if ([b isEqualToCLBeacon:beacon]) {
                    if (self.beaconDictionary[b.name] == nil) self.beaconDictionary[b.name] = [NSMutableArray array];
                    double rssi = beacon.rssi*1.0;
                    [self.beaconDictionary[b.name] addObject:@(rssi)];
                }
            }
        }
    }
}

#pragma mark <CRFStepDetectorDelegate>

- (void)stepDetector:(CRFStepDetector *)stepDetector didDetectStepFrom:(long)fromTS to:(long)toTS stepCount:(int)stepCount frequency:(double)frequency
{
    NSMutableDictionary *beacon_vector = [NSMutableDictionary dictionary];
    for (NSString *key in self.beaconDictionary) {
        beacon_vector[key] = @([CRFCommon mean:self.beaconDictionary[key]]);
    }
    self.isNewStep = YES;

    NSMutableArray *remainingData = [NSMutableArray array];
    for (int i = 0; i < self.mDecompositionMagData.count; ++i) {
        CRFSensorRawData *data = self.mDecompositionMagData[i];
        if (data.step == self.mCurrentStep) {
            NSMutableArray *d = [NSMutableArray arrayWithArray:data.values];
            [d addObject:@(data.timestamp)];
            [d addObject:@(data.step)];
            [d addObject:beacon_vector];
            [d addObject:self.altitudeData ? self.altitudeData.relativeAltitude : @(0.0)];
            [d addObject:self.altitudeData ? self.altitudeData.pressure : @(0.0)];
            [self.delegate sensorManager:self didUpdateSensorDataType:CRFSensorDataTypeCRF data:d];
        } else {
            [remainingData addObject:data];
        }
    }
    self.mDecompositionMagData = remainingData;
    
    remainingData = [NSMutableArray array];
    for (int i = 0; i < self.mOrientationData.count; ++i) {
        CRFSensorRawData *data = self.mOrientationData[i];
        if (data.timestamp > toTS) [remainingData addObject:data];
    }
    
    self.mOrientationData = remainingData;
    self.mCurrentStep = stepCount;
}

@end
