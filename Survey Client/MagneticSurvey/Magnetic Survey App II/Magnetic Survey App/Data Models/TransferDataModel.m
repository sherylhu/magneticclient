//
//  TransferDataModel.m
//  Mag Client iOS
//
//  Created by ivy on 6/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "TransferDataModel.h"

#import "MagneticData.h"

@interface TransferDataModel () {
    NSArray *mag_components;
    NSString *user_id;
    NSDictionary *wifi_vector, *ble_vector;
}

@end

@implementation TransferDataModel

- (id)initWithUserID:(NSString *)userid andMagneticComponents:(NSArray *)magdata {
    self = [super init];
    if (self) {
        user_id = userid;
        mag_components = [NSArray arrayWithArray:magdata];
    }
    return self;
}

- (NSString *)model2String {
    if ([mag_components count] > 0) {
        NSString *temp = @"";
        for (MagneticData *mag in mag_components) {
            NSString *magStr = [mag model2String];
            magStr = [temp containsString:@"{"] ? [NSString stringWithFormat:@",%@", magStr] : magStr;
            temp = [temp stringByAppendingString:magStr];
        }
        
        temp = [NSString stringWithFormat:@"{%@},{},{}", temp];
        
        return temp;
    }
    return @"";
}

@end
