//
//  MagneticData.m
//  Mag Client iOS
//
//  Created by ivy on 4/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "MagneticData.h"

@interface MagneticData () {
    long timestamp;
    NSArray *magvalue;
    int step;
}

@end

@implementation MagneticData

- (id)initWithTimestamp:(long)ts {
    self = [super init];
    if (self) {
        timestamp = ts;
        step = -1;
    }
    return self;
}

- (BOOL)addIntensity:(float)intensity andVertical:(float)vertical andHorizontal:(float)horizontal andOrientation:(float)orientation {
    
    magvalue = [NSArray arrayWithObjects:[NSNumber numberWithFloat:intensity], [NSNumber numberWithFloat:vertical], [NSNumber numberWithFloat:horizontal], [NSNumber numberWithFloat:orientation], nil];
    if ([magvalue count]) {
        NSLog(@"MagData:\nIntensity: %f,\nVertical:%f,\nHorizontal%f,\nOrientation%f", intensity, vertical, horizontal, orientation);
        return YES;
    }
    return NO;
}

- (NSString *)model2String {
    if ([magvalue count] == 4) {
        NSString *temp = [NSString stringWithFormat:@"{%ld,%@,%@,%@,%@}", timestamp, [magvalue objectAtIndex:0], [magvalue objectAtIndex:1], [magvalue objectAtIndex:2], [magvalue objectAtIndex:3]];
        
        return temp;
    }
    return @"";
}

@end
