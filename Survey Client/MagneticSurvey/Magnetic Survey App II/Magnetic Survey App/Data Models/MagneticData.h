//
//  MagneticData.h
//  Mag Client iOS
//
//  Created by ivy on 4/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MagneticData : NSObject

- (id)initWithTimestamp:(long)ts;
- (BOOL)addIntensity:(float)intensity andVertical:(float)vertical andHorizontal:(float)horizontal andOrientation:(float)orientation;

- (NSString *)model2String;

@end
