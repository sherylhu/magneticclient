//
//  TransferDataModel.m
//  Mag Client iOS
//
//  Created by ivy on 6/9/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "TransferDataModel.h"

#import "MagneticData.h"

@interface TransferDataModel () {
    NSArray *mag_components;
    NSString *user_id;
    NSDictionary *wifi_vector, *ble_vector;
}

@end

@implementation TransferDataModel

- (id)initWithUserID:(NSString *)userid andMagneticComponents:(NSArray *)magdata {
    self = [super init];
    if (self) {
        user_id = userid;
        mag_components = [NSArray arrayWithArray:magdata];
        
        wifi_vector = [NSDictionary dictionary];
        ble_vector = [NSDictionary dictionary];
    }
    return self;
}

- (NSString *)model2String {
    
    if ([mag_components count] > 0) {
        NSMutableArray *temp = [NSMutableArray array];
        for (MagneticData *mag in mag_components) {
            NSDictionary *arr = [mag proxy2Gson];
            [temp addObject:arr];
        }
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              temp, @"mag_components",
                              wifi_vector, @"wifi_componnets",
                              ble_vector, @"ble_componnets",
                              nil];
        return [NSString stringWithFormat:@"%@", dict];
    }
    
    
//    if ([mag_components count] > 0) {
//        NSString *temp = @"";
//        for (MagneticData *mag in mag_components) {
//            NSString *magStr = [mag model2String];
//            magStr = [temp containsString:@"{"] ? [NSString stringWithFormat:@",%@", magStr] : magStr;
//            temp = [temp stringByAppendingString:magStr];
//        }
//
//        temp = [NSString stringWithFormat:@"{%@},{},{}", temp];
//
//        return temp;
//    }
    return @"";
}

- (id)proxy2Gson {
    
    NSMutableArray *temp = [NSMutableArray array];
    for (MagneticData *mag in mag_components) {
        NSDictionary *arr = [mag proxy2Gson];
        [temp addObject:arr];
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          user_id, @"user_id",
                          temp, @"mag_components",
                          wifi_vector, @"wifi_componnets",
                          ble_vector, @"ble_componnets",
                          //            self.gpsComponent, @"gps_comps",
                          //            self.floorDetected, @"floor",
                          nil];
    return dict;
}

@end
