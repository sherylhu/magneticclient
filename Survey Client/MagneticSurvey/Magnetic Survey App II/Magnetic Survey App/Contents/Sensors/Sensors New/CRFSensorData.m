//
//  CRFSensorData.m
//  CRF
//
//  Created by Siyan HU on 17/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import "CRFSensorData.h"

@implementation CRFSensorData

- (NSString *)description
{
    return [NSString stringWithFormat:@"[CRFSensorData] intensity: %f, vertical: %f, horizontal: %f, orientation: %f, timestamp: %f, step: %f", self.intensity, self.vertical, self.horizontal, self.orientation, self.timestamp, self.step];
}

@end
