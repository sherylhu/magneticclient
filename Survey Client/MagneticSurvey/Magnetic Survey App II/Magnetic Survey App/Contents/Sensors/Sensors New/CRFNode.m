//
//  CRFNode.m
//  CRF
//
//  Created by Siyan HU on 16/4/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import "CRFNode.h"

@implementation CRFNode

- (NSString *)description
{
    return [NSString stringWithFormat:@"Node [%d,%d]: %@", self.i, self.j, @(self.potential)];
}

@end
