//
//  TriFloatArray.m
//  Mag IOS
//
//  Created by ivy on 1/5/2017.
//  Copyright © 2017 Ivy Siyan HU. All rights reserved.
//

#import "TriFloatArray.h"

@implementation TriFloatArray

- (id)init {
    self = [super init];
    if (self) {
        self.x = 0;
        self.y = 0;
        self.z = 0;
    }
    return self;
}

- (int)count {
    int weight = 0;
    if (self.x == self.x) {
        weight ++;
    }
    if (self.y == self.y) {
        weight ++;
    }
    if (self.z == self.z) {
        weight ++;
    }
    return weight;
}

- (float)getValueForIndex:(int)index {
    switch (index) {
        case 0:
            return self.x;
            break;
        case 1:
            return self.y;
            break;
        case 2:
            return self.z;
            break;
        default:
            break;
    }
    return MAXFLOAT;
}

- (void)setValue:(float)value forIndex:(int)index {
    switch (index) {
        case 0:
            self.x = value;
            break;
        case 1:
            self.y = value;
            break;
        case 2:
            self.z = value;
            break;
        default:
            break;
    }
}

@end
