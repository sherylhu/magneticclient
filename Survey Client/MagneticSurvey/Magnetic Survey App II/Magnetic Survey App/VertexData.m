//
//  VertexData.m
//  Magnetic Survey App
//
//  Created by ivy on 15/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "VertexData.h"

@implementation VertexData

- (void)createButtonsWithIndex:(int)index {
    self.fromBtn = [self createFromButtonWithIndex:index];
    self.toBtn = [self createToButtonWithIndex:index];
}


#pragma mark - UIViews
- (UIButton *)createFromButtonWithIndex:(int)index {
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(self.fromPnt.x - 8, self.fromPnt.y - 8, 16, 16)];
    [btn setTitle:[NSString stringWithFormat:@"%d_from", index] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"setfrom"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (UIButton *)createToButtonWithIndex:(int)index {
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(self.toPnt.x - 8, self.toPnt.y - 8, 16, 16)];
    [btn setTitle:[NSString stringWithFormat:@"%d_to", index] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"setto"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

#pragma mark - Actions
- (void)buttonClicked {
    [self.delegate getTimestamp:self.timestamp];
}

@end
