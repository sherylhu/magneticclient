//
//  CRFLocationManager.m
//  CRF
//
//  Created by Samuel Chow on 22/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import "CRFLocationManager.h"
#import "CRFBeaconManager.h"
#import "CRFHeatMap.h"
#import "CRFPolygon.h"
#import "CRFLine.h"
#import "CRFNode.h"
#import "CRFPriorityQueue.h"
#import "FastDTW.h"
#import "ManhattanDistance.h"

using namespace fastdtw;

typedef enum : NSUInteger {
    CRFMethodBLE,
    CRFMethodMagneticField,
    CRFMethodFusion,
} CRFMethod;

#define a 0.56496839593958803000
#define b 0.11408853567798248000
#define sigma_step_length 0.1

#define START 1
#define WINDOW 5
#define MAG_STEP_RANGE 5
#define PRIORITY_QUEUE_SIZE 20
#define MULTIAREA_THREHOLD 3
#define ALTITUDE_THRESHOLD 3
#define ALTITUDE_STEP_WINDOW 6
#define METHOD CRFMethodBLE

@interface CRFLocationManager ()

@property (strong, nonatomic) CRFPolygon *polygon;

@property (strong, nonatomic) NSMutableArray *nodePot;
@property (strong, nonatomic) NSMutableArray *observations;
@property (strong, nonatomic) NSMutableArray *heading;
@property (strong, nonatomic) NSMutableArray *step_len;
@property (strong, nonatomic) NSMutableArray *rAltitude;
@property (strong, nonatomic) NSMutableArray *trace;
@property (strong, nonatomic) NSMutableArray *track;
@property (strong, nonatomic) NSMutableArray *beacon_trace;
@property (strong, nonatomic) NSArray *beacon_heatmap;

@property (strong, nonatomic) CRFPriorityQueue *priorityQueue;
@property (strong, nonatomic) NSOperationQueue *operationQueue;

@property (strong, nonatomic) NSMutableDictionary *pointCache; // (x,y) to constraints satisfied BOOL
@property (strong, nonatomic) NSDictionary *ij2bpCache; // crf grid index (i,j) to beacon reference point

@property (assign, nonatomic) int areaId;
@property (assign, nonatomic) int areaThreshold;
@property (assign, nonatomic) int lastRestartStep;

@property (assign, nonatomic) CGPoint beaconEstimate;
@property (assign, nonatomic) CGPoint crfEstimate;
@end

@implementation CRFLocationManager

- (instancetype)initWithAreaId:(int)areaId
{
    if (self = [super init]) {
        self.areaId = areaId;
        self.heatMap = [[CRFHeatMap alloc] initWithSetting:[CRFCommon loadSiteSetting:areaId]];
        self.beaconManager = [[CRFBeaconManager alloc] initWithLocationManager:self];
        [self genMapConstraint];
        [self precomputeCaching];
    }
    
    return self;
}

#pragma mark - Setting

- (void)initialize
{
    // Multidimensional array and priority queue initialization
    self.areaThreshold = MULTIAREA_THREHOLD;
    self.nodePot = [NSMutableArray array];
    self.observations = [NSMutableArray array];
    self.heading = [NSMutableArray array];
    self.step_len = [NSMutableArray array];
    self.rAltitude = [NSMutableArray array];
    self.track = [NSMutableArray array];
    self.trace = [NSMutableArray array];
    self.beacon_trace = [NSMutableArray array];
    self.pointCache = [NSMutableDictionary dictionary];
    self.operationQueue = [[NSOperationQueue alloc] init];
    self.operationQueue.maxConcurrentOperationCount = 1;
    
    self.priorityQueue = [[CRFPriorityQueue alloc] initWithCapacity:PRIORITY_QUEUE_SIZE];
    self.priorityQueue.comparator = ^NSComparisonResult(CRFNode *obj1, CRFNode *obj2) {
        if (obj1.potential > obj2.potential) {
            return NSOrderedDescending;
        } else if (obj1.potential < obj2.potential) {
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    };
}

- (void)resetLocationManager
{
    [self.operationQueue cancelAllOperations];
    self.beaconEstimate = CGPointZero;
    self.nodePot = nil;
    self.observations = nil;
    self.heading = nil;
    self.step_len = nil;
    self.rAltitude = nil;
    self.trace = nil;
    self.track = nil;
    self.operationQueue = nil;
    self.priorityQueue = nil;
    self.pointCache = nil;
    self.beacon_trace = nil;
}

- (void)genMapConstraint
{
    NSArray *outConstraint = [CRFCommon loadConstraint:self.heatMap.setting.outconstraint];
    CRFBuilder *builder = [CRFPolygon builder];
    for (int i = 0; i < outConstraint.count; ++i) {
        [builder addVertex:CGPointMake([outConstraint[i][0] doubleValue], [outConstraint[i][1] doubleValue])];
    }
    
    for (NSString *inconstraintfile in self.heatMap.setting.inconstraint) {
        NSArray *inConstraint = [CRFCommon loadConstraint:inconstraintfile];
        [builder close];
        for (int i = 0; i < inConstraint.count; ++i) {
            [builder addVertex:CGPointMake([inConstraint[i][0] doubleValue], [inConstraint[i][1] doubleValue])];
        }
    }
    
    self.polygon = [builder build];
}

// mapping CRF reference points to iBeacon reference points with nearest neighbour strategy
- (void)precomputeCaching
{
    NSMutableDictionary *ij2bpCache = [NSMutableDictionary dictionary];
    for (int i = 0; i < self.heatMap.vectorX.count; i++) {
        for (int j = 0; j < self.heatMap.vectorY.count; j++) {
            double c_x = [self.heatMap.vectorX[i] doubleValue];
            double c_y = [self.heatMap.vectorY[j] doubleValue];
            if ([self satisfyMapConstraintX:c_x Y:c_y]) {
                double mindist = MAXFLOAT;
                int refId = 0;
                for (NSValue *pointValue in self.beaconManager.point2RefIdMap) {
                    CGPoint point = [pointValue CGPointValue];
                    int rId = [self.beaconManager.point2RefIdMap[pointValue] intValue];
                    double dist = [CRFCommon euclideanFrom:point to:CGPointMake(c_x, c_y)];
                    if (dist < mindist) {
                        mindist = dist;
                        refId = rId;
                    }
                }
                ij2bpCache[[NSValue valueWithCGPoint:CGPointMake(i, j)]] = @(refId);
            }
        }
    }
    self.ij2bpCache = ij2bpCache;
}

#pragma mark - Public API

- (void)start
{
    [self initialize];
}

- (void)stop
{
    [self resetLocationManager];
}

- (void)feedSensorData:(CRFSensorData *)sensorData
{
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(runCRF:) object:sensorData];
    [self.operationQueue addOperation:operation];
}

- (NSArray *)getNeigbourNode:(CGPoint)point errorRadiusInMeter:(double)error;
{
    NSMutableArray *ret = [NSMutableArray array];
    double errorRadiusInPixel = error*self.heatMap.setting.scale;
    
    double gridWidth = ([self.heatMap.boundary_X[1] doubleValue] - [self.heatMap.boundary_X[0] doubleValue])/self.heatMap.vectorX.count;
    double gridHeight = ([self.heatMap.boundary_Y[1] doubleValue] - [self.heatMap.boundary_Y[0] doubleValue])/self.heatMap.vectorY.count;
    
    int kX = (int)round(errorRadiusInPixel/gridWidth);
    int kY = (int)round(errorRadiusInPixel/gridHeight);
    
    int I = MIN((int)self.heatMap.vectorX.count-1, MAX(0, (int)((point.x-[self.heatMap.boundary_X[0] doubleValue])*(self.heatMap.gridX-1)/([self.heatMap.boundary_X[1] doubleValue]-[self.heatMap.boundary_X[0] doubleValue]))));
    int J = MIN((int)self.heatMap.vectorY.count-1, MAX(0, (int)((point.y-[self.heatMap.boundary_Y[0] doubleValue])*(self.heatMap.gridY-1)/([self.heatMap.boundary_Y[1] doubleValue]-[self.heatMap.boundary_Y[0] doubleValue]))));
    
    for (int i = -kX; i <= kX; i++) {
        for (int j = -kY; j <= kY; j++) {
            double x = [self.heatMap.vectorX[MIN((int)self.heatMap.vectorX.count-1, MAX(0, (int)I+i))] doubleValue];
            double y = [self.heatMap.vectorY[MIN((int)self.heatMap.vectorY.count-1, MAX(0, (int)J+j))] doubleValue];
            if (![self satisfyMapConstraintX:x Y:y]) continue;
            if ([CRFCommon euclideanFrom:point to:CGPointMake(x, y)] <= errorRadiusInPixel) {
                [ret addObject:@[@(MIN((int)self.heatMap.vectorX.count-1, MAX(0, (int)I+i))),@(MIN((int)self.heatMap.vectorY.count-1, MAX(0, (int)J+j)))]];
            }
        }
    }
    
    return ret;
}

- (CGPoint)correctPoint:(CGPoint)point
{
    if ([self satisfyMapConstraintX:point.x Y:point.y]) return point;
    
    NSArray *search = [self getNeigbourNode:point errorRadiusInMeter:10];
    
    double min_dist = MAXFLOAT;
    CGPoint ret = CGPointZero;
    
    for (NSArray *coor in search) {
        CGPoint sP = CGPointMake([self.heatMap.vectorX[[coor[0] intValue]] doubleValue], [self.heatMap.vectorY[[coor[1] intValue]] doubleValue]);
        double dist = [CRFCommon euclideanFrom:sP to:point];
        if (dist < min_dist) {
            min_dist = dist;
            ret = sP;
        }
    }
    if (CGPointEqualToPoint(CGPointZero, ret)) ret = point;
    return ret;
}

- (BOOL)satisfyMapConstraintX:(double)x Y:(double)y
{
    NSValue *pointValue = [NSValue valueWithCGPoint:CGPointMake(x, y)];
    if (self.pointCache[pointValue] != nil) {
        return [self.pointCache[pointValue] boolValue];
    }
    BOOL ret = [self.polygon contains:CGPointMake(x, y)];
    self.pointCache[pointValue] = @(ret);
    return ret;
}

#pragma mark - CRF

- (void)runCRF:(CRFSensorData *)sensorData
{
    [self.trace addObject:sensorData];

    if (sensorData.step-self.lastRestartStep < START) return;
    if (self.trace.count > 0) {
        CRFSensorData *lastData = self.trace[self.trace.count-2];
        int stepDiff = ((int)sensorData.step-self.lastRestartStep)-((int)lastData.step-self.lastRestartStep);
        if (stepDiff == 0) return;
    }
    
    long startTime = [CRFCommon getCurrentTimestampMilliSecond];
    
    [self prepareForNewStep];
    int step = MAX(0, (self.trace.count == 0) ? 0 : (((CRFSensorData *)self.trace[self.trace.count-2]).step-self.lastRestartStep-1));
    if (step < START) return;
    
    NSLog(@"Dealing with step = %d [%d]", step, step+self.lastRestartStep);
    
    NSArray *index = [CRFHeatMap findInterval:self.trace step:step+self.lastRestartStep];

    // averaging magnetic field intensities
    int count = 0;
    for (int j = MAX(0, [index[1] intValue]-WINDOW); j <= MIN([index[1] intValue]+WINDOW, [self.trace count]-1); ++j) {
        self.observations[step][0] = @([self.observations[step][0] doubleValue] + ((CRFSensorData *)self.trace[j]).intensity);
        self.observations[step][1] = @([self.observations[step][1] doubleValue] + ((CRFSensorData *)self.trace[j]).vertical);
        self.observations[step][2] = @([self.observations[step][2] doubleValue] + ((CRFSensorData *)self.trace[j]).horizontal);
        count += 1;
    }
    self.observations[step][0] = @([self.observations[step][0] doubleValue]/count);
    self.observations[step][1] = @([self.observations[step][1] doubleValue]/count);
    self.observations[step][2] = @([self.observations[step][2] doubleValue]/count);

    // averaging orientation
    count = 0;
    for (int j = [index[0] intValue]; j <= [index[1] intValue]; ++j) {
        self.heading[step][0] = @([self.heading[step][0] doubleValue] + cos((((CRFSensorData *)self.trace[j]).orientation+180)*M_PI/180));
        self.heading[step][1] = @([self.heading[step][1] doubleValue] + sin((((CRFSensorData *)self.trace[j]).orientation+180)*M_PI/180));
        count += 1;
    }
    self.heading[step][0] = @([self.heading[step][0] doubleValue]/count);
    self.heading[step][1] = @([self.heading[step][1] doubleValue]/count);
    
    // averaging relative altitude
    count = 0;
    for (int j = [index[0] intValue]; j <= [index[1] intValue]; ++j) {
        self.rAltitude[step] = @([self.rAltitude[step] doubleValue] + ((CRFSensorData *)self.trace[j]).rAltitude);
        count += 1;
    }
    self.rAltitude[step] = @([self.rAltitude[step] doubleValue]/count);

    // calculate step length
    double step_frequency = 1e9 / (((CRFSensorData *)self.trace[[index[1] intValue]]).timestamp - ((CRFSensorData *)self.trace[[index[0] intValue]]).timestamp);
    self.step_len[step] = @(a+step_frequency*b);
    
    // area classification by relative altitude difference
    if (step-ALTITUDE_STEP_WINDOW > 0) {
        double prevAlt = fabs([self.rAltitude[step-ALTITUDE_STEP_WINDOW] doubleValue]);
        double currAlt = fabs([self.rAltitude[step] doubleValue]);
        double delta = fabs(prevAlt-currAlt);
        if (delta > ALTITUDE_THRESHOLD) {
            self.operationQueue.suspended = YES;
            [self.delegate locationManager:self didUpdateAreaId:(self.areaId == 1100 ? 1101: 1100)]; //TODO: determine by geographic altitude
            return;
        }
    }
    
    NSDictionary *target_vector = sensorData.beacon_vector;
    if (target_vector) {
        
        // area classification by continuous ibeacon signal patterns
        int new_areaId = [self.beaconManager computeCurrentArea:target_vector];
        if (new_areaId > 0 && self.areaId != new_areaId && step+self.lastRestartStep < 2*MULTIAREA_THREHOLD) {
            // only if current total step is less than a threshold
            // for case that initial area is incorrect
            if (self.areaId != new_areaId) {
                self.areaThreshold -= 1;
                if (self.areaThreshold == 0) {
                    [self.delegate locationManager:self didUpdateAreaId:new_areaId];
                    return;
                }
            } else {
                // reset if new area detection is discontinuous to prevent outliers
                self.areaThreshold = MULTIAREA_THREHOLD;
            }
        }
        
        // beacon similarity heatmap
        self.beacon_heatmap = [self.beaconManager computeBeaconHeatMap:target_vector atArea:self.areaId];
        
        // beacon estimation
        CGPoint beaconEstimate = [self.beaconManager computeLocation:target_vector atArea:self.areaId];
        if ([CRFCommon pointIsValid:beaconEstimate]) {
            if (self.beacon_trace.count > 0) {
                CGPoint lastPoint = [[self.beacon_trace lastObject] CGPointValue];
                double tt_dist = [CRFCommon euclideanFrom:lastPoint to:beaconEstimate];
                double stepLen = MIN((2)*self.heatMap.setting.scale, tt_dist);
                double new_x = (beaconEstimate.x*stepLen+lastPoint.x*(tt_dist-stepLen))/tt_dist;
                double new_y = (beaconEstimate.y*stepLen+lastPoint.y*(tt_dist-stepLen))/tt_dist;
                beaconEstimate = CGPointMake(new_x, new_y);
            }
            if ([CRFCommon pointIsValid:beaconEstimate]) {
                if (self.beacon_trace.count == 0 || !CGPointEqualToPoint([[self.beacon_trace lastObject] CGPointValue], beaconEstimate)) {
                    [self.beacon_trace addObject:[NSValue valueWithCGPoint:beaconEstimate]];
                }
                [self.delegate locationManager:self didUpdateBeaconEstimate:beaconEstimate];
                self.beaconEstimate = beaconEstimate;
            }
        }
    }
    
    // compute node likelihood
    double Z = 0.0;
    for (int i = 0; i < self.heatMap.vectorX.count; i++) {
        for (int j = 0; j < self.heatMap.vectorY.count; j++) {
            Z += [self computePotentialAtStep:step i:i j:j];
        }
    }
    
    // restart if ALL nodes with highest potential are not constraint-satisfied
    int restartScore = 0;
    for (CRFNode *node in [self.priorityQueue toArray]) {
        int score = ![self satisfyMapConstraintX:([self.heatMap.vectorX[node.i] doubleValue]+[self.step_len[step] doubleValue]*self.heatMap.setting.scale*[self.heading[step][0] doubleValue])
                                               Y:([self.heatMap.vectorY[node.j] doubleValue]-[self.step_len[step] doubleValue]*self.heatMap.setting.scale*[self.heading[step][1] doubleValue])];
        restartScore += score;
    }
    if (self.priorityQueue.size > 0 && (restartScore == self.priorityQueue.size)) {
        [self restartFromStep:step+self.lastRestartStep];
        return;
    }
    
    // normalize node likelihood
    [self.priorityQueue clear];
    double maxLikelihood = 0.0;
    NSMutableArray *now = [NSMutableArray arrayWithArray:@[@(0.0),@(0.0)]];
    for (int i = 0; i < self.heatMap.vectorX.count; ++i) {
        for (int j = 0; j < self.heatMap.vectorY.count; ++j) {
            self.nodePot[step][i][j] = @([self.nodePot[step][i][j] doubleValue]/Z);
            
            if ([self.nodePot[step][i][j] doubleValue] > 1e-5) {
                CRFNode *node = [[CRFNode alloc] init];
                node.i = i;
                node.j = j;
                node.potential = [self.nodePot[step][i][j] doubleValue];
                [self.priorityQueue add:node];
                
                if (self.priorityQueue.size > PRIORITY_QUEUE_SIZE)
                    [self.priorityQueue poll];
            }
            
            if ([self.nodePot[step][i][j] doubleValue] > maxLikelihood) {
                maxLikelihood = [self.nodePot[step][i][j] doubleValue];
                now[0] = @(i);
                now[1] = @(j);
            }
        }
    }
    
    // restart if maximum likelihood is zero
    if (maxLikelihood == 0.0) {
        [self restartFromStep:step+self.lastRestartStep];
        return;
    }
    
    // output route
    if (step >= START) {
        NSMutableArray *trace = [NSMutableArray array];
        while (YES) {
            [trace addObject:[NSMutableArray arrayWithArray:@[self.heatMap.vectorX[[now[0] intValue]], self.heatMap.vectorY[[now[1] intValue]]]]];
            now = self.track[step][[now[0] intValue]][[now[1] intValue]];
            if ([now[0] intValue] == 0 && [now[1] intValue] == 0) break;
            
            if (step-[now[2] intValue] > 1) {
                NSArray *last = trace[trace.count-1];
                double a1 = [self.heatMap.vectorX[[now[0] intValue]] doubleValue];
                double a2 = [self.heatMap.vectorY[[now[1] intValue]] doubleValue];
                [trace addObject:[NSMutableArray arrayWithArray:@[@(([last[0] doubleValue]+a1)/2), @(([last[1] doubleValue]+a2)/2)]]];
            }

            step = [now[2] intValue];
            if (step <= START) {
                [trace addObject:[NSMutableArray arrayWithArray:@[self.heatMap.vectorX[[now[0] intValue]], self.heatMap.vectorY[[now[1] intValue]]]]];
                break;
            }
        }
        trace = [[[trace reverseObjectEnumerator] allObjects] mutableCopy];
        self.crfEstimate = CGPointMake([[trace lastObject][0] doubleValue], [[trace lastObject][1] doubleValue]);
        
        long endTime = [CRFCommon getCurrentTimestampMilliSecond];
        NSLog(@"Time: %ldms", endTime-startTime);
        [self.delegate locationManager:self didUpdateRoute:trace];
    }    
}

- (double)computePotentialAtStep:(int)step i:(int)i j:(int)j
{
    double x = [self.heatMap.vectorX[i] doubleValue];
    double y = [self.heatMap.vectorY[j] doubleValue];
    if ([CRFCommon euclideanFrom:CGPointMake(x, y) to:[CRFCommon pointIsValid:self.crfEstimate] ? self.crfEstimate : self.beaconEstimate] >= 6 * self.heatMap.setting.scale) return 0;
    
    if ([self satisfyMapConstraintX:[self.heatMap.vectorX[i] doubleValue] Y:[self.heatMap.vectorY[j] doubleValue]]) {
        double pot = (self.ij2bpCache[[NSValue valueWithCGPoint:CGPointMake(i, j)]] != nil) ? [self.beacon_heatmap[[self.ij2bpCache[[NSValue valueWithCGPoint:CGPointMake(i, j)]] intValue]] doubleValue] : 1.0;
        
        if (step == START) {
            self.nodePot[step][i][j] = @(1);
        } else {
            int rangeX = (int)round([self.step_len[step] doubleValue]/(([self.heatMap.boundary_X[1] doubleValue]-[self.heatMap.boundary_X[0] doubleValue])/self.heatMap.vectorX.count*1.0/self.heatMap.setting.scale))+1;
            int rangeY = (int)round([self.step_len[step] doubleValue]/(([self.heatMap.boundary_Y[1] doubleValue]-[self.heatMap.boundary_Y[0] doubleValue])/self.heatMap.vectorY.count*1.0/self.heatMap.setting.scale))+1;
            double localMaxLikelihood = 0;
            double localMaxMagPotential = 0;
            NSMutableArray *idx = [NSMutableArray arrayWithArray:@[@(-1),@(-1),@(-1)]];
            for (int k1 = -rangeX; k1 <= rangeX; ++k1) {
                for (int k2 = -rangeY; k2 <= rangeY; ++k2) {
                    if ([self validX:i+k1 Y:j+k2] && [self satisfyMapConstraintX:[self.heatMap.vectorX[i+k1] doubleValue] Y:[self.heatMap.vectorY[j+k2] doubleValue]]) {
                        double featureAngle = [self computeFeatureAngle:[NSMutableArray arrayWithArray:@[@(k1),@(k2)]] mu:[NSMutableArray arrayWithArray:@[self.heading[step][0],@(-[self.heading[step][1] doubleValue])]]];
                        if ((k1!=0 || k2!=0) && featureAngle < 22.5) continue;

                        for (int look_back = 1; look_back <= MIN(2, step-1); ++look_back) {
                            double likelihood = [self.nodePot[step-look_back][i+k1][j+k2] doubleValue];
                            if (likelihood < localMaxLikelihood) continue;
                            
                            switch (METHOD) {
                                case CRFMethodBLE:
                                {
                                    // BLE Localization
                                    double featureBeacon = pot;
                                    likelihood *= featureBeacon;
                                    if (featureBeacon > localMaxMagPotential) {
                                        localMaxMagPotential = featureBeacon;
                                    }
                                    if (likelihood < localMaxLikelihood) continue;
                                }
                                    break;
                                    
                                case CRFMethodMagneticField:
                                {
                                    // Magnetic Field Localization
                                    double featureMag = 1.0;
                                    
                                    // magnetic field
                                    TimeSeries<double, 1> mu_intensity;
                                    TimeSeries<double, 1> mu_vertical;
                                    TimeSeries<double, 1> mu_horizontal;
                                    TimeSeries<double, 1> obs_intensity;
                                    TimeSeries<double, 1> obs_vertical;
                                    TimeSeries<double, 1> obs_horizontal;
                                    
                                    NSArray *now = @[@(i+k1),@(j+k2)];
                                    int footnote = step-look_back;
                                    int idx = 0;
                                    while (YES) {
                                        int i = [now[0] intValue];
                                        int j = [now[1] intValue];
                                        
                                        double mu_intensity_val = [self.heatMap.visual[i][j][0] doubleValue];
                                        double mu_vertical_val = [self.heatMap.visual[i][j][1] doubleValue];
                                        double mu_horizontal_val = [self.heatMap.visual[i][j][2] doubleValue];
                                        double obs_intensity_val = [self.observations[footnote][0] doubleValue];
                                        double obs_vertical_val = [self.observations[footnote][1] doubleValue];
                                        double obs_horizontal_val = [self.observations[footnote][2] doubleValue];
                                        
                                        mu_intensity.addLast(idx, TimeSeriesPoint<double, 1>(&mu_intensity_val));
                                        mu_vertical.addLast(idx, TimeSeriesPoint<double, 1>(&mu_vertical_val));
                                        mu_horizontal.addLast(idx, TimeSeriesPoint<double, 1>(&mu_horizontal_val));
                                        obs_intensity.addLast(idx, TimeSeriesPoint<double, 1>(&obs_intensity_val));
                                        obs_vertical.addLast(idx, TimeSeriesPoint<double, 1>(&obs_vertical_val));
                                        obs_horizontal.addLast(idx, TimeSeriesPoint<double, 1>(&obs_horizontal_val));
                                        
                                        if ([self.track[footnote][i][j] isEqual:@[@(-1),@(-1),@(-1)]] && footnote > START) {
                                            [self computePotentialAtStep:footnote i:i j:j];
                                        }
                                        
                                        now = self.track[footnote][[now[0] intValue]][[now[1] intValue]];
                                        footnote = [now[2] intValue];
                                        if (footnote < MAX(START, step-look_back-MAG_STEP_RANGE)) {
                                            break;
                                        }
                                        idx++;
                                    }
                                    
                                    if (idx > 0) {
                                        featureMag = [self computeFeatureMag:mu_intensity mu_ver:mu_vertical mu_hor:mu_horizontal obs_int:obs_intensity obs_ver:obs_vertical obs_hor:obs_horizontal];
                                        likelihood *= featureMag;
                                        if (featureMag > localMaxMagPotential) {
                                            localMaxMagPotential = featureMag;
                                        }
                                        if (likelihood < localMaxLikelihood) continue;
                                    }
                                }
                                    break;
                                    
                                default:
                                    break;
                            }

                            // orientation
                            NSMutableArray *orientation_vector = [NSMutableArray arrayWithArray:@[@(-k1), @(k2)]];
                            NSMutableArray *ttHeading = [NSMutableArray arrayWithArray:@[@(0), @(0)]];
                            int count = 0;
                            for (int footnote = step-look_back+1; footnote <= step; ++footnote) {
                                count++;
                                ttHeading[0] = @([ttHeading[0] doubleValue]+[self.heading[footnote][0] doubleValue]);
                                ttHeading[1] = @([ttHeading[1] doubleValue]+[self.heading[footnote][1] doubleValue]);
                            }
                            ttHeading[0] = @([ttHeading[0] doubleValue]/look_back);
                            ttHeading[1] = @([ttHeading[1] doubleValue]/look_back);
                            if (k1 != 0 || k2 != 0) {
                                double featureOrient = [self computeFeatureOrient:orientation_vector mu:ttHeading];
                                likelihood *= featureOrient;
                            }
                            if (likelihood < localMaxLikelihood) continue;

                            // step length
                            double stepLen = [CRFCommon euclideanFrom:CGPointMake([self.heatMap.vectorX[i] doubleValue], [self.heatMap.vectorY[j] doubleValue]) to:CGPointMake([self.heatMap.vectorX[i+k1] doubleValue], [self.heatMap.vectorY[j+k2] doubleValue])]*1.0/self.heatMap.setting.scale;
                            double ttStepLen = 0;
                            for (int footnote = step-look_back+1; footnote <= step; ++footnote)
                                ttStepLen += [self.step_len[footnote] doubleValue];
                            
                            double featureStepLen = [self computeFeatureStepLen:stepLen mu:ttStepLen];
                            likelihood *= featureStepLen;
                            
                            
                            if (localMaxLikelihood <= likelihood) {
                                localMaxLikelihood = likelihood;
                                idx[0] = @(i+k1);
                                idx[1] = @(j+k2);
                                idx[2] = @(step-look_back);
                            }
                        }
                    }
                }
            }
            
            self.nodePot[step][i][j] = @(localMaxLikelihood);
            self.track[step][i][j][0] = idx[0];
            self.track[step][i][j][1] = idx[1];
            self.track[step][i][j][2] = idx[2];
        }
    }
    return [self.nodePot[step][i][j] doubleValue];
}

- (void)restartFromStep:(int)step
{
    self.operationQueue.suspended = YES;
    
    self.lastRestartStep = step+1;
    self.beaconEstimate = CGPointZero;
    [self.nodePot removeAllObjects];
    [self.beacon_trace removeAllObjects];
    [self.observations removeAllObjects];
    [self.heading removeAllObjects];
    [self.step_len removeAllObjects];
    [self.track removeAllObjects];
    [self.trace removeAllObjects];
    [self.priorityQueue clear];
    self.beacon_heatmap = nil;
    
    self.operationQueue.suspended = NO;
}

- (void)prepareForNewStep
{
    [self.observations addObject:[NSMutableArray arrayWithArray:@[@(0),@(0),@(0)]]];
    [self.heading addObject:[NSMutableArray arrayWithArray:@[@(0),@(0)]]];
    [self.step_len addObject:@(0)];
    [self.rAltitude addObject:@(0)];
    
    NSMutableArray *arrayA_nodePot = [NSMutableArray array];
    NSMutableArray *arrayA_track = [NSMutableArray array];
    for (int j = 0; j < self.heatMap.vectorX.count; j++) {
        NSMutableArray *arrayB_nodePot = [NSMutableArray array];
        NSMutableArray *arrayB_track = [NSMutableArray array];
        for (int k = 0; k < self.heatMap.vectorY.count; k++) {
            [arrayB_nodePot addObject:@(0)];
            [arrayB_track addObject:[NSMutableArray arrayWithArray:@[@(-1), @(-1), @(-1)]]];
        }
        [arrayA_nodePot addObject:arrayB_nodePot];
        [arrayA_track addObject:arrayB_track];
    }
    [self.nodePot addObject:arrayA_nodePot];
    [self.track addObject:arrayA_track];
}

#pragma mark - Feature function

- (double)computeFeatureMag:(TimeSeries<double, 1>)mu_intensity mu_ver:(TimeSeries<double, 1>)mu_vertical mu_hor:(TimeSeries<double, 1>)mu_horizontal
                    obs_int:(TimeSeries<double, 1>)obs_intensity obs_ver:(TimeSeries<double, 1>)obs_vertical obs_hor:(TimeSeries<double, 1>)obs_horizontal
{
    double intensity = FAST::getWarpDistBetween(mu_intensity, obs_intensity, ManhattanDistance());
    double vertical = FAST::getWarpDistBetween(mu_vertical, obs_vertical, ManhattanDistance());
    double horizontal = FAST::getWarpDistBetween(mu_horizontal, obs_horizontal, ManhattanDistance());
    return exp(-(intensity+vertical+horizontal)/3);
}

- (double)computeFeatureStepLen:(double)obs mu:(double)mu
{
    double ret = pow((obs-mu), 2)/pow(sigma_step_length, 2);
    return exp(-ret/2);
}

- (double)computeFeatureOrient:(NSArray *)obs mu:(NSArray *)mu
{
    double norm1 = sqrt(pow([obs[0] doubleValue], 2)+pow([obs[1] doubleValue], 2));
    double norm2 = sqrt(pow([mu[0] doubleValue], 2)+pow([mu[1] doubleValue], 2));
    double dot = [obs[0] doubleValue] * [mu[0] doubleValue] + [obs[1] doubleValue] * [mu[1] doubleValue];
    dot = dot/norm1/norm2;
    double delta = acos(dot)*180/M_PI;
    
    return 1.0/delta;
}

- (double)computeFeatureAngle:(NSArray *)obs mu:(NSArray *)mu
{
    double norm1 = sqrt(pow([obs[0] doubleValue], 2)+pow([obs[1] doubleValue], 2));
    double norm2 = sqrt(pow([mu[0] doubleValue], 2)+pow([mu[1] doubleValue], 2));
    double dot = [obs[0] doubleValue] * [mu[0] doubleValue] + [obs[1] doubleValue] * [mu[1] doubleValue];
    dot = dot/norm1/norm2;
    double delta = acos(dot)*180/M_PI;

    return delta;
}

- (double)computeFeatureMag:(NSArray *)obs mu:(NSArray *)mu
{
    return exp(-(pow([obs[0] doubleValue]-[mu[0] doubleValue], 2)/5.0+pow([obs[1] doubleValue]-[mu[1] doubleValue], 2)/5.0+pow([obs[2] doubleValue]-[mu[2] doubleValue], 2)/5.0)/2);
}

#pragma mark - Calculation Helper

- (BOOL)validX:(double)x Y:(double)y
{
    return x >= 0 && y >= 0 && x < self.heatMap.vectorX.count && y < self.heatMap.vectorY.count;
}

@end
