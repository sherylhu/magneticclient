//
//  CRFPolygon.h
//  CRF
//
//  Created by Siyan HU on 27/12/2015.
//  Copyright © 2015 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CRFPolygon;

@interface CRFBoundingBox : NSObject

@end

@interface CRFBuilder : NSObject

- (instancetype)addVertex:(CGPoint)point;
- (instancetype)close;
- (CRFPolygon *)build;

@end

@interface CRFPolygon : NSObject

+ (instancetype)polygonWithSides:(NSArray *)sides boundingBox:(CRFBoundingBox *)boundingBox;
+ (CRFBuilder *)builder;

- (BOOL)contains:(CGPoint)point;

@end
