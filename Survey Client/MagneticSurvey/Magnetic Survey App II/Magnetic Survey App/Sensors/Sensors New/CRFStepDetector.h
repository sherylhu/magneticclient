//
//  CRFStepDetector.h
//  Sensor
//
//  Created by Siyan HU on 3/1/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CRFSensorRawData, CRFStepDetector;

@protocol CRFStepDetectorDelegate <NSObject>

@required
- (void)stepDetector:(CRFStepDetector *)stepDetector didDetectStepFrom:(long)fromTS to:(long)toTS stepCount:(int)stepCount frequency:(double)frequency;

@end

@interface CRFStepDetector : NSObject

- (instancetype)initWithSamplePerSecond:(int)samplePerSecond;

- (void)addGyroData:(CRFSensorRawData *)gyroData;
- (void)addAccData:(CRFSensorRawData *)accData;
- (void)start;
- (void)stop;

@property (weak, nonatomic) id<CRFStepDetectorDelegate> delegate;

@end
