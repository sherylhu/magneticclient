//
//  CRFPriorityQueue.h
//  CRF
//
//  Created by Siyan HU on 14/2/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFPriorityQueue : NSObject

/**
 * Can be used to set a custom comparator. Should be used when using a PriorityQueue with custom objects. If this is not set, natural ordering will be used.
 */
@property (nonatomic, assign) NSComparator comparator;

- (id)initWithObjects:(NSSet*)objects;
- (id)initWithCapacity:(int)capacity;

/**
 * Can be used to determine if the queue is empty.
 *
 * @return YES if the queue is empty. NO if the queue is not empty.
 */
- (BOOL)isEmpty;

/**
 * Can be used to determine if an object already exists in the queue.
 *
 * @return YES if the object exists in the queue. NO if the object does not exist in the queue.
 */
- (BOOL)contains:(id<NSObject>)object;

/**
 * Can be used to determine the number of objects in the queue.
 *
 * @return The number of objects in the queue.
 */
- (NSUInteger)size;

/**
 * Removes all the elements of the priority queue.
 */
- (void)clear;

/**
 * Adds the specified object to the priority queue.
 *
 * @param object The object to be added.
 * @return YES if the object was added. NO if it couldn't be added.
 * @throws NSInternalInconsistencyException If the element cannot be compared with the elements in the
 *             priority queue using the ordering of the priority queue.
 * @throws NSInvalidArgumentException If {@code o} is {@code null}.
 */
- (BOOL)add:(id<NSObject>)object;

/**
 * Removes the specified object from the priority queue.
 *
 * @param object The object to be removed.
 * @return YES if the object was in the priority queue, NO if the object
 *         was not in the priority queue.
 */
- (BOOL)remove:(id<NSObject>)object;

/**
 * Gets but does not remove the head of the queue.
 *
 * @return the head of the queue or null if the queue is empty.
 */
- (id<NSObject>)peek;

/**
 * Gets and removes the head of the queue.
 *
 * @return the head of the queue or null if the queue is empty.
 */
- (id<NSObject>)poll;

/**
 * Creates and returns an NSArray from the contents of the queue.
 *
 * @return An array of all elements in the queue.
 */
- (NSArray*)toArray;

@end
