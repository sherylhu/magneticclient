//
//  CRFNode.h
//  CRF
//
//  Created by Siyan HU on 16/4/2016.
//  Copyright © 2016 Chow Ka Ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRFNode : NSObject

@property (assign, nonatomic) int i;
@property (assign, nonatomic) int j;
@property (assign, nonatomic) double potential;

@end
