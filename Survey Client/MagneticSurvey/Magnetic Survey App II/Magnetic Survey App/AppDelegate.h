//
//  AppDelegate.h
//  Magnetic Survey App
//
//  Created by ivy on 7/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

