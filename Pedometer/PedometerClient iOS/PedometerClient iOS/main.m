//
//  main.m
//  PedometerClient iOS
//
//  Created by ivy on 18/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
