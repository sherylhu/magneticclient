//
//  SensorHandler.h
//  PedometerClient iOS
//
//  Created by ivy on 21/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensorHandler : NSObject

@property (nonatomic, strong) NSMutableArray *acceData;
@property (nonatomic, strong) NSMutableArray *mgntData;

@end
