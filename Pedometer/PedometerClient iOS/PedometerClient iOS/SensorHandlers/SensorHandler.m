//
//  SensorHandler.m
//  PedometerClient iOS
//
//  Created by ivy on 21/8/2017.
//  Copyright © 2017 Siyan HU. All rights reserved.
//

#import "SensorHandler.h"

#import <CoreMotion/CoreMotion.h>

@interface SensorHandler () {
    CMMotionManager *cmManager;
    NSOperationQueue *gettingQueue;
}

@end

@implementation SensorHandler

static SensorHandler *_instance = nil;

+ (SensorHandler *)instance {
    if (_instance) {
        return _instance;
    }
    
    @synchronized ([SensorHandler class]) {
        if (!_instance) {
            _instance = [[self alloc]init];
            [_instance initiate];
            BOOL available = [_instance availability];
            if (available) {
                return _instance;
            }
        }
        return _instance;
    }
    
    return nil;
}

- (void)initiate {
    cmManager = [[CMMotionManager alloc]init];
    [cmManager setDeviceMotionUpdateInterval:0.02];
    
    [cmManager setShowsDeviceMovementDisplay:YES];
}

- (BOOL)availability {
    if (cmManager.isDeviceMotionAvailable) {
        NSLog(@"Device Motion Status: %d", cmManager.isDeviceMotionActive);
        return YES;
    }
    return NO;
}

#pragma mark - CMMotionManager
- (void)getDeviceMotion {
    __block CMDeviceMotion *dm = [cmManager deviceMotion];
    gettingQueue = [NSOperationQueue currentQueue];
    [cmManager startDeviceMotionUpdatesToQueue:gettingQueue withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
        
        dm = motion;
        
        NSLog(@"Rotation Rate: [%f, %f, %f];\nGravity Vertex: [%f, %f, %f];\nAttitude(pitch, roll, yaw): [%f, %f, %f]\nAccelerator: [%f, %f, %f]\nMagnetic Data: [%f, %f, %f, %f]",
              dm.rotationRate.x, dm.rotationRate.y, dm.rotationRate.z,
              dm.gravity.x, dm.gravity.y, dm.gravity.z,
              dm.attitude.pitch, dm.attitude.roll, dm.attitude.yaw,
              dm.userAcceleration.x, dm.userAcceleration.y, dm.userAcceleration.z
              );
        if (!self.acceData) {
            self.acceData = [NSMutableArray array];
        }
        @synchronized (self.acceData) {
            
        }
    }];
}

@end
